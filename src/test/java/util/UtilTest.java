/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import util.Util;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 *
 * @author Michel
 */
public class UtilTest {

    static char ds; // Decimal Separator
    static char gs; // Grouping Separator


    @BeforeClass
    public static void getDecimalSeparator() {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();

        ds = dfs.getDecimalSeparator();
        gs = dfs.getGroupingSeparator();
    }


    @Test
    public void testGetDoubleValLocale() throws ParseException {
        double testInput = 10234.456d;
        String testString = Util.getStringValLocale(testInput);
//        String testString = "10.234,456"; // locale: dutch

        double rsltVal = Util.getDoubleValueLocale(testString);
         System.out.println("testVal: " + rsltVal + " testString: " + testString + " locale decimal: " + ds);

         // expect position of decimal at position 5
         assertEquals(5, testString.indexOf(ds));
        assertEquals(10234.456d, rsltVal, 0.001);
    }

    @Test
    public void testGetStringValLocale() {
        Double testVal = 10234.456d;
        String testString = Util.getStringValLocale(testVal);

        System.out.println("testString: " + testString + " locale decimal: " + ds);
        assertEquals("10234" + ds + "456", testString);
    }

    @Test
    public void testGetStringValueWithnoDecimal() {
        String testString = Util.getStringValue(100, "0");
        assertEquals("100", testString);
    }


    @Test
    public void testGetStringValueWithFormatOneDecimal() {
        String testString = Util.getStringValue(100, "0.0");
        assertEquals("100" + ds + "0", testString);
    }



    @Test
    public void testGetStringValueWithFormatTenDecimals() {
        String testString = Util.getStringValue(100, "0.0000000000");
        assertEquals("100" + ds + "0000000000", testString);
    }



    @Test(expected = IllegalArgumentException.class)
    public void testGetStringValueWithInvalidFormat() {
        double testDouble = 11.12D;
        String testString = Util.getStringValue(testDouble, "3,2");
        assertEquals("11,12", testString);
    }


    @Test
    public void testGetStringValueWithFloat() {
        float testFloat = 11F;
        String testString = Util.getStringValue(testFloat, "0");
        assertEquals("11", testString);
    }


    @Test
    public void testGetStringValueWithFloatTwoDecimals() throws ParseException {
        NumberFormat format = NumberFormat.getInstance();

        float testFloat = 11.12F;
        String testString = Util.getStringValue(testFloat, "0.00");
        Number number = format.parse(testString);
        float result = number.floatValue();
        float expected = Float.parseFloat("11.12");
        assertEquals(expected, result, 0.001);
    }


    @Test
    public void testGetStringValueWithFloatTenDecimals() throws ParseException {
        NumberFormat format = NumberFormat.getInstance();

        float testFloat = 11.0123456789F;
        String testString = Util.getStringValue(testFloat, "0.0000000000");
        Number number = format.parse(testString);
        float result = number.floatValue();

        float expected = Float.parseFloat("11.0123456789");
        assertEquals(expected, result, 0.0000000001);
    }


    @Test
    public void testGetStringValueWithFloatElevenDecimals() throws ParseException {
        NumberFormat format = NumberFormat.getInstance();

        float testFloat = 11.01234567891F;
        String testString = Util.getStringValue(testFloat, "0.0000000000");
        Number number = format.parse(testString);
        float result = number.floatValue();
        float expected = Float.parseFloat("11.0123456789");
        assertEquals(expected, result, 0.0000000001);
    }


    @Test
    public void testGetStringValueWithFloatElevenDecimalsRounded() throws ParseException {
        NumberFormat format = NumberFormat.getInstance();

        float testFloat = 11.01234567895F;
        String testString = Util.getStringValue(testFloat, "0.0000000000");
        Number number = format.parse(testString);
        float result = number.floatValue();
        float expected = Float.parseFloat("11.012345679");
        assertEquals(expected, result, 0.0000000001);
    }


    @Test
    public void testGetStringValueWithDouble() {
        double testDouble = 11.0D;
        String testString = Util.getStringValue(testDouble, "0");
        assertEquals("11", testString);
    }


    @Test
    public void testGetStringValueWithDoubleTenDecimals() throws ParseException {
        double testDouble = 11.0123456789D;
        String testString = Util.getStringValue(testDouble, "0.0000000000");

        assertEquals("11" + ds + "0123456789", testString);
    }



    @Test
    public void testGetStringValueWithDoubleWithElevenDecimals() {
        double testDouble = 11.01234567891D;
        String testString = Util.getStringValue(testDouble, "0.00000000000");
        assertEquals("11" + ds + "01234567891", testString);
    }



    @Test
    public void testGetStringValueWithDoubleWithElevenDecimalsRounding() {
        double testDouble = 11.01234567895D;
        String testString = Util.getStringValue(testDouble, "0.0000000000");
        assertEquals("11" + ds + "0123456790", testString);
    }



    @SuppressWarnings("unused")
    @Test(expected = NumberFormatException.class)
    public void testGetStringValueWithNullValue() {
        String testString = Util.getStringValue(null, "0");
    }


    @SuppressWarnings("unused")
    @Test(expected = NumberFormatException.class)
    public void testGetStringValueWithStringValue() {
        String testString = Util.getStringValue("15" + ds + "3", "0");
    }



    @Test
    public void testConvertToDoubleWithFloat() {
        Float testFloat = 11F;
        double actual = Util.convertToDouble(testFloat);
        double result = 11.0D;
        assertEquals(result, actual, 0);
    }


    @Test
    public void testConvertToDoubleWithFloatZero() {
        Float testFloat = 0.0F;
        double actual = Util.convertToDouble(testFloat);
        double result = 0.0D;
        assertEquals(result, actual, 0);
    }


    @Test
    public void testConvertToDoubleWithFloatFiveDecimals() {
        Float testFloat = 11.0123456789F;
        double actual = Util.convertToDouble(testFloat);
        double result = 11.0123456789D;
        assertEquals(result, actual, 0.00001);
    }


    @Test
    public void testConvertToDoubleWithInt() {
        int testInt = 2;
        double actual = Util.convertToDouble(testInt);
        double result = 2.0D;
        assertEquals(result, actual, 0);
    }


    @Test
    public void testConvertToDoubleWithIntZero() {
        int testInt = 0;
        double actual = Util.convertToDouble(testInt);
        double result = 0.0D;
        assertEquals(result, actual, 0);
    }


    @SuppressWarnings("unused")
    @Test(expected = NumberFormatException.class)
    public void testConvertToDoubleWithString() {
        String testString = "11" + ds + "2";
        double actual = Util.convertToDouble(testString);
    }


    @SuppressWarnings("unused")
    @Test(expected = NumberFormatException.class)
    public void testConvertToDoubleWithNull() {
        double actual = Util.convertToDouble(null);
    }


    @Test
    public void testResolvePaths() {
        List<String> testList = new ArrayList<String>();

        testList.add("Spatie%20x_Olimexinoasym.a2l");
        testList.add("d/Spatie%20x_Olimexinoasym.a2l");
        testList.add("../test.csv");

        // No conversion: URISyntaxException: Illegal character in path at index 18
        testList.add("absPath/abs33/test 33.csv");

        testList.add("absPath/abs44/test%2044");
        testList.add("../../../path2/test%2055.csv");
        testList.add("../../../absPath/pathspace%20xx/tes%20%20t66.csv");
        testList.add("../../../../../path2/test%2077.csv");

        testList.add("C:/a/b/c/test%2088.csv"); // Not URI
        testList.add("/C:/a/b/c/test%2099.csv");
        testList.add("file:/C:/a/b/c/test%201010.csv");

        // a Filename MUST be present in full path
        File testBase = new File("d:\\a\\b\\c\\file.tst");
        // File testBase = new File("d:\\a\\b\\c\\");
        // BEWARE: URI.resolve() function behaves differently when directory 'd:\\a\\b\\c\\' doesn't
        // exist!!!

        // test
        Util.resolvePaths(testBase, testList);

        // list results
        for (String elem : testList) {
            System.out.println("Abs path default FileSystem: " + elem);
        }

        // check results
        assertEquals("d:\\a\\b\\c\\Spatie x_Olimexinoasym.a2l", testList.get(0));
        assertEquals("d:\\a\\b\\c\\d\\Spatie x_Olimexinoasym.a2l", testList.get(1));
        assertEquals("d:\\a\\b\\test.csv", testList.get(2));

        // should give: URISyntaxException: Illegal character in path at index 18
        assertEquals("absPath/abs33/test 33.csv", testList.get(3));// not converted:

        assertEquals("d:\\a\\b\\c\\absPath\\abs44\\test 44", testList.get(4));
        assertEquals("d:\\path2\\test 55.csv", testList.get(5));
        assertEquals("d:\\absPath\\pathspace xx\\tes  t66.csv", testList.get(6));
        assertEquals("\\..\\path2\\test 77.csv", testList.get(7));

        assertEquals("\\a\\b\\c\\test 88.csv", testList.get(8)); // Is not what we want, but is accepted
        // for the time being. 'C:\\a\\b\\c\\test 88.csv' would be more desirable

        assertEquals("C:\\a\\b\\c\\test 99.csv", testList.get(9));
        assertEquals("C:\\a\\b\\c\\test 1010.csv", testList.get(10));
    }


    @Test
    public void testRelativizePaths() {
        List<String> testList = new ArrayList<String>();

        // create test input
        testList.add("D:/a/b/c/tstasap");
        testList.add("D:/a/b\\c/tst asap11");
        testList.add("D:\\a\\b\\c\\d\\test asap22");
        testList.add("D:/a/b/test asap33.a2l");
        testList.add("D:\\test asap44.a2l");

        testList.add("e:\\a\\b\\c\\absPath\\abs55\\test 55");
        testList.add("noroot66");
        testList.add("absPath\\abs  77\\test 77.csv");
        testList.add("C:/a/b  b/c\\test88.csv");

        File testProject = new File("D://a//b//c//testProject.hml");

        Util.relativizePaths(testProject, testList);

        // list results
        for (String elem : testList) {
            System.out.println("Path in URI (relative if possible): " + elem);
        }

        // check results
        assertEquals("tstasap", testList.get(0));
        assertEquals("tst%20asap11", testList.get(1));
        assertEquals("d/test%20asap22", testList.get(2));
        assertEquals("../test%20asap33.a2l", testList.get(3));
        assertEquals("../../../test%20asap44.a2l", testList.get(4));
        assertEquals("file:/e:/a/b/c/absPath/abs55/test%2055", testList.get(5));
        assertEquals("noroot66", testList.get(6));
        assertEquals("absPath/abs%20%2077/test%2077.csv", testList.get(7));
        assertEquals("file:/C:/a/b%20%20b/c/test88.csv", testList.get(8));
    }


    @Test
    public void testSwap() {
        byte[] arr;

        // 2 byte groups
        arr = Util.swapEndianness(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }, 2);
        assertArrayEquals(new byte[] { 2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11 }, arr);

        // 4 byte groups
        arr = Util.swapEndianness(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }, 4);
        assertArrayEquals(new byte[] { 4, 3, 2, 1, 8, 7, 6, 5, 12, 11, 10, 9 }, arr);

        // 8 byte groups
        arr = Util.swapEndianness(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 }, 8);
        assertArrayEquals(new byte[] { 8, 7, 6, 5, 4, 3, 2, 1, 16, 15, 14, 13, 12, 11, 10, 9 }, arr);

        // nothing swapped if other than 2,4 or 8 bytes itemsize
        arr = Util.swapEndianness(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 }, 99);
        assertArrayEquals(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 }, arr);

        // nothing swapped for remainder of group (array is not a multiple of group size)
        arr = Util.swapEndianness(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, 8);
        assertArrayEquals(new byte[] { 8, 7, 6, 5, 4, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0 }, arr);
    }
}
