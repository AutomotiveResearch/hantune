/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package can;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Set;

import can.CanMessage;
import can.CanSignal;
import can.DbcManager;
import can.DbcObject;
import can.DbcParser;
import org.junit.Test;

/**
 *
 * @author Roel van den Boom
 */
public class CanSignalTest {

    @Test
    public void getPhysicalValueTest() {
        CanMessage msg = new CanMessage(1, "testMsg", 8);
        // Arguments of constructor: name, multiplexer, startbit, length, format, type, factor, offset, minimum, maximum, unit
       CanSignal canSignal = new CanSignal(msg, "signalName1", null, 0, 16, 1, "+", 1, 0, 0, 100, "%");
       // Test positive unsigned integers
       assertEquals(100, canSignal.getPhysicalValue(100), 1e-12);
       canSignal = new CanSignal(msg, "signalName2", null, 0, 16, 1, "-", 1, 0, 0, 100, "%");
       // Test positive and negative signed integers
       assertEquals(100, canSignal.getPhysicalValue(100), 1e-12);
       assertEquals(-100, canSignal.getPhysicalValue(-100), 1e-12);
       canSignal = new CanSignal(msg, "signalName3", null, 0, 32, 1, "-", 2.384185791e-07, 0, 0, 100, "%");
       // Test positive an negative signed fixed point numbers
       assertEquals(0.000023841858, canSignal.getPhysicalValue(100), 1e-12);
       assertEquals(-0.000023841858, canSignal.getPhysicalValue(-100), 1e-12);
    }


    @Test
    public void checkUniqueAdditionalDbcElements() {

        File dbcFile = new File("../src/test/resources/can/ExistingDbc.dbc");
        DbcObject existingDbc = DbcParser.parseDBC(dbcFile);
        DbcManager.addDbc(existingDbc);

        // Check for ALL duplicates
        DbcObject additionalDbc = DbcParser.parseDBC(new File("../src/test/resources/can/AdditionalDbcAllDuplicates.dbc"));
        Set<String> duplicates = DbcManager.findExistingDuplicates(additionalDbc);
        // duplicates.forEach(System.out::println);
        assertFalse(duplicates.isEmpty());
        assertEquals(602, duplicates.size());

        // Check for NONE duplicates
        additionalDbc = DbcParser.parseDBC(new File("../src/test/resources/can/AdditionalDbcNoneDuplicates.dbc"));
        duplicates = DbcManager.findExistingDuplicates(additionalDbc);
        // duplicates.forEach(System.out::println);
        assertTrue(duplicates.isEmpty());

        // Check for exactly 6 duplicates
        additionalDbc = DbcParser.parseDBC(new File("../src/test/resources/can/AdditionalDbc6Duplicates.dbc"));
        duplicates = DbcManager.findExistingDuplicates(additionalDbc);
        // duplicates.forEach(System.out::println);
        assertFalse(duplicates.isEmpty());
        assertEquals(6, duplicates.size());
    }

}
