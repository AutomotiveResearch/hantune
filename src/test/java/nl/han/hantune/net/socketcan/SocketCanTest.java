package nl.han.hantune.net.socketcan;

import ErrorLogger.AppendToLogfile;
import org.junit.Ignore;
import org.junit.Test;
import tel.schich.javacan.CanChannels;
import tel.schich.javacan.CanFrame;
import tel.schich.javacan.NetworkDevice;
import tel.schich.javacan.RawCanChannel;
import tel.schich.javacan.platform.linux.UnixFileDescriptor;
import tel.schich.javacan.platform.linux.epoll.EPollSelector;
import tel.schich.javacan.select.IOEvent;
import tel.schich.javacan.select.SelectorRegistration;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.EnumSet;
import java.util.List;

import static javatests.TestSupport.assertEquals;
import static tel.schich.javacan.CanFrame.FD_NO_FLAGS;
import static tel.schich.javacan.CanSocketOptions.RECV_OWN_MSGS;


public class SocketCanTest {
    // Simple test to check for first message on Olimexino. Not event driven
    //        ms       ID  len  data
    //  send  9271.4  0665  2   FF 00
    //  rcv   9272.8  0666  8   FF 04 80 08 08 00 01 01
    @Ignore("Test only succeeds when Olimexino with PCAN-USB is connected to SocketCAN (linux only)")
    @Test
    public void testCanSocketMessage() throws Exception {

        System.setProperty("javacan.native.javacan-core.path",
                System.getProperty("user.dir") + "/lib/native/libjavacan-core.so");
        try (final RawCanChannel socket = CanChannels.newRawChannel()) {
            socket.bind(lookupCan0Dev());

            socket.configureBlocking(false);
            socket.setOption(RECV_OWN_MSGS, false);

            final CanFrame input = CanFrame.create(0x665, FD_NO_FLAGS, new byte[]{(byte) 0xFF, 0x00});
            socket.write(input);
            Thread.sleep(5);
            final CanFrame readFrame = socket.read();
            System.out.printf("error: %04X\n", readFrame.getError()); // error equal to CAN ID??
            ByteBuffer data = ByteBuffer.allocate(10);
            readFrame.getData(data);
            System.out.printf("readFrame: CanId: %03X len %d  Data:  %02X %02X %02X %02X %02X %02X %02X %02X",
                    readFrame.getId(), readFrame.getDataLength(),
                    data.get(0), data.get(1), data.get(2), data.get(3), data.get(4), data.get(5), data.get(6),
                    data.get(7));

            assertEquals(readFrame.getDataLength(), 8, " should be equal");
            assertEquals(data.get(0), (byte)0xFF, "first data byte should be 0xFF");
            assertEquals(data.get(1), (byte)0x04, "second data byte should be 0x04");
        }
    }

    private static boolean keepRunning = true;
    private boolean dataReceived = false;

    // More elaborate test using polling event
    // Expect message like (PCAN-USB on Olimexino, first 8 messages):
    //        ms       ID  len  data
    //  1)    9271.4  0665  2   FF 00
    //  2)    9272.8  0666  8   FF 04 80 08 08 00 01 01
    //  3)    9275.3  0665  1   FD
    //  4)    9276.2  0666  6   FF 20 00 08 00 00
    //  5)    9277.3  0665  1   FB
    //  6)    9278.3  0666  8   FF 20 00 08 00 00 00 14
    //  7)    9279.3  0665  2   FA 01
    //  8)    9280.3  0666  8   FF 00 00 08 2E 00 00 00
    //  665: send, 666 receive:
    @Ignore("Test only succeeds when Olimexino with PCAN-USB is connected to SocketCAN (linux only)")
    @Test
    public void testCanSocketMessageReceiveEvent() throws Exception {

        System.setProperty("javacan.native.javacan-core.path",
                System.getProperty("user.dir") + "/lib/native/libjavacan-core.so");
        System.setProperty("javacan.native.javacan-epoll.path",
                System.getProperty("user.dir") + "/lib/native/libjavacan-epoll.so");
        try (final RawCanChannel socket = CanChannels.newRawChannel()) {
            try (EPollSelector selector = EPollSelector.open()) {
                socket.bind(lookupCan0Dev());
                socket.configureBlocking(false);
                socket.setOption(RECV_OWN_MSGS, false);

                selector.register(socket, EnumSet.of(SelectorRegistration.Operation.READ));

                TestRunnable testRunnable = new TestRunnable(socket, selector);
                Thread testThread = new Thread(testRunnable);
                testThread.start();

                dataReceived = true;
                long sendTime = System.currentTimeMillis();

                waitForReceivedThenSend(socket, new byte[]{(byte) 0xFF, 0x00});
                System.out.println("send 1: " + (System.currentTimeMillis() - sendTime) + " ms");
                sendTime = System.currentTimeMillis();
                waitForReceivedThenSend(socket, new byte[]{(byte) 0xFD});
                System.out.println("send 2: " + (System.currentTimeMillis() - sendTime) + " ms");
                sendTime = System.currentTimeMillis();
                waitForReceivedThenSend(socket, new byte[]{(byte) 0xFB});
                System.out.println("send 3: " + (System.currentTimeMillis() - sendTime) + " ms");
                sendTime = System.currentTimeMillis();
                waitForReceivedThenSend(socket, new byte[]{(byte) 0xFA, 0x01});
                System.out.println("send 4: " + (System.currentTimeMillis() - sendTime) + " ms");

                Thread.sleep(1000);
                testRunnable.doStop();

                keepRunning = false;
                // no assert here, all received message should appear as specified above
            }
        }
    }


    // This test revealed that a delay of (at least) 1 ms was necessary to make selector stay open
    // See line: Thread.sleep(1);
    @Ignore
    @Test
    public void testConnectDelayForSelectorToStayOpen() throws Exception {
        // TODO this is a quick and dirty solution to load *.so files when needed. Should be improved
        System.setProperty("javacan.native.javacan-core.path",
                System.getProperty("user.dir") + "/lib/native/libjavacan-core.so");
        System.setProperty("javacan.native.javacan-epoll.path",
                System.getProperty("user.dir") + "/lib/native/libjavacan-epoll.so");

        try (final RawCanChannel socket = CanChannels.newRawChannel()) {
            try (EPollSelector selector = EPollSelector.open()) {
                socket.bind(lookupCan0Dev());
                socket.configureBlocking(false);
                socket.setOption(RECV_OWN_MSGS, false);

                selector.register(socket, EnumSet.of(SelectorRegistration.Operation.READ));

                TestRunnable testRunnable = new TestRunnable(socket, selector);
                Thread testThread = new Thread(testRunnable);
                testThread.start();

                Thread.sleep(1); // This sleep of (at least) 1 ms seems necessary, otherwise:
                // Exception in thread "Thread-1" java.nio.channels.ClosedSelectorException in
                // receiveDataTest() line:
                // List<IOEvent<UnixFileDescriptor>> events = selector.select(Duration.ofMillis(3000));
                // socket.write(CanFrame.create(0x665, FD_NO_FLAGS, new byte[]{(byte) 0xFF, 0x00}));
            }
        } catch (IOException e) {
            AppendToLogfile.appendMessage("Could not initialize SocketCAN");
            AppendToLogfile.appendError(Thread.currentThread(), e);
        }
    }



    public class TestRunnable implements Runnable {
        private boolean doStop = false;
        final RawCanChannel socket;
        final EPollSelector selector;

        TestRunnable(final RawCanChannel socket, final EPollSelector selector) {
            this.socket = socket;
            this.selector = selector;
//            this.se
        }

        public synchronized void doStop() {
            this.doStop = true;
        }

        private synchronized boolean keepRunning() {
            return !this.doStop;
        }


        @Override
        public void run() {
            while (keepRunning()) {
                System.out.println("run selector: " + selector.isOpen());
                if (!receiveDataTest(socket, selector)) {
                    doStop();
                    keepRunning = false;
                }
            }
        }
    }


    private void waitForReceivedThenSend(RawCanChannel socket,
                                         byte[] dataBytes) throws IOException, InterruptedException {
        while (keepRunning) {
            if (dataReceived) {
                System.out.print("Sending data: ");
                for (byte dataByte : dataBytes) {
                    System.out.printf(" %02X", dataByte);
                }
                System.out.println();

                socket.write(CanFrame.create(0x665, FD_NO_FLAGS, dataBytes));
                dataReceived = false;
                return;
            } else {
                System.out.println("Waiting for sending...");
                Thread.sleep(2);
            }
        }
    }

    private boolean receiveDataTest(RawCanChannel socket, EPollSelector selector) {
        CanFrame readFrame;
        try {
            long timeRead = System.currentTimeMillis();
            List<IOEvent<UnixFileDescriptor>> events = selector.select(Duration.ofMillis(3000));

            System.out.println(
                    "waiting for receive, after select. Millis: " + (System.currentTimeMillis() - timeRead));
            if (events.isEmpty()) {
                System.out.println("events is empty");
                throw new IOException("IOException receive timeout..");
            } else {
                System.out.println("events filled:" + events);
                readFrame = socket.read();
                System.out.printf("error: %04X\n", readFrame.getError());

                ByteBuffer data = ByteBuffer.allocate(10);
                readFrame.getData(data);
                System.out.printf("readFrame: CanId: %03X len %d  Data:  %02X %02X %02X %02X %02X %02X %02X %02X\n",
                        readFrame.getId(), readFrame.getDataLength(),
                        data.get(0), data.get(1), data.get(2), data.get(3), data.get(4), data.get(5), data.get(6),
                        data.get(7));
                dataReceived = true;
            }
            return true;
        } catch (IOException e) {
            System.out.println(" receiveDataTest IOException:  " + e.getMessage());
        }
        return false;
    }


    private static NetworkDevice lookupCan0Dev() {
        try {
            return NetworkDevice.lookup("can0");
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
        return null;
    }

}
