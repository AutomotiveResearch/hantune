/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune;

import HANtune.HANtune;
import javax.swing.SwingUtilities;
import nl.han.hantune.scripting.jython.JythonScript;
import nl.han.hantune.scripting.Script;
import nl.han.hantune.scripting.ScriptingManager;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Michiel Klifman
 */
public class HANtuneTest {
    @BeforeClass
    public static void setUpAll() {
        ScriptingManager instance = ScriptingManager.getInstance();
        instance.removeAllScripts();
    }

    @Test
    public void testSetProjectStartupScripts() {
        Script script1 = new JythonScript("script1", "");
        Script script2 = new JythonScript("script2", "");
        Script script3 = new JythonScript("script3", "");
        ScriptingManager manager = ScriptingManager.getInstance();
        manager.addScript(script1);
        manager.addScript(script2);
        manager.addScript(script3);

        SwingUtilities.invokeLater(() -> {
            HANtune.getInstance().initHANtune();
            HANtune.getInstance().setProjectStartupScript(script3);

            int expectedIndex = 0;
            int index = manager.getScripts().indexOf(script3);
            assertThat(index, is(expectedIndex));
            assertThat(script3.isRunOnStartup(), is(true));
        });
    }
}
