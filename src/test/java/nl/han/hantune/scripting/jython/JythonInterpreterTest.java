/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting.jython;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.SwingUtilities;

import nl.han.hantune.scripting.jython.JythonInterpreter;
import nl.han.hantune.scripting.jython.JythonScript;
import org.junit.Ignore;
import org.junit.Test;

import ASAP2.ASAP2Data;
import ASAP2.ASAP2Import;
import ASAP2.ASAP2Measurement;
import ASAP2.Asap2Parameter;
import HANtune.HANtune;
import HANtune.HANtuneManager;
import can.CanMessage;
import can.CanSignal;
import can.DbcObject;
import datahandling.CurrentConfig;
import datahandling.Signal;

/**
 *
 * @author Michiel Klifman
 */
public class JythonInterpreterTest {

    private static final String PYTHON_SCRIPT_STARTED = "Python script started\r\n";
    private static final String PYTHON_SCRIPT_FINISHED = "Python script finished\r\n";
    private static final String NEW_LINE = "\r\n";

    /**
     * Calls a script that will try to: print PYTHON_SCRIPT_STARTED, sleep
     * a little and then print PYTHON_SCRIPT_ENDED. If the script is executed
     * successfully it will print both PYTHON_SCRIPT_STARTED and
     * PYTHON_SCRIPT_ENDED.
     */
    @Test
    public void testInterpret() {
        OutputStream outputStream = new ByteArrayOutputStream();
        JythonInterpreter interpreter = new JythonInterpreter(outputStream);
        JythonScript pythonScript = new JythonScript("Test.py", "../src/test/java/nl/han/hantune/scripting/jython/Test.py");
        interpreter.interpret(pythonScript);
        String output = "";
        while (pythonScript.isRunning()) {
            output = outputStream.toString();
        }
        assertThat(output, is(PYTHON_SCRIPT_STARTED + PYTHON_SCRIPT_FINISHED));
    }

    /**
     * Calls a script that will try to: print PYTHON_SCRIPT_STARTED, sleep
     * a little and then print PYTHON_SCRIPT_ENDED. If the script is
     * successfully interrupted only PYTHON_SCRIPT_STARTED is printed.
     */
    @Test
    public void testInterrupt() {
        OutputStream outputStream = new ByteArrayOutputStream();
        JythonInterpreter interpreter = new JythonInterpreter(outputStream);
        JythonScript pythonScript = new JythonScript("Test.py", "../src/test/java/nl/han/hantune/scripting/jython/Test.py");
        pythonScript.setRunning(true);
        SwingUtilities.invokeLater(() -> {
            interpreter.interpret(pythonScript);
        });
        String output = "";
        while (pythonScript.isRunning()) {
            output = outputStream.toString();
            if (output.equals(PYTHON_SCRIPT_STARTED)) {
                interpreter.interrupt(pythonScript);
            }
        }
        assertThat(output, is(PYTHON_SCRIPT_STARTED));
        assertThat(output, not(PYTHON_SCRIPT_STARTED + PYTHON_SCRIPT_FINISHED));
    }

    /**
     * Calls a script that will try to find the signals defined in this method,
     * get their values, add them up and print the result. When the script is
     * done running the result is compared to the expected result set in this
     * method.
     */
    //Todo: test fails, should be improved
    @Test
    @Ignore
    public void testAddUpSignals() {
        double canValue = 10;
        double asap2Value = 20;
        double expectedResult = canValue + asap2Value;

        //Mock a CAN signal and its file
        CanMessage canMsg = new CanMessage(123, "msgName", 8);
        CanSignal canSignal = new CanSignal(canMsg, "CanSignal", "", 0, 0, 0, "", 0, 0, 0, 0, "");
        canSignal.setValueAndTime(canValue, 0);
        CanMessage canMessage = new CanMessage(0, "", 0);
        canMessage.addSignal(canSignal);
        DbcObject dbc = new DbcObject("", "");
        dbc.addMessage(canMessage);
        CurrentConfig.getInstance().addDescriptionFile(dbc);

        //Mock an ASAP2 signal and its file
        ASAP2Measurement asap2Signal = new ASAP2Measurement();
        asap2Signal.setName("Asap2Signal");
        asap2Signal.setValueAndTime(asap2Value, 0);
        ASAP2Data asap2 = new ASAP2Data();
        asap2.setMeasurements(new ArrayList<>(Arrays.asList(asap2Signal)));
        CurrentConfig.getInstance().addDescriptionFile(asap2);

        OutputStream outputStream = new ByteArrayOutputStream();
        JythonInterpreter interpreter = new JythonInterpreter(outputStream);
        JythonScript pythonScript = new JythonScript("AddUpSignalsTest.py", "test/java/nl/han/hantune/scripting/jython/AddUpSignalsTest.py");
        interpreter.interpret(pythonScript);
        String output = "";
        while (pythonScript.isRunning()) {
            output = outputStream.toString();
        }
        double result = Double.parseDouble(output.replace(NEW_LINE, ""));
        assertThat(result, is(expectedResult));
    }

    /**
     * Note: This method does NOT test if HANtune can send parameters. It only
     * tests if a script can instruct HANtune to send a parameter. It does this
     * by first setting the parameter 'in sync with the target' and calling a script
     * that will attempt to send the parameter. Since there is no active connection
     * HANtune cannot send the parameter and should set the parameter to 'not in
     * sync with the target'. If the script cannot find the parameter or is not
     * able to call its send method, inSync will remain true.
     *
     * @throws InterruptedException
     */
    @Test
    @SuppressWarnings("CallToThreadYield")
    public void testSendParamters() throws InterruptedException {
        HANtuneManager manager = new HANtuneManager(HANtune.getInstance());
        CurrentConfig.getInstance().setHANtuneManager(manager);
        File asap2File = new File("../src/test/java/nl/han/hantune/scripting/jython/script_testing.a2l");
        ASAP2Import asap2Importer = new ASAP2Import();
        ASAP2Data asap2 = asap2Importer.importASAP2(asap2File);
        CurrentConfig.getInstance().addDescriptionFile(asap2);
        CurrentConfig.getInstance().setASAP2Data(asap2);

        Asap2Parameter parameter = CurrentConfig.getInstance().getReferenceByName("DemoParameter", Asap2Parameter.class);
        parameter.setInSync(true);

        JythonInterpreter interpreter = new JythonInterpreter();
        JythonScript pythonScript = new JythonScript("SendParameter.py", "../src/test/java/nl/han/hantune/scripting/jython/SendParameter.py");
        interpreter.interpret(pythonScript);

        while (parameter.isInSync()) {
            Thread.yield();
        }
        assertThat(parameter.isInSync(), is(false));
    }

    /**
     * Calls a script that will create a new ScriptedSignal. When the script is
     * done running, it tries to find the new signal in HANtune. If the signal
     * is found the test will succeed. If the signal is null the test will fail.
     */
    @Test
    @SuppressWarnings("CallToThreadYield")
    public void testCreateSignal() {

        JythonInterpreter interpreter = new JythonInterpreter();
        JythonScript pythonScript = new JythonScript("Test.py", "../src/test/java/nl/han/hantune/scripting/jython/CreateSignalTest.py");
        interpreter.interpret(pythonScript);

        while (pythonScript.isRunning()) {
            Thread.yield();
        }
        Signal result = CurrentConfig.getInstance().getReferenceByName("testSignal", Signal.class);
        assertNotNull(result);
    }
}
