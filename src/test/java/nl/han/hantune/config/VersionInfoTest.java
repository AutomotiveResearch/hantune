/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.config;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VersionInfoTest {

    @Test
    public void testVersionFileOk() {
        VersionInfo testSubject =
            VersionInfo.getInstanceTest("../src/test/resources/nl/han/hantune/config/TestVersionData.properties");

        assertEquals("HANtune 1.23.567", testSubject.getHANtuneXYVersionCommitCount());
        assertEquals("d3f449cd - TestBranchName", testSubject.getCommitIDBranchName());
        assertEquals("HANtune 1.23", testSubject.getHANtuneXYVersion());
        assertEquals("HANtune 1.23.567 commit: d3f449cd", testSubject.getHANtuneXYVersionCommitCountCommitID());
        assertEquals("1.23.567 - HANtune version text", testSubject.getXYVersionCommitCountVersionName());
        assertEquals("build 567", testSubject.getBuildCommitCount());
        assertEquals("Branch: TestBranchName", testSubject.getBranchBranchName());
        assertEquals(1.23, testSubject.getVersionFloat(), 0.00001);
        assertEquals("d3f449cd", testSubject.getCommitId());
    }


    @Test
    public void testVersionFileOk2() {
        VersionInfo testSubject =
            VersionInfo.getInstanceTest("../src/test/resources/nl/han/hantune/config/TestVersionData2.properties");

        assertEquals("HANtune 2.2.1232", testSubject.getHANtuneXYVersionCommitCount());
        assertEquals("2.2.1232 - \"HANtune full version quoted\"", testSubject.getXYVersionCommitCountVersionName());
        assertEquals("HANtune 33_branch_text - build 1232", testSubject.getHANtuneConditionalBranchNameBuildCommitCount());
        assertEquals(2.2, testSubject.getVersionFloat(), 0.00001);
    }



    @Test
    public void shouldNotYieldBranchNameIfMaster() {
        VersionInfo testSubject =
            VersionInfo.getInstanceTest("../src/test/resources/nl/han/hantune/config/TestVersionData4.properties");

        // when branch name equals 'master', then branch property should contain empty string
        assertEquals("HANtune - build 345", testSubject.getHANtuneConditionalBranchNameBuildCommitCount());
    }


    @Test
    public void shouldHandleInvalidNumberFormat() {
        VersionInfo testSubject =
            VersionInfo.getInstanceTest("../src/test/resources/nl/han/hantune/config/TestVersionData4.properties");

        assertEquals(0.0, testSubject.getVersionFloat(), 0.00001);
    }


    @Test
    public void testVersionFileSkipTypo() {
        VersionInfo testSubject =
            VersionInfo.getInstanceTest("../src/test/resources/nl/han/hantune/config/TestVersionData3.properties");

        // typo in VersionNumberr, therefore default 0.0 version
        assertEquals("HANtune 0.0.1232", testSubject.getHANtuneXYVersionCommitCount());
        assertEquals(0.0, testSubject.getVersionFloat(), 0.00001);
    }


    @Test
    public void testVersionFileNotFound() {
        // try to open nonexistent file
        VersionInfo testSubject = VersionInfo.getInstanceTest("../src/test/java/HANtune/resources/NotFound.properties");

        // should yield all default version numbers/values
        assertEquals("0.0.0 - Error 'config/version.properties'", testSubject.getXYVersionCommitCountVersionName());
        assertEquals("00000000 - --", testSubject.getCommitIDBranchName());
    }

}
