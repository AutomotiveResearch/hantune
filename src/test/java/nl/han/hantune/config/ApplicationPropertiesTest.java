/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.config;

import org.junit.Ignore;
import org.junit.Test;

import java.awt.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import static nl.han.hantune.config.ApplicationProperties.APPLIC_PROPERTIES_NAME;
import static nl.han.hantune.config.ConfigProperties.BARVIEWER_LABEL_POSITION;
import static nl.han.hantune.config.ConfigProperties.BOOLEANVIEWER_LABEL_POSITION;
import static nl.han.hantune.config.ConfigProperties.DIGITALVIEWER_LABEL_POSITION;
import static nl.han.hantune.config.ConfigProperties.GAUGEVIEWER_LABEL_POSITION;
import static nl.han.hantune.config.ConfigProperties.LEDVIEWER_LABEL_POSITION;
import static nl.han.hantune.config.ConfigProperties.PARAM_OUTOFSYNC_COLOR;
import static nl.han.hantune.config.ConfigProperties.TABLEEDITOR_LABEL_POSITION;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_BOOLEAN_USE_MID_VALUE;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MAX;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MID;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MIN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ApplicationPropertiesTest {
    static final String LF = System.lineSeparator(); // use general linefeed

    @Before
    public void setup() {
        String propFile = "../src/test/resources/nl/han/hantune/config/TestApplic.properties";

        // invoke private methods for UnitTesting using reflection
        try {
            Method applPropInit = ApplicationProperties.class.getDeclaredMethod("initApplicationProperties", String.class);
            applPropInit.setAccessible(true);
            applPropInit.invoke(null, propFile);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testApplicationProperties() {

        assertEquals(ConfigProperties.TopBottomLabelPos.TOP, ApplicationProperties.getTopBottomLabelPos(BARVIEWER_LABEL_POSITION));
        assertEquals(ConfigProperties.TopBottomLabelPos.BOTTOM, ApplicationProperties.getTopBottomLabelPos(BOOLEANVIEWER_LABEL_POSITION));
        assertEquals(ConfigProperties.TopBottomLabelPos.TOP, ApplicationProperties.getTopBottomLabelPos(DIGITALVIEWER_LABEL_POSITION));
        assertEquals(ConfigProperties.TopBottomLabelPos.BOTTOM, ApplicationProperties.getTopBottomLabelPos(GAUGEVIEWER_LABEL_POSITION));
        assertEquals(ConfigProperties.TopBottomLabelPos.TOP, ApplicationProperties.getTopBottomLabelPos(LEDVIEWER_LABEL_POSITION));
        assertEquals(ConfigProperties.TopBottomLabelPos.BOTTOM, ApplicationProperties.getTopBottomLabelPos(TABLEEDITOR_LABEL_POSITION));
        assertEquals(new Color(123, 234, 125), ApplicationProperties.getColor(PARAM_OUTOFSYNC_COLOR));

        // not found in TestApplic file. Therefore default version should be used
        assertEquals(new Color(50, 50, 255), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MIN));
        assertEquals(new Color(50, 255, 50), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MID));
        assertEquals(new Color(255, 50, 50), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MAX));
        assertFalse(ApplicationProperties.getBoolean(TABLE_FIELD_BOOLEAN_USE_MID_VALUE));
    }


    // This 'test' automatically generates an application.tmpl file in the src directory
    // It generates a commented version of all default properties found in ConfigProperties
    @Test
    public void createApplicationPropertiesTemplateFile() throws IOException {
        Properties props = new Properties();
        props.putAll(ConfigProperties.getAllProperties()); // ensure default properties are written

        BufferedWriter bw = new BufferedWriter(new FileWriter("../src/application.tmpl"));

        String comment = "# Property file to use with HANtune." + LF
                + "# File contains all properties and their default values for HANtune." + LF
                + "# To actively use altered property values: " + LF
                + "#  - Copy/rename this file to '" + APPLIC_PROPERTIES_NAME + "' and open it in an editor" + LF
                + "#  - Uncomment a property and alter it to your desired value" + LF
                + "#  - Restart HANtune" + LF + LF + LF
                + "# color properties: use 0-255 for <Red,Green,Blue>. Example: 255,255,255 -> White " + LF
                + "# boolean properties: use only true/false" + LF
                + "# topBottom properties: use only TOP or BOTTOM" + LF + LF;
        bw.write(comment);
        Properties expectedProps = ConfigProperties.getAllProperties();
        expectedProps.forEach((key, val) -> {
            try {
                bw.write("#" + key.toString() + "=" + val.toString() + LF);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        bw.close();

        assertEquals(props, expectedProps);
        System.out.println("application.tmpl file successfully generated");
        assertTrue(true);
    }
}
