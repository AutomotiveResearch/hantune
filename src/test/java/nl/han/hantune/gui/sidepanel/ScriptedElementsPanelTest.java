/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.sidepanel;

import java.awt.Component;
import javax.swing.JScrollPane;
import javax.swing.JTree;

import nl.han.hantune.gui.sidepanel.ScriptedElementsPanel;
import nl.han.hantune.scripting.ScriptedSignal;
import nl.han.hantune.scripting.ScriptingManager;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Michiel Klifman
 */
public class ScriptedElementsPanelTest {


    @Test
    public void testScriptedSignalUpdate() {
        ScriptedElementsPanel panel = new ScriptedElementsPanel("test");
        JScrollPane scrollPane = null;
        for (Component c : panel.getComponents()) {
            if (c instanceof JScrollPane) {
                scrollPane = (JScrollPane) c;
                break;
            }
        }
        if (scrollPane == null) {
            Assert.fail();
        } else {
            ScriptingManager.getInstance().addScriptedSignal(new ScriptedSignal("testSignal"));

            panel.scriptedSignalUpdate();
            JTree tree = (JTree) scrollPane.getViewport().getView();
            int result = tree.getRowCount();
            int expectedResult = 2;
            Assert.assertEquals(result, expectedResult);
        }
    }

}
