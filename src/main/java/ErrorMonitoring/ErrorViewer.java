/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorMonitoring;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import ErrorLogger.AppendToLogfile;
import HANtune.HANtuneWindow;
import util.MessagePane;

/**
 *
 * @author Mathijs
 */
public class ErrorViewer extends HANtuneWindow implements ActionListener, IErrorObserver {

    private static final long serialVersionUID = 1L;
    private static final Dimension DEFAULT_SIZE = new Dimension(425, 200);
    public JTable errTableA;
    public JTable errTableS;
    private ErrorMonitor errorMonitor = null;
    DefaultTableModel tableModelA = new DefaultTableModel(new Object[][] {}, new Object[] {"Code", "Parameter", "Occurrence", "TimeStamp", "Info"});
    DefaultTableModel tableModelS = new DefaultTableModel(new Object[][] {}, new Object[] {"Code", "Parameter", "Occurrence", "TimeStamp", "Info"});

    public ErrorViewer() {
        this.setTitle("HANtune - ErrorViewer");
        defaultMinimumSize = new Dimension(375, 140);
        this.getContentPane().setPreferredSize(DEFAULT_SIZE);
        initializePopupMenu();
        addCustomPopupMenu();
        loadSettings();
    }

    public void setErrorMonitor(ErrorMonitor error_monitor) {
        // store member
        this.errorMonitor = error_monitor;
        // register ourselves
        if (this.errorMonitor != null) {
            this.errorMonitor.registerObserver(this);
        }
    }

     @Override
    public void rebuild() {
        GridBagConstraints c = new GridBagConstraints();
        // MAIN PANEL *********************************************************
        JPanel panel = new JPanel(new GridBagLayout());
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        this.getContentPane().add(panel, c);

        // PANEL ACTIVE *******************************************************
        JPanel panelA = new JPanel(new GridBagLayout());
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0.5;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        panel.add(panelA, c);

        // PANEL ACTIVE LABEL *************************************************
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0;
        c.weighty = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.NORTHWEST;
        panelA.add(new JLabel("Active Error List:"), c);

        // PANEL ACTIVE TABLE *************************************************
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.NORTHWEST;

        errTableA = new JTable(tableModelA);
        errTableA.setEnabled(false);
        JScrollPane scrollPaneA = new JScrollPane(errTableA);
        scrollPaneA.setComponentPopupMenu(super.getMenu());
        panelA.add(scrollPaneA, c);

        // PANEL STORED *******************************************************
        JPanel panelS = new JPanel(new GridBagLayout());
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 0.5;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        panel.add(panelS, c);

        // PANEL STORED LABEL *************************************************
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0;
        c.weighty = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.NORTHWEST;
        panelS.add(new JLabel("Stored Error List:"), c);

        // PANEL STORED TABLE *************************************************
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.NORTHWEST;
        errTableS = new JTable(tableModelS);
        JScrollPane scrollPaneS = new JScrollPane(errTableS);
        errTableS.setEnabled(false);
        scrollPaneS.setComponentPopupMenu(super.getMenu());
        panelS.add(scrollPaneS, c);

        // POPUPMENU TABLE ACTIVE *********************************************
        JPopupMenu popupMenuA = new JPopupMenu();

        JMenuItem delActive = new JMenuItem("Clear Active List");
        delActive.setActionCommand("clearlistA");
        delActive.addActionListener(this);
        popupMenuA.add(delActive);

        JMenuItem errPrintA = new JMenuItem("Create Error Logfile");
        errPrintA.setActionCommand("errPrintA");
        errPrintA.addActionListener(this);
        popupMenuA.add(errPrintA);

        errTableA.setComponentPopupMenu(popupMenuA);

        // POPUPMENU TABLE STORED *********************************************
        JPopupMenu popupMenuS = new JPopupMenu();

        JMenuItem delStored = new JMenuItem("Clear Stored List");
        delStored.setActionCommand("clearlistS");
        delStored.addActionListener(this);
        popupMenuS.add(delStored);

        JMenuItem errPrintS = new JMenuItem("Create Error Logfile");
        errPrintS.setActionCommand("errPrintS");
        errPrintS.addActionListener(this);
        popupMenuS.add(errPrintS);

        errTableS.setComponentPopupMenu(popupMenuS);

        // POPUPMENU WINDOW ***************************************************
        panel.setComponentPopupMenu(super.getMenu());
        loadSettings();
    }

    void addCustomPopupMenu(){
        JMenuItem reset = new JMenuItem("Reset to default size");
        reset.setActionCommand("reset");
        reset.addActionListener(this);
        super.addMenuItemToPopupMenu(reset);
    }

    @Override
    public void loadSettings() {
        loadWindowSettings();
        repaint();
    }

    @Override
    public void clearWindow() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()) {
            case "clearlistA":
                try {
                if (this.errorMonitor != null) {
                    this.errorMonitor.clearErrors(true, false);
                }
                emptyTable(tableModelA);
                break;
                } catch (NullPointerException ex) {
                    AppendToLogfile.appendError(Thread.currentThread(), ex);
                    MessagePane.showInfo("Action cannot be performed, please check the connection.");
                    break;
                }
            case "clearlistS":
                try {
                if (this.errorMonitor != null) {
                    this.errorMonitor.clearErrors(false, true);
                }
                emptyTable(tableModelS);
                break;
                } catch (NullPointerException ex) {
                    AppendToLogfile.appendError(Thread.currentThread(), ex);
                    MessagePane.showInfo("Action cannot be performed, please check the connection.");
                    break;
                }
            case "errPrintA":
                printErrorLists();
                break;
            case "errPrintS":
                printErrorLists();
                break;
            case "reset":
                Dimension defaultWindowSize = (new Dimension(((getSize().width - getContentPane().getSize().width) + DEFAULT_SIZE.width), ((getSize().height - getContentPane().getSize().height) + DEFAULT_SIZE.height)));
                setSize(defaultWindowSize);
                repaint();
                break;
        }
    }

    /**
     * Fills the given table with the given data
     *
     * @param alTable the data for the table
     * @param table the table to fill
     */
    public void fillTable(DefaultTableModel model, ArrayList<ErrorObject> alTable, JTable table) {
        model.setRowCount(alTable.size());
        int row = 0;
        for (ErrorObject obj : alTable) {
            model.setValueAt(obj.getCode(), row, 0);
            model.setValueAt(obj.getParameter(), row, 1);
            model.setValueAt(obj.getOccurrence(), row, 2);
            model.setValueAt(obj.getTimestamp(), row, 3);
            model.setValueAt(obj.getInfo(), row, 4);
            row++;
        }
    }

    /**
     * Empty the rows from the table model
     */
    private void emptyTable(DefaultTableModel model) {
        model.setRowCount(0);
    }

    /**
     * Print the error lists to a CSV file
     */
    private void printErrorLists() {
        Errorlog errorLog = new Errorlog();
        errorLog.openDatalog(errTableA, errTableS);
        errorLog.closeDatalog();
        MessagePane.showInfo("Logfile created at: " + currentConfig.getLogFile_Path());
    }

    @Override
    public void update(ArrayList<ErrorObject> active, ArrayList<ErrorObject> stored) {
        // update the tables on the viewer
        fillTable(tableModelA, active, errTableA);
        fillTable(tableModelS, stored, errTableS);
    }

    @Override
    public void doDefaultCloseAction() {
        // remove ourselves
        if (this.errorMonitor != null) {
            this.errorMonitor.removeObserver(this);
        }
        super.doDefaultCloseAction();
    }
}
