package XCP;

import ErrorLogger.AppendToLogfile;
import tel.schich.javacan.RawCanChannel;
import tel.schich.javacan.platform.linux.epoll.EPollSelector;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Thread.currentThread;

public class SocketCanRunnable implements Runnable {
    private final AtomicBoolean keepRunning = new AtomicBoolean(false);
    private final Thread worker;

    private final RawCanChannel socket;
    private final EPollSelector selector;
    private final XCPonSocketCan socketCan;

    RawCanChannel getSocket() {
        return socket;
    }

    SocketCanRunnable(final RawCanChannel socket, final EPollSelector selector, XCPonSocketCan socketCan) {
        this.socket = socket;
        this.selector = selector;
        this.socketCan = socketCan;
        worker = new Thread(this);
    }

    public void interruptSocket() {
        keepRunning.set(false);
        worker.interrupt();
        try {
            if (socket.isOpen()) {
                socket.close();
            }
            if (selector.isOpen()) {
                selector.close();
            }
        } catch (IOException e) {
            AppendToLogfile.appendMessage("Error interrupting socketCan");
            AppendToLogfile.appendError(currentThread(), e);
        }
    }

    public void start() {
        worker.start();
    }

    @Override
    public void run() {
        keepRunning.set(true);
        while (keepRunning.get()) {
            if (!socketCan.handleRcvSocketEvent(socket, selector)) {
                interruptSocket();
            }
        }
    }
}
