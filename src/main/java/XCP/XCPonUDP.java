/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package XCP;

import ErrorLogger.AppendToLogfile;
import HANtune.HANtune;
import datahandling.CurrentConfig;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import util.Util;

/**
 *
 * @author Michiel Klifman
 */
public class XCPonUDP extends XCP {

    private final CurrentConfig currentConfig = CurrentConfig.getInstance();
    private DatagramSocket udpSocket;
    private final ScheduledExecutorService heartbeatScheduler = Executors.newScheduledThreadPool(1);
    private final String ip = currentConfig.getXcpsettings().getEthernetIP();
    private final int port = currentConfig.getXcpsettings().getEthernetPort();
    private final int heartbeatInterval = currentConfig.getXcpsettings().getHeartbeatInterval();

    public XCPonUDP() {
        super();

        // Ethernet specific settings
        combineODT = true; // Ethernet allows to combine multiple items per ODT
        useSlaveTimestamp =  false; // The slave does not generate a timestamp for each sample
        allow64BitSignals = true; // The slave allows to use 64 bit signals

        xcpTimeOutTimer.startTimer();
    }

    /**
     * Makes a connection to a XCP slave over an Ethernet network.
     *
     * @return whether this method has been executed successfully
     */
    @Override
    protected boolean connect() {
        try {
            udpSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            return false;
        }
        connected = true;
        new Thread(() -> {
            byte[] udpData = new byte[256];
            DatagramPacket udpPacket = new DatagramPacket(udpData, udpData.length);
            while (connected) {
                try {
                    udpSocket.receive(udpPacket);
                } catch (IOException ex) {
                    if (connected) {
                        AppendToLogfile.appendError(currentThread(), ex);
                    }
                    continue;
                }
                if (udpPacket.getAddress().getHostAddress().equals(ip)) {
                    if (DEBUG_PRINT) {
                        System.out.println("ETHERNET RECEIVING (" + udpPacket.getData().length + ") : " + Arrays.toString(udpPacket.getData()));
                    }
                    int messageLength = (udpData[1] << 8) | udpData[0];

                    // read the remaining bytes of the message : the DAQ list data
                    byte[] xcpData = Arrays.copyOfRange(udpData, 4, messageLength + 4);
                    // pass data to XCP Protocol Layer
                    receiveData(Util.byteA2charA(xcpData, xcpData.length), XCPonCANBasic.calculateTimeStampMicros());
                }
            }
        }).start();

        byte[] bytes = ByteBuffer.allocate(2).putShort(Integer.valueOf(heartbeatInterval).shortValue()).array();
        heartbeatScheduler.scheduleAtFixedRate(() -> {
            try {
                if (connectionFinalized) {
                    sendData(new char[]{CC_USER_CC, 0x10, (char) bytes[0], (char) bytes[1]});
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }, 1, heartbeatInterval, TimeUnit.SECONDS);
        return true;
    }

    /**
     * Closes a connection to a XCP slave over the Ethernet network.
     *
     * @return whether this method has been executed successfully
     */
    @Override
    protected boolean disconnect() {
        if (udpSocket != null && !udpSocket.isClosed()) {
            connected = false;
            stopXCP();
            heartbeatScheduler.shutdownNow();
            udpSocket.close();
        }
        return true;
    }

    /**
     * Sends an array of databytes to the XCP slave device over the Ethernet
     * network.
     *
     * @param data message data array
     * @return whether the data has been send succesfully
     */
    @Override
    protected boolean sendData(char[] data) {
        // check connection
        if (!connected || udpSocket.isClosed()) {
            return false;
        }

        // send message and return status
        XCPonEthernet.XCPTCPMessage xcptcpm = new XCPonEthernet.XCPTCPMessage(data.length, data);
        byte[] raw = xcptcpm.getRawData();
        if (DEBUG_PRINT) {
            System.out.println("ETHERNET SENDING (" + raw.length + ") : " + Arrays.toString(raw));
        }

        try {
            DatagramPacket udpPacket = new DatagramPacket(raw, raw.length, InetAddress.getByName(ip), port);
            udpSocket.send(udpPacket);

            XCP_waiting = true;
            xcpTimeOutTimer.setTimeInMiliseconds(0);
            cl.incrementCurrentID();
        } catch (IOException ex) {
            AppendToLogfile.appendError(currentThread(), ex);
            return false;
        }
        return true;
    }

}
