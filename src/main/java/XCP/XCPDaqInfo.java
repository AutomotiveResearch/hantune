/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

/**
 * Object structure for XCP DAQ information. Containts all information
 * the XCP master and Application layer need to know about a XCP Data Acquisition.
 *
 * @author Aart-Jan
 */
public class XCPDaqInfo {

    /* GET_DAQ_PROCESSOR_INFO */

    /**
     * Indicates whether the DAQ configuration is static of dynamic. True when dynamic, false when static.
     */
    boolean daq_config_type = false;
    /**
     * Indicates whether DAQ lists can prescale an event channel.
     */
    public boolean prescaler_supported = false;
    /**
     * Indicates whether DAQ lists can be added to a RESUME mode configuration.
     */
    public boolean resume_supported = false;
    /**
     * Indicates whether bitwise data stimulation is supported.
     */
    public boolean bit_stim_supported = false;
    /**
     * Indicates whether timestamp mode is supported.
     */
    public boolean timestamp_supported = false;
    /**
     * Indicates whether the Identification Field can be turned off.
     */
    public boolean pid_off_supported = false;
    /**
     * Overload identification type.
     *
     * @see XCP.XCP#DAQ_OVERL_IND_NONE
     * @see XCP.XCP#DAQ_OVERL_IND_MSB
     * @see XCP.XCP#DAQ_OVERL_IND_EVT
     * @see XCP.DAQ_OVERL_IND_N_ALLOWED
     */
    public char daq_overload = 0x00;

    // DAQ Identification Field Type
    // See: XCP -Part 2- Protocol Layer Specification -1.0:
    //     - DAQ_HDR_PID: Identification Field Type “absolute ODT number”
    //     - DAQ_HDR_ODT_DAQB: Identification Field Type “relative ODT number and absolute DAQ list number”
    enum DaqIdFieldType {
        DAQ_HDR_PID(0 << 6),        // DAQ identification: PID (= absolute ODT). Default, currently implemented
        DAQ_HDR_ODT_DAQB(1 << 6),   // DAQ identification: PID (= relative ODT) + absolute DAQ(BYTE). implemented
        //  DAQ_HDR_ODT_DAQW(2 << 6),     // DAQ Hdr: PID (= relative ODT) + absolute DAQ(WORD). NOT implemented
        //  DAQ_HDR_ODT_FIL_DAQW(3 << 6); // DAQ Hdr: ODT + FIL?? + DAQ(WORD). NOT implemented
        DAQ_HDR_NOT_IMPLEMENTED((byte)0xFF);
        private final int value;


        private DaqIdFieldType(int value)
        {
            this.value = value;
        }

        public static DaqIdFieldType getEnumValue(int value){
            for (XCPDaqInfo.DaqIdFieldType e: DaqIdFieldType.values()) {
                if(e.value == value)
                    return e;
            }
            return DAQ_HDR_NOT_IMPLEMENTED; //Value out of enum scope
        }

    }
    public DaqIdFieldType daqIdFldType = DaqIdFieldType.DAQ_HDR_NOT_IMPLEMENTED;

    /**
     * Indicates the Optimisation Type.
     *
     * @see XCP.XCP#DAQ_OPT_OM_DEFAULT
     * @see XCP.XCP#DAQ_OPT_OM_ODT_TYPE_16
     * @see XCP.XCP#DAQ_OPT_OM_ODT_TYPE_32
     * @see XCP.XCP#DAQ_OPT_OM_ODT_TYPE_64
     * @see XCP.XCP#DAQ_OPT_OM_ODT_TYPE_ALIGNMENT
     * @see XCP.XCP#DAQ_OPT_OM_MAX_ENTRY_SIZE
     */
    public char daq_optimisation_type = 0x00;
    /**
     * Indicates the Address Extension type.
     */
    public char daq_address_ext            = 0x00;
    /**
     * Indicates the number of predefined DAQ lists.
     */
    public char min_daq                    = 0x00;
    /**
     * Indicates the total number of DAQ lists available in the slave device.
     */
    public char max_daq                    = 0x00;
    /**
     * Indicates the maximum number of event channels available in the slave device.
     */
    public char max_event_channel          = 0x00;


    /* GET_DAQ_RESOLUTION_INFO */

    /**
     * Indicates the granularity for size of an ODT entry for DAQ direction.
     */
    public char daq_odt_entry_granularity  = 0x00;
    /**
     * Indicates the maximum size of an ODT entry for DAQ direction.
     */
    public char daq_odt_entry_size_max     = 0x00;
    /**
     * Indicates the granularity for size of an ODT entry for STIM direction.
     */
    public char stim_odt_entry_granularity = 0x00;
    /**
     * Indicates the maximum size of an ODT entry for STIM direction.
     */
    public char stim_odt_entry_size_max    = 0x00;
    /**
     * Indicates the timestamp unit and size.
     */
    public char timestamp_mode             = 0x00;
    /**
     * Indicates whether the slave does always send timestamps.
     */
    public boolean timestamp_fixed         = false;
    /**
     * Indicates the size (bytes) of the timestamp
     */
    public char timestamp_size             = 0x00;
    /**
     * Indicates the unit of the timestamp
     */
    public char timestamp_unit             = 0x00;
    /**
     * Indicates the timestamp ticks per unit.
     */
    public char timestamp_ticks            = 0x00;
    /**
     * Hold the timestamp stepsize in microseconds.
     */
    public long timestamp_step             = 0;

    /**
     * Calculates timestamp stepsize in microseconds.
     */
    public long calcTimestampStepsize(){
        long step = 0;
        if(timestamp_size <= 0) return step;

        switch(timestamp_unit){
            case XCP.DAQ_TIMESTAMP_UNIT_1S:
                step = 1000000;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_100MS:
                step = 100000;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_10MS:
                step = 10000;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_1MS:
                step = 1000;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_100US:
                step = 100;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_10US:
                step = 10;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_1US:
                step = 1;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_100NS:
                step = 0;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_10NS:
                step = 0;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_1NS:
                step = 0;
                break;
            default:
                step = 0;
                break;
        }

        step = step / timestamp_ticks;

        return step;
    }

}
