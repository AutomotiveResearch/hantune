/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

import java.util.HashMap;
import java.util.Map;
import peak.can.basic.TPCANBaudrate;

public class XCPSettings {

    /**
     * Baudrate used for this CAN connection
     */
    private TPCANBaudrate CAN_baudrate = TPCANBaudrate.PCAN_BAUD_500K;
    /**
     * Message identifier used for XCP messages to be send from the master to the slave device
     */
    private int CAN_send_id = 0x665;
    /**
     * Message identifier filter used for XCP respons messages from the slave device
     */
    private int CAN_receive_id = 0x666;
    /**
     * Number of event to be used for DAQ list
     */
    public char event_id = 0;
    /**
     * Frequency of the DAQ list event at the slave device. This frequency is the maximum possible sample frequency.
     */
    private float slaveFrequency = 0;
    /**
     * Default frequency to be set during DAQ list configuration.
     */
    public int sample_freq_init = 10;
    /**
     * Slave device identifier
     */
    public String slaveid = "";
    /**
     * Timeout prescaler
     */
    public int tprescaler = 1;
    /**
     * Network used for this CAN connection
     */
    private String CAN_net = "";
    /**
     * IP Address used for an ethernet connection
     */
    private String ETHERNET_IP = "";
    /**
     * Port used for an ethernet connection
     */
    private int ETHERNET_PORT = 1000;
    /**
     * Bandwidth of the ethernet connection (Mbit/s)
     */
    private int ETHERNET_BANDWIDTH = 100;
    /**
     * Protocols for an ethernet connection
     */
    public enum EthernetProtocol {
        TCP, UDP
    };
    /**
     * Protocol used for an ethernet connection
     */
    private EthernetProtocol ethernetProtocol = EthernetProtocol.TCP;
    /**
     * Hearbeat interval used for an udp connection (in seconds)
     */
    private int heartbeatInterval = 10;
    /**
     * Port used for a UART connection
     */
    private String UART_PORT = "COM1";
    /**
     * Baudrate used for a UART connection
     */
    private int UART_BAUDRATE = 115200;
    private boolean usbVirtualComPort = true;

    private boolean errorMonitoringEnabled;

    public static final Map<String, TPCANBaudrate> BAUD_RATES = new HashMap<>();

    static {
        BAUD_RATES.put("F4240", TPCANBaudrate.PCAN_BAUD_1M);
        BAUD_RATES.put("7A120", TPCANBaudrate.PCAN_BAUD_500K);
        BAUD_RATES.put("3D090", TPCANBaudrate.PCAN_BAUD_250K);
        BAUD_RATES.put("1E848", TPCANBaudrate.PCAN_BAUD_125K);
        BAUD_RATES.put("186A0", TPCANBaudrate.PCAN_BAUD_100K);
        BAUD_RATES.put("C350", TPCANBaudrate.PCAN_BAUD_50K);
        BAUD_RATES.put("4E20", TPCANBaudrate.PCAN_BAUD_20K);
        BAUD_RATES.put("2710", TPCANBaudrate.PCAN_BAUD_10K);
        BAUD_RATES.put("1388", TPCANBaudrate.PCAN_BAUD_5K);
    }

    /**
     * Sets baudrate used by CAN device
     */
    public void setCANBaudrate(TPCANBaudrate br){
        CAN_baudrate = br;
    }

    /**
     * Sets network used by CAN device
     */
    public void setCANNet(String net){
        CAN_net = net;
    }

    /**
     * Returns network used by CAN device
     */
    public String getCANNet(){
        return CAN_net;
    }

    /**
     * Returns baudrate used by CAN device
     */
    public TPCANBaudrate getCANBaudrate(){
        return CAN_baudrate;
    }

    /**
     * Sets CAN identifier used for transmission
     */
    public void setCANIDTx(int id){
        if(id > 0x7ff || id < 0) id = 0x665;
        CAN_send_id = id;
    }

    /**
     * Returns CAN identifier used for reception
     */
    public int getCANIDTx(){
        return CAN_send_id;
    }

    /**
     * Sets CAN identifier used for transmission
     */
    public void setCANIDRx(int id){
        if(id > 0x7ff || id < 0) id = 0x666;
        CAN_receive_id = id;
    }

    /**
     * Returns CAN identifier used for reception
     */
    public int getCANIDRx(){
        return CAN_receive_id;
    }

    /**
     * Sets prescaler for timeout
     */
    public void setTPrescaler(int p){
        if(p > 500 || p < 1) p = 1;
        tprescaler = p;
    }

    /**
     * Returns prescaler for timeout
     */
    public int getTPrescaler(){
        return tprescaler;
    }

    /**
     * Sets the ip address which should be used for an ethernet connection
     * @param ip
     */
    public void setEthernetIP(String ip) {
        this.ETHERNET_IP = ip;
    }

    /**
     * @return the ip address which should be used for an ethernet connection
     */
    public String getEthernetIP() {
        return ETHERNET_IP;
    }

    /**
     * Sets the port which should be used for an ethernet connection
     * @param port
     */
    public void setEthernetPort(int port) {
        this.ETHERNET_PORT = port;
    }

    /**
     * @return the port which should be used for an ethernet connection
     */
    public int getEthernetPort() {
        return ETHERNET_PORT;
    }

    /**
     * Sets the bandwidth of the Ethernet channel
     * @param port
     */
    public void setEthernetBandwidth(int bandwidth) {
        this.ETHERNET_BANDWIDTH = bandwidth;
    }

    /**
     * @return the bandwidth of the Ethernet channel
     */
    public int getEthernetBandwidth() {
        return ETHERNET_BANDWIDTH;
    }

    /**
     * @return the ethernet protocol (i.e. TCP or UDP)
     */
    public EthernetProtocol getEthernetProtocol() {
        return ethernetProtocol;
    }

    /**
     * Sets the ethernet protocol (i.e. TCP or UDP)
     * @param protocol
     */
    public void setEthernetProtocol(EthernetProtocol protocol) {
        this.ethernetProtocol = protocol;
    }

    /**
     * @return the heartbeat interval (in seconds)
     */
    public int getHeartbeatInterval() {
        return heartbeatInterval;
    }

    /**
     * Sets the heartbeat interval (in seconds)
     * @param interval
     */
    public void setHeartbeatInterval(int interval) {
        this.heartbeatInterval = interval;
    }

    /**
     * Sets the port which should be used for a UART connection
     * @param port
     */
    public void setUARTPort(String port) {
        this.UART_PORT = port;
    }

    /**
     * @return the port which should be used for a UART connection
     */
    public String getUARTPort() {
        return UART_PORT;
    }

    /**
     * Sets the port which should be used for a UART connection
     * @param port
     */
    public void setUARTBaudrate(int baudrate) {
        this.UART_BAUDRATE = baudrate;
    }

    /**
     * @return the port which should be used for a UART connection
     */
    public int getUARTBaudrate() {
        return UART_BAUDRATE;
    }

    /**
     * @return the UART bandwidth. If a usb virtual com port is used this method
     * returns a bandwidth of 1 Mbit/s. Else the bandwidth is equal to the baudrate.
     */
    public int getUARTBandwidth() {
        if (usbVirtualComPort) {
            return 1048576;
        }
        return UART_BAUDRATE;
    }

    public boolean isUsbVirtualComPort() {
        return usbVirtualComPort;
    }

    public void setUsbVirtualComPort(boolean usbVirtualComPort) {
        this.usbVirtualComPort = usbVirtualComPort;
    }

    /**
     * @return the slaveFrequency
     */
    public float getSlaveFrequency() {
        return slaveFrequency;
    }

    /**
     * @param slaveFrequency the slaveFrequency to set
     */
    public void setSlaveFrequency(float slaveFrequency) {
        this.slaveFrequency = slaveFrequency;
    }

    /**
     * @return the errorMonitoringEnabled
     */
    public boolean isErrorMonitoringEnabled() {
        return errorMonitoringEnabled;
    }

    /**
     * @param errorMonitoringEnabled the errorMonitoringEnabled to set
     */
    public void setErrorMonitoringEnabled(boolean errorMonitoringEnabled) {
        this.errorMonitoringEnabled = errorMonitoringEnabled;
    }

    /**
     * @return the errorMonitorSupported
     */
    public boolean isErrorMonitoringSupported() {
        // An optional check could be inserted here to see if the slave actually
        // supports error monitoring. In the past it did a check for the slaveid
        // contains one of the following strings: STM32, RC12-10, RC28-14 or
        // XCP-RC30. This was removed because the slave id is derived from the
        // Simulink model's filename and this naming restricting is unwanted.
        // Additionally, for UDS support this wouldn't work because UDS does not
        // have a slaveid feature. For now assume that all targets that HANtune
        // connect to, will have error monitoring support, which is currently
        // the case with all HANcoder supported targets.
        return true;
    }
}
