/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

import ErrorLogger.AppendToLogfile;
import datahandling.CurrentConfig;
import tel.schich.javacan.CanChannels;
import tel.schich.javacan.CanFilter;
import tel.schich.javacan.CanFrame;
import tel.schich.javacan.RawCanChannel;
import tel.schich.javacan.platform.linux.LinuxNetworkDevice;
import tel.schich.javacan.platform.linux.epoll.EPollSelector;
import tel.schich.javacan.select.SelectorRegistration;
import util.Util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.EnumSet;

import static tel.schich.javacan.CanFrame.FD_NO_FLAGS;
import static tel.schich.javacan.CanSocketOptions.FILTER;
import static tel.schich.javacan.CanSocketOptions.RECV_OWN_MSGS;

/**
 * XCP on SocketCan Transport Layer. This class extends the XCP Layer class by adding a transport layer specific
 * interface for connecting a XCP slave device and receiving and transmitting XCP data over SocketCAN
 */
public class XCPonSocketCan extends XCP {
    static {
        // set systemproperties for correct loading of libjavacan-*.so files
        System.setProperty("javacan.native.javacan-core.path",
                System.getProperty("user.dir") + "/lib/native/libjavacan-core.so");
        System.setProperty("javacan.native.javacan-epoll.path",
                System.getProperty("user.dir") + "/lib/native/libjavacan-epoll.so");
    }

    private static final CurrentConfig currentConfig = CurrentConfig.getInstance();
    //    private static RawCanChannel socket = null;
    private SocketCanRunnable socketCanRunnable = null;

    public XCPonSocketCan() {
        super();
        xcpTimeOutTimer.startTimer();
        useSlaveTimestamp = false; // The slave generates a timestamp for each sample
    }


    /**
     * Makes a connection to a XCP slave over SocketCAN
     *
     * @return true if this method has been executed successfully
     */
    @Override
    protected boolean connect() {
        // set systemproperties for correct loading of libjavacan-*.so files
        // Preconditions to connect with SocketCAN, after reset.
        // Bring up kernel modules:
        //   sudo modprobe can
        //   sudo modprobe can_raw
        //   sudo modprobe peak_usb
        // Up socketCan:
        //   sudo ip link set can0 up type can bitrate 500000
        // For a permanent setup, see HANtune_installation_linux.md
        try {
            RawCanChannel socket = CanChannels.newRawChannel();
            EPollSelector selector = EPollSelector.open();

            socket.bind(LinuxNetworkDevice.lookup("can0"));
            socket.configureBlocking(false);
            socket.setOption(RECV_OWN_MSGS, false);

            selector.register(socket, EnumSet.of(SelectorRegistration.Operation.READ));

            // set filter for CAN ID
            CanFilter[] filter = {new CanFilter(currentConfig.getXcpsettings().getCANIDTx(),
                    currentConfig.getXcpsettings().getCANIDTx()), new CanFilter(
                    currentConfig.getXcpsettings().getCANIDRx(), currentConfig.getXcpsettings().getCANIDRx()),};
            socket.setOption(FILTER, filter);

            socketCanRunnable = new SocketCanRunnable(socket, selector, this);
            socketCanRunnable.start();

        } catch (LinkageError | IOException e) {
            String errMsg = "Could not initialize SocketCAN\n" + e.getMessage();
            setError(errMsg, true);
            AppendToLogfile.appendMessage(errMsg);
            AppendToLogfile.appendError(Thread.currentThread(), e);
            return false;
        }

        // start xcp communication
        connected = true;

        return true;
    }


    /**
     * Closes a connection to a XCP slave over a SocketCAN network.
     *
     * @return whether this method has been executed successfully
     */
    @Override
    protected boolean disconnect() {
        // stop xcp communication
        stopXCP();

        if (currentConfig.getCan() != null) {
            currentConfig.getCan().stopSession();
        }
        if (socketCanRunnable != null) {
            socketCanRunnable.interruptSocket();
        }

        return true;
    }


    /**
     * Sends an array of databytes to the XCP slave device over a CAN network.
     *
     * @param data message data array
     * @return true when data has been sent successfully
     */
    @Override
    protected boolean sendData(char[] data) {
        if (!sendCanMessage(currentConfig.getXcpsettings().getCANIDTx(), Util.charA2byteA(data))) {
            return false;
        }

        XCP_waiting = true;
        xcpTimeOutTimer.setTimeInMiliseconds(0);
        cl.incrementCurrentID();

        return true;
    }


    private boolean sendCanMessage(int id, byte[] dataBytes) {
        if (!connected) {
            return false;
        }
        // print debug information
        if (DEBUG_PRINT) {
            System.out.print(">>>sndXCPsocketCAN ID: " + id + " " + dataBytes.length + " databyte(s) (");
            for (byte dataByte : dataBytes) {
                System.out.printf(" 0x%02X", dataByte);
            }
            System.out.println(" )");
        }

        final CanFrame canFrame = CanFrame.create(id, FD_NO_FLAGS, dataBytes);
        try {
            socketCanRunnable.getSocket().write(canFrame);
        } catch (IOException e) {
            setError("Error sending data on SocketCAN\n" + e.getMessage(), true);
            AppendToLogfile.appendError(currentThread(), e);

            return false;
        }

        return true;
    }


    boolean handleRcvSocketEvent(RawCanChannel socket, EPollSelector selector) {
        CanFrame readFrame;
        try {
            // wait until new data arrives or timeout
            int timeoutMillis = XCP.TIMEOUT_T7;
            if (selector.select(Duration.ofMillis(timeoutMillis)).isEmpty()) {
                AppendToLogfile.appendMessage(
                        "SocketCAN reception timeout. No data received for " + timeoutMillis + " ms");
                return false;
            } else {
                readFrame = socket.read();
                long timeStamp = XCPonCANBasic.calculateTimeStampMicros();

                ByteBuffer data = ByteBuffer.allocate(readFrame.getDataLength());
                readFrame.getData(data);

                if (DEBUG_PRINT) {
                    System.out.printf("rcvXCPsocketCAN>>> %d Id: %03X len %d  Data:", timeStamp, readFrame.getId(),
                            readFrame.getDataLength());
                    for (byte dataByte : data.array()) {
                        System.out.printf(" %02X", dataByte);
                    }
                    System.out.println();
                }

                processRcvXcpData(data.array(), timeStamp);
                return true;
            }
        } catch (IOException e) {
            System.out.println(" handleRcvSocketEvent IOException:  " + e.getMessage());
        }
        return false;
    }


    private void processRcvXcpData(byte[] msgData, long timestamp) {
        try {
            receiveData(Util.byteA2charA(msgData, msgData.length), timestamp);
        } catch (Exception e) {
            String errMsg = "Error while processing SocketCAN message";
            AppendToLogfile.appendMessage(errMsg);
            AppendToLogfile.appendError(currentThread(), e);
            // set error and exit command list sequence
            e.printStackTrace();
            System.err.println(e.getMessage());
            setError(errMsg + "\n" + e.getMessage(), true);

            socketCanRunnable.interruptSocket();
            XCP_isProcessing = false;
            XCP_waiting = false;
            cl.exit();
        }
    }

}
