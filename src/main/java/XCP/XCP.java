/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package XCP;

import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.SwingUtilities;

import ErrorLogger.AppendToLogfile;
import ErrorLogger.defaultExceptionHandler;
import ErrorMonitoring.ErrorObject;
import ErrorMonitoring.IErrorInfoController;
import ErrorMonitoring.IErrorInfoProcessor;
import HANtune.HANtune;
import HANtune.actions.LogReaderAction;
import datahandling.CurrentConfig;
import nl.han.hantune.datahandling.DataLogger.LogDataWriterCsv;
import util.Util;
import util.XCPTimeOutTimer;

/**
 * XCP Protocol Layer. This class is used to handle XCP communication. XCP
 * messages can be send and received using this class, but it also takes care of
 * error, event and service handling. Data acquisition messages will be passed
 * to the Application Layer, where a predefined event&listener mechanism will
 * distribute the received data.
 *
 * @author Aart-Jan
 */
@SuppressWarnings("SpellCheckingInspection")
public class XCP extends Thread implements IErrorInfoController {

    public static final int MAX_DAQ_ITEMS = 251; // max number of DAQ items in a single DAQ list

    private boolean hasDAQList = false;
    private boolean daqProcessPause = false;
    private CurrentConfig currentConfig = CurrentConfig.getInstance();
    protected XCPTimeOutTimer xcpTimeOutTimer = new XCPTimeOutTimer();
    private XcpTargetDaqlistSizeLimiter targetDaqlistSizeLimit = new XcpTargetDaqlistSizeLimiter();
    /**
     * Flag to set xcp running.
     */
    public boolean isXCPRunning = true;
    protected boolean XCP_isProcessing = false;
    private boolean XCP_sending = false;
    /**
     * Boolean used to check if HANtune is getting the slave id from the slave
     * device
     */
    public boolean getSlaveId = false;
    /**
     * Boolean used to check if multiple items per ODT can be used.
     */
    public boolean combineODT = false;
    /**
     * Boolean used to check whether the slave does generate and send a
     * timestamp.
     */
    public boolean useSlaveTimestamp = true;
    /**
     * Boolean used to check whether the slave does allow using 64 bit signals.
     */
    public boolean allow64BitSignals = false;
    /**
     * Timestamp (raw) of the previously received data (in microseconds)
     */
    private long timestampRawPrevious = 0;
    /**
     * Number of timestamp overflows at the slave
     */
    private long timestampRawOverflow = 0;

    // ***** (STD) STANDARD COMMANDS *****
    /**
     * Command code for: Set up connection with slave
     */
    public static final char CC_CONNECT = 0xFF;
    /**
     * Command code for: Disconnect from slave
     */
    public static final char CC_DISCONNECT = 0xFE;
    /**
     * Command code for: Get current session status from slave
     */
    public static final char CC_GET_STATUS = 0xFD;
    /**
     * Command code for: Synchronize command execution after time-out
     */
    public static final char CC_SYNCH = 0xFC;
    /**
     * Command code for: Get communication mode info
     */
    public static final char CC_GET_COMM_MODE_INFO = 0xFB;
    /**
     * Command code for: Get identification from slave
     */
    public static final char CC_GET_ID = 0xFA;
    /**
     * Command code for: Request to save to non-volatile memory
     */
    public static final char CC_SET_REQUEST = 0xF9;
    /**
     * Command code for: Get seed for unlocking a protected resource
     */
    public static final char CC_GET_SEED = 0xF8;
    /**
     * Command code for: Send key for unlocking a protected resource
     */
    public static final char CC_UNLOCK = 0xF7;
    /**
     * Command code for: Set Memory Transfer Address in slave
     */
    public static final char CC_SET_MTA = 0xF6;
    /**
     * Command code for: Upload from master to slave
     */
    public static final char CC_UPLOAD = 0xF5;
    /**
     * Command code for: Upload from master to slave (short version)
     */
    public static final char CC_SHORT_UPLOAD = 0xF4;
    /**
     * Command code for: Build checksum over memory range
     */
    public static final char CC_BUILD_CHECKSUM = 0xF3;
    /**
     * Command code for: Refer to transport layer specific command
     */
    public static final char CC_TRANSPORT_LAYER_CC = 0xF2;
    /**
     * Command code for: Refer to user defined command
     */
    public static final char CC_USER_CC = 0xF1;
    // ***** (CAL) CALIBRATION COMMANDS *****
    /**
     * Command code for: Download from master to slave
     */
    public static final char CC_DOWNLOAD = 0xF0;
    /**
     * Command code for: Download from master to slave (block mode)
     */
    public static final char CC_DOWNLOAD_NEXT = 0xEF;
    /**
     * Command code for: Download from master to slave (fixed size)
     */
    public static final char CC_DOWNLOAD_MAX = 0xEE;
    /**
     * Command code for: Download from master to slave (short version)
     */
    public static final char CC_SHORT_DOWNLOAD = 0xED;
    /**
     * Command code for: Modify bits
     */
    public static final char CC_MODIFY_BITS = 0xEC;
    // ***** (PAG) PAGE SWITCHING COMMANDS *****
    /**
     * Command code for: Set calibration page
     */
    public static final char CC_SET_CAL_PAGE = 0xEB;
    /**
     * Command code for: Get calibration page
     */
    public static final char CC_GET_CAL_PAGE = 0xEA;
    /**
     * Command code for: Get general information on PAG processor
     */
    public static final char CC_GET_PAG_PROCESSOR_INFO = 0xE9;
    /**
     * Command code for: Get specific information for a SEGMENT
     */
    public static final char CC_GET_SEGMENT_INFO = 0xE8;
    /**
     * Command code for: Get specific information for a PAGE
     */
    public static final char CC_GET_PAGE_INFO = 0xE7;
    /**
     * Command code for: Set mode for a segment
     */
    public static final char CC_SET_SEGMENT_MODE = 0xE6;
    /**
     * Command code for: Get mode for a segment
     */
    public static final char CC_GET_SEGMENT_MODE = 0xE5;
    /**
     * Command code for: Copy page
     */
    public static final char CC_COPY_CAL_PAGE = 0xE4;
    // ***** (DAQ) Data Acquisition and Stimulation commands *****
    /**
     * Command code for: Clear DAQ list configuration
     */
    public static final char CC_CLEAR_DAQ_LIST = 0xE3;
    /**
     * Command code for: Set pointer to ODT entry
     */
    public static final char CC_SET_DAQ_PTR = 0xE2;
    /**
     * Command code for: Write element in ODT entry
     */
    public static final char CC_WRITE_DAQ = 0xE1;
    /**
     * Command code for: Set mode for DAQ list
     */
    public static final char CC_SET_DAQ_LIST_MODE = 0xE0;
    /**
     * Command code for: Get mode for DAQ list
     */
    public static final char CC_GET_DAQ_LIST_MODE = 0xDF;
    /**
     * Command code for: Start/stop/select DAQ list
     */
    public static final char CC_START_STOP_DAQ_LIST = 0xDE;
    /**
     * Command code for: Start/stop DAQ lists (synchronously)
     */
    public static final char CC_START_STOP_SYNCH = 0xDD;
    /**
     * Command code for: Get DAQ clock from slave
     */
    public static final char CC_GET_DAQ_CLOCK = 0xDC;
    /**
     * Command code for: Read element from ODT entry
     */
    public static final char CC_READ_DAQ = 0xDB;
    /**
     * Command code for: Get general information on DAQ processor
     */
    public static final char CC_GET_DAQ_PROCESSOR_INFO = 0xDA;
    /**
     * Command code for: Get general information on DAQ processing resolution
     */
    public static final char CC_GET_DAQ_RESOLUTION_INFO = 0xD9;
    /**
     * Command code for: Get specific information for a DAQ list
     */
    public static final char CC_GET_DAQ_LIST_INFO = 0xD8;
    /**
     * Command code for: Get specific information for an event channel
     */
    public static final char CC_GET_DAQ_EVENT_INFO = 0xD7;
    /**
     * Command code for: Clear dynamic DAQ configuration
     */
    public static final char CC_FREE_DAQ = 0xD6;
    /**
     * Command code for: Allocate DAQ lists
     */
    public static final char CC_ALLOC_DAQ = 0xD5;
    /**
     * Command code for: Allocate ODTs to a DAQ list
     */
    public static final char CC_ALLOC_ODT = 0xD4;
    /**
     * Command code for: Allocate ODT entries to an ODT
     */
    public static final char CC_ALLOC_ODT_ENTRY = 0xD3;
    // ***** (PGM) Non-volatile memory programing commands *****
    /**
     * Command code for: Indicate the beginning of a programming sequence
     */
    public static final char CC_PROGRAM_START = 0xD2;
    /**
     * Command code for: Clear a part of non-volatile memory
     */
    public static final char CC_PROGRAM_CLEAR = 0xD1;
    /**
     * Command code for: Program a non-volatile memory segment
     */
    public static final char CC_PROGRAM = 0xD0;
    /**
     * Command code for: Indicate the end of a programming sequence
     */
    public static final char CC_PROGRAM_RESET = 0xCF;
    /**
     * Command code for: Get general information on PGM processor
     */
    public static final char CC_GET_PGM_PROCESSOR_INFO = 0xCE;
    /**
     * Command code for: Get specific information for a SECTOR
     */
    public static final char CC_GET_SECTOR_INFO = 0xCD;
    /**
     * Command code for: Prepare non-volatile memory programming
     */
    public static final char CC_PROGRAM_PREPARE = 0xCC;
    /**
     * Command code for: Set data format before programming
     */
    public static final char CC_PROGRAM_FORMAT = 0xCB;
    /**
     * Command code for: Program a non-volatile memory segment (block mode)
     */
    public static final char CC_PROGRAM_NEXT = 0xCA;
    /**
     * Command code for: Program a non-volatile memory segment (fixed size)
     */
    public static final char CC_PROGRAM_MAX = 0xC9;
    /**
     * Command code for: Program verify
     */
    public static final char CC_PROGRAM_VERIFY = 0xC8;
    // ***** Packet Identifiers Slave -> Master *****
    /**
     * Packet identifier for XCP respons messages from slave
     */
    public static final char PID_RES = 0xFF;
    /**
     * Packet identifier for XCP error messages from slave
     */
    public static final char PID_ERR = 0xFE;
    /**
     * Packet identifier for XCP event messages from slave
     */
    public static final char PID_EV = 0xFD;
    /**
     * Packet identifier for XCP service messages from slave
     */
    public static final char PID_SERV = 0xFC;
    // ***** Error Codes *****
    /**
     * Error code for: Command processor synchronisation
     */
    public static final char ERR_CMD_SYNCH = 0x00;
    /**
     * Error code for: Timeout
     */
    public static final char ERR_TIMEOUT = 0x01;
    /**
     * Error code for: No error
     */
    public static final char ERR_NONE = 0xFF;
    /**
     * Error code for: No error set
     */
    public static final char ERR_EMPTY = 0xFE;
    /**
     * Error code for: Command was not executed
     */
    public static final char ERR_CMD_BUSY = 0x10;
    /**
     * Error code for: Command rejected because DAQ is running
     */
    public static final char ERR_DAQ_ACTIVE = 0x11;
    /**
     * Error code for: Command rejected because PGM is running
     */
    public static final char ERR_PGM_ACTIVE = 0x12;
    /**
     * Error code for: Unknown command or not implemented optional command
     */
    public static final char ERR_CMD_UNKNOWN = 0x20;
    /**
     * Error code for: Command syntax invalid
     */
    public static final char ERR_CMD_SYNTAX = 0x21;
    /**
     * Error code for: Command syntax valid but command parameter(s) out of
     * range
     */
    public static final char ERR_OUT_OF_RANGE = 0x22;
    /**
     * Error code for: The memory location is write protected
     */
    public static final char ERR_WRITE_PROTECTED = 0x23;
    /**
     * Error code for: The memory location is not accessible
     */
    public static final char ERR_ACCESS_DENIED = 0x24;
    /**
     * Error code for: Access denied. Seed & Key is required
     */
    public static final char ERR_ACCESS_LOCKED = 0x25;
    /**
     * Error code for: Selected page not available
     */
    public static final char ERR_PAGE_NOT_VALID = 0x26;
    /**
     * Error code for: Selected page mode not available
     */
    public static final char ERR_MODE_NOT_VALID = 0x27;
    /**
     * Error code for: Selected segment not valid
     */
    public static final char ERR_SEGMENT_NOT_VALID = 0x28;
    /**
     * Error code for: Sequence error
     */
    public static final char ERR_SEQUENCE = 0x29;
    /**
     * Error code for: DAQ configuration not valid
     */
    public static final char ERR_DAQ_CONFIG = 0x2A;
    /**
     * Error code for: Memory overflow error
     */
    public static final char ERR_MEMORY_OVERFLOW = 0x30;
    /**
     * Error code for: Generic error
     */
    public static final char ERR_GENERIC = 0x31;
    /**
     * Error code for: The slave internal program verify routine detects an
     * error
     */
    public static final char ERR_VERIFY = 0x32;
    // ***** Event Codes *****
    /**
     * Event code for: Slave starting in RESUME mode
     */
    public static final char EV_RESUME_MODE = 0x00;
    /**
     * Event code for: The DAQ configuration in non-volatile memory has been
     * cleared
     */
    public static final char EV_CLEAR_DAQ = 0x01;
    /**
     * Event code for: The DAQ configuration has been stored into non-volatile
     * memory
     */
    public static final char EV_STORE_DAQ = 0x02;
    /**
     * Event code for: The calibration data has been stored into non-volatile
     * memory
     */
    public static final char EV_STORE_CAL = 0x03;
    /**
     * Event code for: Slave requesting to restart time-out
     */
    public static final char EV_CMD_PENDING = 0x05;
    /**
     * Event code for: DAQ processor overload
     */
    public static final char EV_DAQ_OVERLOAD = 0x06;
    /**
     * Event code for: Session terminated by slave device
     */
    public static final char EV_SESSION_TERMINATED = 0x07;
    /**
     * Event code for: User-defined event
     */
    public static final char EV_USER = 0xFE;
    /**
     * Event code for: Transport Layer specific event
     */
    public static final char EV_TRANSPORT = 0xFF;
    // ***** Service Request Codes *****
    /**
     * Service code for: Slave requesting to be res
     */
    public static final char SERV_RESET = 0x00;
    /**
     * Service code for: The remaining data bytes of the packet contain plain
     * ASCII text. The line separator is LF or CR/LF. The text must be null
     * terminated to indicate the end of the overall packet.
     */
    public static final char SERV_TEXT = 0x01;
    // ***** Checksum Types (BUILD_CHECKSUM) *****
    /**
     * Checksum type: Add BYTE into a BYTE checksum, ignore overflows
     */
    public static final char XCP_CHECKSUM_TYPE_ADD11 = 0x01;
    /**
     * Checksum type: Add BYTE into a WORD checksum, ignore overflows
     */
    public static final char XCP_CHECKSUM_TYPE_ADD12 = 0x02;
    /**
     * Checksum type: Add BYTE into a DWORD checksum, ignore overflows
     */
    public static final char XCP_CHECKSUM_TYPE_ADD14 = 0x03;
    /**
     * Checksum type: Add WORD into a WORD checksum, ignore overflows, blocksize
     * must be modulo 2
     */
    public static final char XCP_CHECKSUM_TYPE_ADD22 = 0x04;
    /**
     * Checksum type: Add WORD into a DWORD checksum, ignore overflows,
     * blocksize must be modulo 2
     */
    public static final char XCP_CHECKSUM_TYPE_ADD24 = 0x05;
    /**
     * Checksum type: Add DWORD into DWORD, ignore overflows, blocksize must be
     * modulo 4
     */
    public static final char XCP_CHECKSUM_TYPE_ADD44 = 0x06;
    /**
     * Checksum type: See CRC error detection algorithms
     */
    public static final char XCP_CHECKSUM_TYPE_CRC16 = 0x07;
    /**
     * Checksum type: See CRC error detection algorithms
     */
    public static final char XCP_CHECKSUM_TYPE_CRC16CCITT = 0x08;
    /**
     * Checksum type: See CRC error detection algorithms
     */
    public static final char XCP_CHECKSUM_TYPE_CRC32 = 0x09;
    /**
     * Checksum type: User defined, ASAM MCD 2MC DLL Interface
     */
    public static final char XCP_CHECKSUM_TYPE_DLL = 0xFF;
    // ***** DAQ Timestamp Sizes *****
    /**
     * Timestamp size: No timestamp
     */
    public static final char DAQ_TIMESTAMP_OFF = 0x00;
    /**
     * Timestamp size: BYTE (1 byte)
     */
    public static final char DAQ_TIMESTAMP_BYTE = 0x01;
    /**
     * Timestamp size: WORD (2 bytes)
     */
    public static final char DAQ_TIMESTAMP_WORD = 0x02;
    /**
     * Timestamp size: DWORD (4 bytes)
     */
    public static final char DAQ_TIMESTAMP_DWORD = 0x04;
    // ***** DAQ Timestamp Units *****
    /**
     * Timestamp unit: 1 nanosecond
     */
    public static final char DAQ_TIMESTAMP_UNIT_1NS = 0x00;
    /**
     * Timestamp unit: 10 nanoseconds
     */
    public static final char DAQ_TIMESTAMP_UNIT_10NS = 0x10;
    /**
     * Timestamp unit: 100 nanoseconds
     */
    public static final char DAQ_TIMESTAMP_UNIT_100NS = 0x20;
    /**
     * Timestamp unit: 1 microsecond
     */
    public static final char DAQ_TIMESTAMP_UNIT_1US = 0x30;
    /**
     * Timestamp unit: 10 microseconds
     */
    public static final char DAQ_TIMESTAMP_UNIT_10US = 0x40;
    /**
     * Timestamp unit: 100 microseconds
     */
    public static final char DAQ_TIMESTAMP_UNIT_100US = 0x50;
    /**
     * Timestamp unit: 1 millisecond
     */
    public static final char DAQ_TIMESTAMP_UNIT_1MS = 0x60;
    /**
     * Timestamp unit: 10 milliseconds
     */
    public static final char DAQ_TIMESTAMP_UNIT_10MS = 0x70;
    /**
     * Timestamp unit: 100 milliseconds
     */
    public static final char DAQ_TIMESTAMP_UNIT_100MS = 0x80;
    /**
     * Timestamp unit: 1 second
     */
    public static final char DAQ_TIMESTAMP_UNIT_1S = 0x90;
    // ***** DAQ overload indications *****
    /**
     * Overload indication: No overload indication
     */
    public static final char DAQ_OVERL_IND_NONE = 0x00;
    /**
     * Overload indication: Overload indication in MSB of PID
     */
    public static final char DAQ_OVERL_IND_MSB = 0x01;
    /**
     * Overload indication: Overload indication by Event Packet
     */
    public static final char DAQ_OVERL_IND_EVT = 0x02;
    /**
     * Overload indication: Not allowed
     */
    public static final char DAQ_OVERL_IND_N_ALLOWED = 0x03;

    // ***** DAQ optimisation types *****
    /**
     * DAQ optimisation type: DEFAULT
     */
    public static final char DAQ_OPT_OM_DEFAULT = 0x00;
    /**
     * DAQ optimisation type: TYPE_16
     */
    public static final char DAQ_OPT_OM_ODT_TYPE_16 = 0x01;
    /**
     * DAQ optimisation type: TYPE_32
     */
    public static final char DAQ_OPT_OM_ODT_TYPE_32 = 0x02;
    /**
     * DAQ optimisation type: TYPE_64
     */
    public static final char DAQ_OPT_OM_ODT_TYPE_64 = 0x03;
    /**
     * DAQ optimisation type: TYPE_ALIGNMENT
     */
    public static final char DAQ_OPT_OM_ODT_TYPE_ALIGNMENT = 0x04;
    /**
     * DAQ optimization type: MAX_ENTRY_SIZE
     */
    public static final char DAQ_OPT_OM_MAX_ENTRY_SIZE = 0x05;
    // ***** TIMEOUT DEFINES *****
    /**
     * Timeout value T1
     */
    public static final char TIMEOUT_T1 = 100;   /// 100 because the controller cant respond faster. used to be 1000.
    /**
     * Timeout value T2
     */
    public static final char TIMEOUT_T2 = 1000;
    /**
     * Timeout value T3
     */
    public static final char TIMEOUT_T3 = 2000;
    /**
     * Timeout value T4
     */
    public static final char TIMEOUT_T4 = 10000;
    /**
     * Timeout value T5
     */
    public static final char TIMEOUT_T5 = 1000;
    /**
     * Timeout value T6
     */
    public static final char TIMEOUT_T6 = 1000;
    /**
     * Timeout value T7
     */
    public static final char TIMEOUT_T7 = 2000;
    // ***** Pre-actions codes *****
    /**
     * Code for pre-action: No pre-action
     */
    public static final char P_ACT_NONE = 0;
    /**
     * Code for pre-action: Wait T7
     *
     * @see XCP.XCP#TIMEOUT_T7
     */
    public static final char P_ACT_WAIT = 1;
    /**
     * Code for pre-action: SYNCH
     *
     * @see XCP.XCP#CC_SYNCH
     */
    public static final char P_ACT_SYNCH = 2;
    /**
     * Code for pre-action: SYNCH + SET_MTA
     *
     * @see XCP.XCP#CC_SYNCH
     * @see XCP.XCP#CC_SET_MTA
     */
    public static final char P_ACT_SYNCH_SET_MTA = 3;
    /**
     * Code for pre-action: SYNCH + DOWNLOAD
     *
     * @see XCP.XCP#CC_SYNCH
     * @see XCP.XCP#CC_DOWNLOAD
     */
    public static final char P_ACT_SYNCH_DOWNLOAD = 4;
    /**
     * Code for pre-action: SYNCH + SET_DAQ_PTR
     *
     * @see XCP.XCP#CC_SYNCH
     * @see XCP.XCP#CC_SET_DAQ_PTR
     */
    public static final char P_ACT_SYNCH_SET_DAQ_PTR = 5;
    /**
     * Code for pre-action: SYNCH + PROGRAM
     *
     * @see XCP.XCP#CC_SYNCH
     * @see XCP.XCP#CC_PROGRAM
     */
    public static final char P_ACT_SYNCH_PROGRAM = 6;
    /**
     * Code for pre-action: GET_SEED
     *
     * @see XCP.XCP#CC_GET_SEED
     */
    public static final char P_ACT_GET_SEED = 7;
    /**
     * Code for pre-action: Unlock slave
     *
     * @see XCP.XCP#CC_UNLOCK
     */
    public static final char P_ACT_UNLOCK = 8;
    /**
     * Code for pre-action: SET_MTA
     *
     * @see XCP.XCP#CC_SET_MTA
     */
    public static final char P_ACT_SET_MTA = 9;
    /**
     * Code for pre-action: UPLOAD + DOWNLOAD
     *
     * @see XCP.XCP#CC_UPLOAD
     * @see XCP.XCP#CC_DOWNLOAD
     */
    public static final char P_ACT_UPLOAD_DOWNLOAD = 10;
    /**
     * Code for pre-action: START_STOP_x
     *
     * @see XCP.XCP#CC_START_STOP_SYNCH
     * @see XCP.XCP#CC_START_STOP_DAQ_LIST
     */
    public static final char P_ACT_START_STOP = 11;
    /**
     * Code for pre-action: Re-init DAQ
     */
    public static final char P_ACT_REINIT = 12;
    // ***** Action codes *****
    /**
     * Code for action: None action
     */
    public static final char ACT_NONE = 0;
    /**
     * Code for action: Repeat infinite times
     */
    public static final char ACT_REPEAT_INF = 1;
    /**
     * Code for action: Repeat 2 times
     */
    public static final char ACT_REPEAT_2 = 2;
    /**
     * Code for action: Restart session
     */
    public static final char ACT_RESTART = 3;
    /**
     * Code for action: Retry other syntax
     */
    public static final char ACT_RETRY_SYNTAX = 4;
    /**
     * Code for action: Retry other parameter
     */
    public static final char ACT_RETRY_PARAMETER = 5;
    /**
     * Code for action: Display error
     */
    public static final char ACT_DISPLAY_ERROR = 6;
    /**
     * Code for action: Use alternative
     */
    public static final char ACT_ALTERNATIVE = 7;
    /**
     * Code for action: Use asap2Data
     */
    public static final char ACT_ASAP2 = 8;
    /**
     * Code for action: New flashware version necessary
     */
    public static final char ACT_FLASH_NEW = 9;
    // ***** Parameter variables for XCP command: GET_SEED *****
    /**
     * GET_SEED mode. (0x00 = (first part of) seed; 0x01 = remaining part of
     * seed)
     */
    private char get_seed_mode = 0x00;
    /**
     * GET_SEED resource. (0x01 = CAL/PAG; 0x04 = DAQ; 0x08 = STIM; 0x10 = PGM)
     */
    private char get_seed_resource = 0x01;
    /**
     * GET_SEED length. Length of the seed returned by the slave device.
     */
    private char get_seed_length = 0x00;
    /**
     * GET_SEED data array. Array of seed databytes, returned by the slave
     * device.
     */
    private char[] get_seed_data = new char[0];
    // ***** Parameter variables for XCP command: SET_MTA *****
    /**
     * MTA address at slave. Last set or active MTA address at slave device.
     */
    private long set_mta_address = 0x00000000;
    /**
     * MTA address extension. Last set or active MTA address extension at slave
     * device.
     */
    private char set_mta_address_ext = 0x00;
    /**
     * MTA increment size. Increment the MTA address with this value when an
     * UPLOAD, DOWNLOAD or BUILD_CHECKSUM command has been executed successfully
     * at the slave device.
     */
    private char set_mta_incr = 0x00;
    // ***** Parameter variables for XCP command: DAQ LISTS *****
    /**
     * Number of the last DAQ list that has been used in a XCP command
     */
    private char daq_ptr_list = 0x00;
    /**
     * Number of the last ODT that has been used in a XCP command
     */
    private char daq_ptr_odt = 0x00;
    /**
     * Number of the last ODT entry that has been used in a XCP command
     */
    private char daq_ptr_entry = 0x00;
    /**
     * Increment the ODT entry pointer with this value when a WRITE_DAQ or
     * READ_DAQ command has been executed successfully at the slave device
     */
    private char daq_ptr_incr = 0x00;
    /**
     * Number of DAQ re-init tries, after an error has occured
     */
    private char daq_reinit_cnt = 0;
    // ***** Respons buffer variables for command: UPLOAD *****
    /**
     * Upload buffer array for data responses of the slave device on UPLOAD
     * commands
     */
    private char[] upload_data = new char[0];
    // ***** Respons buffer variables for command: UPLOAD *****
    /**
     * Upload buffer array for data responses of the slave device on UPLOAD
     * commands
     */
    private char[] upload_data_error = new char[0];
    // ***** Respons buffer variables for command: USER *****
    /**
     * Upload buffer array for data responses of the slave device on USER
     * commands
     */
    private char[] user_data = new char[0];
    /**
     * Index pointer to the upload buffer array element to be filled with data
     * on the next data arrival
     */
    private char upload_ptr = 0x00;
    // ***** Respons buffer variables for command: BUILD_CHECKSUM *****
    /**
     *
     */
    private char checksum_type = 0x00;
    /**
     * Checksum type returned by the slave device
     */
    private char[] checksum_data = new char[0];
    /**
     * Maximum blocksize returned by the slave device, when an out of range
     * error occurred
     */
    private long checksum_blocksize = 0x00;
    // ***** Respons buffer variables for command: START_STOP_DAQ_LIST *****
    /**
     * Identifier of the first ODT that will be send by the slave device after a
     * DAQ list has been started
     */
    private char[] odtFirstPID = new char[0];

    // ***** Respons buffer variables for command: GET_DAQ_CLOCK *****
    /**
     * Value of the clock returned by the slave device after executing the
     * GET_DAQ_CLOCK XCP command
     */
    private long daq_clock = 0x00;
    // ***** Respons buffer variables for command: EVENT *****
    /**
     * Index pointer to the event object array element to be filled with data on
     * the next data arrival on the GET_DAQ_EVENT_INFO XCP command
     */
    private char event_ptr = 0x00;
    // ***** Data object declarations *****
    /**
     * Array of XCPCmd objects, containing all XCP commands
     *
     * @see XCP.XCPCmd
     */
    private XCPCmd[] CMD = new XCPCmd[0];
    /**
     * XCPCmdList object, used for XCP command sequence handling
     *
     * @see XCP.XCPCmdList
     */
    protected XCPCmdList cl = new XCPCmdList();
    /**
     * Array of XCPCmd objects, used for storing event information received from
     * the XCP slave device
     *
     * @see XCP.XCPEvent
     */
    public XCPEvent[] event = new XCPEvent[0];
    /**
     * Array of XCPDaqList objects, used for storing current DAQ list
     * configuration
     *
     * @see XCP.XCPDaqList
     */
    private XCPDaqList[] daqlist = new XCPDaqList[0];
    /**
     * boolean to indicate if the daqlists are running on the ECU or not.
     */
    private boolean daqlistAllStopped = false;
    /**
     * XCPDaqInfo object, used for storing DAQ information returned by the XCP
     * slave device.
     *
     * @see XCP.XCPDaqInfo
     */
    private XCPDaqInfo daqinfo = new XCPDaqInfo();
    /**
     * XCPInfo object, used for storing common XCP information returned by the
     * XCP slave device
     *
     * @see XCP.XCPInfo
     */
    private XCPInfo info = new XCPInfo();
    /**
     * SeedKeyNative object, used as an interface to make calls to the DLL file
     * that calculates the correct key from a given seed and resource.
     *
     * @see XCP.SeedKeyNative
     */
    private SeedKeyNative sk = null;
    // ***** Communication history *****
    /**
     * Command code of the last send command
     */
    private char last_cmd = 0;
    /**
     * Daqlist id of the last daqlist
     */
    private char last_daqlist = 0;
    /**
     * Pre-action code of the last executed pre-action
     */
    private char last_pre_action = 0;
    /**
     * Action code of the last executed action
     */
    private char last_action = 0;
    /**
     * Error code of the last error that has occurred
     */
    private char last_error = 0;
    // ***** Timer declarations *****
    /**
     * Timestamp value of the moment that a timeout will occur when the slave
     * device hasn't send a response
     */
    private int timer_wait = 0;
    // ***** Common declarations *****
    /**
     * Indicates whether a connection is active
     */
    protected boolean connected = false;
    /**
     * Indicates whether connection has been finalized
     */
    protected boolean connectionFinalized = false;
    /**
     * Flag to indicate whether there is a XCP connection to the slave device
     */
    private boolean XCP_connected = false;
    /**
     * Flag to indicate whether the master is waiting for a XCP command response
     */
    public boolean XCP_waiting = false;
    /**
     * Whether or not Universal CAN data should be processed
     */
    private boolean processCanData = true;
    /**
     * Flag to indicate whether the last command has to be repeated
     */
    private boolean repeat = false;

    // TODO for testing only, set to false when finished
    /**
     * Flag to indicate whether debug messages have to be printed to the command
     * line
     */
    //todo SET to false when finished
    public static final boolean DEBUG_PRINT = false;
    // ***** Error declarations *****
    /**
     * Indicates whether an error has occurred
     */
    private boolean error = false;

    /**
     * Binary semaphore for obtaining mutual exclusive access to memory read/
     * write functionality on the target. This is typically done in a XCP
     * command sequence that starts with SET_MTA followed by one or more UPLOAD
     * or DOWNLOAD commands. Such a sequency should always complete first before
     * being interrupted by another sequency for accessing memory.
     * <p>
     * A memory access XCP sequence is only allowed to be started if this
     * semaphore is true. When false, the caller needs to either abort or wait
     * until the semaphore becomes true again.
     */
    private volatile boolean memoryAccessSemaphore = true;

    /**
     * Contains the error message, when an error has occurred and error is set
     * to true
     *
     * @see XCP.XCP#error
     */
    private String error_msg = "";
    protected final HANtune hantune = HANtune.getInstance();
    private char CC_ERR_SET_CMD = 0xF1;

    /**
     * Constructor that initializes the XCP Protocol Layer and starts the
     * background periodic method.
     */
    public XCP() {
        super("XCP Thread");
        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
        init();
        this.setPriority(Thread.MAX_PRIORITY);
    }

    /**
     * Sets error flag active and stores the error message
     *
     * @param error_msg Contains the error message
     * @param error Indicates whether an error has occurred
     * @see XCP.XCP#getErrorMsg()
     */
    public void setError(String error_msg, boolean error) {
        this.error = error;
        this.error_msg = error_msg;
    }

    /**
     * Resets error flag and empties the error message
     */
    public void resetError() {
        error = false;
        error_msg = "";
    }

    /**
     * Returns the last occurred error message
     *
     * @return error message
     */
    public String getErrorMsg() {
        return error_msg;
    }

    /**
     * True if a number of CC_ALLOC_ODT_ENTRY commands could not be finished due
     * to memory limits at target side
     *
     * @return true when limited,
     */
    public boolean isNumberOfDaqListItemsRestricted() {
        return targetDaqlistSizeLimit.isTargetSizeLimited();
    }

    /**
     * Holds number of DaqList items that can be safely configured on current
     * target
     *
     * @return number of items, only valid when
     * isNumberOfDaqListItemsRestricted() returned true;
     */
    public int getRestrictedDaqListSize() {
        return targetDaqlistSizeLimit.getLimitedSize();
    }

    /**
     * Initializes the XCP Protocol Layer. All commands are added into the
     * XCPCmd array.
     */
    private void init() {
        // init command settings
        CMD = new XCPCmd[0xFFF - XCPInfo.OS + 1];

        // command definition format
        // CMD( name, timeout, command_len, respons_len )
        // STD - Standard Commands
        CMD[CC_CONNECT - XCPInfo.OS] = new XCPCmd("CONNECT", TIMEOUT_T1, (char) 2, (char) 8, (char) 0x00);
        CMD[CC_DISCONNECT - XCPInfo.OS] = new XCPCmd("DISCONNECT", TIMEOUT_T1, (char) 1, (char) 1, (char) 0x00);
        CMD[CC_GET_STATUS - XCPInfo.OS] = new XCPCmd("GET_STATUS", TIMEOUT_T1, (char) 1, (char) 6, (char) 0x00);
        CMD[CC_SYNCH - XCPInfo.OS] = new XCPCmd("SYNCH", TIMEOUT_T1, (char) 1, (char) 2, (char) 0x00);
        CMD[CC_GET_COMM_MODE_INFO - XCPInfo.OS]
                = new XCPCmd("GET_COMM_MODE_INFO", TIMEOUT_T1, (char) 1, (char) 8, (char) 0x00);
        CMD[CC_GET_ID - XCPInfo.OS] = new XCPCmd("GET_ID", TIMEOUT_T1, (char) 2, (char) 8, (char) 0x00);
        CMD[CC_SET_REQUEST - XCPInfo.OS] = new XCPCmd("SET_REQUEST", TIMEOUT_T1, (char) 4, (char) 1, (char) 0x00);
        CMD[CC_GET_SEED - XCPInfo.OS] = new XCPCmd("GET_SEED", TIMEOUT_T1, (char) 3, (char) 2, (char) 0x00);
        CMD[CC_UNLOCK - XCPInfo.OS] = new XCPCmd("UNLOCK", TIMEOUT_T1, (char) 2, (char) 2, (char) 0x00);
        CMD[CC_SET_MTA - XCPInfo.OS] = new XCPCmd("SET_MTA", TIMEOUT_T1, (char) 8, (char) 1, (char) 0x00);
        CMD[CC_UPLOAD - XCPInfo.OS] = new XCPCmd("UPLOAD", TIMEOUT_T1, (char) 2, (char) 1, (char) 0x00);
        CMD[CC_SHORT_UPLOAD - XCPInfo.OS] = new XCPCmd("SHORT_UPLOAD", TIMEOUT_T1, (char) 8, (char) 1, (char) 0x00);
        CMD[CC_BUILD_CHECKSUM - XCPInfo.OS] = new XCPCmd("BUILD_CHECKSUM", TIMEOUT_T2, (char) 8, (char) 8, (char) 0x00);

        CMD[CC_USER_CC - XCPInfo.OS] = new XCPCmd("USER_COMMAND", TIMEOUT_T1, (char) 8, (char) 1, (char) 0x00);

        // CAL - Calibration Commands
        CMD[CC_DOWNLOAD - XCPInfo.OS] = new XCPCmd("DOWNLOAD", TIMEOUT_T1, (char) 2, (char) 1, (char) 0x01);
        CMD[CC_DOWNLOAD_NEXT - XCPInfo.OS] = new XCPCmd("DOWNLOAD_NEXT", TIMEOUT_T1, (char) 2, (char) 1, (char) 0x01);
        CMD[CC_DOWNLOAD_MAX - XCPInfo.OS] = new XCPCmd("DOWNLOAD_MAX", TIMEOUT_T1, (char) 1, (char) 1, (char) 0x01);
        CMD[CC_SHORT_DOWNLOAD - XCPInfo.OS] = new XCPCmd("SHORT_DOWNLOAD", TIMEOUT_T1, (char) 8, (char) 1, (char) 0x01);
        CMD[CC_MODIFY_BITS - XCPInfo.OS] = new XCPCmd("MODIFY_BITS", TIMEOUT_T1, (char) 6, (char) 1, (char) 0x01);

        // PAG - Page Switching Commands
        CMD[CC_SET_CAL_PAGE - XCPInfo.OS] = new XCPCmd("SET_CAL_PAGE", TIMEOUT_T1, (char) 4, (char) 1, (char) 0x01);
        CMD[CC_GET_CAL_PAGE - XCPInfo.OS] = new XCPCmd("GET_CAL_PAGE", TIMEOUT_T1, (char) 3, (char) 4, (char) 0x01);
        CMD[CC_GET_PAG_PROCESSOR_INFO - XCPInfo.OS]
                = new XCPCmd("GET_PAG_PROCESSOR_INFO", TIMEOUT_T1, (char) 1, (char) 3, (char) 0x01);
        CMD[CC_GET_SEGMENT_INFO - XCPInfo.OS]
                = new XCPCmd("GET_SEGMENT_INFO", TIMEOUT_T1, (char) 5, (char) 8, (char) 0x01);
        CMD[CC_GET_PAGE_INFO - XCPInfo.OS] = new XCPCmd("GET_PAGE_INFO", TIMEOUT_T1, (char) 4, (char) 3, (char) 0x01);
        CMD[CC_SET_SEGMENT_MODE - XCPInfo.OS]
                = new XCPCmd("SET_SEGMENT_MODE", TIMEOUT_T1, (char) 3, (char) 1, (char) 0x01);
        CMD[CC_GET_SEGMENT_MODE - XCPInfo.OS]
                = new XCPCmd("GET_SEGMENT_MODE", TIMEOUT_T1, (char) 3, (char) 3, (char) 0x01);
        CMD[CC_COPY_CAL_PAGE - XCPInfo.OS] = new XCPCmd("COPY_CAL_PAGE", TIMEOUT_T1, (char) 5, (char) 1, (char) 0x01);

        // DAQ - Data Acquisition Commands
        CMD[CC_CLEAR_DAQ_LIST - XCPInfo.OS] = new XCPCmd("CLEAR_DAQ_LIST", TIMEOUT_T1, (char) 4, (char) 1, (char) 0x04);
        CMD[CC_SET_DAQ_PTR - XCPInfo.OS] = new XCPCmd("SET_DAQ_PTR", TIMEOUT_T1, (char) 6, (char) 1, (char) 0x04);
        CMD[CC_WRITE_DAQ - XCPInfo.OS] = new XCPCmd("WRITE_DAQ", TIMEOUT_T1, (char) 8, (char) 1, (char) 0x04);
        CMD[CC_SET_DAQ_LIST_MODE - XCPInfo.OS]
                = new XCPCmd("SET_DAQ_LIST_MODE", TIMEOUT_T1, (char) 8, (char) 1, (char) 0x04);
        CMD[CC_GET_DAQ_LIST_MODE - XCPInfo.OS]
                = new XCPCmd("GET_DAQ_LIST_MODE", TIMEOUT_T1, (char) 4, (char) 8, (char) 0x04);
        CMD[CC_START_STOP_DAQ_LIST - XCPInfo.OS]
                = new XCPCmd("START_STOP_DAQ_LIST", TIMEOUT_T1, (char) 4, (char) 2, (char) 0x04);
        CMD[CC_START_STOP_SYNCH - XCPInfo.OS]
                = new XCPCmd("START_STOP_SYNCH", TIMEOUT_T1, (char) 2, (char) 1, (char) 0x04);
        CMD[CC_GET_DAQ_CLOCK - XCPInfo.OS] = new XCPCmd("GET_DAQ_CLOCK", TIMEOUT_T1, (char) 1, (char) 8, (char) 0x04);
        CMD[CC_READ_DAQ - XCPInfo.OS] = new XCPCmd("READ_DAQ", TIMEOUT_T1, (char) 1, (char) 8, (char) 0x04);
        CMD[CC_GET_DAQ_PROCESSOR_INFO - XCPInfo.OS]
                = new XCPCmd("GET_DAQ_PROCESSOR_INFO", TIMEOUT_T1, (char) 1, (char) 8, (char) 0x04);
        CMD[CC_GET_DAQ_RESOLUTION_INFO - XCPInfo.OS]
                = new XCPCmd("GET_DAQ_RESOLUTION_INFO", TIMEOUT_T1, (char) 1, (char) 8, (char) 0x04);
        CMD[CC_GET_DAQ_LIST_INFO - XCPInfo.OS]
                = new XCPCmd("GET_DAQ_LIST_INFO", TIMEOUT_T1, (char) 4, (char) 6, (char) 0x04);
        CMD[CC_GET_DAQ_EVENT_INFO - XCPInfo.OS]
                = new XCPCmd("GET_DAQ_EVENT_INFO", TIMEOUT_T1, (char) 4, (char) 7, (char) 0x04);
        CMD[CC_FREE_DAQ - XCPInfo.OS] = new XCPCmd("FREE_DAQ", TIMEOUT_T1, (char) 1, (char) 1, (char) 0x04);
        CMD[CC_ALLOC_DAQ - XCPInfo.OS] = new XCPCmd("ALLOC_DAQ", TIMEOUT_T1, (char) 4, (char) 1, (char) 0x04);
        CMD[CC_ALLOC_ODT - XCPInfo.OS] = new XCPCmd("ALLOC_ODT", TIMEOUT_T1, (char) 5, (char) 1, (char) 0x04);
        CMD[CC_ALLOC_ODT_ENTRY - XCPInfo.OS]
                = new XCPCmd("ALLOC_ODT_ENTRY", TIMEOUT_T1, (char) 6, (char) 1, (char) 0x04);

        // PGM - Programming Commands
        CMD[CC_PROGRAM_START - XCPInfo.OS] = new XCPCmd("PROGRAM_START", TIMEOUT_T3, (char) 1, (char) 7, (char) 0x10);
        CMD[CC_PROGRAM_CLEAR - XCPInfo.OS] = new XCPCmd("PROGRAM_CLEAR", TIMEOUT_T4, (char) 8, (char) 1, (char) 0x10);
        CMD[CC_PROGRAM - XCPInfo.OS] = new XCPCmd("PROGRAM", TIMEOUT_T5, (char) 2, (char) 1, (char) 0x10);
        CMD[CC_PROGRAM_RESET - XCPInfo.OS] = new XCPCmd("PROGRAM_RESET", TIMEOUT_T5, (char) 1, (char) 1, (char) 0x10);
        CMD[CC_GET_PGM_PROCESSOR_INFO - XCPInfo.OS]
                = new XCPCmd("GET_PGM_PROCESSOR_INFO", TIMEOUT_T1, (char) 1, (char) 3, (char) 0x10);
        CMD[CC_GET_SECTOR_INFO - XCPInfo.OS]
                = new XCPCmd("GET_SECTOR_INFO", TIMEOUT_T1, (char) 3, (char) 8, (char) 0x10);
        CMD[CC_PROGRAM_PREPARE - XCPInfo.OS]
                = new XCPCmd("PROGRAM_PREPARE", TIMEOUT_T3, (char) 4, (char) 1, (char) 0x10);
        CMD[CC_PROGRAM_FORMAT - XCPInfo.OS] = new XCPCmd("PROGRAM_FORMAT", TIMEOUT_T1, (char) 5, (char) 1, (char) 0x10);
        CMD[CC_PROGRAM_NEXT - XCPInfo.OS] = new XCPCmd("PROGRAM_NEXT", TIMEOUT_T1, (char) 2, (char) 3, (char) 0x10);
        CMD[CC_PROGRAM_MAX - XCPInfo.OS] = new XCPCmd("PROGRAM_MAX", TIMEOUT_T5, (char) 1, (char) 1, (char) 0x10);
        CMD[CC_PROGRAM_VERIFY - XCPInfo.OS] = new XCPCmd("PROGRAM_VERIFY", TIMEOUT_T3, (char) 8, (char) 1, (char) 0x10);
    }

    /**
     * Makes a connection to the XCP slave device. This method has to be
     * implemented in the XCP Transport Layer
     *
     * @return whether this method has been executed successfully
     */
    protected boolean connect() {
        return true;
    }

    /**
     * Closes a connection to the XCP slave device. This method has to be
     * implemented in the XCP Transport Layer
     *
     * @return whether this method has been executed successfully
     */
    protected boolean disconnect() {
        return true;
    }

    /**
     * Returns whether the master is connected to the slave device
     *
     * @return whether the master is connected to the slave device
     */
    public boolean isConnected() {
        return connected;
    }

    public boolean shouldProcessCanData() {
        return processCanData;
    }

    public void setProcessCanData(boolean processCanData) {
        this.processCanData = processCanData;
    }

    /**
     * Sends an array of databytes to the XCP slave device. This method has to
     * be implemented in the XCP Transport Layer
     *
     * @param XCPdata
     * @return whether the data has been send succesfully
     * @throws Exception If sending data to the XCP slave is not possible.
     */
    protected boolean sendData(char[] XCPdata) throws Exception {

        return true;
    }

    // ***** XCP functions *****
    /**
     * Starts processing of XCP command list and responses
     */
    public void startXCP() {
        resetError();
        cl.start();
    }

    /**
     * Stops processing of XCP command list and responses
     */
    public void stopXCP() {
        cl.stop();
    }

    /**
     * Do nothing, wait an amount of nanoseconds
     */
    public void doNothing() {
        try {
            Thread.sleep(0, 100000);
        } catch (InterruptedException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            Thread.currentThread().interrupt();  // set interrupt flag

        } catch (Exception e) {
            Thread.currentThread().interrupt();  // set interrupt flag
            AppendToLogfile.appendError(Thread.currentThread(), e);
        }
    }

    /**
     * Starts a new XCP session with the XCP slave device
     *
     * @return whether this command was executed successfully
     */
    public boolean startSession() {
        try {
            // make new connection using the Transport Layer, or return with error
            if (!connect()) {
                cl.exit();
                return false;
            }
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            HANtune.getInstance().connectionDialog.showError(e.getMessage());

        }

        // make XCP connection
        xcpCONNECT();

        // start processing and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        if (error) {
            return false;
        }

        if (DEBUG_PRINT) {
            System.out.println("startSession ready (" + !error + ")");
        }
        return !error;
    }

    /**
     * Stops current active session with XCP slave device
     *
     * @return whether this command was executed successfully
     */
    public boolean stopSession() {
        resetError();
        if (XCP_connected) {
            // stop processing of current command list and send disconnect command
            cl.exit();
            xcpDISCONNECT();

            // stop all DAQ lists when not protected
            if (!info.protected_daq) {
                daqlistAllStopped = true;
                xcpSTART_STOP_SYNCH((char) 0x00);
            }

            // start new command list and wait until completed
            startXCP();
            while (!cl.isSequenceCompleted()) {
                doNothing();
            }

            XCP_connected = false;
        }

        isXCPRunning = false;

        // disconnect using Transport Layer
        if (!disconnect()) {
            cl.exit();
            return false;
        }
        if (DEBUG_PRINT) {
            System.out.println("stopSession ready (" + !error + ")");
        }
        return !error;
    }

    /**
     * Resets current session.
     *
     * @return whether this command was executed successfully
     */
    public boolean resetSession() {
        // send disconnect command
        xcpDISCONNECT();

        // start new command list and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        // start new session
        startSession();

        return !error;
    }

    /**
     * Returns all common slave device information
     *
     * @return information object containing all common slave device information
     */
    public XCPInfo requestInfo() {
        // get slave identification
        xcpGET_ID();

        // get communication information when supported
        if (info.comm_info_optional) {
            xcpGET_COMM_MODE_INFO();
        }

        // get status information
        xcpGET_STATUS();

        // start new command list and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        if (DEBUG_PRINT) {
            System.out.println("getInfo ready (" + !error + ")");
        }
        if (error) {
            return null;
        }

        // return gathered information in an information object
        return info;
    }

    public XCPInfo getInfo() {
        return info;
    }

    /**
     * Configures the XCP slave device
     *
     * @return whether this command was executed successfully
     */
    public boolean configure() {
        // TODO:
        // SET_REQUEST : set configuration id

        // start new command list and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        if (DEBUG_PRINT) {
            System.out.println("configure ready (" + !error + ")");
        }

        return !error;
    }

    // ***** CALIBRATION functions *****
    /**
     * Sets value at specified memory address on the XCP slave device
     *
     * @param address memory address
     * @param address_ext memory address extension
     * @param data array of databytes
     * @return whether this command was executed successfully
     */
    public boolean setParameter(long address, char address_ext, char[] data) {
        if (data.length == 0) {
            return false;
        }

        while (!cl.isSequenceCompleted() || XCP_waiting) {
            doNothing();
        }

        // wait for availability of the semaphore
        while (!memoryAccessSemaphore) {
            try {
                Thread.sleep(0, 100000);
            } catch (InterruptedException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                Thread.currentThread().interrupt();  // set interrupt flag
                break;
            }

        }
        // take the semaphore to lock in mutual exclusive access
        memoryAccessSemaphore = false;

        // calculate how many DOWNLOAD commands are needed to set the parameter
        char part_max = (char) (info.MAX_CTO - 2);
        char parts = (char) ((data.length + part_max - 1) / part_max);
        int ptr = 0;

        // split data into multiple DOWNLOAD commands
        char[] part;
        int i, j;
        for (i = parts - 1; i >= 0; i--) {
            if (i == 0 && (data.length % part_max) != 0) {
                part = new char[data.length % part_max];
            } else {
                part = new char[part_max];
            }
            for (j = 0; j < part.length; j++) {
                part[j] = data[j + ptr];
            }
            ptr += part.length;
            xcpDOWNLOAD(part);
        }

        // set Memory Transfer Address
        xcpSET_MTA(address, address_ext);
        // start new command list and wait until completed
        startXCP();

        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        // release semaphore to give others a chance to obtain mutual
        // exclusive access.
        memoryAccessSemaphore = true;

        if (DEBUG_PRINT) {
            System.out.println("setParameter ready (" + !error + ")");
        }
        return !error;
    }

    /**
     * Gets value of an item at specified memory address on the XCP slave device
     *
     * @param address memory address
     * @param address_ext memory address extension
     * @param size number of bytes to get
     * @return whether this command was executed successfully
     */
    public char[] getItem(long address, char address_ext, int size) {
        while (cl.isActive()) {
            //make sure HANtune is done sending any parameters
            doNothing();
        }

        if (size == 0) {
            return null;
        }

        // calculate how many UPLOAD respons messages are needed to receive the data
        char part_max = (char) (info.MAX_CTO - 1);
        char parts = (char) ((size + part_max - 1) / part_max);

        if (size <= part_max) {
            // use SHORT_UPLOAD
            // request data using one signle SHORT_UPLOAD command
            xcpSHORT_UPLOAD((char) size, address, address_ext);
        } else {
            // use SET_MTA & UPLOAD
            // request data using multiple UPLOAD commands
            for (char i = 0; i < parts; i++) {
                if (i == 0 && (size % part_max) != 0) {
                    xcpUPLOAD((char) (size % part_max));
                } else {
                    xcpUPLOAD(part_max);
                }
            }
            // set Memory Transfer Address
            xcpSET_MTA(address, address_ext);
        }

        upload_data = new char[size];
        upload_ptr = 0;

        // start new command list and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        if (DEBUG_PRINT) {
            System.out.println("getItem ready (" + !error + ")");
        }
        if (error) {
            return null;
        }

        if (info.byte_order == 0x00) {
            upload_data = Util.reverseArray(upload_data);
        }
        return upload_data;
    }

    /**
     * Returns checksum
     *
     * @return checksum array
     */
    public char[] getChecksum() {
        // TODO

        // start new command list and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        if (DEBUG_PRINT) {
            System.out.println("getChecksum ready (" + !error + ")");
        }
        if (error) {
            return null;
        }

        return checksum_data;
    }

    // ***** DAQ-LIST functions *****
    /**
     * Prepares DAQ list configuration into command list sequence
     *
     * @param daqlist DAQ list configuration
     */
    private void setDL() {
        char i, j, k;

        // reset array of first absolute ODT identifier of each DAQ list
        odtFirstPID = new char[daqlist.length];

        // set mode for daq lists
        for (i = 0; i < daqlist.length; i++) {
            xcpSET_DAQ_LIST_MODE(
                    daqlist[i].getDirection(),
                    daqlist[i].getTimestamp(),
                    daqlist[i].getPidOff(),
                    i,
                    daqlist[i].getEventNo(),
                    (char) daqlist[i].getPrescaler(),
                    daqlist[i].getPriority());
        }

        // daq list odt entries
        for (i = 0; i < daqlist.length; i++) {
            for (j = 0; j < daqlist[i].odt.length; j++) {
                for (k = 0; k < daqlist[i].odt[j].odt_entry.length; k++) {
                    // write element in odt entry
                    xcpWRITE_DAQ(
                            daqlist[i].odt[j].odt_entry[k].getSize(),
                            daqlist[i].odt[j].odt_entry[k].getAddress(),
                            daqlist[i].odt[j].odt_entry[k].getAddressExt());

                    // set daq list pointer
                    xcpSET_DAQ_PTR(i, j, k);
                }
            }
        }

        // allocate daq list odt entries
        for (i = 0; i < daqlist.length; i++) {
            for (j = 0; j < daqlist[i].odt.length; j++) {
                xcpALLOC_ODT_ENTRY(i, j, (char) daqlist[i].odt[j].odt_entry.length);
            }
        }

        // allocate daq list object descriptor tables (odt)
        for (i = 0; i < daqlist.length; i++) {
            xcpALLOC_ODT(i, (char) daqlist[i].odt.length);
        }

        // allocate daq lists
        xcpALLOC_DAQ((char) daqlist.length);

        // clear daq configuration
        xcpFREE_DAQ();

        // stop all DAQ lists
        daqlistAllStopped = true;
        xcpSTART_STOP_SYNCH((char) 0x00);

        startXCP();
    }

    /**
     * Sets DAQ list configuration at XCP slave device
     *
     * @param dLs DAQ list configuration
     * @return whether this command was executed successfully
     */
    public boolean setDaqLists(XCPDaqList[] dLs) {
        // reset completed flag
        cl.setSequenceCompleted(false);
        targetDaqlistSizeLimit.resetSuccesfulCommandCounter();

        // clone daq list
        daqProcessPause = true;
        daqlist = dLs.clone();

        // prepare DAQ list configuration
        setDL();

        // wait until completed
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        hasDAQList = true;
        daqProcessPause = false;

        if (DEBUG_PRINT) {
            System.out.println("setDaqLists ready (" + !error + ")");
        }

        if (!error) {
            hantune.daqListReceivedUpdate(0);
        }

        return !error;
    }

    /**
     * Returns current DAQ list configuration at XCP slave device
     *
     * @return DAQ list configuration object
     */
    public XCPDaqList[] getDaqLists() {
        // get daqlist configuration from slave
        // not implemented yet
        return daqlist;
    }

    /**
     * returns true if all daqlists have stopped
     *
     * @return daqlistAllStopped
     */
    public boolean getDaqlistAllStopped() {
        return daqlistAllStopped;
    }

    /**
     * Modifies prescaler of specified DAQ list
     *
     * @param daq_id id of daq to be altered
     * @param ps new prescaler to be set
     * @return true if there is an error
     */
    public boolean setPrescaler(char daq_id, char ps) {
        // does daqlist exist yet?
        if (daq_id > (daqlist.length - 1)) {
            setError("Specified DAQ list does not exist", true);
            return false;
        }

        last_daqlist = daq_id;

        // set prescaler
        daqlist[daq_id].setPrescaler(ps);

        // start DAQ-list
        daqlistAllStopped = false;
        xcpSTART_STOP_DAQ_LIST((char) 0x01, daq_id);

        // modify prescaler of DAQ-list
        xcpSET_DAQ_LIST_MODE(
                daqlist[daq_id].getDirection(),
                daqlist[daq_id].getTimestamp(),
                daqlist[daq_id].getPidOff(),
                daq_id,
                daqlist[daq_id].getEventNo(),
                (char) daqlist[daq_id].getPrescaler(),
                daqlist[daq_id].getPriority());

        // stop DAQ-list
        xcpSTART_STOP_DAQ_LIST((char) 0x00, daq_id);

        // start and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        if (DEBUG_PRINT) {
            System.out.println("setPrescaler ready (" + !error + ")");
        }

        return !error;
    }

    /**
     * Returns DAQ information from XCP slave device
     *
     * @return DAQ information object
     */
    public XCPDaqInfo getDaqInfo() {
        // get information
        xcpGET_DAQ_PROCESSOR_INFO();
        xcpGET_DAQ_RESOLUTION_INFO();

        // start new command list and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        if (DEBUG_PRINT) {
            System.out.println("getDaqInfo ready (" + !error + ")");
        }
        if (error) {
            return null;
        }

        return daqinfo;
    }

    /**
     * Returns DAQ information from XCP slave device, as stored in the object
     *
     * @return DAQ information object
     */
    public XCPDaqInfo getDaqInfoObject() {
        return daqinfo;
    }

    /**
     * Gets available event information at the XCP slave device
     *
     * @return event information object array
     */
    public XCPEvent[] getEvents() {
        // start information gathering using the DAQ processor info
        xcpGET_DAQ_PROCESSOR_INFO();

        // start new command list and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        if (DEBUG_PRINT) {
            System.out.println("getEvents ready (" + !error + ")");
        }
        if (error) {
            return null;
        }

        return event;
    }

    /**
     * Gets DAQ clock value from XCP slave device
     *
     * @return clock value
     */
    public long getDaqClock() {
        xcpGET_DAQ_CLOCK();

        // start new command list and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        if (DEBUG_PRINT) {
            System.out.println("getDaqClock ready (" + !error + ")");
        }
        if (error) {
            return 0;
        }

        return daq_clock;
    }

    /**
     * Toggles DAQ list status
     *
     * @param mode mode (0=stop all; 1=start list; 2=stop list)
     * @param listNumbers DAQ list numbers
     * @return whether this command was executed successfully
     */
    public boolean toggleDaq(char mode, char[] listNumbers) {
        char i;
        switch (mode) {
            case 0:
                // stop all daq lists
                daqlistAllStopped = true;
                xcpSTART_STOP_SYNCH((char) 0x00);
                break;
            case 1:
                daqlistAllStopped = false;
                for (i = 0; i < listNumbers.length; i++) {
                    last_daqlist = i;
                    // start daq list
                    xcpSTART_STOP_DAQ_LIST((char) 0x01, listNumbers[i]);
                    // start new command list and wait until completed
                    startXCP();
                    while (!cl.isSequenceCompleted()) {
                        doNothing();
                    }
                }
                break;
            case 2:
                daqlistAllStopped = false;
                for (i = 0; i < listNumbers.length; i++) {
                    // stop daq list
                    xcpSTART_STOP_DAQ_LIST((char) 0x00, listNumbers[i]);
                    // start new command list and wait until completed
                    startXCP();
                    while (!cl.isSequenceCompleted()) {
                        doNothing();
                    }
                }
                break;
        }

        // start new command list and wait until completed
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }

        if (DEBUG_PRINT) {
            System.out.println("toggleDaq ready (" + !error + ")");
        }

        return !error;
    }

    // ***** XCP response handling functions *****
    /**
     * Receives and processes new XCP message data
     *
     * @param XCPdata message data array
     * @param timestampMaster timestamp at the moment of message arrival at the
     * slave in microseconds
     */
    public void receiveData(char[] XCPdata, long timestampMaster) {
        // stop XCP processing
        stopXCP();

        // check message length
        if (XCPdata.length < 1) {
            setError("Invalid XCP response message: unexpected length", true);
            cl.exit();
            return;
        }

        // PROCESS RESPONSE
        if (XCPdata[0] == PID_RES) {
            while (XCP_sending) {
                doNothing();
            }
            xcpTimeOutTimer.setTimeInMiliseconds(0);
            XCP_waiting = false;
            XCP_isProcessing = true;
            processResponse(XCPdata);
            XCP_isProcessing = false;
        }

        // EVENT HANDLING?
        if (XCPdata[0] == PID_EV) {
            processEvent(XCPdata);
        }

        // DAQ message?
        if (XCPdata[0] <= 0xFB) {
            if (daqlist.length == 0) {
                //Band-aid fix
                return;
            }
            processDaq(XCPdata, timestampMaster);
        }

        // SERVICE HANDLING?
        if (XCPdata[0] == PID_SERV) {
            processService(XCPdata);
        }

        // ERROR HANDLING?
        if (XCPdata[0] == PID_ERR) {
            processError(XCPdata);
        }
        // start XCP processing again
        cl.start();

    }

    /**
     * Processes a DAQ list message
     *
     * @param xcpData message data array
     * @param timestampMaster timestamp of message arrival
     */
    private void processDaq(char[] xcpData, long timestampMaster) {
        // return if no data present or paused
        if (daqProcessPause || xcpData.length <= 1) {
            return;
        }

        int daqId = determineDaqListId(xcpData);

        // determine table identifier
        int odtId = determineTableIdentifier(xcpData[0], daqId);

        // extract timestamp of slave, use timestamp of master by default
        long timestamp = determineTimestamp(xcpData, odtId, timestampMaster);
        // determine actual data bytes
        char[] dataSet = determineDataBytes(xcpData, odtId);

        processDaqItems(dataSet, daqId, odtId, timestamp);
//        LogDataWriterCsv.getInstance().checkLogSize();
    }

    /**
     * When true, a DAQ message holds a PID plus DAQ list number. Therefore more
     * than 251 signals are allowed when spread over multiple DAQ lists
     *
     * @return true if XCP header contains DAQ list number
     */
    public boolean isDaqlistNumberInXcpHdr() {
        return daqinfo.daqIdFldType == XCPDaqInfo.DaqIdFieldType.DAQ_HDR_ODT_DAQB;
    }

    private void processDaqItems(char[] daqListData, int daqId, int odtId, long timestamp) {
        // determine number of items in odt
        int entryCnt;
        XCPDaqList.XCPOdt odt;
        if (daqlist.length > daqId && daqlist[daqId].odt.length > odtId) {
            odt = daqlist[daqId].odt[odtId];
            entryCnt = odt.getOdtEntryCnt();
        } else {
            return;
        }

        // loop through items
        int dataIdx = 0;
        for (int entryIdx = 0; entryIdx < entryCnt; entryIdx++) {
            int size = odt.odt_entry[entryIdx].getSize();
            char[] itemData = new char[size];

            System.arraycopy(daqListData, dataIdx, itemData, 0, size);

            // process odt entry
            processSingleDaqItem(itemData, timestamp, daqId, odtId, entryIdx);

            dataIdx += size;
        }
    }

    private int determineDaqListId(char[] xcpData) {
        if (isDaqlistNumberInXcpHdr()) {
            return xcpData[1];
        } else {
            // derive daqId from array of firstPID's. Index of array is daqId
            for (char icnt = 0; icnt < odtFirstPID.length; icnt++) {
                if (xcpData[0] >= odtFirstPID[icnt]) {
                    return icnt;
                }
            }
        }
        return 0;
    }

    private int determineTableIdentifier(int odt, int daqId) {
        if (isDaqlistNumberInXcpHdr()) {
            return odt;
        } else {
            return (char) (odt - odtFirstPID[daqId]);
        }
    }

    private char[] determineDataBytes(char[] xcpData, int odt) {
        int offset = isDaqlistNumberInXcpHdr() ? 2 : 1;
        if (useSlaveTimestamp && odt == 0) {
            offset += daqinfo.timestamp_size;
        }
        return Arrays.copyOfRange(xcpData, offset, xcpData.length);
    }

    private long determineTimestamp(char[] XCPdata, int odtId, long masterTime) {
        long timestampSlave = 0; // final results is in microseconds

        // extract timestamp from databytes
        if (useSlaveTimestamp && odtId == 0) {
            long timestampRaw;
            switch (daqinfo.timestamp_size) {
                case 1:
                    timestampRaw = XCPdata[1];
                    break;
                case 2:
                    if (info.byte_order == 0x00) {
                        timestampRaw = XCPdata[1] + (XCPdata[2] << 8);
                    } else {
                        timestampRaw = (XCPdata[1] << 8) + XCPdata[2];
                    }
                    break;
                case 4:
                    if (info.byte_order == 0x00) {
                        timestampRaw = XCPdata[1] + (XCPdata[2] << 8) + (XCPdata[3] << 16) + (XCPdata[4] << 24);
                    } else {
                        timestampRaw = (XCPdata[1] << 24) + (XCPdata[2] << 16) + (XCPdata[3] << 8) + XCPdata[4];
                    }
                    break;
                default:
                    timestampRaw = 0;
                    break;
            }

            // overflow of timestamp at slave?
            if (timestampRaw < timestampRawPrevious) {
                timestampRawOverflow++;
            }
            timestampRawPrevious = timestampRaw;

            // convert by using the timestamp unit information
            timestampSlave = (timestampRaw + timestampRawOverflow * (long) Math.pow(2, 8 * daqinfo.timestamp_size))
                    * daqinfo.timestamp_step;
        }

        return timestampSlave > 0 ? timestampSlave : masterTime;
    }

    /**
     * Processes a DAQ list message element
     *
     * @param XCPdata message data array
     * @param timestamp timestamp of message arrival
     */
    private void processSingleDaqItem(char[] data, long timestamp, int daqId, int odtId, int entryId) {
        // reverse the bytes if necessary
        if (info.byte_order == 0x00) {
            data = Util.reverseArray(data);
        }

        // determine the relative id of the element in the DAQ list
        char relativeId = daqlist[daqId].odt[odtId].odt_entry[entryId].getRelativeId();

        // valid id?
        if (relativeId < daqlist[daqId].getEntryCnt()) {
            // flags
            boolean firstSignal = relativeId == 0;
            boolean lastSignal = relativeId == daqlist[daqId].getEntryCnt() - 1;

            // retrieve name of the asap2 signal and convert the data to a value
            String asap2_name;
            asap2_name = daqlist[daqId].odt[odtId].odt_entry[entryId].getName();
            double value = currentConfig.getASAP2Data().getASAP2Measurements().get(asap2_name).convertValue(data);
            if (DEBUG_PRINT) {
                System.out.print(asap2_name + " " + value);
                for (char dat : data) {
                    System.out.print(" 0x" + Integer.toHexString(dat));
                }
                System.out.println();
                System.out.println();
            }
            // publish the data to screen
            SwingUtilities.invokeLater(() -> {
                // don't display communic data on screen when reading logfile
                if (!LogReaderAction.getInstance().isActive()) {
                    currentConfig.getASAP2Data().getMeasurementByName(asap2_name).setValueAndTime(value, timestamp);
                }

                // add the data to the datalog if necessary
                LogDataWriterCsv logDataWriter = LogDataWriterCsv.getInstance();
                if (logDataWriter.isWritingActive()) {
                    logDataWriter.addValue(asap2_name, value);
                    if (lastSignal) {
                        logDataWriter.writeValues(timestamp);
                    }
                }
            });

            // notify all observers, if the last element of the DAQ list is encountered
            if (lastSignal) {
                hantune.daqListReceivedUpdate(timestamp);
            }
        }
    }

    /**
     * Iterates through the asap2measurements and returns the index of the asap2
     * name.
     *
     * @param asap2_name index of the given asap2 name
     * @return
     * @ret
     */
    public int getIndexAsap2name(String asap2_name) {
        int returnvalue;
        returnvalue = currentConfig.getASAP2Data().getMyIndex(asap2_name);

        return returnvalue;
    }

    /**
     * Processes Event messages
     *
     * @param XCPdata message data array
     * @param ts timestamp value
     */
    private void processEvent(char[] XCPdata) {
        // check message length
        if (XCPdata.length != 2) {
            setError("Invalid XCP response message during event handling", true);
            cl.exit();
            return;
        }

        // restart time-out?
        if (XCPdata[1] == EV_CMD_PENDING) {
            xcpTimeOutTimer.setTimeInMiliseconds(0);
        }

        // DAQ processor overload?
        if (XCPdata[1] == EV_DAQ_OVERLOAD) {
            // TODO: show error, not implemented yet
        }

        // session terminated by slave?
        if (XCPdata[1] == EV_SESSION_TERMINATED) {
            // go offline
            cl.exit();
            XCP_waiting = false;
            XCP_connected = false;
        }

        // calibration has been saved in non-volatile memory?
        if (XCPdata[1] == EV_STORE_CAL) {
            info.store_cal_req = false;
        }

        // all (selected) DAQ lists have been saved into non-volatile memory?
        if (XCPdata[1] == EV_STORE_DAQ) {
            info.store_daq_req = false;
        }

        // all DAQ lists in non-volatile memory have been cleared?
        if (XCPdata[1] == EV_CLEAR_DAQ) {
            info.clear_daq_req = false;
        }
    }

    /**
     * Processes Service message
     *
     * @param XCPdata message data array
     */
    private void processService(char[] XCPdata) {
        // check message length
        if (XCPdata.length < 2) {
            setError("Invalid XCP response message during service handling", true);
            cl.exit();
            return;
        }

        // request to reset sessions?
        if (XCPdata[1] == SERV_RESET) {
            resetSession();
        }

        if (XCPdata[1] == SERV_TEXT) {
            // not implemented yet
        }
    }

    /**
     * Processes Response message
     *
     * @param XCPdata message data array
     */
    private void processResponse(char[] XCPdata) {
        // local defines
        char daq_id;
        char event_id;
        // reset error and waiting flag
        cl.cmd[cl.getPreviousID()].error = ERR_NONE;
        // check response length
        // source of the error: java.lang.ArrayIndexOutOfBoundsException: -200 in (XCPdata.length != CMD[cl.cmd[cl.getPreviousID()].cmd - XCPInfo.OS].CRM_len) JASON

        char cmd = cl.cmd[cl.getPreviousID()].cmd;

        if ((last_cmd != CC_GET_SEED)
                && (last_cmd != CC_USER_CC)
                && (last_cmd != CC_UPLOAD)
                && (last_cmd != CC_SHORT_UPLOAD)
                && (last_cmd != CC_START_STOP_DAQ_LIST)
                && (last_cmd != CC_SYNCH)
                && cmd != 0
                && XCPdata.length != CMD[cmd - XCPInfo.OS].CRM_len) {
            setError("Invalid XCP response message, cmd = " + (int) cmd + ", CRM_len = "
                    + (cmd > 0 ? (int) CMD[cmd - XCPInfo.OS].CRM_len : 0) + ", PACKET_len = " + XCPdata.length, true);
            cl.exit();
            return;
        }

        // process message based on last send command
        switch (last_cmd) {
            case CC_CONNECT:
                // save ressource
                info.support_cal_pag = (XCPdata[1] & 0x01) > 0;
                info.support_daq = (XCPdata[1] & 0x04) > 0;
                info.support_stim = (XCPdata[1] & 0x08) > 0;
                info.support_pgm = (XCPdata[1] & 0x10) > 0;

                // save comm_mode_basic
                info.byte_order = (char) (XCPdata[2] & 0x01);
                info.address_granularity = (char) ((XCPdata[2] & 0x06) | 0x01);
                info.slave_block_mode = (XCPdata[2] & 0x40) > 0;
                info.comm_info_optional = (XCPdata[2] & 0x80) > 0;

                // save communication limits
                info.MAX_CTO = XCPdata[3];
                cl.setMAX_CTO(info.MAX_CTO);
                if (info.byte_order == 0x00) {
                    info.MAX_DTO = (char) (XCPdata[4] + ((XCPdata[5] << 8)));
                } else {
                    info.MAX_DTO = (char) (XCPdata[5] + ((XCPdata[4] << 8)));
                }

                // save layer versions
                info.protocol_version = XCPdata[6];
                info.transport_version = XCPdata[7];

                if (XCP.DEBUG_PRINT) {
                    System.out.println("info.support_cal_pag: " + info.support_cal_pag);
                    System.out.println("info.support_daq: " + info.support_daq);
                    System.out.println("info.support_stim: " + info.support_stim);
                    System.out.println("info.support_pgm: " + info.support_pgm);
                    System.out.println("info.byte_order: " + (int) info.byte_order);
                    System.out.println("info.address_granularity: " + (int) info.address_granularity);
                    System.out.println("info.slave_block_mode: " + info.slave_block_mode);
                    System.out.println("info.comm_info_optional: " + info.comm_info_optional);
                    System.out.println("info.MAX_CTO: " + (int) info.MAX_CTO);
                    System.out.println("info.MAX_DTO: " + (int) info.MAX_DTO);
                    System.out.println("info.protocol_version: " + info.protocol_version);
                    System.out.println("info.transport_version: " + info.transport_version);
                }

                XCP_connected = true;
                break;

            case CC_GET_STATUS:
                // save session status
                info.store_cal_req = (XCPdata[1] & 0x01) > 0;
                info.store_daq_req = (XCPdata[1] & 0x04) > 0;
                info.clear_daq_req = (XCPdata[1] & 0x08) > 0;
                info.daq_running = (XCPdata[1] & 0x40) > 0;
                info.slave_resume = (XCPdata[1] & 0x80) > 0;

                // save resource protection
                info.protected_cal_pag = (XCPdata[2] & 0x01) > 0;
                info.protected_daq = (XCPdata[2] & 0x04) > 0;
                info.protected_stim = (XCPdata[2] & 0x08) > 0;
                info.protected_pgm = (XCPdata[2] & 0x10) > 0;

                // save session configuration id
                if (info.byte_order == 0x00) {
                    info.session_id = (char) (XCPdata[4] + ((XCPdata[5] << 8)));
                } else {
                    info.session_id = (char) (XCPdata[5] + ((XCPdata[4] << 8)));
                }
                break;

            case CC_GET_COMM_MODE_INFO:
                // save comm_mode_optional
                info.master_block_mode = (XCPdata[2] & 0x01) > 0;
                info.interleaved_mode = (XCPdata[2] & 0x02) > 0;

                // save communication parameters
                XCPInfo.MAX_BS = XCPdata[4];
                XCPInfo.MIN_ST = XCPdata[5];
                XCPInfo.QUEUE_SIZE = XCPdata[6];

                // save driver version
                info.xcp_driver_version = XCPdata[7];
                break;

            case CC_GET_ID:
                // set the right timeout mode.
                getSlaveId = true;
                // save identifier length and init identifier data array
                if (info.byte_order == 0x00) {
                    info.slave_identifier_len
                            = (XCPdata[4] + (XCPdata[5] << 8) + (XCPdata[6] << 16) + (XCPdata[7] << 24));
                } else {
                    info.slave_identifier_len
                            = (XCPdata[7] + (XCPdata[6] << 8) + (XCPdata[5] << 16) + (XCPdata[4] << 24));
                }
                if (info.slave_identifier_len > 0xFF) {
                    info.slave_identifier_len = 0xFF;
                }
                info.slave_identifier = new char[info.slave_identifier_len];

                if (XCPdata[1] == 0x00) {
                    // slave sets MTA
                    // receive identifier data using (multiple) UPLOAD command(s)
                    char cto_cnt = (char) ((info.slave_identifier_len + (info.MAX_CTO - 2)) / (info.MAX_CTO - 1));
                    for (char j = 0; j < cto_cnt; j++) {
                        if (j == 0) {
                            xcpUPLOAD((char) (info.slave_identifier_len - ((cto_cnt - 1) * (info.MAX_CTO - 1))));
                        } else {
                            xcpUPLOAD((char) (info.MAX_CTO - 1));
                        }
                    }
                } else {
                    // ID is transferred in the remaining bytes of the response
                    for (char i = 0; i < info.slave_identifier_len; i++) {
                        info.slave_identifier[i] = XCPdata[i + 7];
                    }
//                    if(info.byte_order == 0x00){
//                        info.slave_identifier = util.Util.reverseArray(info.slave_identifier);
//                    }
                }
                break;

            case CC_GET_SEED:
                // first part of seed?
                if (get_seed_length == 0) {
                    // save seed length and init seed data array
                    get_seed_length = XCPdata[1];
                    get_seed_data = new char[get_seed_length];
                }

                // save parts of seed
                char seed_length = XCPdata[1];
                char id = (char) (get_seed_length - seed_length);
                char ptr = 2;
                for (char i = id; i < (id + XCPdata.length - 2); i++) {
                    get_seed_data[i] = XCPdata[ptr];
                    ptr++;
                }

                // last part of seed?
                if (seed_length == (XCPdata.length - 2)) {
                    if (info.byte_order == 0x00) {
                        get_seed_data = Util.reverseArray(get_seed_data);
                    }
                    // start unlocking
                    xcpUNLOCK();
                } else {
                    // ask for remaining part of seed
                    xcpGET_SEED((char) 0x01, get_seed_resource);
                }
                break;

            case CC_UNLOCK:
                // save resource protection
                info.protected_cal_pag = (XCPdata[1] & 0x01) > 0;
                info.protected_daq = (XCPdata[1] & 0x04) > 0;
                info.protected_stim = (XCPdata[1] & 0x08) > 0;
                info.protected_pgm = (XCPdata[1] & 0x10) > 0;
                break;

            case CC_USER_CC:
            case CC_UPLOAD:
            case CC_SHORT_UPLOAD:
                if (last_cmd == CC_UPLOAD) {
                    // increment MTA
                    set_mta_address += set_mta_incr;
                }

                // save slave identifier?
                if (info.slave_identifier_len > 0) {
                    for (char i = 0; i < (XCPdata.length - 1); i++) {
                        // save this part of the slave identifier and decrement pointer
                        info.slave_identifier[info.slave_identifier.length - info.slave_identifier_len]
                                = XCPdata[i + 1];
                        info.slave_identifier_len--;
                    }
                    // last part of slave identifier?
                    if (info.slave_identifier_len == 0) {
                    }
                }

                if (last_cmd == CC_USER_CC) {
                    user_data = XCPdata;
                    break;
                }

                // save upload data for use with ErrorHandler
                upload_data_error = XCPdata;
                // save upload data when needed
                if (upload_ptr < upload_data.length) {
                    for (char i = 0; i < (XCPdata.length - 1); i++) {
                        upload_data[i + upload_ptr] = XCPdata[i + 1];
                    }
                    upload_ptr += XCPdata.length - 1;
                }

                // event name?
                if (cl.cmd[cl.getPreviousID((char) 2)].cmd == CC_GET_DAQ_EVENT_INFO) {
                    // save parts of name into buffer
                    char[] u_data = new char[XCPdata.length - 1];
                    for (char j = 0; j < (XCPdata.length - 1); j++) {
                        u_data[j] = XCPdata[j + 1];
                    }
                    if (info.byte_order == 0x00) {
                        u_data = Util.reverseArray(u_data);
                    }
                    // save event identifier
                    if (info.byte_order == 0x00) {
                        event_id = (char) (cl.cmd[cl.getPreviousID((char) 2)].param[1]
                                + (cl.cmd[cl.getPreviousID((char) 2)].param[2] << 8));
                    } else {
                        event_id = (char) (cl.cmd[cl.getPreviousID((char) 2)].param[2]
                                + (cl.cmd[cl.getPreviousID((char) 2)].param[1] << 8));
                    }
                    // save event name
                    event[event_id].setName(new String(u_data));
                }
                break;

            case CC_BUILD_CHECKSUM:
                // increment MTA
                set_mta_address += set_mta_incr;
                // positive respons (negative response is given as an error)
                checksum_type = XCPdata[1];
                for (char i = 0; i < 4; i++) {
                    checksum_data[i] = XCPdata[i + 4];
                }
                if (info.byte_order == 0x00) {
                    checksum_data = Util.reverseArray(checksum_data);
                }
                break;

            case CC_DOWNLOAD:
                // increment MTA
                set_mta_address += set_mta_incr;
                break;

            case CC_WRITE_DAQ:
                // increment daq list pointer
                daq_ptr_entry += daq_ptr_incr;
                break;

            case CC_GET_DAQ_LIST_MODE:
                // save DAQ list identifier
                daq_id = (char) (cl.cmd[cl.getPreviousID()].param[2] + (cl.cmd[cl.getPreviousID()].param[1] << 8));
                // valid DAQ list identifier (no overflow)?
                if (daq_id < daqlist.length) {
                    // save DAQ list information
                    daqlist[daq_id].setSelected((XCPdata[1] & 0x01) > 0);
                    daqlist[daq_id].setRunning((XCPdata[1] & 0x40) > 0);
                    daqlist[daq_id].setResume((XCPdata[1] & 0x80) > 0);
                    daqlist[daq_id].setDirection((XCPdata[1] & 0x02) > 0);
                    daqlist[daq_id].setTimestamp((XCPdata[1] & 0x10) > 0);
                    daqlist[daq_id].setPidOff((XCPdata[1] & 0x20) > 0);
                    if (info.byte_order == 0x00) {
                        daqlist[daq_id].setEventNo((char) (XCPdata[4] + XCPdata[5] << 8));
                    } else {
                        daqlist[daq_id].setEventNo((char) (XCPdata[5] + XCPdata[4] << 8));
                    }
                    daqlist[daq_id].setPrescaler(XCPdata[6]);
                    daqlist[daq_id].setPriority(XCPdata[7]);

                    if (XCP.DEBUG_PRINT) {
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getSelected: " + daqlist[daq_id].getSelected());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getRunning: " + daqlist[daq_id].getRunning());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getResume: " + daqlist[daq_id].getResume());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getDirection: " + daqlist[daq_id].getDirection());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getTimestamp: " + daqlist[daq_id].getTimestamp());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getPidOff: " + daqlist[daq_id].getPidOff());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getEventNo: " + daqlist[daq_id].getEventNo());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getPrescaler: " + daqlist[daq_id].getPrescaler());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getPriority: " + daqlist[daq_id].getPriority());
                    }

                }
                break;

            case CC_START_STOP_DAQ_LIST:
                // save first ODT identifier the slave device will send
                if (XCPdata.length > 1) {
                    odtFirstPID[last_daqlist] = XCPdata[1];
                }
                break;

            case CC_GET_DAQ_CLOCK:
                // save DAQ clock from slave device
                if (info.byte_order == 0x00) {
                    daq_clock = (XCPdata[4] + (XCPdata[5] << 8) + (XCPdata[6] << 16) + (XCPdata[7] << 24));
                } else {
                    daq_clock = (XCPdata[7] + (XCPdata[6] << 8) + (XCPdata[5] << 16) + (XCPdata[4] << 24));
                }

                System.out.println("daq_clock: " + daq_clock);

                // TODO: synchronisation
                break;

            case CC_READ_DAQ:
                // valid DAQ list ODT entry (no overflow)?
                if (daq_ptr_list < daqlist.length
                        && daq_ptr_odt < daqlist[daq_ptr_list].odt.length
                        && daq_ptr_entry < daqlist[daq_ptr_list].odt[daq_ptr_odt].odt_entry.length) {
                    // save ODT entry information
                    daqlist[daq_ptr_list].odt[daq_ptr_odt].odt_entry[daq_ptr_entry].setBitOffset(XCPdata[1]);
                    daqlist[daq_ptr_list].odt[daq_ptr_odt].odt_entry[daq_ptr_entry].setSize(XCPdata[2]);
                    daqlist[daq_ptr_list].odt[daq_ptr_odt].odt_entry[daq_ptr_entry].setAddressExt(XCPdata[3]);
                    if (info.byte_order == 0x00) {
                        daqlist[daq_ptr_list].odt[daq_ptr_odt].odt_entry[daq_ptr_entry].setAddress((XCPdata[4]
                                + (XCPdata[5] << 8) + (XCPdata[6] << 16) + (XCPdata[7] << 24)));
                    } else {
                        daqlist[daq_ptr_list].odt[daq_ptr_odt].odt_entry[daq_ptr_entry].setAddress((XCPdata[7]
                                + (XCPdata[6] << 8) + (XCPdata[5] << 16) + (XCPdata[4] << 24)));
                    }
                }
                // increment daq list pointer
                daq_ptr_entry += daq_ptr_incr;
                break;

            case CC_GET_DAQ_PROCESSOR_INFO:
                getSlaveId = false;
                // save DAQ_PROPERTIES
                daqinfo.daq_config_type = (XCPdata[1] & 0x01) > 0;
                daqinfo.prescaler_supported = (XCPdata[1] & 0x02) > 0;
                daqinfo.resume_supported = (XCPdata[1] & 0x04) > 0;
                daqinfo.bit_stim_supported = (XCPdata[1] & 0x08) > 0;
                daqinfo.timestamp_supported = (XCPdata[1] & 0x10) > 0;
                daqinfo.pid_off_supported = (XCPdata[1] & 0x20) > 0;
                daqinfo.daq_overload = (char) (XCPdata[1] & 0xC0);

                daqinfo.max_daq = (char) (XCPdata[3] + (XCPdata[2] << 8));
                if (info.byte_order == 0x00) {
                    daqinfo.max_event_channel = (char) (XCPdata[4] + (XCPdata[5] << 8));
                } else {
                    daqinfo.max_event_channel = (char) (XCPdata[5] + (XCPdata[4] << 8));
                }

                event = new XCPEvent[daqinfo.max_event_channel];
                daqinfo.min_daq = XCPdata[6];

                // save DAQ_KEY_BYTE
                daqinfo.daq_optimisation_type = (char) (XCPdata[7] & 0x0F);
                daqinfo.daq_address_ext = (char) (XCPdata[7] & 0x30);
                daqinfo.daqIdFldType = XCPDaqInfo.DaqIdFieldType.getEnumValue(XCPdata[7] & 0xC0);

                if (daqinfo.daqIdFldType == XCPDaqInfo.DaqIdFieldType.DAQ_HDR_NOT_IMPLEMENTED) {
                    setError(
                            "XCP message: CC_GET_DAQ_PROCESSOR_INFO, DAQ Identification Field Type not implemented: 0x"
                            + Integer.toHexString(XCPdata[7] & 0xC0), true);
                    cl.exit();
                    return;
                }

                // ask for event channel information when available
                if (daqinfo.max_event_channel > 0) {
                    event_ptr = daqinfo.max_event_channel;
                    xcpGET_DAQ_EVENT_INFO((char) (event_ptr - 1));
                }

                if (XCP.DEBUG_PRINT) {
                    System.out.println("daqinfo.daq_config_type: " + daqinfo.daq_config_type);
                    System.out.println("daqinfo.prescaler_supported: " + daqinfo.prescaler_supported);
                    System.out.println("daqinfo.resume_supported: " + daqinfo.resume_supported);
                    System.out.println("daqinfo.bit_stim_supported: " + daqinfo.bit_stim_supported);
                    System.out.println("daqinfo.timestamp_supported: " + daqinfo.timestamp_supported);
                    System.out.println("daqinfo.pid_off_supported: " + daqinfo.pid_off_supported);
                    System.out.println("daqinfo.daq_overload: " + (int) daqinfo.daq_overload);
                    System.out.println("daqinfo.min_daq: " + (int) daqinfo.min_daq);
                    System.out.println("daqinfo.max_daq: " + (int) daqinfo.max_daq);
                    System.out.println("daqinfo.max_event_channel: " + (int) daqinfo.max_event_channel);
                    System.out.println("daqinfo.daq_optimisation_type: " + (int) daqinfo.daq_optimisation_type);
                    System.out.println("daqinfo.daq_address_ext: " + (int) daqinfo.daq_address_ext);
                    System.out.println("daqinfo.daqIdFldType: " + daqinfo.daqIdFldType);
                }
                break;

            case CC_GET_DAQ_RESOLUTION_INFO:
                // save DAQ resolution information
                daqinfo.daq_odt_entry_granularity = XCPdata[1];
                daqinfo.daq_odt_entry_size_max = XCPdata[2];
                daqinfo.stim_odt_entry_granularity = XCPdata[3];
                daqinfo.stim_odt_entry_size_max = XCPdata[4];
                daqinfo.timestamp_mode = XCPdata[5];
                daqinfo.timestamp_fixed = (daqinfo.timestamp_mode & 0x08) == 0x08;
                daqinfo.timestamp_size = (char) ((daqinfo.timestamp_mode & 0x01) + (daqinfo.timestamp_mode & 0x02)
                        + (daqinfo.timestamp_mode & 0x04));
                daqinfo.timestamp_unit = (char) (daqinfo.timestamp_mode & 0xF0);
                if (info.byte_order == 0x00) {
                    daqinfo.timestamp_ticks = (char) (XCPdata[6] + ((XCPdata[7] << 8)));
                } else {
                    daqinfo.timestamp_ticks = (char) (XCPdata[7] + ((XCPdata[6] << 8)));
                }
                daqinfo.timestamp_step = daqinfo.calcTimestampStepsize();
                if (XCP.DEBUG_PRINT) {
                    System.out.println("daqinfo.daq_odt_entry_granularity " + (int) daqinfo.daq_odt_entry_granularity);
                    System.out.println("daqinfo.daq_odt_entry_size_max " + (int) daqinfo.daq_odt_entry_size_max);
                    System.out.println(
                            "daqinfo.stim_odt_entry_granularity " + (int) daqinfo.stim_odt_entry_granularity);
                    System.out.println("daqinfo.stim_odt_entry_size_max " + (int) daqinfo.stim_odt_entry_size_max);
                    System.out.println("daqinfo.timestamp_mode " + (int) daqinfo.timestamp_mode);
                    System.out.println("daqinfo.timestamp_size " + (int) daqinfo.timestamp_size);
                    System.out.println("daqinfo.timestamp_unit " + (int) daqinfo.timestamp_unit);
                    System.out.println("daqinfo.timestamp_ticks " + (int) daqinfo.timestamp_ticks);
                }

                break;

            case CC_GET_DAQ_LIST_INFO:
                // save DAQ list identifier
                daq_id = (char) (cl.cmd[cl.getPreviousID()].param[2] + (cl.cmd[cl.getPreviousID()].param[1]));
                // valid DAQ list identifier (no overflow)?
                if (daq_id < daqlist.length) {
                    // save DAQ list information
                    daqlist[daq_id].setPredefined((XCPdata[1] & 0x01) > 0);
                    daqlist[daq_id].setEventFixed((XCPdata[1] & 0x02) > 0);
                    daqlist[daq_id].setDaqAllowed((XCPdata[1] & 0x04) > 0);
                    daqlist[daq_id].setStimAllowed((XCPdata[1] & 0x08) > 0);
                    daqlist[daq_id].setMaxOdt(XCPdata[2]);
                    daqlist[daq_id].setMaxOdtEntries(XCPdata[3]);
                    if (info.byte_order == 0x00) {
                        daqlist[daq_id].setFixedEvent((char) (XCPdata[4] + (XCPdata[5] << 8)));
                    } else {
                        daqlist[daq_id].setFixedEvent((char) (XCPdata[5] + (XCPdata[4] << 8)));
                    }
                    if (XCP.DEBUG_PRINT) {
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getPredefined: " + daqlist[daq_id].getPredefined());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getEventFixed: " + daqlist[daq_id].getEventFixed());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getDaqAllowed: " + daqlist[daq_id].getDaqAllowed());
                        System.out.println(
                                "daqlist[" + (int) daq_id + "].getStimAllowed: " + daqlist[daq_id].getStimAllowed());
                        System.out
                                .println("daqlist[" + (int) daq_id + "].getMaxOdt: " + (int) daqlist[daq_id].getMaxOdt());
                        System.out.println("daqlist[" + (int) daq_id + "].getMaxOdtEntries: " + (int) daqlist[daq_id]
                                .getMaxOdtEntries());
                    }

                }
                break;

            case CC_GET_DAQ_EVENT_INFO:
                // save event channel identifier
                event_id = (char) (cl.cmd[cl.getPreviousID()].param[2] + (cl.cmd[cl.getPreviousID()].param[1]));
                // valid event channel identifier (no overflow)?
                if (event_id < daqinfo.max_event_channel) {
                    // save event channel information
                    event[event_id] = new XCPEvent();
                    event[event_id].setDaqAllowed((XCPdata[1] & 0x04) > 0);
                    event[event_id].setStimAllowed((XCPdata[1] & 0x08) > 0);
                    event[event_id].setMaxDaqList(XCPdata[2]);
                    event[event_id].setTimeCycle(XCPdata[4]);
                    event[event_id].setTimeUnit(XCPdata[5]);
                    event[event_id].setPriority(XCPdata[6]);
                    // last event channel not reached yet?
                    if (event_ptr > 1) {
                        // ask for next event channel information
                        event_ptr--;
                        xcpGET_DAQ_EVENT_INFO((char) (event_ptr - 1));
                    }
                    // ask for event channel name
                    if (XCPdata[3] > 0) {
                        xcpUPLOAD(XCPdata[3]);
                    }
                }
                break;

            case CC_ALLOC_ODT_ENTRY:
                targetDaqlistSizeLimit.incrementSuccesfulCommandCounter();
                break;

        }
    }

    /**
     * Processes a XCP timeout
     */
    private void processTimeout() {
        // set error code and information
        last_error = ERR_TIMEOUT;
        setError("XCP communication timeout", false);

        // process timeout based on last send command
        switch (last_cmd) {
            case CC_CONNECT:
            case CC_SYNCH:
                last_pre_action = P_ACT_NONE;
                break;

            case CC_UPLOAD:
            case CC_BUILD_CHECKSUM:
            case CC_DOWNLOAD:
            case CC_DOWNLOAD_MAX:
            case CC_MODIFY_BITS:
            case CC_PROGRAM_CLEAR:
            case CC_PROGRAM:
            case CC_PROGRAM_PREPARE:
            case CC_PROGRAM_MAX:
                last_pre_action = P_ACT_SYNCH_SET_MTA;
                break;

            case CC_DOWNLOAD_NEXT:
                last_pre_action = P_ACT_SYNCH_DOWNLOAD;
                break;
            case CC_WRITE_DAQ:
                last_pre_action = P_ACT_SYNCH_SET_DAQ_PTR;
                break;
            case CC_PROGRAM_NEXT:
                last_pre_action = P_ACT_SYNCH_PROGRAM;
                break;
            default:
                last_pre_action = P_ACT_SYNCH;
                break;
        }
        last_action = (last_cmd == CC_CONNECT) ? ACT_REPEAT_INF : ACT_REPEAT_2;
        // prepare pre-actions and actions into XCP command list sequence
        processActions();
    }

    /**
     * Processes error message. Determines which actions and pre-actions do have
     * to be inserted into the XCP command list sequence.
     *
     * @param XCPdata message data array
     */
    private void processError(char[] XCPdata) {
        // check message length
        if ((XCPdata.length != 2) && (last_cmd != CC_BUILD_CHECKSUM)) {
            setError("Invalid XCP response message during error handling", true);
            cl.exit();
            return;
        }
        // update last occured error code
        last_error = XCPdata[1];

        // process error based on error code (and last send command)
        switch (XCPdata[1]) {
            case ERR_CMD_SYNCH:
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // no action
                last_action = ACT_NONE;
                break;

            case ERR_CMD_BUSY:
                setError("XCP command was not executed", false);
                // pre-action: wait t7
                last_pre_action = P_ACT_WAIT;
                // action: repeat infinite
                last_action = ACT_REPEAT_INF;
                break;

            case ERR_DAQ_ACTIVE:
                setError("XCP command rejected because DAQ is running", false);
                switch (last_cmd) {
                    case CC_CLEAR_DAQ_LIST:
                    case CC_WRITE_DAQ:
                    case CC_SET_DAQ_LIST_MODE:
                        // pre-action: START_STOP_x
                        last_pre_action = P_ACT_START_STOP;
                        // action: repeat 2 times
                        last_action = ACT_REPEAT_2;
                        break;
                    case CC_SET_DAQ_PTR:
                    default:
                        // no pre-action
                        last_pre_action = P_ACT_NONE;
                        // action: repeat 2 times
                        last_action = ACT_REPEAT_2;
                        break;
                }
                break;

            case ERR_PGM_ACTIVE:
                setError("XCP command rejected because PGM is running", false);
                switch (last_cmd) {
                    case CC_PROGRAM_RESET:
                        // no pre-action
                        last_pre_action = P_ACT_NONE;
                        // action: repeat 2 times
                        last_action = ACT_REPEAT_2;
                        break;
                    default:
                        // pre-action: wait t7
                        last_pre_action = P_ACT_WAIT;
                        // action: repeat infinite
                        last_action = ACT_REPEAT_INF;
                        break;
                }
                break;

            case ERR_CMD_UNKNOWN:
                setError("Unknown XCP command or not implemented optional XCP command", false);
                switch (last_cmd) {
                    case CC_DOWNLOAD_NEXT:
                    case CC_DOWNLOAD_MAX:
                        // pre-action: SET_MTA
                        last_pre_action = P_ACT_SET_MTA;
                        // action: use alternative
                        last_action = ACT_ALTERNATIVE;
                        break;
                    case CC_MODIFY_BITS:
                        // pre-action: UPLOAD + DOWNLOAD
                        last_pre_action = P_ACT_UPLOAD_DOWNLOAD;
                        // action: use alternative
                        last_action = ACT_ALTERNATIVE;
                        break;
                    case CC_SYNCH:
                        // no pre-action
                        last_pre_action = P_ACT_NONE;
                        // action: restart session
                        last_action = ACT_RESTART;
                        break;
                    case CC_GET_PAG_PROCESSOR_INFO:
                    case CC_GET_SEGMENT_INFO:
                    case CC_GET_DAQ_PROCESSOR_INFO:
                    case CC_GET_DAQ_RESOLUTION_INFO:
                    case CC_GET_DAQ_LIST_INFO:
                    case CC_GET_DAQ_EVENT_INFO:
                    case CC_GET_PGM_PROCESSOR_INFO:
                    case CC_GET_SECTOR_INFO:
                        // no pre-action
                        last_pre_action = P_ACT_NONE;
                        // action: use asap2Data
                        last_action = ACT_ASAP2;
                        break;
                    case CC_SHORT_UPLOAD:
                    case CC_SHORT_DOWNLOAD:
                    case CC_PROGRAM_NEXT:
                    case CC_PROGRAM_MAX:
                        // no pre-action
                        last_pre_action = P_ACT_NONE;
                        // action: use alternative
                        last_action = ACT_ALTERNATIVE;
                        break;
                    default:
                        // no pre-action
                        last_pre_action = P_ACT_NONE;
                        // action: display error
                        last_action = ACT_DISPLAY_ERROR;
                        break;
                }
                break;

            case ERR_CMD_SYNTAX:
                setError("XCP command syntax invalid", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: retry other syntax
                last_action = ACT_RETRY_SYNTAX;
                break;

            case ERR_OUT_OF_RANGE:
                setError("XCP command syntax valid but command parameter(s) out of range", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: retry other parameter
                last_action = ACT_RETRY_PARAMETER;

                // save max block size for checksum?
                if (last_cmd == CC_BUILD_CHECKSUM) {
                    checksum_blocksize
                            = (long) (XCPdata[7] + (XCPdata[6] << 8) + (XCPdata[5] << 16) + (XCPdata[4] << 24));
                }
                break;

            case ERR_WRITE_PROTECTED:
                setError("XCP memory location is write protected", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: display error
                last_action = ACT_DISPLAY_ERROR;
                break;

            case ERR_ACCESS_DENIED:
                setError("XCP memory location is not accessible", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: display error
                last_action = ACT_DISPLAY_ERROR;
                break;

            case ERR_ACCESS_LOCKED:
                setError("XCP access denied, seed & key is required", false);
                switch (last_cmd) {
                    case CC_UNLOCK:
                        // no pre-action
                        last_pre_action = P_ACT_NONE;
                        // action: restart session
                        last_action = ACT_RESTART;
                        break;
                    default:
                        // pre-action: unlock slave
                        last_pre_action = P_ACT_UNLOCK;
                        // action: repeat 2 times
                        last_action = ACT_REPEAT_2;
                        break;
                }
                break;

            case ERR_PAGE_NOT_VALID:
                setError("Selected XCP page not available", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: retry other parameter
                last_action = ACT_RETRY_PARAMETER;
                break;

            case ERR_MODE_NOT_VALID:
                setError("Select XCP page mode not available", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: retry other parameter
                last_action = ACT_RETRY_PARAMETER;
                break;

            case ERR_SEGMENT_NOT_VALID:
                setError("Selected XCP segment not valid", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: retry other parameter
                last_action = ACT_RETRY_PARAMETER;
                break;

            case ERR_SEQUENCE:
                setError("XCP sequence error", false);
                switch (last_cmd) {
                    case CC_UNLOCK:
                        // pre-action: UNLOCK
                        last_pre_action = P_ACT_GET_SEED;
                        // action: repeat 2 times
                        last_action = ACT_REPEAT_2;
                        break;
                    case CC_DOWNLOAD_NEXT:
                        // pre-action: SET_MTA
                        last_pre_action = P_ACT_SET_MTA;
                        // action: repeat 2 times
                        last_action = ACT_REPEAT_2;
                        break;
                    case CC_ALLOC_DAQ:
                    case CC_ALLOC_ODT:
                    case CC_ALLOC_ODT_ENTRY:
                        // pre-action: reinit DAQ
                        last_pre_action = P_ACT_REINIT;
                        // action: repeat 2 times
                        last_action = ACT_REPEAT_2;
                        break;
                    default:
                        // no pre-action
                        last_pre_action = P_ACT_NONE;
                        // action: repeat 2  times
                        last_action = ACT_REPEAT_2;
                        break;
                }
                break;

            case ERR_DAQ_CONFIG:
                setError("XCP DAQ configuration not valid", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: display error
                last_action = ACT_DISPLAY_ERROR;
                break;

            case ERR_MEMORY_OVERFLOW:
                setError("XCP memory overflow error", false);
                switch (last_cmd) {
                    case CC_ALLOC_ODT_ENTRY:
                        targetDaqlistSizeLimit.setDaqListSizeLimited();
                        // pre-action: reinit DAQ
                        last_pre_action = P_ACT_REINIT;
                        // action: retry other parameter
                        last_action = ACT_RETRY_PARAMETER;
                        break;

                    case CC_ALLOC_DAQ:
                    case CC_ALLOC_ODT:
                        // pre-action: reinit DAQ
                        last_pre_action = P_ACT_REINIT;
                        // action: retry other parameter
                        last_action = ACT_RETRY_PARAMETER;
                        break;

                    default:
                        // no pre-action
                        last_pre_action = P_ACT_NONE;
                        // action: display error
                        last_action = ACT_DISPLAY_ERROR;
                        break;
                }
                break;

            case ERR_GENERIC:
                setError("XCP generic error", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: restart session
                last_action = ACT_RESTART;
                break;

            case ERR_VERIFY:
                setError("XCP verify error", false);
                // no pre-action
                last_pre_action = P_ACT_NONE;
                // action: new flashware version necessary
                last_action = ACT_FLASH_NEW;
                break;

        }
        // prepeare pre-actions and actions into XCP command list sequence
        processActions();
    }

    /**
     * Calls further processing methods for preparing pre-actions and actions
     */
    private void processActions() {
        cl.cmd[cl.getPreviousID()].error = last_error;
        repeat = false;
        insertAction();
        insertPreAction();
    }

    /**
     * Insert pre-actions into the XCP command list sequence
     */
    private void insertPreAction() {
        // buffer for command parameters
        char[] par;

        // insert specified pre-actions
        switch (last_pre_action) {

            case P_ACT_NONE:
                // no pre-action
                XCP_waiting = false;
                break;

            case P_ACT_WAIT:
                timer_wait += TIMEOUT_T7;
                break;

            case P_ACT_SYNCH:
                XCP_waiting = false;
                if (repeat) {
                    xcpSYNCH();
                }
                break;

            case P_ACT_SYNCH_SET_MTA:
                XCP_waiting = false;
                if (repeat) {
                    xcpSET_MTA(set_mta_address, set_mta_address_ext);
                    xcpSYNCH();
                }
                break;

            case P_ACT_SYNCH_DOWNLOAD:
                XCP_waiting = false;
                if (repeat) {
                    // TODO: DOWNLOAD_NEXT not implemented yet
                    // xcpDOWNLOAD(data);
                    // xcpSYNCH();
                }
                break;

            case P_ACT_SYNCH_SET_DAQ_PTR:
                XCP_waiting = false;
                if (repeat) {
                    xcpSET_DAQ_PTR(daq_ptr_list, daq_ptr_odt, daq_ptr_entry);
                    xcpSYNCH();
                }
                break;

            case P_ACT_SYNCH_PROGRAM:
                XCP_waiting = false;
                if (repeat) {
                    // TODO: PROGRAM_NEXT not implemented yet
                    // xcpPROGRAM(...)
                    // xcpSYNCH();
                }
                break;

            case P_ACT_GET_SEED:
                XCP_waiting = false;
                cl.deleteCmds(CC_UNLOCK);
                char res1 = CMD[cl.cmd[cl.getPreviousID((char) -2)].cmd - XCPInfo.OS].resource;
                if (res1 > 0) {
                    xcpGET_SEED((char) 0x00, res1);
                }
                break;

            case P_ACT_UNLOCK:
                XCP_waiting = false;
                char res2 = CMD[last_cmd - XCPInfo.OS].resource;
                if (res2 > 0) {
                    xcpGET_SEED((char) 0x00, res2);
                }
                break;

            case P_ACT_SET_MTA:
                XCP_waiting = false;
                xcpSET_MTA(set_mta_address, set_mta_address_ext);
                break;

            case P_ACT_UPLOAD_DOWNLOAD:
                // TODO: MODIFY_BITS not implemented yet
                break;

            case P_ACT_START_STOP:
                XCP_waiting = false;
                switch (last_cmd) {
                    case CC_CLEAR_DAQ_LIST:
                    case CC_SET_DAQ_LIST_MODE:
                    case CC_WRITE_DAQ:
                        // stop only one daqlist
                        xcpSTART_STOP_DAQ_LIST((char) 0x00, daq_ptr_list);
                        break;
                    default:
                        // stop all daqlists
                        daqlistAllStopped = true;
                        xcpSTART_STOP_SYNCH((char) 0x00);
                        break;
                }
                break;

            case P_ACT_REINIT:
                XCP_waiting = false;
                // delete daq list configuration commands and re-init daqlist
                cl.deleteCmds(CC_FREE_DAQ);
                cl.deleteCmds(CC_ALLOC_DAQ);
                cl.deleteCmds(CC_ALLOC_ODT);
                cl.deleteCmds(CC_ALLOC_ODT_ENTRY);
                cl.deleteCmds(CC_SET_DAQ_LIST_MODE);
                cl.deleteCmds(CC_SET_DAQ_PTR);
                cl.deleteCmds(CC_WRITE_DAQ);
                if (daq_reinit_cnt < 2) {
                    daq_reinit_cnt++;
                    setDL();
                } else {
                    String txt = "Could not set DAQ list configuration.\n"
                            + "Due to memory limitations on target side, a maximum of "
                            + targetDaqlistSizeLimit.getLimitedSize()
                            + " signals can be configured.\n"
                            + "Default DAQ list has been resized when necessary";
                    setError(txt, true);
                    cl.exit();
                }
                break;
        }
    }

    /**
     * Insert actions into XCP command list sequence
     */
    private void insertAction() {
        // insert specified actions
        switch (last_action) {

            case ACT_REPEAT_INF:
                if (cl.cmd[cl.getPreviousID()].repeat < 5) {
                    repeat = true;
                    cl.insertCopy(cl.getPreviousID(), cl.getCurrentID());
                } else {
                    error = true;
                    cl.exit();
                    last_pre_action = P_ACT_NONE;
                }
                break;

            case ACT_REPEAT_2:
                if (cl.cmd[cl.getPreviousID()].repeat < 2) {
                    repeat = true;
                    cl.insertCopy(cl.getPreviousID(), cl.getCurrentID());
                } else {
                    if (last_action == P_ACT_REINIT) {
                        daq_reinit_cnt = 0;
                    }
                    error = true;
                    cl.exit();
                    last_pre_action = P_ACT_NONE;
                }
                break;

            case ACT_RESTART:
                resetSession();
                break;

            case ACT_RETRY_SYNTAX:
                if (cl.cmd[cl.getCurrentID() - 1].repeat < 1) {
                    repeat = true;
                    cl.insertCopy(cl.getPreviousID(), cl.getCurrentID());
                } else {
                    error = true;
                    cl.exit();
                    last_pre_action = P_ACT_NONE;
                }
                break;

            case ACT_RETRY_PARAMETER:
                if (cl.cmd[cl.getCurrentID() - 1].repeat < 1) {
                    repeat = true;
                    cl.insertCopy(cl.getPreviousID(), cl.getCurrentID());
                } else {
                    error = true;
                    cl.exit();
                    last_pre_action = P_ACT_NONE;
                }
                break;

            case ACT_DISPLAY_ERROR:
                error = true;
                cl.exit();
                last_pre_action = P_ACT_NONE;
                break;

            case ACT_ALTERNATIVE:
                // TODO: alternative commands not implemented yet
                break;

            case ACT_ASAP2:
                error = true;
                cl.exit();
                last_pre_action = P_ACT_NONE;
                break;

            case ACT_FLASH_NEW:
                // TODO: PGM not implemented yet
                break;
        }
    }

    // ***** STD - Standard Commands *****
    /**
     * Inserts CONNECT command into XCP command list sequence
     */
    private void xcpCONNECT() {
        char[] par = new char[1];
        par[0] = 0x00;
        cl.insertCmd(cl.getCurrentID(), CC_CONNECT, par);
    }

    /**
     * Inserts DISCONNECT command into XCP command list sequence
     */
    private void xcpDISCONNECT() {
        char[] par = new char[0];
        cl.insertCmd(cl.getCurrentID(), CC_DISCONNECT, par);
    }

    /**
     * Inserts GET_STATUS command into XCP command list sequence
     */
    private void xcpGET_STATUS() {
        char[] par = new char[0];
        cl.insertCmd(cl.getCurrentID(), CC_GET_STATUS, par);
    }

    /**
     * Inserts SYNCH command into XCP command list sequence
     */
    private void xcpSYNCH() {
        char[] par = new char[0];
        cl.insertCmd(cl.getCurrentID(), CC_SYNCH, par);
    }

    /**
     * Inserts GET_COMM_MODE_INFO command into XCP command list sequence
     */
    private void xcpGET_COMM_MODE_INFO() {
        char[] par = new char[0];
        cl.insertCmd(cl.getCurrentID(), CC_GET_COMM_MODE_INFO, par);
    }

    /**
     * Inserts GET_ID command into XCP command list sequence
     */
    private void xcpGET_ID() {
        char[] par = new char[1];
        par[0] = 0x01;
        cl.insertCmd(cl.getCurrentID(), CC_GET_ID, par);
    }

    /**
     * Inserts SET_REQUEST command into XCP command list sequence
     *
     * @param session_id session identifier
     * @param store_cal_req store calibration flag
     * @param store_daq_req store DAQ list configuration flag
     * @param clear_daq_req clear DAQ list configuration flag
     */
    private void xcpSET_REQUEST(char session_id, boolean store_cal_req, boolean store_daq_req, boolean clear_daq_req) {
        // create mode bitmask from flags
        char mode = 0x00;
        if (store_cal_req) {
            mode |= 0x01;
        }
        if (store_daq_req) {
            mode |= 0x04;
        }
        if (clear_daq_req) {
            mode |= 0x08;
        }

        // create parameters
        char[] par = new char[3];
        par[0] = mode;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((session_id >> 8) & 0xFF);
            par[1] = (char) ((session_id) & 0xFF);
        } else {
            par[1] = (char) ((session_id >> 8) & 0xFF);
            par[2] = (char) ((session_id) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_SET_REQUEST, par);
    }

    /**
     * Inserts GET_SEED command into XCP command list sequence
     *
     * @param mode mode (0x00 = (first part of) seed; 0x01 = remaining part of
     * seed)
     * @param resource resource to unlock
     */
    private void xcpGET_SEED(char mode, char resource) {
        if (mode == 0) {
            get_seed_length = 0x00;
        }
        get_seed_mode = mode;
        get_seed_resource = resource;
        char[] par = new char[2];
        par[0] = get_seed_mode;
        par[1] = get_seed_resource;
        cl.insertCmd(cl.getCurrentID(), CC_GET_SEED, par);
    }

    /**
     * Inserts UNLOCK command into XCP command list sequence
     */
    private void xcpUNLOCK() {
        // calculate key from seed using external API
        SeedKeyNative seedkey = new SeedKeyNative();
        /*
         * seedkey.DLL_file = System.getProperty("user.dir") + "/FeaserKey.dll";
         * char status = seedkey.ComputeKeyFromSeed(get_seed_resource,
         * get_seed_data); if(status > 0){ setError("Could not calculate key
         * from seed", true); cl.exit(); return; }
         */
        seedkey.key_length = 1;
        seedkey.key = new char[1];
        seedkey.key[0] = 0x01;

        // send key to slave (using multiple UNLOCK commands if needed)
        char cto_cnt = (char) ((seedkey.key_length + (info.MAX_CTO - 3)) / (info.MAX_CTO - 2));
        char ptr = 0;
        char[] par;
        for (char j = 0; j < cto_cnt; j++) {
            if (j < (cto_cnt - 1)) {
                par = new char[info.MAX_CTO - 1];
            } else {
                par = new char[seedkey.key_length - ptr + 1];
            }
            par[0] = (char) (seedkey.key_length - ptr);
            for (char i = 0; i < par.length - 1; i++) {
                par[i + 1] = (char) (seedkey.key[ptr] & 0xFF);
                ptr++;
            }
            cl.insertCmd((char) (cl.getCurrentID() + j), CC_UNLOCK, par);
        }
    }

    /**
     * Inserts SET_MTA command into XCP command list sequence
     *
     * @param address memory address
     * @param address_ext memory address extension
     */
    private void xcpSET_MTA(long address, char address_ext) {
        char[] par = new char[7];
        par[0] = 0x00;
        par[1] = 0x00;
        par[2] = address_ext;
        if (info.byte_order == 0x00) {
            par[6] = (char) ((address >> 24) & 0xFF);
            par[5] = (char) ((address >> 16) & 0xFF);
            par[4] = (char) ((address >> 8) & 0xFF);
            par[3] = (char) ((address) & 0xFF);
        } else {
            par[3] = (char) ((address >> 24) & 0xFF);
            par[4] = (char) ((address >> 16) & 0xFF);
            par[5] = (char) ((address >> 8) & 0xFF);
            par[6] = (char) ((address) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_SET_MTA, par);
    }

    /**
     * Inserts UPLOAD command into XCP command list sequence
     *
     * @param len length of the desired value
     */
    private void xcpUPLOAD(char len) {
        char[] par = new char[1];
        par[0] = len;
        cl.insertCmd(cl.getCurrentID(), CC_UPLOAD, par);
        set_mta_incr = len;
    }

    /**
     * Inserts SHORT_UPLOAD command into XCP command list sequence
     *
     * @param len length of the desired value
     * @param address memory address
     * @param address_ext memory address extension
     */
    private void xcpSHORT_UPLOAD(char len, long address, char address_ext) {
        char[] par = new char[7];
        par[0] = len;
        par[1] = 0x00;
        par[2] = address_ext;
        if (info.byte_order == 0x00) {
            par[6] = (char) ((address >> 24) & 0xFF);
            par[5] = (char) ((address >> 16) & 0xFF);
            par[4] = (char) ((address >> 8) & 0xFF);
            par[3] = (char) ((address) & 0xFF);
        } else {
            par[3] = (char) ((address >> 24) & 0xFF);
            par[4] = (char) ((address >> 16) & 0xFF);
            par[5] = (char) ((address >> 8) & 0xFF);
            par[6] = (char) ((address) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_SHORT_UPLOAD, par);
    }

    /**
     * Inserts BUILD_CHECKSUM command into XCP command list sequence
     *
     * @param blocksize block size to calculate checksum from
     */
    private void xcpBUILD_CHECKSUM(long blocksize) {
        char[] par = new char[7];
        par[0] = 0x00;
        par[1] = 0x00;
        par[2] = 0x00;
        par[3] = (char) ((blocksize >> 24) & 0xFF);
        par[4] = (char) ((blocksize >> 16) & 0xFF);
        par[5] = (char) ((blocksize >> 8) & 0xFF);
        par[6] = (char) ((blocksize) & 0xFF);
        cl.insertCmd(cl.getCurrentID(), CC_BUILD_CHECKSUM, par);
        set_mta_incr = 0;
    }

    // ***** CAL - Calibration Commands *****
    /**
     * Inserts DOWNLOAD command into XCP command list sequence
     *
     * @param data data array to download into slave device
     */
    private void xcpDOWNLOAD(char[] data) {
        char[] par = new char[data.length + 1];
        par[0] = (char) data.length;
        if (info.byte_order == 0x00) {
            data = Util.reverseArray(data);
        }
        for (char i = 0; i < data.length; i++) {
            par[i + 1] = data[i];
        }
        cl.insertCmd(cl.getCurrentID(), CC_DOWNLOAD, par);
        set_mta_incr = (char) data.length;
    }

    /**
     * Inserts DOWNLOAD_NEXT command into XCP command list sequence
     */
    private void xcpDOWNLOAD_NEXT() {
        // TODO, optional
    }

    /**
     * Inserts DOWNLOAD_MAX command into XCP command list sequence
     */
    private void xcpDOWNLOAD_MAX() {
        // TODO, optional
    }

    /**
     * Inserts SHORT_DOWNLOAD command into XCP command list sequence
     */
    private void xcpSHORT_DOWNLOAD() {
        // TODO, optional
    }

    /**
     * Inserts MODIFY_BITS command into XCP command list sequence
     */
    private void xcpMODIFY_BITS() {
        // TODO, optional
    }

    // ***** PAG - Page Switching Commands *****
    /**
     * Inserts SET_CAL_PAGE command into XCP command list sequence
     */
    private void xcpSET_CAL_PAGE() {
        // not yet implemented
    }

    /**
     * Inserts GET_CAL_PAGE command into XCP command list sequence
     */
    private void xcpGET_CAL_PAGE() {
        // not yet implemented
    }

    /**
     * Inserts GET_PAG_PROCESSOR_INFO command into XCP command list sequence
     */
    private void xcpGET_PAG_PROCESSOR_INFO() {
        // not yet implemented
    }

    /**
     * Inserts GET_SEGMENT_INFO command into XCP command list sequence
     */
    private void xcpGET_SEGMENT_INFO() {
        // not yet implemented
    }

    /**
     * Inserts GET_PAGE_INFO command into XCP command list sequence
     */
    private void xcpGET_PAGE_INFO() {
        // not yet implemented
    }

    /**
     * Inserts SET_SEGMENT_MODE command into XCP command list sequence
     */
    private void xcpSET_SEGMENT_MODE() {
        // not yet implemented
    }

    /**
     * Inserts GET_SEGMENT_MODE command into XCP command list sequence
     */
    private void xcpGET_SEGMENT_MODE() {
        // not yet implemented
    }

    /**
     * Inserts COPY_CAL_PAGE command into XCP command list sequence
     */
    private void xcpCOPY_CAL_PAGE() {
        // not yet implemented
    }

    // ***** DAQ - Data Acquisition and Stimulation Commands *****
    /**
     * Inserts CLEAR_DAQ_LIST command into XCP command list sequence
     *
     * @param list_no identifier of the DAQ list to clear
     */
    private void xcpCLEAR_DAQ_LIST(char list_no) {
        char[] par = new char[3];
        par[0] = 0x00;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((list_no >> 8) & 0xFF);
            par[1] = (char) ((list_no) & 0xFF);
        } else {
            par[1] = (char) ((list_no >> 8) & 0xFF);
            par[2] = (char) ((list_no) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_CLEAR_DAQ_LIST, par);
    }

    /**
     * Inserts SET_DAQ_PTR command into XCP command list sequence
     *
     * @param list_no identifier of the DAQ list to select
     * @param odt_no identifier of the ODT to select
     * @param entry_no identifier of the ODT entry to select
     */
    private void xcpSET_DAQ_PTR(char list_no, char odt_no, char entry_no) {
        char[] par = new char[5];
        par[0] = 0x00;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((list_no >> 8) & 0xFF);
            par[1] = (char) ((list_no) & 0xFF);
        } else {
            par[1] = (char) ((list_no >> 8) & 0xFF);
            par[2] = (char) ((list_no) & 0xFF);
        }
        par[3] = odt_no;
        par[4] = entry_no;
        cl.insertCmd(cl.getCurrentID(), CC_SET_DAQ_PTR, par);
    }

    /**
     * Inserts WRITE_DAQ command into XCP command list sequence
     *
     * @param size size of the element
     * @param address memory address
     * @param address_ext memory address extension
     */
    private void xcpWRITE_DAQ(char size, long address, char address_ext) {
        char[] par = new char[7];
        par[0] = 0xFF;
        par[1] = size;
        par[2] = address_ext;
        if (info.byte_order == 0x00) {
            par[6] = (char) ((address >> 24) & 0xFF);
            par[5] = (char) ((address >> 16) & 0xFF);
            par[4] = (char) ((address >> 8) & 0xFF);
            par[3] = (char) ((address) & 0xFF);
        } else {
            par[3] = (char) ((address >> 24) & 0xFF);
            par[4] = (char) ((address >> 16) & 0xFF);
            par[5] = (char) ((address >> 8) & 0xFF);
            par[6] = (char) ((address) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_WRITE_DAQ, par);
        daq_ptr_incr = (char) 1;
    }

    /**
     * Inserts SET_DAQ_LIST_MODE command into XCP command list sequence
     *
     * @param stim direction flag
     * @param timestamp flag whether timestamp is enabled
     * @param pid_off flag whether Identification Field is enabled
     * @param list_no identifier of the DAQ list
     * @param event_no identifier of the event channel
     * @param prescaler prescaler to be used
     * @param priority priority to be used
     */
    private void xcpSET_DAQ_LIST_MODE(boolean stim, boolean timestamp, boolean pid_off, char list_no, char event_no, char prescaler, char priority) {
        char[] par = new char[7];
        par[0] = 0x00;
        if (stim) {
            par[0] |= 0x02;
        }
        if (timestamp) {
            par[0] |= 0x10;
        }
        if (pid_off) {
            par[0] |= 0x20;
        }
        if (info.byte_order == 0x00) {
            par[2] = (char) ((list_no >> 8) & 0xFF);
            par[1] = (char) ((list_no) & 0xFF);
        } else {
            par[1] = (char) ((list_no >> 8) & 0xFF);
            par[2] = (char) ((list_no) & 0xFF);
        }
        if (info.byte_order == 0x00) {
            par[4] = (char) ((event_no >> 8) & 0xFF);
            par[3] = (char) ((event_no) & 0xFF);
        } else {
            par[3] = (char) ((event_no >> 8) & 0xFF);
            par[4] = (char) ((event_no) & 0xFF);
        }
        par[5] = prescaler;
        par[6] = priority;
        cl.insertCmd(cl.getCurrentID(), CC_SET_DAQ_LIST_MODE, par);
    }

    /**
     * Inserts GET_DAQ_LIST_MODE command into XCP command list sequence
     *
     * @param list_no identifier of the DAQ list
     */
    private void xcpGET_DAQ_LIST_MODE(char list_no) {
        char[] par = new char[3];
        par[0] = 0x00;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((list_no >> 8) & 0xFF);
            par[1] = (char) ((list_no) & 0xFF);
        } else {
            par[1] = (char) ((list_no >> 8) & 0xFF);
            par[2] = (char) ((list_no) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_GET_DAQ_LIST_MODE, par);
    }

    /**
     * Inserts START_STOP_DAQ_LIST command into XCP command list sequence
     *
     * @param mode (0x00 = stop; 0x01 = start; 0x02 = select)
     * @param list_no identifier of the DAQ list
     */
    private void xcpSTART_STOP_DAQ_LIST(char mode, char list_no) {
        char[] par = new char[3];
        par[0] = mode;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((list_no >> 8) & 0xFF);
            par[1] = (char) ((list_no) & 0xFF);
        } else {
            par[1] = (char) ((list_no >> 8) & 0xFF);
            par[2] = (char) ((list_no) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_START_STOP_DAQ_LIST, par);
        if (mode == 2 && list_no < daqlist.length) {
            daqlist[list_no].setSelected(true);
        }
    }

    /**
     * Inserts START_STOP_SYNCH command into XCP command list sequence
     *
     * @param mode (0x00 = stop all; 0x01 = start selected; 0x02 = stop
     * selected)
     */
    private void xcpSTART_STOP_SYNCH(char mode) {
        char[] par = new char[1];
        par[0] = mode;
        cl.insertCmd(cl.getCurrentID(), CC_START_STOP_SYNCH, par);
    }

    /**
     * Inserts GET_DAQ_CLOCK command into XCP command list sequence
     */
    private void xcpGET_DAQ_CLOCK() {
        char[] par = new char[0];
        cl.insertCmd(cl.getCurrentID(), CC_GET_DAQ_CLOCK, par);
    }

    /**
     * Inserts READ_DAQ command into XCP command list sequence
     */
    private void xcpREAD_DAQ() {
        char[] par = new char[0];
        cl.insertCmd(cl.getCurrentID(), CC_READ_DAQ, par);
        daq_ptr_incr = (char) 1;
    }

    /**
     * Inserts GET_DAQ_PROCESSOR_INFO command into XCP command list sequence
     */
    private void xcpGET_DAQ_PROCESSOR_INFO() {
        char[] par = new char[0];
        cl.insertCmd(cl.getCurrentID(), CC_GET_DAQ_PROCESSOR_INFO, par);
    }

    /**
     * Inserts GET_DAQ_RESOLUTION_INFO command into XCP command list sequence
     */
    private void xcpGET_DAQ_RESOLUTION_INFO() {
        char[] par = new char[0];
        cl.insertCmd(cl.getCurrentID(), CC_GET_DAQ_RESOLUTION_INFO, par);
    }

    /**
     * Inserts GET_DAQ_LIST_INFO command into XCP command list sequence
     *
     * @param list_no identifier of the DAQ list
     */
    private void xcpGET_DAQ_LIST_INFO(char list_no) {
        char[] par = new char[3];
        par[0] = 0x00;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((list_no >> 8) & 0xFF);
            par[1] = (char) ((list_no) & 0xFF);
        } else {
            par[1] = (char) ((list_no >> 8) & 0xFF);
            par[2] = (char) ((list_no) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_GET_DAQ_LIST_INFO, par);
    }

    /**
     * Inserts GET_DAQ_EVENT_INFO command into XCP command list sequence
     *
     * @param event_no identifier of the event channel
     */
    private void xcpGET_DAQ_EVENT_INFO(char event_no) {
        char[] par = new char[3];
        par[0] = 0x00;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((event_no >> 8) & 0xFF);
            par[1] = (char) ((event_no) & 0xFF);
        } else {
            par[1] = (char) ((event_no >> 8) & 0xFF);
            par[2] = (char) ((event_no) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_GET_DAQ_EVENT_INFO, par);
    }

    /**
     * Inserts FREE_DAQ command into XCP command list sequence
     */
    private void xcpFREE_DAQ() {
        char[] par = new char[0];
        cl.insertCmd(cl.getCurrentID(), CC_FREE_DAQ, par);
    }

    /**
     * Inserts ALLOC_DAQ command into XCP command list sequence
     *
     * @param count number of DAQ lists to allocate
     */
    private void xcpALLOC_DAQ(char count) {
        char[] par = new char[3];
        par[0] = 0x00;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((count >> 8) & 0xFF);
            par[1] = (char) ((count) & 0xFF);
        } else {
            par[1] = (char) ((count >> 8) & 0xFF);
            par[2] = (char) ((count) & 0xFF);
        }
        cl.insertCmd(cl.getCurrentID(), CC_ALLOC_DAQ, par);
    }

    /**
     * Inserts ALLOC_ODT command into XCP command list sequence
     *
     * @param list_no identifier of the DAQ list
     * @param count number of ODTs to allocate
     */
    private void xcpALLOC_ODT(char list_no, char count) {
        char[] par = new char[4];
        par[0] = 0x00;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((list_no >> 8) & 0xFF);
            par[1] = (char) ((list_no) & 0xFF);
        } else {
            par[1] = (char) ((list_no >> 8) & 0xFF);
            par[2] = (char) ((list_no) & 0xFF);
        }
        par[3] = count;
        cl.insertCmd(cl.getCurrentID(), CC_ALLOC_ODT, par);
    }

    /**
     * Inserts ALLOC_ODT_ENTRY command into XCP command list sequence
     *
     * @param list_no identifier of the DAQ list
     * @param odt_no identifier of the ODT
     * @param count number of ODT entries to allocate
     */
    private void xcpALLOC_ODT_ENTRY(char list_no, char odt_no, char count) {
        char[] par = new char[5];
        par[0] = 0x00;
        if (info.byte_order == 0x00) {
            par[2] = (char) ((list_no >> 8) & 0xFF);
            par[1] = (char) ((list_no) & 0xFF);
        } else {
            par[1] = (char) ((list_no >> 8) & 0xFF);
            par[2] = (char) ((list_no) & 0xFF);
        }
        par[3] = odt_no;
        par[4] = count;
        cl.insertCmd(cl.getCurrentID(), CC_ALLOC_ODT_ENTRY, par);
    }

    // ***** PGM - Non-volatile Memory Programming Commands *****
    /**
     * Inserts PROGRAM_START command into XCP command list sequence
     */
    private void xcpPROGRAM_START() {
        // not yet implemented
    }

    /**
     * Inserts PROGRAM_CLEAR command into XCP command list sequence
     */
    private void xcpPROGRAM_CLEAR() {
        // not yet implemented
    }

    /**
     * Inserts PROGRAM command into XCP command list sequence
     */
    public void xcpPROGRAM() {
        // not yet implemented
    }

    /**
     * Inserts PROGRAM_RESET command into XCP command list sequence
     */
    private void xcpPROGRAM_RESET() {
        // not yet implemented
    }

    /**
     * Inserts GET_PGM_PROCESSOR_INFO command into XCP command list sequence
     */
    private void xcpGET_PGM_PROCESSOR_INFO() {
        // not yet implemented
    }

    /**
     * Inserts GET_SECTOR_INFO command into XCP command list sequence
     */
    private void xcpGET_SECTOR_INFO() {
        // not yet implemented
    }

    /**
     * Inserts PROGRAM_PREPARE command into XCP command list sequence
     */
    private void xcpPROGRAM_PREPARE() {
        // not yet implemented
    }

    /**
     * Inserts PROGRAM_FORMAT command into XCP command list sequence
     */
    private void xcpPROGRAM_FORMAT() {
        // not yet implemented
    }

    /**
     * Inserts PROGRAM_NEXT command into XCP command list sequence
     */
    private void xcpPROGRAM_NEXT() {
        // not yet implemented
    }

    /**
     * Inserts PROGRAM_MAX command into XCP command list sequence
     */
    private void xcpPROGRAM_MAX() {
        // not yet implemented
    }

    /**
     * Inserts PROGRAM_VERIFY command into XCP command list sequence
     */
    private void xcpPROGRAM_VERIFY() {
        // not yet implemented
    }

    /**
     * Sends next command from the XCP command list sequence
     *
     * @param command identifier of the element on the command list
     * @throws java.lang.Exception If not possible to send command
     */
    private void sendCMD(char command) throws Exception {
        XCP_sending = true;
        // print current command list?
        if (XCP.DEBUG_PRINT) {
            cl.print();
        }

        // check if a XCP connection is active when needed
        if (cl.cmd[command].cmd != CC_CONNECT && !XCP_connected) {
            // set error and exit command list sequence
            setError("Can not send XCP commands when not connected", true);
            cl.exit();
            XCP_waiting = false;
            return;
        }

        // wait for last command to be executed
        while (XCP_waiting) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                Thread.currentThread().interrupt();  // set interrupt flag
                break;
            } catch (Exception e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
            }
        }

        // update last command code that has been send
        last_cmd = cl.cmd[command].cmd;
        char[] param = cl.cmd[command].param;
        XCPCmd xc = CMD[last_cmd - XCPInfo.OS];

        // print send command to command line?
        if (XCP.DEBUG_PRINT) {
            System.out.println(
                    "<<< " + System.currentTimeMillis() + " - " + Integer.toHexString(command) + " " + xc.cmd);
        }

        // configure timeout
        timer_wait = (currentConfig.getXcpsettings().getTPrescaler() * xc.timeout);

        // init message
        char[] msg = new char[param.length + 1];
        msg[0] = last_cmd;
        for (char i = 0; i < param.length; i++) {
            msg[i + 1] = param[i];
        }

        // save current MTA address
        switch (last_cmd) {
            case CC_SET_MTA:
            case CC_SHORT_UPLOAD:
            case CC_SHORT_DOWNLOAD:
                if (info.byte_order == 0x00) {
                    set_mta_address = (param[3] + (param[4] << 8) + (param[5] << 16) + (param[6] << 24));
                } else {
                    set_mta_address = (param[6] + (param[5] << 8) + (param[4] << 16) + (param[3] << 24));
                }
                set_mta_address_ext = param[2];
                break;
            case CC_SET_DAQ_PTR:
                daq_ptr_list = (char) (param[2] + (param[1] << 8));
                daq_ptr_odt = param[3];
                daq_ptr_entry = param[4];
        }

        // send message
        if (!sendData(msg)) {
            // set error and exit command list sequence
            setError("Could not send data", true);
            cl.exit();
            XCP_waiting = false;
            return;
        }
        XCP_sending = false;
    }

    public void processTimeOutGetSlaveId() {
        // set error code and information
        last_error = ERR_TIMEOUT;
        setError("XCP communication timeout", false);
        last_pre_action = P_ACT_SYNCH;
        last_action = ACT_REPEAT_2;
        // prepare pre-actions and actions into XCP command list sequence
        processActions();
    }

    /**
     * Cyclic background method for sending next command list items and
     * detecting timeouts
     */
    @Override
    public void run() {
        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
        try {
            while (isXCPRunning) {
                // check for timeout
                if (XCP_waiting && xcpTimeOutTimer.getTimeInMiliseconds() > timer_wait) {

                    if (!getSlaveId) {
                        processTimeout();
                    } else {
                        processTimeOutGetSlaveId();
                    }
                }

                // process command table
                if (!XCP_waiting && cl.isActive() && !XCP_isProcessing) {
                    if (cl.cmd[cl.getCurrentID()].cmd > 0) {
                        sendCMD(cl.getCurrentID());
                    } else {
                        cl.exit();
                    }
                }

                // configure frequency
                Thread.sleep(0, 500000);
            }
            xcpTimeOutTimer.cancelTimer();
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
        }
    }

    /**
     * Submit the request to clear the errors on the target.
     *
     * @param active True if the active (RAM) errors should be cleared, false
     * otherwise.
     * @param stored True if the stored (EEPROM) errors should be cleared, false
     * otherwise.
     */
    @Override
    public void clearErrorInfo(boolean active, boolean stored) {
        // wait for availability of the semaphore
        while (!memoryAccessSemaphore) {
            try {
                Thread.sleep(0, 100000);
            } catch (InterruptedException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                Thread.currentThread().interrupt();  // set interrupt flag
                break;
            }

        }
        // take the semaphore to lock in mutual exclusive access
        memoryAccessSemaphore = false;

        if (active) {
            // request active errors to be cleared
            setBufferSync((char) 0x02);
            //release buffer
            setBufferRelease();
        }
        if (stored) {
            // request active errors to be cleared
            setBufferSync((char) 0x03);
            //release buffer
            setBufferRelease();
        }
        // release semaphore to give others a chance to obtain mutual
        // exclusive access.
        memoryAccessSemaphore = true;
    }

    /**
     * Submit the request to read the active and stored errors from the target.
     *
     * @param callback Callback that should be called when new error info was
     * read from the target.
     */
    @Override
    public void readErrorInfo(IErrorInfoProcessor callback) {
        // check availability of the semaphore
        if (!memoryAccessSemaphore) {
            /* mutual exclusive access to the target's memory resource is
             * currently not available. abort the operation
             */
            System.out.println("readErrorInfo(...), semaphore unavailable");
            return;
        }
        // take the semaphore to lock in mutual exclusive access
        memoryAccessSemaphore = false;

        // create array for storing XCP reponse data
        char[] received_data = new char[8];
        // construct array lists to store the errors
        ArrayList<ErrorObject> activeErrList = new ArrayList<>();
        // construct array list with stored errors
        ArrayList<ErrorObject> storedErrList = new ArrayList<>();

        // request address of the errorlist on the target and store the response
        getErrorListAddress();
        received_data = saveUSER_DATA();
        // only continue if the address of the error list was received
        if (received_data.length > 1) {
            int error_list_addr;
            char[] errorListRowData;
            int error_size;

            // --------------- read active (RAM) errors ------------------------
            // instruct target to copy active errors to its internal error list
            setBufferSync((char) 0);
            // read out the memory address from the response. it is in bytes
            // 1..4
            error_list_addr = 0;
            error_list_addr |= ((received_data[4] << 24) & 0xff000000);
            error_list_addr |= ((received_data[3] << 16) & 0x00ff0000);
            error_list_addr |= ((received_data[2] << 8) & 0x0000ff00);
            error_list_addr |= ((received_data[1]) & 0x000000ff);
            // set the MTA to the start of the error list
            setMTA(error_list_addr, (char) 0);

            // read the first row from the error list. this one contains list
            // information, including the number of errors that are in the list
            errorListRowData = getErrorListRow();

            error_size = 0; //decodeSize(temp_error);
            error_size |= ((errorListRowData[7] << 24) & 0xff000000);
            error_size |= ((errorListRowData[6] << 16) & 0x00ff0000);
            error_size |= ((errorListRowData[5] << 8) & 0x0000ff00);
            error_size |= ((errorListRowData[4]) & 0x000000ff);

            // read the errors from the list row-by-row
            for (int i = 0; i < error_size; i++) {
                errorListRowData = getErrorListRow();
                // parse the row data
                short code = 0;
                byte param = 0;
                byte occurrence = 0;
                int timestamp = 0;
                code |= ((errorListRowData[1] << 8) & 0xff00);
                code |= ((errorListRowData[0]) & 0x00ff);
                param |= ((errorListRowData[2]) & 0xff);
                occurrence |= ((errorListRowData[3]) & 0xff);
                timestamp |= ((errorListRowData[7] << 24) & 0xff000000);
                timestamp |= ((errorListRowData[6] << 16) & 0x00ff0000);
                timestamp |= ((errorListRowData[5] << 8) & 0x0000ff00);
                timestamp |= ((errorListRowData[4]) & 0x000000ff);
                // add the error to the list
                activeErrList.add(new ErrorObject(String.format("%04X", code), param,
                        occurrence, String.format("%d", timestamp), ""));
            }

            // sync with target to let it know we are done with the list
            setBufferRelease();

            // --------------- read stored (EEPROM) errors ---------------------
            // instruct target to copy active errors to its internal error list
            setBufferSync((char) 1);
            // read out the memory address from the response. it is in bytes
            // 1..4
            error_list_addr = 0;
            error_list_addr |= ((received_data[4] << 24) & 0xff000000);
            error_list_addr |= ((received_data[3] << 16) & 0x00ff0000);
            error_list_addr |= ((received_data[2] << 8) & 0x0000ff00);
            error_list_addr |= ((received_data[1]) & 0x000000ff);
            // set the MTA to the start of the error list
            setMTA(error_list_addr, (char) 0);

            // read the first row from the error list. this one contains list
            // information, including the number of errors that are in the list
            errorListRowData = getErrorListRow();

            error_size = 0; //decodeSize(temp_error);
            error_size |= ((errorListRowData[7] << 24) & 0xff000000);
            error_size |= ((errorListRowData[6] << 16) & 0x00ff0000);
            error_size |= ((errorListRowData[5] << 8) & 0x0000ff00);
            error_size |= ((errorListRowData[4]) & 0x000000ff);

            // read the errors from the list row-by-row
            for (int i = 0; i < error_size; i++) {
                errorListRowData = getErrorListRow();
                // parse the row data
                short code = 0;
                byte param = 0;
                byte occurrence = 0;
                int timestamp = 0;
                code |= ((errorListRowData[1] << 8) & 0xff00);
                code |= ((errorListRowData[0]) & 0x00ff);
                param |= ((errorListRowData[2]) & 0xff);
                occurrence |= ((errorListRowData[3]) & 0xff);
                timestamp |= ((errorListRowData[7] << 24) & 0xff000000);
                timestamp |= ((errorListRowData[6] << 16) & 0x00ff0000);
                timestamp |= ((errorListRowData[5] << 8) & 0x0000ff00);
                timestamp |= ((errorListRowData[4]) & 0x000000ff);
                // add the error to the list
                storedErrList.add(new ErrorObject(String.format("%04X", code), param,
                        occurrence, String.format("%d", timestamp), ""));
            }

            // sync with target to let it know we are done with the list
            setBufferRelease();

            // invoke the error info processor callback
            System.out.println("ErrorInfoAvailable");
            callback.errorInfoAvailable(activeErrList, storedErrList);
        }
        // release semaphore to give others a chance to obtain mutual
        // exclusive access.
        memoryAccessSemaphore = true;
    }

    /**
     * send short user defined command codes
     *
     * @param mode (0x00 = Obtain address, 0x01 = Prepare buffer, 0x02 = Release
     * buffer)
     */
    private void xcpUSER_CC_SHORT(int len, char mode) {
        char[] par = new char[len];
        par[0] = mode;
        cl.insertCmd(cl.getCurrentID(), CC_USER_CC, par);
    }

    /**
     * send long user defined command codes
     *
     * @param option
     */
    private void xcpUSER_CC_LONG(char option) {
        char[] par = new char[2];
        par[0] = 0x01;
        par[1] = option;
        cl.insertCmd(cl.getCurrentID(), CC_USER_CC, par);
    }

    private char[] getErrorListRow() {
        char[] rowData = new char[8];
        char[] received_data = new char[8];

        // upload the first 4 bytes from the row that the MTA currently points to
        xcpUPLOAD((char) 4);
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        received_data = saveUPLOAD_DATA();
        rowData[0] = received_data[1];
        rowData[1] = received_data[2];
        rowData[2] = received_data[3];
        rowData[3] = received_data[4];
        // upload the last 4 bytes from the row that the MTA currently points to
        xcpUPLOAD((char) 4);
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        received_data = saveUPLOAD_DATA();
        rowData[4] = received_data[1];
        rowData[5] = received_data[2];
        rowData[6] = received_data[3];
        rowData[7] = received_data[4];
        // return the received row as byte array
        return rowData;
    }

    /**
     * get the starting point of the error list
     */
    public void getErrorListAddress() {
        xcpUSER_CC_SHORT(1, (char) 0x00);

        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        if (DEBUG_PRINT) {
            System.out.println("getErrorListAddress ready (" + !error + ")");
        }
    }

    /**
     * Set which error buffer to sync (active or stored)
     *
     * @param option active or stored buffer
     */
    public void setBufferSync(char option) {
        xcpUSER_CC_LONG(option);

        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        if (DEBUG_PRINT) {
            System.out.println("setBufferSync ready (" + !error + ")");
        }
    }

    /**
     * set the MTA address
     *
     * @param address MTA address
     * @param address_ext MTA address extention
     */
    public void setMTA(long address, char address_ext) {
        xcpSET_MTA(address, address_ext);
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        if (DEBUG_PRINT) {
            System.out.println("setMTA ready (" + !error + ")");
        }
    }

    /**
     * get a complete row from the errorlist
     *
     * @param len length of the row
     */
    public void getErrorRow(char len) {
        xcpUPLOAD(len);
        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        if (DEBUG_PRINT) {
            System.out.println("getErrorRow ready (" + !error + ")");
        }
    }

    public void setBufferRelease() {
        xcpUSER_CC_SHORT(1, (char) 0x02);

        startXCP();
        while (!cl.isSequenceCompleted()) {
            doNothing();
        }
        if (DEBUG_PRINT) {
            System.out.println("setBufferRelease ready (" + !error + ")");
        }
    }

    public char[] saveUPLOAD_DATA() {
        return upload_data_error;
    }

    public char[] saveUSER_DATA() {
        return user_data;
    }

    public void setConnectionFinalized(boolean connectionFinalized) {
        this.connectionFinalized = connectionFinalized;
    }
    
}
