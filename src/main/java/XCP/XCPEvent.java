/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

/**
 * Object structure for XCP Event information. Containts all information
 * the XCP master and Application layer need to know about an XCP Event.
 *
 * @author Aart-Jan
 */
public class XCPEvent {

    /**
     * Indicates whether DAQ direction is allowed for this event.
     */
    private boolean daq_allowed;
    /**
     * Indicates whether STIM direction is allowed for this event.
     */
    private boolean stim_allowed;
    /**
     * Indicates the maximum number of DAQ lists that can use this event.
     */
    private char max_daq_list = 0x00;
    /**
     * Name of this event channel.
     */
    private String name = "";
    /**
     * Indicates whether this event is cyclic and what sampling period is used
     * by the slave for this event. If value = 0x00, the event isn't cyclic.
     */
    private char time_cycle = 0x00;
    /**
     * Indicates the timestamp unit that is used by the slave for defining the
     * time unit for this event.
     *
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_100MS
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_100NS
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_100US
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_10MS
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_10NS
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_10US
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_1MS
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_1NS
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_1S
     * @see XCP.XCP#DAQ_TIMESTAMP_UNIT_1US
     */
    private char time_unit = 0x00;
    /**
     * Indicates the priority of this event. 0xFF is highest.
     */
    private char priority = 0x00;

    /**
     * Sets whether DAQ direction is allowed for this event.
     *
     * @param daq_allowed Indicates whether DAQ direction is allowed for this event.
     */
    public void setDaqAllowed(boolean daq_allowed){
        this.daq_allowed = daq_allowed;
    }

    /**
     * Returns whether DAQ direction is allowed for this event.
     *
     * @return whether DAQ direction is allowed for this event.
     */
    public boolean getDaqAllowed(){
        return daq_allowed;
    }

    /**
     * Sets whether STIM direction is allowed for this event.
     *
     * @param stim_allowed Indicates whether STIM direction is allowed for this event.
     */
    public void setStimAllowed(boolean stim_allowed){
        this.stim_allowed = stim_allowed;
    }

    /**
     * Returns whether STIM direction is allowed for this event.
     *
     * @return whether STIM direction is allowed for this event.
     */
    public boolean getStimAllowed(){
        return stim_allowed;
    }

    /**
     * Sets the maximum number of DAQ lists that can use this event.
     *
     * @param max_daq_list Indicates the maximum number of DAQ lists that can use this event.
     * @return whether this method has been executed successfully
     */
    public boolean setMaxDaqList(char max_daq_list){
        if(max_daq_list > 0xFF) return false;
        this.max_daq_list = max_daq_list;
        return true;
    }

    /**
     * Returns the maximum number of DAQ lists that can use this event.
     *
     * @return maximum number of DAQ lists that can use this event.
     */
    public char getMaxDaqList(){
        return max_daq_list;
    }

    /**
     * Sets the name of this event channel.
     *
     * @param name Name of this event channel.
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Returns the name of this event.
     *
     * @return name of this event.
     */
    public String getName(){
        return name;
    }

    /**
     * Sets whether this event is cyclic and what sample period is used by the slave for this event.
     *
     * @param time_cycle Indicates whether this event is cyclic and what sampling period is used by the slave for this event.
     * @return whether this method has been executed successfully
     * @see XCP.XCPEvent#time_cycle
     */
    public boolean setTimeCycle(char time_cycle){
        if(time_cycle > 0xFF) return false;
        this.time_cycle = time_cycle;
        return true;
    }

    /**
     * Returns whether this event is cyclic and what sample period is used by the slave for this event.
     *
     * @return whether this event is cyclic and what sample period is used by the slave for this event.
     * @see XCP.XCPEvent#time_cycle
     */
    public char getTimeCycle(){
        return time_cycle;
    }

    /**
     * Sets the timestamp unit used by the slave for defining the time unit of this event.
     *
     * @param time_unit Indicates what timestamp unit is used by the slave for defining the time unit for this event.
     * @return whether this method has been executed successfully
     * @see XCP.XCPEvent#time_unit
     */
    public boolean setTimeUnit(char time_unit){
        if(time_unit > 0xFF) return false;
        if((time_unit & 0xF0) == 0) time_unit = (char)(time_unit << 4);
        this.time_unit = time_unit;
        return true;
    }

    /**
     * Returns the timestamp unit used by the slave for defining the time unit of this event.
     *
     * @return timestamp unit
     * @see XCP.XCPEvent#time_unit
     */
    public char getTimeUnit(){
        return time_unit;
    }

    /**
     * Sets the priority of this event.
     *
     * @param priority Indicates the priority of this event.
     * @return whether this method has been executed successfully
     * @see XCP.XCPEvent#priority
     */
    public boolean setPriority(char priority){
        if(priority > 0xFF) return false;
        this.priority = priority;
        return true;
    }

    /**
     * Returns the priority of this event.
     *
     * @return priority of this event.
     * @see XCP.XCPEvent#priority
     */
    public char getPriority(){
        return priority;
    }

    /**
     * Calculates event frequency based on time unit and time cycle.
     */
    public float calcFrequency(){
        float freq = 0;
        if(time_cycle <= 0) return freq;

        switch(time_unit){
            case XCP.DAQ_TIMESTAMP_UNIT_1S:
                freq = 1 / time_cycle;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_100MS:
                freq = 10 / time_cycle;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_10MS:
                freq = 100 / time_cycle;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_1MS:
                freq = 1000 / time_cycle;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_100US:
                freq = 10000 / time_cycle;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_10US:
                freq = 100000 / time_cycle;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_1US:
                freq = 1000000 / time_cycle;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_100NS:
                freq = 10000000 / time_cycle;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_10NS:
                freq = 100000000 / time_cycle;
                break;
            case XCP.DAQ_TIMESTAMP_UNIT_1NS:
                freq = 1000000000 / time_cycle;
                break;
       }
        return freq;
    }

}
