/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

import java.util.Arrays;

/**
 * Object containing a XCP Daq Item. An XCP Daq Item can be received using
 * DAQ lists or by manually requesting using the XCP UPLOAD commands.
 *
 * @author Aart-Jan
 */
public class XCPDaqItem {
    /**
     * Identifier of the related ASAP2 element
     */
    public char element_id = 0;
    /**
     * Timestamp to indicate the moment of data generation
     */
    public long timestamp = 0;
    /**
     * Array of data bytes
     */
    public char[] data = new char[0];

    /**
     * Constructor method for initialization of a DAQ item
     *
     * @param element_id Indentifier of the related ASAP2 element
     * @param timestamp Timestamp to indicate the moment of data generation
     * @param data Array of data bytes
     */
    public XCPDaqItem(char element_id, long timestamp, char[] data){
        this.element_id = element_id;
        this.timestamp = timestamp;
        this.data = Arrays.copyOf(data, data.length);
    }

}
