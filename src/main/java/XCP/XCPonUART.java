/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

import ErrorLogger.AppendToLogfile;
import ErrorLogger.defaultExceptionHandler;
import datahandling.CurrentConfig;
import jssc.SerialPort;
import jssc.SerialPortException;
import util.Util;

/**
 * XCP Transport Layer. This class extends the XCP Protocol Layer class by
 * adding a transport layer specific interface for connecting a XCP slave device
 * and receiving and transmitting XCP data over a UART network.
 */
public class XCPonUART extends XCP {

    public static boolean DEBUG_UART = false;
    private final CurrentConfig currentConfig = CurrentConfig.getInstance();

    public SerialPort serialPort;

    private class XCPUARTMessage {

        private final int dlc;
        private int cs;
        private final char[] data;

        XCPUARTMessage(int length, char[] data) {
            this.dlc = length; // Packet length
            this.data = data;
            this.cs = 0;
            for(int i=0; i<data.length; i++){
                this.cs = (this.cs + (data[i] & 0xFF) & 0xFF);
            }
        }

        byte[] getRawData() {
            char[] ret = new char[data.length + 2];
            ret[0] = (char) (dlc & 0xFF);
            System.arraycopy(data, 0, ret, 1, data.length);
            ret[ret.length-1] = (char) (cs & 0xFF);
            return Util.charA2byteA(ret);
        }
    }

    public XCPonUART() {
        super();

        // UART specific settings
        useSlaveTimestamp = false; // The slave does generate a timestamp for each sample
        allow64BitSignals = true; // The slave allows to use 64 bit signals
        xcpTimeOutTimer.startTimer();
    }

    /**
     * Makes a connection to a XCP slave over a UART network.
     *
     * @return whether this method has been executed successfully
     */
    @Override
    protected boolean connect() {
        currentConfig.setTimeOffsetMicrosEmpty();
        // retrieve settings
        String UARTPort = currentConfig.getXcpsettings().getUARTPort();
        int UARTBaudrate = currentConfig.getXcpsettings().getUARTBaudrate();

        if (serialPort == null) {
            // create new object
            serialPort = new SerialPort(UARTPort);

            try {
                // open port
                boolean portOpened = serialPort.openPort();
                if(!portOpened){
                    setError("Could not connect to UART port " + UARTPort + ": opening the port failed", true);
                }

                // set port parameters
                boolean portConfigured = serialPort.setParams(UARTBaudrate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
                if(!portConfigured){
                    setError("Could not connect to UART port " + UARTPort + ": configuring the port parameters failed", true);
                }
            } catch (SerialPortException ex) {
                AppendToLogfile.appendMessage("Could not connect to UART port " + UARTPort + ": " + ex.getMessage());
                AppendToLogfile.appendError(currentThread(), ex);
                setError("Could not connect to UART port " + UARTPort + ": " + ex.getMessage(), true);
                return false;
            }

            // start xcp communication
            connected = true;

            // start listening for data
            new Thread(new Runnable() {

                @Override
                public void run() {
                    currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
                    try {
                        int n, cs, cs_calc;

                        while(serialPort.isOpened()){
                        // read the first and second byte : message length
                            n = serialPort.readIntArray(1)[0];

                            // read the remaining bytes of the message : the DAQ list data
                            byte[] data = serialPort.readBytes(n);

                            // read checksum
                            cs = serialPort.readIntArray(1)[0];

                            // calculate checksum
                            cs_calc = 0;
                            for(int i=0; i<data.length; i++){
                                cs_calc = (cs_calc + (data[i] & 0xFF) & 0xFF);
                            }

                            // debug message?
                            if (DEBUG_UART) {
                                if (data.length == 0) {
                                    System.out.print(".");
                                } else {
                                    System.out.println("UART RECEIVED (" + (data.length + 2) + "):");
                                    System.out.print(Integer.toHexString(n & 0xFF) + " ");
                                    for (int i = 0; i < data.length; i++) {
                                        System.out.print(Integer.toHexString(data[i] & 0xFF) + " ");
                                    }
                                    System.out.println(Integer.toHexString(cs & 0xFF) + " ");
                                }
                            }

                            // only process the XCP message if the checksum is valid
                            if(cs == cs_calc){
                                // pass data to XCP Protocol Layer
                                receiveData(Util.byteA2charA(data, data.length),
                                    XCPonCANBasic.calculateTimeStampMicros());
                            }
                        }
                    } catch (SerialPortException ex) {
                        if (connected) {
                            AppendToLogfile.appendError(currentThread(), ex);
                        }
                    }
                }

            }).start();
        }
        return true;
    }

    /**
     * Closes a connection to a XCP slave over the UART network.
     *
     * @return whether this method has been executed successfully
     */
    @Override
    protected boolean disconnect() {
        if (serialPort != null) {
            try {
                serialPort.closePort();
            } catch (SerialPortException ex) {
                AppendToLogfile.appendMessage("Could not disconnect stream: " + ex.getMessage());
                AppendToLogfile.appendError(currentThread(), ex);
                setError("Could not disconnect stream: " + ex.getMessage(), true);
                return false;
            }

            // stop xcp communication
            stopXCP();
            connected = false;
        }
        return true;
    }

    /**
     * Sends an array of databytes to the XCP slave device over the UART network.
     *
     * @param data message data array
     * @return whether the data has been send succesfully
     */
    @Override
    protected boolean sendData(char[] data) {
        // check connection
        if (!connected) {
            return false;
        }

        // send message and return status
        try {
            XCPUARTMessage xcptcpm = new XCPUARTMessage(data.length, data);
            byte[] raw = xcptcpm.getRawData();
            if (DEBUG_UART) {
                System.out.println("UART SENDING (" + raw.length + "):");
                for(int i=0; i<raw.length; i++) System.out.print(Integer.toHexString(raw[i] & 0xFF) + " ");
                System.out.print("\r\n");
            }
            serialPort.writeBytes(raw);

            XCP_waiting = true;
            xcpTimeOutTimer.setTimeInMiliseconds(0);
            cl.incrementCurrentID();
        } catch (SerialPortException ex) {
            AppendToLogfile.appendMessage("Could not write message: " + ex.getMessage());
            AppendToLogfile.appendError(currentThread(), ex);
            System.out.println("Could not write message: " + ex.getMessage());
            return false;
        }
        return true;
    }

}
