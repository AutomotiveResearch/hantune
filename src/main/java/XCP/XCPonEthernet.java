/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;

import ErrorLogger.AppendToLogfile;
import ErrorLogger.defaultExceptionHandler;
import datahandling.CurrentConfig;
import util.MessagePane;
import util.Util;

/**
 * XCP Transport Layer. This class extends the XCP Protocol Layer class by
 * adding a transport layer specific interface for connecting a XCP slave device
 * and receiving and transmitting XCP data over an Ethernet network.
 */
public class XCPonEthernet extends XCP {

    public static boolean DEBUG_ETHERNET = false;
    private final CurrentConfig currentConfig = CurrentConfig.getInstance();

    private Socket ethernetSocket;

    private static short cmdCounter = 0;
    private static short cmdCounterPrevious = 0;
    private static short daqCounter = 0;

    private static long missingPackets = 0;

    protected static class XCPTCPMessage {

        private final short dlc;
        private final short ctr;
        private final char[] data;

        XCPTCPMessage(int length, char[] data) {
            this.dlc = (short) length; // Packet length
            this.ctr = (data[0] & 0xFF) >= 0xFC ? cmdCounter : daqCounter++; // Packet identifier
            this.data = data;
        }

        byte[] getRawData() {
            char[] ret = new char[data.length + 4];
            ret[0] = (char) (dlc & 0xFF);
            ret[1] = (char) ((dlc >> 8) & 0xFF);
            ret[2] = (char) (ctr & 0xFF);
            ret[3] = (char) ((ctr >> 8) & 0xFF);
            System.arraycopy(data, 0, ret, 4, data.length);
            return Util.charA2byteA(ret);
        }
    }

    public XCPonEthernet() {
        super();

        // Ethernet specific settings
        combineODT = true; // Ethernet allows to combine multiple items per ODT
        useSlaveTimestamp =  false; // The slave does not generate a timestamp for each sample
        allow64BitSignals = true; // The slave allows to use 64 bit signals

        xcpTimeOutTimer.startTimer();
    }

    /**
     * Makes a connection to a XCP slave over an Ethernet network.
     *
     * @return whether this method has been executed successfully
     */
    @Override
    protected boolean connect() {
        if (ethernetSocket == null) {
            // retrieve settings
            String ip = currentConfig.getXcpsettings().getEthernetIP();
            int port = currentConfig.getXcpsettings().getEthernetPort();

            try {
                // establish connection
                ethernetSocket = new Socket(ip, port);
                ethernetSocket.setSoTimeout(3000*currentConfig.getXcpsettings().getTPrescaler());
                // disable Nagle's algorithm to decrease latency (at the cost of increased bandwidth)
                ethernetSocket.setTcpNoDelay(true);

                // set traffic class
                // IPTOS_LOWCOST (0x02)
                // IPTOS_RELIABILITY (0x04)
                // IPTOS_THROUGHPUT (0x08)
                // IPTOS_LOWDELAY (0x10)
                ethernetSocket.setTrafficClass(0x10);
            } catch (Exception e) {
                AppendToLogfile.appendMessage("Could not connect to " + ip + ":" + port + " : " + e.getMessage());
                AppendToLogfile.appendError(currentThread(), e);
                setError("Could not connect to " + ip + ":" + port + " : " + e.getMessage(), true);
                return false;
            }

            // start xcp communication
            connected = true;

            // start listening for data
            new Thread(new Runnable() {

                @Override
                public void run() {
                    currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
                    try (DataInputStream input = new DataInputStream(ethernetSocket.getInputStream())) {

                        // read the first and second byte : message length
                        int firstByte;
                        while((firstByte = input.read()) >= 0){
                            int secondByte = input.read();
                            int messageLength = (secondByte << 8) | firstByte;

                            // read the third and fourth byte : message counter
                            cmdCounter = Short.reverseBytes(input.readShort());

                            // missing packets?
                            if(cmdCounter != (cmdCounterPrevious+1)){
                                missingPackets += (cmdCounter - cmdCounterPrevious - 1);
                            }
                            cmdCounterPrevious = cmdCounter;

                            // read the remaining bytes of the message : the DAQ list data
                            byte[] data = new byte[messageLength];
                            if(ethernetSocket.isClosed()) break;
                            input.readFully(data);

                            // debug message?
                            if (DEBUG_PRINT) {
                                System.out.print(
                                    "ETHERNET RECEIVED (" + data.length + ") - Counter : " + cmdCounter
                                        + ", Data : " );
                                for (byte dat : data) {
                                    System.out.print(" 0x" + Integer.toHexString(dat & 0xFF));
                                }
                                System.out.println();
                            }

                            // pass data to XCP Protocol Layer
                            receiveData(Util.byteA2charA(data, data.length),
                                XCPonCANBasic.calculateTimeStampMicros());
                        }
                    } catch (IOException ex) {
                        AppendToLogfile.appendMessage("Error while reading message: " + ex.getMessage());
                        AppendToLogfile.appendError(currentThread(), ex);

                        if ("Socket closed".equals(ex.getMessage())){
                            MessagePane.showWarning("Your Ethernet connection seems to have closed. \n"
                                                  + "This can be caused by the power settings of your PC \n"
                                                  + "Please disable all power save, sleep and hybernation options ");
                        }
                        if (DEBUG_PRINT) System.out.println("Error while reading message: " + ex.getMessage());

                    }
                }

            }).start();
        }
        return true;
    }

    /**
     * Closes a connection to a XCP slave over the Ethernet network.
     *
     * @return whether this method has been executed successfully
     */
    @Override
    protected boolean disconnect() {
        if (ethernetSocket != null && !ethernetSocket.isClosed()) {
            try {
                ethernetSocket.shutdownOutput();
            } catch (IOException ex) {
                AppendToLogfile.appendMessage("Could not disconnect stream: " + ex.getMessage());
                AppendToLogfile.appendError(currentThread(), ex);
                setError("Could not disconnect stream: " + ex.getMessage(), true);
                return false;
            }

            // stop xcp communication
            stopXCP();
            connected = false;
        }
        return true;
    }

    /**
     * Sends an array of databytes to the XCP slave device over the Ethernet
     * network.
     *
     * @param data message data array
     * @return whether the data has been send succesfully
     */
    @Override
    protected boolean sendData(char[] data) {
        // check connection
        if (!connected || ethernetSocket.isClosed()) {
            return false;
        }

        // send message and return status
        try {
            XCPTCPMessage xcptcpm = new XCPTCPMessage(data.length, data);
            byte[] raw = xcptcpm.getRawData();
            if (DEBUG_PRINT) {
                System.out.println("ETHERNET SENDING (" + raw.length + ") : " + Arrays.toString(raw));
            }
            ethernetSocket.getOutputStream().write(raw);

            XCP_waiting = true;
            xcpTimeOutTimer.setTimeInMiliseconds(0);
            cl.incrementCurrentID();
        } catch (IOException ex) {
            AppendToLogfile.appendMessage("Could not write message: " + ex.getMessage());
            AppendToLogfile.appendError(currentThread(), ex);
            System.out.println("Could not write message: " + ex.getMessage());
            return false;
        }
        return true;
    }
}
