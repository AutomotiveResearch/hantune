/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

/**
 * Count the number of successful commands before the target gets out-of-memory error
 * Use this count to limit size of XCP DAQ lists
 */
public class XcpTargetDaqlistSizeLimiter {
    private int succesfulCommandCount = 0;
    private boolean targetSizeLimited = false;

    void resetSuccesfulCommandCounter() {
        succesfulCommandCount = 0;
        targetSizeLimited = false;
    }

    void incrementSuccesfulCommandCounter() {
        if (!targetSizeLimited) {
            succesfulCommandCount++;
        }
    }

    void setDaqListSizeLimited() {
        targetSizeLimited = true;
    }

    int getLimitedSize() {
        if (targetSizeLimited) {
            return succesfulCommandCount;
        } else {
            return 0;
        }
    }

    boolean isTargetSizeLimited() {
        return targetSizeLimited;
    }

}
