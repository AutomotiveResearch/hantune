/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import ErrorLogger.AppendToLogfile;
import HANtune.HANtuneWindowFactory.HANtuneWindowType;
import components.sidepanel.NewWindowMenuManager;
import datahandling.CurrentConfig;
import datahandling.Parameter;
import datahandling.Reference;
import datahandling.Signal;
import datahandling.Table;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.KeyboardFocusManager;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JLayeredPane;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

import nl.han.hantune.scripting.Script;
import nl.han.hantune.gui.tab.TabSelectionManager;
import util.MessagePane;

/**
 * Contains all the functionality related to a tab in the HANtune layout.
 * Note: this is not actually the tab, but the contents of a tab as used in a JTabbedPane.
 *
 * @author Aart-Jan, Michiel Klifman
 */
@SuppressWarnings("serial")
public class HANtuneTab extends JDesktopPane{

    private String title = "";
    public static final int MAX_WINDOWS = 50;
    private TabSelectionManager selectionManager;
    private final List<DataFlavor> flavors = new ArrayList<>();
    private boolean resizeAndMoveModeEnabled;
    private boolean ctrlDown;
    private long ctrlDownStartTime;

    public HANtuneTab(String title){
        this.title = title;
        setRequestFocusEnabled(false);
        setBackground(new java.awt.Color(255, 255, 255));
        setDragMode(JDesktopPane.LIVE_DRAG_MODE);
        setTabPopupMenu();
        setDataFlavors();
        setTransferHandler();
        setKeyboardFocusManagerWithTimer();
        addSelectionMouseListeners();
        setDeleteKey();
    }

    public String getTitle() {
        return title;
    }

    /**
     * Adds a new window of the given type to this HANtuneTab.
     * @param hantuneWindowType the type of the window that should be added.
     * @return the newly added window or null if this tab's limit has been reached
     */
    public HANtuneWindow addWindow(HANtuneWindowType hantuneWindowType){

        if(getComponentCount() >= MAX_WINDOWS){
            MessagePane.showError("You have already reached the limit of " + MAX_WINDOWS + " windows for this tab.");
            return null;
        }
        HANtuneWindow hantuneWindow =  HANtuneWindowFactory.getInstance().createHANtuneWindow(hantuneWindowType);
        add(hantuneWindow, JLayeredPane.DEFAULT_LAYER);

        if(resizeAndMoveModeEnabled){
            hantuneWindow.setResizeAndMoveMode(true);
        }

        hantuneWindow.toFront();
        CurrentConfig.getInstance().getHANtuneManager().addActiveWindowToList(hantuneWindow);
        selectionManager.subscribeWindow(hantuneWindow);
        adaptSize();
        return hantuneWindow;
    }

    public void adaptSize() {
        Dimension newTabSize = new Dimension(0, 0);

        for (Component component : getComponents()) {

            int rightBound = component.getX() + component.getWidth();
            int bottomBound = component.getY() + component.getHeight();

            if (rightBound > newTabSize.width) {
                newTabSize.width = rightBound;
            }
            if (bottomBound > newTabSize.height) {
                newTabSize.height = bottomBound;
            }
        }

        Dimension currentSize = this.getSize();
        if (newTabSize.width != currentSize.width || newTabSize.height != currentSize.height) {
            setPreferredSize(newTabSize);
            revalidate();
        }
    }

    private void setTabPopupMenu() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                if (SwingUtilities.isRightMouseButton(event)) {
                    if (!CurrentConfig.getInstance().isServiceToolMode()) {
                        JPopupMenu menu = createPopupMenu(event.getX(), event.getY());
                        menu.show(event.getComponent(), event.getX(), event.getY());
                    }
                }
                else if(SwingUtilities.isLeftMouseButton(event)){
                    grabFocus();                                                //Make all other windows lose focus
                }
            }
        });
    }

    private JPopupMenu createPopupMenu(int x, int y) {

        NewWindowMenuManager menuManager = new NewWindowMenuManager() {
            @Override
            public <T extends Reference> ReferenceWindow<T> handleMenuItemAction(HANtuneWindowType windowType, Class<T> referenceType) {
                ReferenceWindow<T> window = super.handleMenuItemAction(windowType, referenceType);
                window.setLocation(x, y);
                return window;
            }
        };

        JPopupMenu menu = menuManager.getAllWindowsMenu();
        JCheckBoxMenuItem resizeAndMoveModeMenuItem = new JCheckBoxMenuItem("Resize and Move mode");
        resizeAndMoveModeMenuItem.addActionListener(e -> enableResizeAndMoveMode(!resizeAndMoveModeEnabled));
        resizeAndMoveModeMenuItem.setSelected(resizeAndMoveModeEnabled);
        resizeAndMoveModeMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));
        menu.add(resizeAndMoveModeMenuItem);
        return menu;
    }

    /**
     * Not in use atm. This method would enable "Resize and Move" mode when ctrl
     * was pressed and disable when released. This has been replaced by ctrl+r
     * and a toggle feature because using ctrl only caused some issues. Call
     * this method in the constructor to enable this feature again.
     */
    private void setKeyboardFocusManager() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher((KeyEvent e) -> {
            if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
                enableResizeAndMoveMode(e.getID() == KeyEvent.KEY_PRESSED);
            }
            return false;
        });
    }

    private void setKeyboardFocusManagerWithTimer() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher((KeyEvent e) -> {
            if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
                if (e.getID() == KeyEvent.KEY_PRESSED && !ctrlDown) {
                    ctrlDown = true;
                    ctrlDownStartTime = System.currentTimeMillis();
                } else if (e.getID() == KeyEvent.KEY_RELEASED) {
                    long duration = System.currentTimeMillis() - ctrlDownStartTime;
                    if (duration < 200 && ctrlDownStartTime > 0) {
                        enableResizeAndMoveMode(!isResizeAndMoveModeEnabled());
                    }
                    ctrlDownStartTime = 0;
                    ctrlDown = false;
                }
            }
            return false;
        });
    }

    private void setKeyboardFocusManagerWithShift() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher((KeyEvent e) -> {
            if (e.getKeyCode() == KeyEvent.VK_CONTROL || e.getKeyCode() == KeyEvent.VK_SHIFT) {
                enableResizeAndMoveMode(e.isShiftDown() && e.isControlDown());
            }
            return false;
        });
    }

    private void setDeleteKey() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher((KeyEvent e) -> {
            boolean deleteReleased = e.getKeyCode() == KeyEvent.VK_DELETE && e.getID() == KeyEvent.KEY_RELEASED;
            int indexOfCurrentTab = HANtune.getInstance().getTabbedPane().getSelectedIndex();
            HANtuneTab activeTab = CurrentConfig.getInstance().getHANtuneManager().getTab(indexOfCurrentTab);

            if (deleteReleased && (HANtuneTab.this == activeTab) && resizeAndMoveModeEnabled) {
                selectionManager.closeSelectedWindows();
            }
            return false;
        });
    }

    /**
     * @return A list of all HANtuneWindows in this tab
     */
    public List<HANtuneWindow> getWindows() {
        return Arrays.stream(this.getAllFrames())
                .filter(HANtuneWindow.class::isInstance)
                .map(HANtuneWindow.class::cast)
                .collect(Collectors.toList());
    }

    public void enableResizeAndMoveMode(boolean enable) {
        if (CurrentConfig.getInstance().isServiceToolMode()) {
            return;
        }

        resizeAndMoveModeEnabled = enable;
        for (Component component : this.getAllFrames()) {
            if (component instanceof HANtuneWindow) {
                HANtuneWindow window = (HANtuneWindow) component;
                if (!window.isMarkedForDeletion()) {
                    window.setResizeAndMoveMode(enable);
                }
            }
        }
    }

    public boolean isResizeAndMoveModeEnabled() {
        return resizeAndMoveModeEnabled;
    }

    /**
     * Add the TabSelectionmanager as mouse listeners to the tab
     */
    private void addSelectionMouseListeners() {
        selectionManager = new TabSelectionManager(this);
        addMouseListener(selectionManager);
        addMouseMotionListener(selectionManager);
    }

    /**
     * Will draw a "selection rectangle" on the tab if "Resize and Move" mode is
     * enabled and the TabSelectionManager is currently dragging
     *
     * @param g the Graphics object to draw on
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (selectionManager.isDragging() && resizeAndMoveModeEnabled) {
            selectionManager.drawSelectionRectangle(g);
        }
    }

    private void setDataFlavors() {
        flavors.add(Signal.DATA_FLAVOR);
        flavors.add(Parameter.DATA_FLAVOR);
        flavors.add(Table.DATA_FLAVOR);
        flavors.add(Script.DATA_FLAVOR);
    }

    private void setTransferHandler() {
        this.setTransferHandler(new TransferHandler() {
            DataFlavor supportedFlavor;
            @Override
            public boolean canImport(TransferHandler.TransferSupport support) {
                for (DataFlavor flavor : flavors) {
                    if (support.isDataFlavorSupported(flavor)) {
                        supportedFlavor = flavor;
                        return true;
                    }
                }
                return false;
            }

            @Override
            @SuppressWarnings("unchecked")
            public boolean importData(TransferHandler.TransferSupport support) {
                if (!canImport(support)) {
                    return false;
                }

                try {
                    List<DataFlavor> flavors = Arrays.asList(support.getDataFlavors());
                    List<Reference> data = (List<Reference>) support.getTransferable().getTransferData(supportedFlavor);
                    int x = HANtuneTab.this.getMousePosition().x;
                    int y = HANtuneTab.this.getMousePosition().y;
                    handleTransferData(data, flavors, x, y);
                } catch (UnsupportedFlavorException | IOException exception) {
                    AppendToLogfile.appendError(Thread.currentThread(), exception);
                    return false;
                }
                return true;
            }
        });
    }

    private void handleTransferData(List<Reference> data, List<DataFlavor> flavors, int x, int y) {
        if (CurrentConfig.getInstance().isServiceToolMode()) {
            return; // No popup menu when in Service Tool mode
        }

        NewWindowMenuManager menuManager = new NewWindowMenuManager() {
            @Override
            @SuppressWarnings("unchecked")
            public <T extends Reference> ReferenceWindow<T> handleMenuItemAction(HANtuneWindowType windowType, Class<T> referenceType) {
                ReferenceWindow<T> window = super.handleMenuItemAction(windowType, referenceType);
                window.addMultipleReferences((List<T>) data);
                window.setLocation(x, y);
                return window;
            }
        };

        JPopupMenu menu = null;
        if (flavors.contains(Signal.DATA_FLAVOR) && flavors.contains(Parameter.DATA_FLAVOR)) {
            menu = data.size() <= 1 ? menuManager.getViewerAndEditorMenu() : menuManager.getMultiViewerAndEditorMenu();
        } else if (flavors.contains(Signal.DATA_FLAVOR)) {
            menu = data.size() <= 1 ? menuManager.getViewerMenu() : menuManager.getMultiViewerMenu();
        } else if (flavors.contains(Parameter.DATA_FLAVOR)) {
            menu = data.size() <= 1 ? menuManager.getEditorMenu() : menuManager.getMultiEditorMenu();
        } else if (flavors.contains(Table.DATA_FLAVOR)) {
            menuManager.handleMenuItemAction(HANtuneWindowType.TableEditor, Table.class);
        } else if (flavors.contains(Script.DATA_FLAVOR)) {
            menuManager.handleMenuItemAction(HANtuneWindowType.ScriptWindow, Script.class);
        }

        if (menu != null) {
            menu.show(this, x, y);
        }
    }
}
