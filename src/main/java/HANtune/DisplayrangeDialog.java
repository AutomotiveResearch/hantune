/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import ErrorLogger.AppendToLogfile;
import HANtune.viewers.BarViewer;
import HANtune.viewers.GaugeViewer;
import datahandling.ValueDescription;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.*;

import util.MessagePane;
import util.Util;

/**
 *
 * @author HP G7
 */
@SuppressWarnings("serial")
public class DisplayrangeDialog extends HanTuneDialog {

    private HANtuneWindow hantuneWindow = null;
    private ValueDescription reference;
    private Color color;
    private Color defaultColor;
    /**
     * A return status code - returned if Cancel button has been pressed
     */
    public static final int RET_CANCEL = 0;
    /**
     * A return status code - returned if OK button has been pressed
     */
    public static final int RET_OK = 1;

    public DisplayrangeDialog(HANtuneWindow hantuneWindow, ValueDescription reference, boolean editableColor) {
        this(hantuneWindow, reference);
        if (!editableColor) {
            getContentPane().remove(colorField);
            getContentPane().remove(jButton1);
            pack();
        }
    }

    /**
     * Creates new form DisplayrangeDialog
     *
     * @param hantuneWindow the window for which the limits should be set
     * @param reference the asap2ref of the signal
     */
    public DisplayrangeDialog(HANtuneWindow hantuneWindow, ValueDescription reference) {
        this.hantuneWindow = hantuneWindow;
        this.reference = reference;

        // set program window icon
        Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/HANlogo.png"));
        this.setIconImage(img);

        initComponents();
        loadSettings();

        setTitle(reference + " - Display range");
        setLocationRelativeTo(null);
        setVisible(true);

        if ((reference.getMinimum() < -1000000 || reference.getMaximum() > 1000000) && reference.hasDecimals()) {
            displayLower.setEditor(new JSpinner.NumberEditor(displayLower, "0.######E0"));
        }

        SpinnerNumberModel lowerSpinnerModel = (SpinnerNumberModel) displayLower.getModel();
        lowerSpinnerModel.setMinimum(reference.getMinimum());
        lowerSpinnerModel.setMaximum(reference.getMaximum());

        if ((reference.getMinimum() < -1000000 || reference.getMaximum() > 1000000) && reference.hasDecimals()) {
            displayUpper.setEditor(new JSpinner.NumberEditor(displayUpper, "0.######E0"));
        }

        SpinnerNumberModel upperSpinnerModel = (SpinnerNumberModel) displayUpper.getModel();
        upperSpinnerModel.setMinimum(reference.getMinimum());
        upperSpinnerModel.setMaximum(reference.getMaximum());

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            private static final long serialVersionUID = 7526471155622776151L;
            @Override
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     *
     * The method loops through all asap2 references and will load the settings
     * for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet supported. This HANtuneWindow allows for just a single asap2
     * reference to be present. The developer will informed by a AssertException
     * otherwise
     */
    private void loadSettings() {

        if (hantuneWindow instanceof BarViewer) {
            defaultColor = BarViewer.DEFAULTCOLOR;
        } else if (hantuneWindow instanceof GaugeViewer) {
            defaultColor = GaugeViewer.DEFAULTCOLOR;
        }

        double minimum;
        double maximum;

        if (hantuneWindow.hasSetting(reference.getName(), "limit_lower")) {
            minimum = (Double.parseDouble(hantuneWindow.settings.get(reference.getName()).get("limit_lower")));
        } else {
            minimum = reference.getMinimum();
        }
        if (hantuneWindow.hasSetting(reference.getName(), "limit_upper")) {
            maximum = (Double.parseDouble(hantuneWindow.settings.get(reference.getName()).get("limit_upper")));
        } else {
            maximum = reference.getMaximum();
        }

        displayLower.setValue(minimum);
        displayUpper.setValue(maximum);

        if (hantuneWindow.hasSetting(reference.getName(), "color")) {
            //try and catch for backwards compatibility
            try {
                color = (Color.decode(hantuneWindow.settings.get(reference.getName()).get("color")));
                colorField.setBackground(color);
            } catch (NumberFormatException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                AppendToLogfile.appendMessage("try and catch for backwards compatibility");
                color = (Util.getColor(hantuneWindow.settings.get(reference.getName()).get("color")));
                colorField.setBackground(color);
            }
        } else {
            colorField.setBackground(defaultColor);
        }
    }

    /**
     * @return the return status of this dialog - one of RET_OK or RET_CANCEL
     */
    public int getReturnStatus() {
        return returnStatus;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        okButton = new javax.swing.JButton();
        getRootPane().setDefaultButton(okButton);
        cancelButton = new javax.swing.JButton();
        displayLowerLabel = new javax.swing.JLabel();
        displayLower = new javax.swing.JSpinner();
        displayUpper = new javax.swing.JSpinner();
        displayUpperLabel = new javax.swing.JLabel();
        buttonDefault = new javax.swing.JButton();
        colorField = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(okButton, gridBagConstraints);

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(cancelButton, gridBagConstraints);

        displayLowerLabel.setText("Lower limit");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(displayLowerLabel, gridBagConstraints);

        displayLower.setModel(new javax.swing.SpinnerNumberModel(Double.valueOf(0.0d), null, null, Double.valueOf(1.0d)));
        displayLower.setEditor(new javax.swing.JSpinner.NumberEditor(displayLower, "#.#"));
        displayLower.setPreferredSize(new java.awt.Dimension(120, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(displayLower, gridBagConstraints);

        displayUpper.setModel(new javax.swing.SpinnerNumberModel(Double.valueOf(0.0d), null, null, Double.valueOf(1.0d)));
        displayUpper.setEditor(new javax.swing.JSpinner.NumberEditor(displayUpper, "#.#"));
        displayUpper.setPreferredSize(new java.awt.Dimension(120, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(displayUpper, gridBagConstraints);

        displayUpperLabel.setText("Upper limit");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(displayUpperLabel, gridBagConstraints);

        buttonDefault.setText("Reset to default");
        buttonDefault.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDefaultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(buttonDefault, gridBagConstraints);

        colorField.setPreferredSize(new java.awt.Dimension(120, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(colorField, gridBagConstraints);

        jButton1.setText("Edit color");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(jButton1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(jSeparator1, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        doClose(RET_OK);
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        doClose(RET_CANCEL);
    }//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    private void buttonDefaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDefaultActionPerformed
        displayLower.setValue(reference.getMinimum());
        displayUpper.setValue(reference.getMaximum());
        colorField.setBackground(defaultColor);
    }//GEN-LAST:event_buttonDefaultActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Color newColor = JColorChooser.showDialog(DisplayrangeDialog.this, "Choose color", (colorField.getBackground()));
        if (!(newColor == null)) {
            //set new color
            colorField.setBackground(newColor);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void doClose(int retStatus) {
        returnStatus = retStatus;
        if (returnStatus == RET_OK) {
            if (Double.parseDouble(displayLower.getValue().toString()) > Double.parseDouble(displayUpper.getValue().toString())) {
                MessagePane.showError("The Upper limit must be greater than the lower limit");
                return;
            }
            if (hantuneWindow instanceof BarViewer || hantuneWindow instanceof GaugeViewer) {
                if (Util.getDoubleValue(displayLower.getValue()) < -1E10d) {
                    MessagePane.showError("The lower limit exceeds the minimum value of this viewer (-1E10)");
                    return;
                }
                if (Util.getDoubleValue(displayUpper.getValue()) > 1E10d) {
                    MessagePane.showError("The upper limit exceeds the maximum value of this viewer (1E10)");
                    return;
                }
            }
            hantuneWindow.addSetting(reference.getName(), "limit_lower", displayLower.getValue().toString());
            hantuneWindow.addSetting(reference.getName(), "limit_upper", displayUpper.getValue().toString());
            if (colorField.getBackground() != null) {
                hantuneWindow.addSetting(reference.getName(), "color", Integer.toString(colorField.getBackground().hashCode()));
            }

            hantuneWindow.loadSettings();
        }
        setVisible(false);
        dispose();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonDefault;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel colorField;
    public javax.swing.JSpinner displayLower;
    private javax.swing.JLabel displayLowerLabel;
    public javax.swing.JSpinner displayUpper;
    private javax.swing.JLabel displayUpperLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton okButton;
    // End of variables declaration//GEN-END:variables
    private int returnStatus = RET_CANCEL;
}
