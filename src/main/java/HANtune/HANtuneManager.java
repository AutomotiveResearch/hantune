/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.prefs.Preferences;
import java.util.stream.IntStream;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.border.EmptyBorder;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Characteristic.Type;
import ASAP2.ASAP2Data;
import ASAP2.ASAP2Datatype;
import ASAP2.ASAP2Import;
import ASAP2.ASAP2Measurement;
import ASAP2.ASAP2Object;
import ASAP2.ASAP2XmlImport;
import DAQList.DAQList;
import DAQList.DAQListDialog;
import ErrorLogger.AppendToLogfile;
import ErrorLogger.ErrorLogRestrictor;
import ErrorMonitoring.ErrorObject;
import ErrorMonitoring.ErrorViewer;
import HANtune.CustomFileChooser.FileType;
import HANtune.HANtuneWindowFactory.HANtuneWindowType;
import HANtune.actions.LogReaderAction;
import XCP.XCP;
import XCP.XCPDaqList;
import static XCP.XCPSettings.EthernetProtocol.TCP;
import static XCP.XCPSettings.EthernetProtocol.UDP;
import can.DbcManager;
import can.DbcObject;
import can.DbcParser;
import can.DbcXmlImport;
import datahandling.ASAP2ConvertToXmlImpl;
import datahandling.ConvertToXml;
import datahandling.CurrentConfig;
import datahandling.DbcConvertToXmlImpl;
import datahandling.DescriptionFile;
import datahandling.Reference;
import datahandling.SendParameterDataHandler;
import datahandling.ShmlHmlConverter;
import haNtuneHML.Daqlist;
import haNtuneHML.Daqlist.Signal;
import haNtuneHML.HANtuneDocument;
import haNtuneHML.HANtuneDocument.HANtune.DbcFile;
import haNtuneHML.Layout;
import haNtuneHML.Layout.Tab;
import haNtuneHML.Layout.Tab.Window;
import haNtuneHML.RemoteLogList;
import haNtuneHML.Settings;
import haNtuneHML.Settings.Setting;
import haNtuneSHML.ShmlDocument;
import haNtuneSHML.ShmlDocument.Shml;
import nl.han.hantune.config.VersionInfo;
import nl.han.hantune.datahandling.DataLogger.LogDataWriterCsv;
import nl.han.hantune.gui.dialogs.ModifyLogListDialog;
import nl.han.hantune.scripting.Script;
import nl.han.hantune.scripting.ScriptingManager;
import nl.han.hantune.scripting.jython.JythonScript;
import util.CursorWait;
import util.MessagePane;
import util.MouseEventDispatcher;
import util.Util;

/**
 * @author Aart-Jan
 */
public class HANtuneManager {

    public static final int MAX_TABS = 50;
    private static final int RECENT_PROJECTS_LIST_SIZE = 15;
    private CurrentConfig currentConfig = CurrentConfig.getInstance();
    private String error = "";
    private final HANtune hantune;
    private Preferences userPrefs = Preferences.userNodeForPackage(UserPreferences.class);
    private int DaqlistSize;
    private SendParameterDataHandler sendDataHandler = new SendParameterDataHandler(1);
    private final List<HANtuneWindow> activeWindows = new ArrayList<>();
    private final List<HANtuneTab> tabs = new ArrayList<>();
    private final List<ASAP2Measurement> logList = new ArrayList<>();

    public HANtuneManager(HANtune hantune) {
        this.hantune = hantune;
    }

    /**
     * method to Start HANtune.
     */
    public void startHantune() {
        currentConfig.setHANtuneManager(this);

        hantune.initHANtune();
        hantune.setVisible(true);

        AppendToLogfile.writeSystemInfo();
        ErrorLogRestrictor.Restrictor();
    }

    public String getProjectPathFromArguments(String[] arguments) {
        for (String argument : arguments) {
            if (validatePath(argument)) {
                return argument;
            }
        }
        return null;
    }

    private boolean validatePath(String path) {
        File file = new File(path);
        if (file.exists()) {
            return file.getName().endsWith(".hml");
        }
        return false;
    }

    /**
     * gets the SendParameterDataHandler in HANtuneManager
     *
     * @return SendParameterDataHandler
     */
    public SendParameterDataHandler getSendParameterDataHandler() {
        return sendDataHandler;
    }

    /**
     * Returns the last set error message
     */
    public String getError() {
        return this.error;
    }

    /**
     * Sets the error message
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * Displays a question box to the user, for adding a new tab to the screen.
     */
    public HANtuneTab addTab() {
        HANtuneTab hantuneTab = null;
        String title = HANtune.showQuestion("Add Tab", "Title:", "");
        if (title != null) {
            hantuneTab = addTab(title);
        }
        return hantuneTab;
    }

    /**
     * Adds a new tab with the specified title to the screen.
     *
     * @param title
     */
    @SuppressWarnings("serial")
    public HANtuneTab addTab(String title) {
        HANtuneTab hantuneTab = null;
        title = title.trim();
        if (title.length() > 30) {
            title = title.substring(0, 30);
        }
        if (title.equals("")) {
            MessagePane.showError("Please enter a title!");
            addTab();
        }
        if (!(hantune.getTabbedPane().indexOfTab(title) == -1)) {
            MessagePane.showError(title + " is already in use, please enter another");
            addTab();
        } else if (!"".equals(title)) {
            if (hantune.getTabbedPane().getTabCount() >= MAX_TABS) {
                MessagePane.showError("Already reached the limit of " + MAX_TABS + " tabs!");
                return null;
            }
            hantuneTab = new HANtuneTab(title);
            tabs.add(hantuneTab);
            JScrollPane scrollPane = new JScrollPane();
            scrollPane.setViewport(new JViewport() {
                @Override
                public Component getComponent(int index) {
                    return hantune.isFullScreen() ? hantune.getFullScreenTab() : super.getComponent(index);
                }
            });
            scrollPane.setViewportView(hantuneTab);
            scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
            scrollPane.getHorizontalScrollBar().setUnitIncrement(10);
            scrollPane.getVerticalScrollBar().setUnitIncrement(10);
            hantune.getTabbedPane().addTab(title, new ImageIcon(getClass().getResource("/images/cal.png")),
                    scrollPane);
            initializeTabComponent(title);
        }
        return hantuneTab;
    }

    private void initializeTabComponent(String title) {
        JLabel titleLabel = new JLabel(title);
        titleLabel.addMouseListener(new MouseEventDispatcher(hantune.getTabbedPane()) {
            @Override
            public void mousePressed(MouseEvent event) {
                JLabel label = (JLabel) event.getSource();
                if (event.getButton() == MouseEvent.BUTTON3) {
                    if (!CurrentConfig.getInstance().isServiceToolMode()) {
                        hantune.getTabPopupMenu().show(label, event.getX(), event.getY());
                    }
                } else {
                    dispatchEventToTarget(event);
                }
            }
        });
        hantune.getTabbedPane().setTabComponentAt(hantune.getTabbedPane().getTabCount() - 1, titleLabel);
    }

    /**
     * Edits the tab with the specified index.
     */
    public void renameTab(int index, String name) {
        hantune.getTabbedPane().setTitleAt(index, name);
        JLabel label = (JLabel) hantune.getTabbedPane().getTabComponentAt(index);
        label.setText(name);
    }

    /**
     * Deletes the specified tab from screen.
     */
    public void deleteTab(int index) {
        tabs.remove(getTab(index));
        hantune.getTabbedPane().removeTabAt(index);
    }

    /**
     * Deletes all tabs from screen.
     */
    public void deleteAllTabs() {
        for (HANtuneWindow window : activeWindows) {
            window.clearWindow();
        }
        activeWindows.clear();
        hantune.getTabbedPane().removeAll();
        tabs.clear();
    }

    /**
     * Adds a new window to the specified tab.
     *
     * @param index - the index of the tab to which the window must be added
     * @param hantuneWindowType - the type of the window
     * @return the added window
     */
    public HANtuneWindow addWindowToTab(int index, HANtuneWindowType hantuneWindowType) {
        if (hantune.getTabbedPane().getTabCount() == 0 && hantune.getActiveLayoutId() == -1) {
            MessagePane.showError(
                    "Could not add window because there is no active layout. Please create and/or load one first.");
            return null;
        }

        // only one errorviewer per tab allowed
        if (hantuneWindowType == HANtuneWindowType.ErrorViewer && isErrorViewerPresentInTab(index)) {
            MessagePane.showInfo("Only one ErrorViewer per tab possible");
            return null;
        }

        HANtuneTab hantuneTab = (HANtuneTab) ((JScrollPane) hantune.getTabbedPane().getComponentAt(index))
                .getViewport().getComponent(0);
        HANtuneWindow window = hantuneTab.addWindow(hantuneWindowType);

        // set error monitor for the error viewer so it can register/remove itself as an observer
        if (hantuneWindowType == HANtuneWindowType.ErrorViewer) {
            ((ErrorViewer) window).setErrorMonitor(hantune.getErrorMonitor());
        }

        window.packAndSetMinMax();
        window.setLocation(0, 0);
        window.moveHANtuneWindowToFront();
        window.rebuild();
        return window;
    }

    public HANtuneTab getTab(int index) {
        return (HANtuneTab) ((JScrollPane) hantune.getTabbedPane().getComponentAt(index)).getViewport()
                .getComponent(0);
    }

    public HANtuneTab getTabByTitle(String title) {
        for (HANtuneTab tab : tabs) {
            if (tab.getTitle().equals(title)) {
                return tab;
            }
        }
        return null;
    }

    public boolean isErrorViewerPresentInTab(int tabIndex) {
        HANtuneTab hantuneTab = (HANtuneTab) ((JScrollPane) hantune.getTabbedPane().getComponentAt(tabIndex))
                .getViewport().getComponent(0);
        for (int i = 0; i < hantuneTab.getComponentCount(); i++) {
            if (hantuneTab.getComponent(i) instanceof ErrorViewer) {
                return true;
            }
        }
        return false;
    }

    public boolean isErrorViewerPresent() {
        // loop through all available tabs
        for (int i = 0; i < tabs.size(); i++) {
            // does this tab have an error viewer on it?
            if (isErrorViewerPresentInTab(i)) {
                return true;
            }
        }
        return false;
    }

    public void registerErrorObservers() {
        // clear all observers.
        hantune.getErrorMonitor().clearObservers();
        // find all error viewers and set the error monitor member.
        for (int i = 0; i < tabs.size(); i++) {
            HANtuneTab hantuneTab = (HANtuneTab) ((JScrollPane) hantune.getTabbedPane().getComponentAt(i))
                    .getViewport().getComponent(0);
            for (int j = 0; j < hantuneTab.getComponentCount(); j++) {
                if (hantuneTab.getComponent(j) instanceof ErrorViewer) {
                    ((ErrorViewer) hantuneTab.getComponent(j)).setErrorMonitor(hantune.getErrorMonitor());
                }
            }
        }
        // add the hantune window as an observer, which is needed for updating
        // the label on the status bar.
        hantune.getErrorMonitor().registerObserver(hantune.getMainStatusBar().getErrorObserver());
    }

    public HANtuneTab getHANtuneTab(int index) {
        return (HANtuneTab) ((JScrollPane) hantune.getTabbedPane().getComponentAt(index)).getViewport()
                .getComponent(0);
    }

    /**
     * Starts a new clean project.
     */
    public void newProject() {
        // close any previous project
        closeProject();

        // reinit the error label on the statusbar
        hantune.getMainStatusBar().initErrorLabel();

        // create new project
        try {
            HANtuneDocument hantuneDocument = HANtuneDocument.Factory.newInstance();
            hantuneDocument.addNewHANtune();
            currentConfig.setProject(null, hantuneDocument);
            hantune.getMainMenuBar().updateItemsServiceModeVisibility();
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
        }

        LogDataWriterCsv.getInstance().setPrefix(Util.getProjectName(currentConfig.getProjectFile()));

        // reset pointers
        hantune.clearActiveIds();

        // update projectTree
        hantune.updateProjectTree();
    }

    /**
     * Adds an asap2Data file reference to the project.
     *
     * @param asap2File file to reference
     */
    public void newASAP2(File asap2File) {
        // already present in project?
        if (currentConfig.getHANtuneDocument().getHANtune().getASAP2FileList()
                .contains(asap2File.toURI().getPath())) {
            MessagePane.showInfo("\"" + asap2File.getPath() + "\" is already present in this project!");
            return;
        }
        // add to project
        String asap2Path = asap2File.toURI().getPath();
        currentConfig.getHANtuneDocument().getHANtune().addASAP2File(asap2Path);

        // update projectTree
        hantune.updateProjectTree();
    }

    /**
     * Loads currentConfig.getASAP2Data() information from an
     * currentConfig.getASAP2Data() file into HANtune and its treeview.
     */
    public void loadASAP2(int id) {
        if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()) {
            if (MessagePane.showConfirmDialog("This will close your current connection. \n Do you want to continue?",
                    "Close connection") == 1) {
                return;
            }
            hantune.connectProtocol(false);
        }

        try {
            CursorWait.startWaitCursor(hantune.getRootPane());

            // close current ASAP2
            closeASAP2();

            // load ASAP2 file
            if (id != -1) {
                File asap2File = new File(currentConfig.getHANtuneDocument().getHANtune().getASAP2FileArray(id));
                FileWatcher.removeFileFromList(asap2File);
                // import data from file
                ASAP2Import asap2Import = new ASAP2Import();
                ASAP2Data importASAP2Data = asap2Import.importASAP2(asap2File);

                currentConfig.setASAP2Data(importASAP2Data);

                set64BitSignalsAllowed(currentConfig.canUseDoubles());
                set64BitParamsActive(currentConfig.canUseDoubles());

                if (importASAP2Data == null) {
                    MessagePane.showError("Error while loading ASAP2 file\n" + asap2Import.getError());
                    id = -1;
                } else if (asap2Import.hasWarning()) {
                    MessagePane.showWarning(asap2Import.getWarning());
                }

                hantune.setActiveAsap2Id(id);

                // reload DAQ lists
                if (id != -1) {
                    loadDaqLists();
                }
            }
            updateAllWindows();
        } finally {
            CursorWait.stopWaitCursor(hantune.getRootPane());
        }

    }

    /**
     * Removes an asap2Data file reference from the project.
     *
     * @param id
     */
    public void removeASAP2(int id) {
        int activeAsap2Id = hantune.getActiveAsap2Id();

        // removing current asap2?
        if (id == activeAsap2Id) {

            // store and close active layout
            storeLayout();
            closeLayout();

            // close active asap2
            closeASAP2();
            activeAsap2Id = -1;
        } else if (id < activeAsap2Id) {
            activeAsap2Id--;
        }

        // remove from project
        currentConfig.getHANtuneDocument().getHANtune().removeASAP2File(id);

        // set active asap2
        hantune.setActiveAsap2Id(activeAsap2Id);
    }

    /**
     * Closes the currently opened asap2Data file.
     */
    public void closeASAP2() {
        // close asap2 doc
        currentConfig.setASAP2Data(null);

        // reset pointer
        hantune.setActiveAsap2Id(-1);

        // store and close all DAQ lists
        storeAndCloseDaqLists();

        // clear DAQ list array
        hantune.daqLists.clear();

        // update GUI
        hantune.updateDAQlistGUI();
        hantune.updateDaqListListeners();
    }

    public void newDbc(File dbcFile) {
        for (int i = 0; i < currentConfig.getHANtuneDocument().getHANtune().getDbcFileList().size(); i++) {
            if (dbcFile.getName()
                    .equals(currentConfig.getHANtuneDocument().getHANtune().getDbcFileArray(i).getName())) {
                MessagePane.showInfo("\"" + dbcFile.getPath() + "\" is already present in this project!");
                return;
            }
        }
        DbcFile dbc = currentConfig.getHANtuneDocument().getHANtune().addNewDbcFile();
        dbc.setName(dbcFile.getName());
        dbc.setPath(dbcFile.toURI().getPath());
        hantune.updateProjectTree();
        updateAllWindows();
    }

    public void loadDbcFile(String name) {
        currentConfig.getHANtuneDocument().getHANtune().getDbcFileList().stream()
                .filter(e -> e.getName().equals(name)).findFirst()
                .ifPresent(e -> loadDbcFile(new File(e.getPath())));

        hantune.updateProjectTree();
        updateAllWindows();
    }

    public boolean loadDbcFile(File dbcFile) {
        FileWatcher.removeFileFromList(dbcFile);

        DbcObject dbc = DbcParser.parseDBC(dbcFile);
        if (dbc != null) {
            ErrorObject errObj = new ErrorObject();
            if (DbcManager.isDbcLoadingOk(dbc, errObj)) {
                DbcManager.addDbc(dbc);
                currentConfig.addDescriptionFile((DescriptionFile) dbc);

                // show warnings when present
                if (!errObj.getInfo().isEmpty()) {
                    MessagePane.showWarning(errObj.getInfo());
                }

                return true;
            } else {
                if (!errObj.getInfo().isEmpty()) {
                    MessagePane.showError(errObj.getInfo());
                }
            }
        }

        return false;
    }

    private void loadDbcData(DbcObject dbc) {
        DbcManager.addDbc(dbc);
        currentConfig.addDescriptionFile(dbc);
    }

    public void unLoadDbcFile(File dbcFile) {
        currentConfig.removeDescriptionFileByName(dbcFile.getName());
        DbcManager.removeDbc(dbcFile.getName());
    }

    public void removeDbcFile(String id) {
        storeLayout();
        closeLayout();
        for (int i = 0; i < currentConfig.getHANtuneDocument().getHANtune().getDbcFileList().size(); i++) {
            if (id.equals(currentConfig.getHANtuneDocument().getHANtune().getDbcFileArray(i).getName())) {
                currentConfig.getHANtuneDocument().getHANtune().removeDbcFile(i);
            }
        }
        DbcManager.removeDbc(id);
        hantune.updateProjectTree();
    }

    public void addScriptToProject(File pythonFile) {
        Script script = ScriptingManager.getInstance().getScriptByName(pythonFile.getName());
        if (script != null
                && MessagePane.showConfirmDialog(HANtune.getInstance(),
                        pythonFile.getName() + "already exists, do you want to overwrite it?",
                        "Script already exists in project")
                == JOptionPane.OK_OPTION) {
            script.setPath(pythonFile.getPath());
        } else {
            script = new JythonScript(pythonFile.getName(), pythonFile.getPath());
        }

        ScriptingManager.getInstance().addScript(script);
        hantune.updateProjectTree();
        updateAllWindows();
    }

    public void removeScriptFromProject(Script script) {
        ScriptingManager manager = ScriptingManager.getInstance();
        manager.removeScript(script);
        HANtuneDocument.HANtune hantuneDoc = CurrentConfig.getInstance().getHANtuneDocument().getHANtune();
        for (int i = 0; i < hantuneDoc.getPythonFileList().size(); i++) {
            if (script.getName().equals(hantuneDoc.getPythonFileArray(i).getName())) {
                hantuneDoc.removePythonFile(i);
            }
        }
        hantune.updateProjectTree();
        updateAllWindows();
    }

    /**
     * Method to store and close all the active DAQ lists.
     */
    public void storeAndCloseDaqLists() {
        for (int i = 0; i < hantune.daqLists.size(); i++) {
            if (hantune.daqLists.get(i).isActive()) {
                storeDaqlist(i);
                closeDaqlist(i, false);
            }
        }
    }

    /**
     * Loads the DAQ lists from the project file. Creates a default DAQ list
     * containing all measurements if no DAQ lists are found.
     */
    private void loadDaqLists() {
        HANtuneDocument.HANtune hantuneXML = currentConfig.getHANtuneDocument().getHANtune();
        hantune.daqLists.clear();

        createDefaultDaqList();

        // loop through array of DAQ lists in HML document
        for (Daqlist daqlistXML : hantuneXML.getDaqlistList()) {
            if (!"Default".equals(daqlistXML.getTitle())) {
                DAQList daqList = new DAQList(daqlistXML.getTitle());
                daqList.setPrescaler(daqlistXML.getPrescaler());

                // add signals
                for (Daqlist.Signal signal : daqlistXML.getSignalList()) {
                    ASAP2Measurement measurement
                            = currentConfig.getASAP2Data().getMeasurementByName(signal.getName());
                    if (measurement != null) {
                        daqList.addMeasurement(measurement);
                    }
                }

                hantune.daqLists.add(daqList);
            }
        }

        currentConfig.updateAsap2Listeners();
        hantune.updateDaqListListeners();

        hantune.updateDAQlistGUI();
    }

    /**
     * Method to create a default daqlist at index 0
     */
    private void createDefaultDaqList() {
        // create DAQ list array element
        DAQList daqList = new DAQList("Default");

        set64BitSignalsAllowed(currentConfig.canUseDoubles());
        set64BitParamsActive(currentConfig.canUseDoubles());

        // set the prescaler
        daqList.setPrescaler(10);

        // set signals
        daqList.setASAP2Measurements(createDefaultDaqlistItems(XCP.MAX_DAQ_ITEMS, true));

        // add to DAQ list array at index 0
        if (hantune.daqLists.isEmpty()) {
            hantune.daqLists.add(daqList);
        } else {
            if (hantune.daqLists.get(0).getName().compareTo(daqList.getName()) == 0) {
                hantune.daqLists.remove(0);
            }
            hantune.daqLists.add(0, daqList);
        }

        // timestamp
        Calendar now = Calendar.getInstance();

        // add to the project (HML document)
        Daqlist daqlist;
        if (currentConfig.getHANtuneDocument().getHANtune().getDaqlistList().isEmpty()) {
            daqlist = currentConfig.getHANtuneDocument().getHANtune().addNewDaqlist();
        } else {
            daqlist = currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(0);
            daqList.setPrescaler(daqlist.getPrescaler());
        }
        daqlist.setTitle("Default");
        daqlist.setDate(now);
        daqlist.setDateMod(now);

        // store all details in the project
        storeDaqlist(0);

        hantune.updateDaqListListeners();
    }

    private List<ASAP2Measurement> createDefaultDaqlistItems(int maxItems, boolean showWarning) {
        List<ASAP2Measurement> foundList = new ArrayList<>();

        int count = 0;
        for (ASAP2Measurement measurement : currentConfig.getASAP2Data().getMeasurements()) {
            if (measurement.isAllowed()) {
                foundList.add(measurement);
                if (++count >= maxItems) {
                    break;
                }
            }
        }

        if (showWarning && count >= XCP.MAX_DAQ_ITEMS) {
            MessagePane.showWarning(
                    "Total DAQ list signals available: " + currentConfig.getASAP2Data().getASAP2Measurements().size()
                    + "\n"
                    + "Default DAQ list has been limited to a maximum of " + XCP.MAX_DAQ_ITEMS + " signals.");
        }

        return foundList;
    }

    void updateDefaultDaqList() {
        int maxItems = XCP.MAX_DAQ_ITEMS;
        if (currentConfig.isXcpDaqListSizeLimited() && currentConfig.getXcpDaqListSizeLimit() < maxItems) {
            maxItems = currentConfig.getXcpDaqListSizeLimit();
        }

        limitDefaultDaqListSize(maxItems, true);
    }

    private void limitDefaultDaqListSize(int size, boolean updateSlave) {
        if (size > XCP.MAX_DAQ_ITEMS) {
            size = XCP.MAX_DAQ_ITEMS;
        }

        DAQList defaultDaqList = null;
        for (DAQList dL : hantune.daqLists) {
            if (dL.getName().equals("Default")) {
                defaultDaqList = dL;
                break;
            }
        }

        if (defaultDaqList != null && currentConfig.getASAP2Data() != null) {
            List<ASAP2Measurement> defaultMeasurements = createDefaultDaqlistItems(size, false);
            defaultDaqList.setASAP2Measurements(defaultMeasurements);

            storeDaqlist(hantune.daqLists.indexOf(defaultDaqList));
            if (defaultDaqList.isActive()) {
                closeDaqlist(hantune.daqLists.indexOf(defaultDaqList), updateSlave);
                loadDaqlist(hantune.daqLists.indexOf(defaultDaqList), updateSlave);
            }
            hantune.updateDAQlistGUI();
        }
    }

    /**
     * Adds a new DAQ list to the project
     *
     * @param name - the name of the new DAQ list
     * @param showDialog - whether or not to show a dialog to add signals
     */
    public void newDaqlist(String name, boolean showDialog) {
        DAQList daqList = new DAQList(name);
        daqList.setPrescaler(10);

        if (showDialog) {
            DAQListDialog daqlistdialog = new DAQListDialog(hantune, true, name);
            if (daqlistdialog.isCanceled()) {
                return;
            }

            // Add the selected measurements for this DAQ list.
            for (String nm : daqlistdialog.getDaqlistNames()) {
                ASAP2Measurement mt = currentConfig.getASAP2Data().getASAP2Measurements().get(nm);
                if (mt != null) {
                    // add to DAQ list object
                    daqList.addMeasurement(mt);
                }
            }

            daqList.setPrescaler(daqlistdialog.getPrescaler());
        }
        hantune.daqLists.add(daqList);

        Calendar now = Calendar.getInstance();
        Daqlist daqlist = currentConfig.getHANtuneDocument().getHANtune().addNewDaqlist();
        daqlist.setTitle(name);
        daqlist.setDate(now);
        daqlist.setDateMod(now);

        storeDaqlist(hantune.daqLists.size() - 1);

        // make sure update is called after BOTH addNewDaqlist AND daqList.add have been called!!!
        // to make sure they are balanced when getTreeModel() transfers data between them
        hantune.updateDaqListListeners();

        hantune.updateDAQlistGUI();
        hantune.updateProjectTree();
    }

    /**
     * modifies the current DAQ list
     *
     * @param id
     */
    public void modifyDaqlist(int id) {
        String title = currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(id).getTitle();
        DAQListDialog daqlistdialog = new DAQListDialog(hantune, true, title);
        if (daqlistdialog.isCanceled()) {
            return;
        }

        if (daqlistdialog.isDaqListContentChanged()) {
            hantune.daqLists.get(id).getASAP2Measurements().clear();

            // Add the selected measurements for this DAQ list.
            for (String nm : daqlistdialog.getDaqlistNames()) {
                ASAP2Measurement mt = currentConfig.getASAP2Data().getASAP2Measurements().get(nm);
                if (mt != null) {
                    // add to DAQ list object
                    hantune.daqLists.get(id).addMeasurement(mt);
                }
            }

            // set the prescaler
            hantune.daqLists.get(id).setPrescaler(daqlistdialog.getPrescaler());

            // store all details in the project
            storeDaqlist(id);

            // store and close active DAQ list when it is already active
            if (hantune.daqLists.get(id).isActive()) {
                // close DAQ list
                // todo: should this also be set to false? Like line 882? See ticket #784
                closeDaqlist(id, true);

                // reload DAQ list
                loadDaqlist(id, true);
            }
        } else if (hantune.daqLists.get(id).getPrescaler() != daqlistdialog.getPrescaler()) {
            hantune.changePrescaler(id, (char) daqlistdialog.getPrescaler(), true);
        }

        hantune.updateDaqListListeners();

        // update GUI
        hantune.updateDAQlistGUI();
        hantune.getMainStatusBar().updateDaqFrequency();
    }

    public void modifyDaqlist(int id, String element, boolean add) {
        // clone old measurements
        List<ASAP2Measurement> oldMeasurements = new ArrayList<>();
        for (ASAP2Measurement measurement : hantune.daqLists.get(id).getASAP2Measurements()) {
            oldMeasurements.add(measurement);
        }

        // clear measurements
        hantune.daqLists.get(id).getASAP2Measurements().clear();

        // perform the intended action
        if (add) {
            // add new element
            oldMeasurements.add(currentConfig.getASAP2Data().getASAP2Measurements().get(element));
        } else {
            // remove new element
            oldMeasurements.remove(currentConfig.getASAP2Data().getASAP2Measurements().get(element));
        }

        // verify if names of oldMeasurments exist in currentConfig ASAP2Data and place in hantune daqList
        ASAP2Measurement mmt;
        for (ASAP2Measurement oldMmt : oldMeasurements) {
            if ((mmt = currentConfig.getASAP2Data().getASAP2Measurements().get(oldMmt.getName())) != null) {
                // add to DAQ list object
                hantune.daqLists.get(id).addMeasurement(mmt);
            }
        }

        // store all details in the project
        storeDaqlist(id);

        // force reload?
        if (hantune.daqLists.get(id).isActive()) {
            // close active DAQ list when it is already loaded
            closeDaqlist(id, false);
//            closeDaqlist(id, true); // this causes to create 2 log files when restarting logging

            // load the DAQ list
            loadDaqlist(id, true);
        } else {
            // update GUI
            hantune.updateDAQlistGUI();
        }
    }

    /**
     * Loads the specified DAQ list into an XCP DAQ object.
     */
    public void loadDaqlist(int id, boolean updateSlave) {

        // Check if an ASAP2file has been loaded successfully
        if (currentConfig.getASAP2Data() == null) {
            return; // No ASAP2 file loaded, so DAQlist can't be loaded either
        }

        // check if the DAQ list is supported by the ASAP2 file
        for (Signal signal : currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(id)
                .getSignalList()) {
            if (!currentConfig.getASAP2Data().getASAP2Measurements().containsKey(signal.getName())) {
                MessagePane.showError(
                        "This DAQ list is not compatible with the loaded ASAP2 file.\nPlease modify the DAQ list, try another ASAP2 file or load another DAQ list.");
                hantune.updateDaqListListeners();
                return;
            }
        }

        ErrorObject errObj = new ErrorObject();
        if (!isDaqlistSizeOk(id, errObj)) {
            MessagePane.showInfo(errObj.getInfo());
            return;
        }

        // maximum number of active DAQ lists reached yet?
        if (hantune.getActiveDaqlistIdCnt() == getDaqlistSize()) {
            MessagePane.showInfo(
                    "You have reached the maximum number of active DAQ lists (" + getDaqlistSize() + "), please " +
                        "unload an active DAQ list first");
            hantune.updateDaqListListeners();
            return;
        }

        // set active
        hantune.daqLists.get(id).setActive(true);
        hantune.resetActiveDaqlistIdCnt();
        int relativeId = hantune.daqLists.get(id).getRelativeId();

        // determine the number of elements in the DAQ list
        int cnt = hantune.daqLists.get(id).getASAP2Measurements().size();
        int cnt_all = cnt;

        // neglect 64 bit signals if necessary
        if (currentConfig.getXcp() != null && !currentConfig.getXcp().allow64BitSignals) {
            for (int i = 0; i < hantune.daqLists.get(id).getASAP2Measurements().size(); i++) {
                // decrement the amount of signals if 64 bit
                if (hantune.daqLists.get(id).getASAP2Measurements().get(i).getDatatypeSize() == 8) {
                    cnt--;
                }
            }
        }

        // if there are elements present in the DAQ list
        if (relativeId > -1 && cnt > 0) {
            // new DAQ list object
            XCPDaqList xcpDaqList = new XCPDaqList();
            hantune.daqLists.get(id).setXCPDaqList(xcpDaqList);

            // set the name and length of the DAQ list
            xcpDaqList.setName(hantune.daqLists.get(id).getName());
            xcpDaqList.setEntryCnt(cnt);

            // link the DAQ list to an event with a prescaler
            xcpDaqList.setEventNo((char) relativeId);
            xcpDaqList.setPrescaler((char) hantune.daqLists.get(id).getPrescaler());

            // add timestamp to XCP DAQ messages?
            if (currentConfig.getXcp() != null) {
                xcpDaqList.setTimestamp(currentConfig.getXcp().useSlaveTimestamp);
            } else {
                xcpDaqList.setTimestamp(false);
            }

            // fill the DAQ list object, combine items?
            if (currentConfig.getXcp() != null && currentConfig.getXcp().combineODT) {
                // maximum number of bytes per transport packet
                int packet_max = currentConfig.getXcp().getInfo().MAX_DTO;

                // packet overhead = 5 bytes: length (2) + counter (2) + ODT-ID (1)
                int packet_overhead = 5;
                if (currentConfig.getXcp().useSlaveTimestamp) {
                    packet_overhead += currentConfig.getXcp().getDaqInfoObject().timestamp_size;
                }

                // number of entries per ODT
                int odt_entries_max = (int) Math.floor((packet_max - packet_overhead) / 8);
                // 8 bytes per entry is worst-case

                // set the number of ODT (object descriptor tables) of the DAQ list
                int odt_cnt = (int) Math.ceil((double) cnt / odt_entries_max);
                xcpDaqList.init((char) odt_cnt);

                // determine and set data length of the packet overhead
                xcpDaqList
                        .setDataLen((hantune.currentConfig.getBusHeader() + packet_overhead * 8) * odt_cnt);

                // loop through the entries
                int odt_ptr = 0;
                int odt_entry_ptr = 0;
                for (int i = 0; i < cnt_all; i++) {
                    if (!currentConfig.getXcp().allow64BitSignals
                            && hantune.daqLists.get(id).getASAP2Measurements().get(i).getDatatypeSize() == 8) {
                        // skip 64 bit signal
                    } else {
                        // determine ODT pointer
                        odt_ptr = (int) Math.floor(i / odt_entries_max);

                        // fill the ODT with the ODT entries
                        if (i % odt_entries_max == 0) {
                            if (odt_ptr == (odt_cnt - 1)) {
                                // last ODT
                                xcpDaqList.odt[odt_ptr].init((char) (cnt - i));
                            } else {
                                // up until last ODT
                                xcpDaqList.odt[odt_ptr].init((char) odt_entries_max);
                            }
                        }

                        // configure the entry
                        odt_entry_ptr = i % odt_entries_max;
                        xcpDaqList.odt[odt_ptr].odt_entry[odt_entry_ptr]
                                .setName(hantune.daqLists.get(id).getASAP2Measurements().get(i).getName());
                        xcpDaqList.odt[odt_ptr].odt_entry[odt_entry_ptr].setAddress(
                                hantune.daqLists.get(id).getASAP2Measurements().get(i).getAddress());
                        xcpDaqList.odt[odt_ptr].odt_entry[odt_entry_ptr].setSize(
                                hantune.daqLists.get(id).getASAP2Measurements().get(i).getDatatypeSize());
                        xcpDaqList.odt[odt_ptr].odt_entry[odt_entry_ptr].setRelativeId((char) i);

                        // increment data length
                        xcpDaqList.setDataLen(xcpDaqList.getDataLen()
                                + xcpDaqList.odt[odt_ptr].odt_entry[odt_entry_ptr].getSize() * 8);
                    }
                }
            } else {
                // set the number of ODT (object descriptor tables) of the DAQ list
                // each DAQ list element is given an individual ODT
                xcpDaqList.init((char) cnt);

                // packet overhead = 1 byte: ODT-ID (1)
                int packet_overhead = 1;

                // determine and set data length of the packet overhead
                xcpDaqList.setDataLen((hantune.currentConfig.getBusHeader() + packet_overhead * 8) * cnt);

                // fill the DAQ-list with the ODT entries
                // one entry per ODT
                int odt_ptr = 0;
                for (int i = 0; i < cnt_all; i++) {
                    if (currentConfig.getXcp() != null && !currentConfig.getXcp().allow64BitSignals
                            && hantune.daqLists.get(id).getASAP2Measurements().get(i).getDatatypeSize() == 8) {
                        // skip 64 bit signal
                    } else {
                        // one entry per ODT
                        xcpDaqList.odt[odt_ptr].init((char) 1);

                        // configure the entry
                        xcpDaqList.odt[odt_ptr].odt_entry[0]
                                .setName(hantune.daqLists.get(id).getASAP2Measurements().get(i).getName());
                        xcpDaqList.odt[odt_ptr].odt_entry[0].setAddress(
                                hantune.daqLists.get(id).getASAP2Measurements().get(i).getAddress());
                        xcpDaqList.odt[odt_ptr].odt_entry[0].setSize(
                                hantune.daqLists.get(id).getASAP2Measurements().get(i).getDatatypeSize());
                        xcpDaqList.odt[odt_ptr].odt_entry[0].setRelativeId((char) i);

                        // increment data length
                        xcpDaqList.setDataLen(
                                xcpDaqList.getDataLen() + xcpDaqList.odt[odt_ptr].odt_entry[0].getSize() * 8);

                        // increment pointer
                        odt_ptr++;
                    }
                }
            }

            // configure and start DAQ lists at slave?
            if (updateSlave) {
                startDaqlists();
            }
        }

        // reset reload flag
        hantune.daqLists.get(id).setReload(false);

        // update last modified date
        currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(id)
                .setDateMod(Calendar.getInstance());

        // update GUI
        hantune.updateDAQlistGUI();
    }

    /**
     * @param id holds daqlist index
     * @param err object for possible error string
     * @return true if size OK, false if NOT OK. err info will hold error text
     */
    private boolean isDaqlistSizeOk(int id, ErrorObject err) {
        XCP xcp = currentConfig.getXcp();
        if (xcp != null) {
            // if DAQ Identification type of XCP contains PID header, check the size
            if (xcp.isConnected() && !xcp.isDaqlistNumberInXcpHdr()) {
                //Start with the size of the DAQlist-to-be-loaded
                int signalCounter = hantune.daqLists.get(id).getASAP2Measurements().size();
                //Add the size of the DAQlists which already have been loaded
                for (DAQList daqList : hantune.daqLists) {
                    if (daqList.isActive()) {
                        signalCounter += daqList.getASAP2Measurements().size();
                    }
                }
                if (signalCounter > XCP.MAX_DAQ_ITEMS) {
                    err.setInfo(
                            "You are unable to use more than " + XCP.MAX_DAQ_ITEMS + " signals, you are using a total of "
                            + signalCounter + " signals");
                    return false;
                }

            }
        }
        return true;
    }

    /**
     * Unloads the specified DAQ list.
     */
    public void unloadDaqlist(int id, boolean updateSlave, boolean updateGUI) {
        // store and close active DAQ list when it is already loaded
        storeDaqlist(id);
        closeDaqlist(id, updateSlave);

        // update GUI
        if (updateGUI) {
            hantune.updateDAQlistGUI();
        }
    }

    /**
     * Creates the XCPDaqList array to be used at the slave.
     */
    public XCPDaqList[] generateXCPDaqListArray() {
        XCPDaqList[] xcpDaqListArray = new XCPDaqList[hantune.getActiveDaqlistIdCnt()];

        // loop through DAQ lists
        for (int i = 0; i < hantune.daqLists.size(); i++) {
            if (hantune.daqLists.get(i).isActive()) {
                xcpDaqListArray[hantune.daqLists.get(i).getRelativeId()]
                        = hantune.daqLists.get(i).getXCPDaqList();
            }
        }

        return xcpDaqListArray;
    }

    /**
     * Configures and starts the DAQ lists at the slave.
     */
    public void startDaqlists() {
        // upload DAQList when ECU is connected
        if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()) {
            List<Character> lists = new ArrayList<>();

            // stop all DAQ lists
            hantune.toggleDaqlists((char) 0, lists);

            // extract XCP DAQ list array
            XCPDaqList[] xcpDaqListArray = generateXCPDaqListArray();

            // configure DAQ lists
            if (!currentConfig.getXcp().setDaqLists(xcpDaqListArray)) {
                MessagePane
                        .showError("Could not initialize DAQ list:\n\n" + currentConfig.getXcp().getErrorMsg());
                if (currentConfig.getXcp().isNumberOfDaqListItemsRestricted()) {
                    currentConfig.setXcpDaqListSizeLimited(true);
                    currentConfig.setXcpDaqListSizeLimit(currentConfig.getXcp().getRestrictedDaqListSize());
                    limitDefaultDaqListSize(currentConfig.getXcpDaqListSizeLimit(), false);
                }
                hantune.connectProtocol(false);
                return;
            }

            // create list of DAQ list identifiers
            for (int i = 0; i < currentConfig.getXcp().getDaqLists().length; i++) {
                lists.add((char) i);
            }

            // start all DAQ lists
            if (!hantune.toggleDaqlists((char) 1, lists)) {
                MessagePane
                        .showError("Could not start DAQ list:\n\n" + currentConfig.getXcp().getErrorMsg());
            } else {
                LogDataWriterCsv datalog = LogDataWriterCsv.getInstance();
                if (datalog.isWritingStandby()) {
                    datalog.enableDatalogging(datalog.isWritingStandby(),
                            datalog.isReadWhileWriteStandby(), true);

                    hantune.updateDatalogGuiComponents();
                }
            }
        }
    }

    /**
     * Performs a rename of the specified DAQ list.
     *
     * @param id
     * @param name
     */
    public void renameDaqlist(int id, String name) {
        // rename
        currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(id).setTitle(name);
        hantune.daqLists.get(id).setName(name);

        // update last modified date
        currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(id)
                .setDateMod(Calendar.getInstance());

        // update GUI
        hantune.updateDAQlistGUI();
    }

    /**
     * Removes a DAQ list from the project.
     *
     * @param id
     */
    public void removeDaqlist(int id) {
        // removing current DAQ list?
        if (hantune.daqLists.get(id).isActive()) {
            closeDaqlist(id, true);
        }

        // remove from project
        currentConfig.getHANtuneDocument().getHANtune().removeDaqlist(id);
        hantune.daqLists.remove(id);
        hantune.updateDaqListListeners();

        // update GUI
        hantune.updateDAQlistGUI();
    }

    /**
     * Copies the selected DAQ list within the project.
     *
     * @param id
     */
    public void copyDaqlist(int id) {
        // copy DAQ list in the HML document
        Daqlist newDaqlist = currentConfig.getHANtuneDocument().getHANtune().addNewDaqlist();
        newDaqlist.set(currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(id).copy());
        newDaqlist.setTitle("Copy of " + newDaqlist.getTitle());

        // copy DAQ list in the DAQ list array
        DAQList newDAQList = new DAQList("Copy of " + hantune.daqLists.get(id).getName());

        List<ASAP2Measurement> measurements_src = hantune.daqLists.get(id).getASAP2Measurements();
        List<ASAP2Measurement> measurements_dst = new ArrayList<>();
        for (int i = 0; i < measurements_src.size(); i++) {
            measurements_dst.add(measurements_src.get(i));
        }
        newDAQList.setASAP2Measurements(measurements_dst);
        hantune.daqLists.add(newDAQList);
        newDAQList.setPrescaler(hantune.daqLists.get(id).getPrescaler());

        hantune.updateDaqListListeners();

        // update GUI
        hantune.updateDAQlistGUI();
        hantune.updateProjectTree();
    }

    /**
     * Closes the currently opened DAQ list.
     *
     * @param id the active daqlist id that has to be closed.
     */
    public void closeDaqlist(int id, boolean updateSlave) {
        // replace with new empty XCPDaqList object
        hantune.daqLists.get(id).setXCPDaqList(new XCPDaqList());

        // reset flag
        hantune.daqLists.get(id).setActive(false);

        // reset counter
        hantune.resetActiveDaqlistIdCnt();

        // configure and start DAQ lists at slave?
        if (updateSlave) {
            startDaqlists();
        }
    }

    /**
     * Updates the XML content of the DAQ list being mentioned.
     *
     * @param id id of DAQ list
     */
    public void storeDaqlist(int id) {
        if (id < 0) {
            // do nothing
            return;
        }

        Daqlist daqlist = currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(id);

        // clear DAQ list
        daqlist.getSignalList().clear();

        // set modification date
        daqlist.setDateMod(Calendar.getInstance());

        // set prescaler value
        daqlist.setPrescaler(hantune.daqLists.get(id).getPrescaler());

        // loop through signals
        int cnt = hantune.daqLists.get(id).getASAP2Measurements().size();
        for (int i = 0; i < cnt; i++) {
            Daqlist.Signal signal
                    = currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(id).addNewSignal();

            // set properties
            signal.setName(hantune.daqLists.get(id).getASAP2Measurements().get(i).getName());
            signal.setAddress(hantune.daqLists.get(id).getASAP2Measurements().get(i).getAddress());
            signal.setSize(hantune.daqLists.get(id).getASAP2Measurements().get(i).getDatatypeSize());
        }
    }

    /**
     * Initializes the XCP DAQ list object array
     *
     * @param max this is the number of daqlists allowed on the ecu
     */
    public void setDaqlistSize(int max) {
        // store in global variable
        DaqlistSize = max;
    }

    /**
     * return the size of xcpDaqLists
     */
    public int getDaqlistSize() {
        return DaqlistSize;
    }

    public void modifyLogList(int id) {
        if (hantune.getActiveAsap2Id() == -1) {
            MessagePane.showInfo("Please open an ASAP2 file first!");
        } else {
            ModifyLogListDialog daqlistdialog = new ModifyLogListDialog(hantune, true);
            daqlistdialog.setVisible(true);
        }
    }

    public List<ASAP2Measurement> getLogList() {
        return logList;
    }

    private void saveLogList() {
        RemoteLogList list = currentConfig.getHANtuneDocument().getHANtune().getRemoteLogList();
        if (list == null) {
            list = currentConfig.getHANtuneDocument().getHANtune().addNewRemoteLogList();
        }
        for (ASAP2Measurement m : logList) {
            RemoteLogList.Signal signal = list.addNewSignal();
            signal.setName(m.getName());
        }
    }

    private void loadLogList() {
        HANtuneDocument.HANtune hantuneXML = currentConfig.getHANtuneDocument().getHANtune();
        for (RemoteLogList.Signal signal : hantuneXML.getRemoteLogList().getSignalList()) {
            ASAP2Measurement measurement
                    = currentConfig.getASAP2Data().getMeasurementByName(signal.getName());
            if (measurement != null) {
                System.out.println("adding signal");
                logList.add(measurement);
            } else {
                System.out.println("signal null");
            }
        }
    }

    /**
     * reset the reloadDaqList
     */
    public void resetReload() {
        // loop throught DAQ lists
        for (int i = 0; i < hantune.daqLists.size(); i++) {
            // reset reload flag
            hantune.daqLists.get(i).setReload(false);
        }
    }

    /**
     * Checks whether the daq list or timeout prescaler settings can cause
     * unwanted time-outs and generates a warning when this can happen
     */
    public void checkDaqListTimeOut() {
        // Check time-out settings for prescaler
        int TPrescaler = currentConfig.getXcpsettings().getTPrescaler();
        float SlaveFrequency = currentConfig.getXcpsettings().getSlaveFrequency();
        float maxDaqFrequency = getDaqListHighestFrequency();
        double minAllowableDaqFreq = (1 / (TPrescaler * 2 * 0.95)); // Tprescaler = 1 means 2sec. Tprescaler
        // = 2, 4sec, etc factor 0.95 is for
        // safety margin
        if ((maxDaqFrequency < minAllowableDaqFreq) && (maxDaqFrequency != 0)) {
            MessagePane.showWarning("High daq list prescalers can cause time-outs."
                    + "\nPlease keep your DAQ list prescalers below "
                    + Math.round(SlaveFrequency * (TPrescaler * 2))
                    + " or raise your Time-Out prescaler \nin \"Communication\" -> \"Communication\" Settings");
        }
    }

    /**
     * @return the highest sample frequency being used by a DAQ list
     */
    public float getDaqListHighestFrequency() {
        float highestSampleFrequency = 0;
        float slaveFrequency = currentConfig.getXcpsettings().getSlaveFrequency();

        for (DAQList daqList : hantune.daqLists) {
            if (daqList.isActive()) {
                highestSampleFrequency
                        = Math.max(highestSampleFrequency, daqList.getSampleFrequency(slaveFrequency));
            }
        }

        return highestSampleFrequency;
    }

    /**
     * Adds a new layout to the project.
     *
     * @param name
     */
    public void newLayout(String name) {
        Calendar now = Calendar.getInstance();

        // add to project
        Layout layout = currentConfig.getHANtuneDocument().getHANtune().addNewLayout();
        layout.setTitle(name);
        layout.setDate(now);
        layout.setDateMod(now);

        // add default tab
        Tab tab = layout.addNewTab();
        tab.setTitle(name);

        // update projectTree
        hantune.updateProjectTree();
    }

    /**
     * Opens the specified layout.
     *
     * @param id
     * @return
     */
    public void loadLayout(int id) {
        CursorWait.startWaitCursor(hantune.getRootPane());

        storeLayout();
        closeLayout();
        if (id != -1) {
            try {

                deleteAllTabs();
                List<Tab> hmlTabs
                        = currentConfig.getHANtuneDocument().getHANtune().getLayoutArray(id).getTabList();

                for (Tab hmlTab : hmlTabs) {

                    HANtuneTab tab = addTab(hmlTab.getTitle());

                    for (Window hmlWindow : hmlTab.getWindowList()) {

                        HANtuneWindowType type
                                = HANtuneWindowFactory.HANtuneWindowType.valueOf(hmlWindow.getType());
                        HANtuneWindow window = tab.addWindow(type);

                        for (Window.Setting hmlWindowSetting : hmlWindow.getSettingList()) {
                            window.addWindowSetting(hmlWindowSetting.getName(),
                                    hmlWindowSetting.getStringValue());
                        }

                        // Does not have to be an asap2 reference, but is still
                        // called this because of legacy reasons
                        List<Window.ASAP2> hmlReferences = hmlWindow.getASAP2List();
                        List<Reference> references = new ArrayList<>();
                        for (Window.ASAP2 hmlReference : hmlReferences) {
                            for (Window.ASAP2.Setting hmlReferenceSetting : hmlReference.getSettingList()) {
                                window.addSetting(hmlReference.getName(), hmlReferenceSetting.getName(),
                                        hmlReferenceSetting.getStringValue());
                            }

                            if (window instanceof ReferenceWindow) {
                                String name = hmlReference.getName();
                                String source = hmlReference.getSource() != null ? hmlReference.getSource()
                                        : ASAP2Object.DEFAULT_SOURCE;
                                Reference reference
                                        = DummyReferenceFactory.createReferenceForWindow(name, source, type);
                                references.add(reference);
                            }
                        }
                        if (window instanceof ReferenceWindow) {
                            @SuppressWarnings("unchecked")
                            ReferenceWindow<Reference> referenceWindow = (ReferenceWindow<Reference>) window;
                            referenceWindow.setReferences(references);
                        }

                        window.packAndSetMinMax();
                        window.setSize(hmlWindow.getWidth(), hmlWindow.getHeight());
                        window.setLocation(hmlWindow.getX(), hmlWindow.getY());
                        window.setLayer(hmlWindow.getLayer());
                    }
                }

                // switch to first tab
                if (hantune.getTabbedPane().getTabCount() > 0) {
                    hantune.getTabbedPane().getComponentAt(0).setVisible(true);
                }

            } catch (Exception e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                MessagePane.showError("Error while loading layout: " + e.getMessage());
                setError(e.getMessage());
                id = -1;
            }

            // update last modified date
            currentConfig.getHANtuneDocument().getHANtune().getLayoutArray(id)
                    .setDateMod(Calendar.getInstance());
        }
        hantune.setActiveLayoutId(id);
        hantune.getTabbedPane().setVisible(id != -1);
        updateAllWindows();

        CursorWait.stopWaitCursor(hantune.getRootPane());
    }

    /**
     * Performs a rename of the specified layout.
     *
     * @param id
     * @param name
     */
    public void renameLayout(int id, String name) {
        // rename
        currentConfig.getHANtuneDocument().getHANtune().getLayoutArray(id).setTitle(name);

        // update last modified date
        currentConfig.getHANtuneDocument().getHANtune().getLayoutArray(id)
                .setDateMod(Calendar.getInstance());

        // update projectTree
        hantune.updateProjectTree();
    }

    /**
     * Removes a layout from the project.
     *
     * @param id
     */
    public void removeLayout(int id) {
        int activeLayoutId = hantune.getActiveLayoutId();

        // removing current layout?
        if (id == activeLayoutId) {
            // close current layout
            closeLayout();
            activeLayoutId = -1;
        } else if (id < activeLayoutId) {
            activeLayoutId--;
        }

        // remove from project
        currentConfig.getHANtuneDocument().getHANtune().removeLayout(id);

        // set active layoutId
        hantune.setActiveLayoutId(activeLayoutId);
    }

    /**
     * Copies the selected layout within the project.
     *
     * @param id
     */
    public void copyLayout(int id) {
        // copy layout
        Layout newLayout = currentConfig.getHANtuneDocument().getHANtune().addNewLayout();
        newLayout.set(currentConfig.getHANtuneDocument().getHANtune().getLayoutArray(id).copy());
        newLayout.setTitle("Copy of " + newLayout.getTitle());

        // update projectTree
        hantune.updateProjectTree();
    }

    /**
     * Closes the currently opened layout.
     */
    public void closeLayout() {
        // delete all tabs
        deleteAllTabs();

        // reset pointer
        hantune.setActiveLayoutId(-1);
    }

    /**
     * Updates the XML content of the project related to the currently active
     * layout.
     */
    public void storeLayout() {
        int activeLayoutId = hantune.getActiveLayoutId();
        if (activeLayoutId == -1) {
            // do nothing
            return;
        }
        Layout layout = currentConfig.getHANtuneDocument().getHANtune().getLayoutArray(activeLayoutId);

        // clear layout
        layout.getTabList().clear();

        // set modification date
        layout.setDateMod(Calendar.getInstance());

        // loop through tabs
        for (int tabIndex = 0; tabIndex < hantune.getTabbedPane().getTabCount(); tabIndex++) {
            HANtuneTab hantuneTab
                    = (HANtuneTab) ((JScrollPane) hantune.getTabbedPane().getComponentAt(tabIndex)).getViewport()
                            .getComponent(0);

            // add tab
            Tab tab = layout.addNewTab();
            tab.setTitle(hantune.getTabbedPane().getTitleAt(tabIndex));

            // loop through windows of tab
            for (int windowIndex = 0; windowIndex < hantuneTab.getAllFrames().length; windowIndex++) {

                HANtuneWindow hantuneWindow = (HANtuneWindow) hantuneTab.getAllFrames()[windowIndex];

                // add window
                Window window = tab.addNewWindow();

                // window settings
                for (String windowSettingName : hantuneWindow.windowSettings.keySet()) {
                    Window.Setting windowSetting = window.addNewSetting();

                    windowSetting.setName(windowSettingName);
                    windowSetting.setStringValue(hantuneWindow.windowSettings.get(windowSettingName));
                }
                if (hantuneWindow instanceof ReferenceWindow) {
                    @SuppressWarnings("unchecked")
                    ReferenceWindow<Reference> referenceWindow = (ReferenceWindow<Reference>) hantuneWindow;
                    // window asap2Ref
                    for (Reference reference : referenceWindow.references) {
                        Window.ASAP2 windowAsap2 = window.addNewASAP2();

                        windowAsap2.setName(reference.getName());
                        windowAsap2.setSource(reference.getSource());

                        // window asap2Ref settings
                        if (hantuneWindow.settings.containsKey(reference.getName())) {
                            for (String windowAsap2SettingName : hantuneWindow.settings
                                    .get(reference.getName()).keySet()) {
                                Window.ASAP2.Setting windowAsap2Setting = windowAsap2.addNewSetting();

                                windowAsap2Setting.setName(windowAsap2SettingName);
                                windowAsap2Setting.setStringValue(hantuneWindow.settings
                                        .get(reference.getName()).get(windowAsap2SettingName));
                            }
                        }
                    }

                }
                if (hantuneWindow.settings.containsKey("")) {
                    Window.ASAP2 windowAsap2 = window.addNewASAP2();
                    windowAsap2.setName("");
                    for (String setting : hantuneWindow.settings.get("").keySet()) {
                        Window.ASAP2.Setting windowAsap2Setting = windowAsap2.addNewSetting();
                        windowAsap2Setting.setName(setting);
                        windowAsap2Setting.setStringValue(hantuneWindow.settings.get("").get(setting));
                    }
                }

                // set properties
                window.setType(HANtuneWindowFactory.getHANtuneWindowType(hantuneWindow).toString());
                window.setX(hantuneWindow.getX());
                window.setY(hantuneWindow.getY());
                window.setWidth(hantuneWindow.getWidth());
                window.setHeight(hantuneWindow.getHeight());
                window.setLayer(hantuneWindow.getLayer());
            }
        }
    }

    /**
     * Opens the specified project into HANtune.
     *
     * @param projectFile
     * @param doImport
     * @return
     */
    public boolean openProject(File projectFile, boolean doImport) {
        try {
            HANtuneDocument hantuneDoc;

            if (ServiceToolDocumentFileImpl.isServiceSHML(projectFile)) {
                currentConfig.setServiceToolMode(true);

                if ((hantuneDoc = loadProjectDataFromShmlData(projectFile)) == null) {
                    return false;
                }
            } else {
                currentConfig.setServiceToolMode(false);

                if ((hantuneDoc = loadProjectDataFromHmlData(projectFile)) == null) {
                    return false;
                }
            }

            // replace relative paths for absolute paths
            HANtuneDocument.HANtune hantuneXML = hantuneDoc.getHANtune();
            Util.resolvePaths(projectFile, hantuneXML.getASAP2FileList());

            hantuneXML.getDbcFileList().stream()
                    .forEach(file -> file.setPath(Util.resolvePath(projectFile, file.getPath())));

            hantuneXML.getPythonFileList().stream()
                    .forEach(file -> file.setPath(Util.resolvePath(projectFile, file.getPath())));

            // if file is imported, add the new lists to the hantunedoc
            if (doImport) {
                addDocumentLists(hantuneDoc);
            } else {
                // store project
                currentConfig.setProject(projectFile, hantuneDoc);

                hantune.getMainMenuBar().updateItemsServiceModeVisibility();

                if (hantuneXML.isSetSettings()) {
                    loadXcpConfigSettings();
                }

                if (currentConfig.isServiceToolMode()) {
                    hantune.setActiveAsap2Id(0);
                    loadDaqLists();
                } else {
                    if (hantuneXML.isSetActiveAsap2() && hantuneXML.getActiveAsap2() != -1) {
                        loadASAP2(hantuneXML.getActiveAsap2());
                    } else if (hantuneXML.sizeOfASAP2FileArray() > 0) {
                        loadASAP2(0);
                    }
                }
                loadLogList();

                loadLists(hantuneXML);

                // load project data
                LogDataWriterCsv.getInstance().setPrefix(Util.getProjectName(projectFile));

                // save filepath in userPrefs for recently used files
                userPrefs.put("lastOpenProject", projectFile.getAbsolutePath());
                addProjectPathToUserPrefs(userPrefs);
            }

            // reinit the error label on the statusbar
            hantune.getMainStatusBar().initErrorLabel();

            hantune.updateProjectTree();
            hantune.updateTitleBar();
            hantune.updateServiceToolGuiComponents();
            hantune.updateDAQlistGUI();
            runProjectStartupScripts();
            updateAllWindows();
            if (!currentConfig.isServiceToolMode()) {
                hantune.showProjectPanel(true);
            }

        } catch (NumberFormatException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            setError(e.getMessage());
            return false;
        }

        return true;
    }

    private void runProjectStartupScripts() {
        ScriptingManager.getInstance().getScripts().forEach(s -> {
            if (s.isRunOnStartup()) {
                s.start();
            }
        });
    }

    public void updateProjectDataTab() {
        hantune.updateProjectTree();
    }

    /*
     * save filepath in userPrefs for recently used files
     */
    public void addProjectPathToUserPrefs(Preferences userPrefs) {
        String lastProjectPathList = "";
        List<String> projectPaths
                = getLastProjectPathList(userPrefs);                   // Create a list to store the seperate path elements in
        if (projectPaths.size()
                > RECENT_PROJECTS_LIST_SIZE) {                  // When the list threatens to become too large...
            projectPaths.remove(0);                                             // Remove the oldest element to create space for the new one
        }

        for (String path : projectPaths) {
            lastProjectPathList
                    = lastProjectPathList.concat(path + ";");       // Build up new string with paths to store in registry
        }

        lastProjectPathList = lastProjectPathList.concat(
                userPrefs.get("lastOpenProject", "") + ";");   // Add the last opened project to the paths
        userPrefs.put("lastProjectPathList", lastProjectPathList);              // Store the string in registry under key "lastProjectPahtList"
    }

    public List<String> getLastProjectPathList(Preferences userPrefs) {
        List<String> projectPaths = new ArrayList<>();
        String lastProjectPathList
                = userPrefs.get("lastProjectPathList", "");  // Get the last project paths from the registry

        while (!lastProjectPathList.equalsIgnoreCase("")) {
            int i
                    = lastProjectPathList.indexOf(";");                           // find the first seperator to get the first project path
            if (i == -1) {
                break;                                                          // no seperator present? abort...
            }
            String path = lastProjectPathList.substring(0, i);                  // get the last project path
            lastProjectPathList
                    = lastProjectPathList.substring(i + 1);         // remove last project path from the string
            if (projectPaths.contains(path)) {
                projectPaths.remove(path);                                      // remove duplicate entry
            }
            projectPaths.add(path);                                             // add path to end of ArrayList
        }

        if (projectPaths.size()
                > RECENT_PROJECTS_LIST_SIZE) {                   // if the list is too long, take away excessive path elements
            projectPaths.subList(0, projectPaths.size()
                    - RECENT_PROJECTS_LIST_SIZE).clear();    // remove the excessive elements from the front of the list: take away the olders ones
        }
        return projectPaths;
    }

    /**
     * @param projectFile
     * @return null if some error, stored in this.error HANtuneDocument if OK.
     */
    public HANtuneDocument loadProjectDataFromHmlData(File projectFile) {
        DocumentFile<HANtuneDocument> docFile = new HANtuneDocumentFileImpl();
        HANtuneDocument hantuneDoc = docFile.loadProjectData(projectFile);
        if (hantuneDoc == null) {
            setError(docFile.getError());
            return null;
        }

        if (!docFile.isDocVersionOK()) {
            if (MessagePane.showConfirmDialog(HANtune.getInstance(), docFile.getError(),
                    "Confirm HML version") != JOptionPane.OK_OPTION) {
                AppendToLogfile.appendMessage(docFile.getError() + " NO");
                return null;
            }
            AppendToLogfile.appendMessage(docFile.getError() + " YES");

        }

        // validate to specification
        if (!docFile.isValidDocument()) {
            String str = "This file is not a valid HANtune file";
            setError(str + "\nCheck error file for details.");
            AppendToLogfile.appendMessage(str + docFile.getError());
            return null;
        }

        // check backwards compatibility
        if (hantuneDoc.getHANtune().getVersion() < VersionInfo.getInstance().getVersionFloat()) {
            hantuneDoc = execCompatibility(hantuneDoc);
        }

        return hantuneDoc;
    }

    /**
     * projectFile contains SHML data. Load SHML and convert to ASAP and
     * hantuneDoc
     *
     * @param projectFile
     * @return null if some error. HANtuneDocument if OK.
     */
    private HANtuneDocument loadProjectDataFromShmlData(File projectFile) {
        DocumentFile<ShmlDocument> docFile = new ServiceToolDocumentFileImpl();
        ShmlDocument shmlDoc = docFile.loadProjectData(projectFile);
        if (shmlDoc == null) {
            setError(docFile.getError());
            return null;
        }

        // check version
        if (!docFile.isDocVersionOK()) {
            if (MessagePane.showConfirmDialog(HANtune.getInstance(), docFile.getError(),
                    "Confirm SHML version") != JOptionPane.OK_OPTION) {
                AppendToLogfile.appendMessage(docFile.getError() + " NO");

                return null;
            }
            AppendToLogfile.appendMessage(docFile.getError() + " YES");
        }

        if (!docFile.isValidDocument()) {
            String str = new String("Not a valid SHML file.");
            setError(str + "\nCheck error file for details.");
            AppendToLogfile.appendMessage(str + docFile.getError());
            return null;
        }

        // do hantune conversion
        HANtuneDocument hantuneDoc = ShmlHmlConverter.createHANtuneDocFromShmlDoc(shmlDoc);

        // do ASAP2 conversion
        ASAP2XmlImport asap2DataFromXml = new ASAP2XmlImport(shmlDoc.getShml().getASAP2Data());
        currentConfig.setASAP2Data(asap2DataFromXml.convertXmlAsapToASAP2());
        if (currentConfig.getASAP2Data() == null) {
            String str = new String("Error reading ASAP2 data from file: " + projectFile.getName());
            MessagePane.showError(str);
            AppendToLogfile.appendMessage(str);
            return null;
        }

        // do DBC conversion, read list of all dbc objects
        for (dbcXmlData.DbcXmlData dbcXml : shmlDoc.getShml().getDbcDataList()) {
            DbcObject dbcObj = DbcXmlImport.importXmlDbc(dbcXml);
            if (dbcObj == null) {
                String str = new String("Error reading DBC data from file: " + projectFile.getName());
                MessagePane.showError(str);
                AppendToLogfile.appendMessage(str);
                return null;
            }
            loadDbcData(dbcObj);
        }

        return hantuneDoc;
    }

    private void loadLists(HANtuneDocument.HANtune hantuneXML) {
        if (hantuneXML.isSetActiveLayout() && hantuneXML.getActiveLayout() != -1) {
            loadLayout(hantuneXML.getActiveLayout());
        } else if (hantuneXML.sizeOfLayoutArray() > 0) {
            loadLayout(0);
        } else {
            currentConfig.createNewLayout("Unnamed");
            currentConfig.getHANtuneManager().loadLayout(0);
        }

        if (hantuneXML.sizeOfActiveDAQlistArray() > 0) {
            hantuneXML.getActiveDAQlistList().stream().forEach(i -> loadDaqlist(i, false));
        } else if (hantuneXML.sizeOfASAP2FileArray() > 0) {
            loadDaqlist(0, false);
        }

        if (hantuneXML.sizeOfActiveDbcArray() > 0) {
            hantuneXML.getActiveDbcList().forEach(elem -> loadDbcFile(elem));
        } else if (hantuneXML.sizeOfDbcFileArray() > 0) {
            loadDbcFile(hantuneXML.getDbcFileList().get(0).getName());
        }

        hantuneXML.getPythonFileList().stream()
                .forEach(file -> {
                    Script script = new JythonScript(file.getName(), file.getPath());
                    script.setRunOnStartup(file.getRunOnStartup());
                    ScriptingManager.getInstance().addScript(script);
                });
    }

    private void loadXcpConfigSettings() {
        // default settings
        currentConfig.getXcpsettings().setTPrescaler(1);

        // load settings
        Settings sett = currentConfig.getHANtuneDocument().getHANtune().getSettings();
        for (int i = 0; i < sett.sizeOfSettingArray(); i++) {
            Setting s = sett.getSettingArray(i);
            switch (s.getName()) {
                case "can_id_send":
                    currentConfig.getXcpsettings().setCANIDTx(Integer.parseInt(s.getStringValue(), 16));
                    break;
                case "can_id_receive":
                    currentConfig.getXcpsettings().setCANIDRx(Integer.parseInt(s.getStringValue(), 16));
                    break;
                case "can_baudrate":
                    currentConfig.getXcpsettings().setCANBaudrate(Util.string2baud(s.getStringValue()));
                    break;
                case "can_net":
                    currentConfig.getXcpsettings().setCANNet(s.getStringValue());
                    break;
                case "timeout_prescaler":
                    currentConfig.getXcpsettings().setTPrescaler(Integer.parseInt(s.getStringValue()));
                    break;
                case "ethernet_ip":
                    currentConfig.getXcpsettings().setEthernetIP(s.getStringValue());
                    break;
                case "ethernet_port":
                    currentConfig.getXcpsettings().setEthernetPort(Integer.parseInt(s.getStringValue()));
                    break;
                case "ethernet_bandwidth":
                    currentConfig.getXcpsettings()
                            .setEthernetBandwidth(Integer.parseInt(s.getStringValue()));
                    break;
                case "ethernet_protocol":
                    currentConfig.getXcpsettings()
                            .setEthernetProtocol("TCP".equals(s.getStringValue()) ? TCP : UDP);
                    break;
                case "heartbeat_interval":
                    currentConfig.getXcpsettings().setHeartbeatInterval(Integer.parseInt(s.getStringValue()));
                    break;
                case "uart_port":
                    currentConfig.getXcpsettings().setUARTPort(s.getStringValue());
                    break;
                case "uart_baudrate":
                    currentConfig.getXcpsettings().setUARTBaudrate(Integer.parseInt(s.getStringValue()));
                    break;
                case "uart_usb":
                    currentConfig.getXcpsettings()
                            .setUsbVirtualComPort(Boolean.parseBoolean(s.getStringValue()));
                    break;
                case "connection_mode":
                    currentConfig.setProtocol(parseProtocol(s.getStringValue()));
                    break;
                case "can_api":
                    break;
            }
        }
    }

    private void addDocumentLists(HANtuneDocument hantuneDoc) {
        currentConfig.getHANtuneDocument().getHANtune().getASAP2FileList()
                .addAll(hantuneDoc.getHANtune().getASAP2FileList());
        currentConfig.getHANtuneDocument().getHANtune().getDaqlistList()
                .addAll(hantuneDoc.getHANtune().getDaqlistList());
        currentConfig.getHANtuneDocument().getHANtune().getLayoutList()
                .addAll(hantuneDoc.getHANtune().getLayoutList());
        currentConfig.getHANtuneDocument().getHANtune().getCalibrationList()
                .addAll(hantuneDoc.getHANtune().getCalibrationList());
    }

    public CurrentConfig.Protocol parseProtocol(String name) {
        for (CurrentConfig.Protocol protocol : CurrentConfig.Protocol.values()) {
            if (protocol.name().equals(name)) {
                return protocol;
            }
        }

        // pre 2.0 backwards compatibility
        if (name.equals("ETHERNET")) {
            return CurrentConfig.Protocol.XCP_ON_ETHERNET;
        } else if (name.equals("UART")) {
            return CurrentConfig.Protocol.XCP_ON_UART;
        } else {
            return CurrentConfig.Protocol.XCP_ON_CAN;
        }
    }

    /**
     * Checks whether the file needs compatibility modifications and performs
     * these if necessary.
     *
     * @param doc
     * @return
     */
    public HANtuneDocument execCompatibility(HANtuneDocument doc) {
        // set correct Window/ASAP2 item format
        if (doc.getHANtune().getVersion() < (float) 0.5) {
            for (Layout layout : doc.getHANtune().getLayoutList()) {
                for (Tab tab : layout.getTabList()) {
                    for (Window window : tab.getWindowList()) {
                        for (Window.ASAP2 windowAsap2 : window.getASAP2List()) {
                            Window.ASAP2 a_new = Window.ASAP2.Factory.newInstance();
                            if (windowAsap2.sizeOfSettingArray() == 0) {
                                a_new.setName(windowAsap2.xmlText().replace("<xml-fragment>", "")
                                        .replace("</xml-fragment>", ""));
                                windowAsap2.set(a_new);
                            }
                        }
                    }
                }
            }
        }
        if (doc.getHANtune().getVersion() < (float) 1.1) {
            for (Layout layout : doc.getHANtune().getLayoutList()) {
                for (Tab tab : layout.getTabList()) {
                    for (Window window : tab.getWindowList()) {
                        window.setLayer(0);
                    }
                }
            }
        }
        return doc;
    }

    /**
     * Closes the current project. All related windows, resources, etc will be
     * closed here. Requests for closing should have been done before this call.
     */
    public void closeProject() {
        LogDataWriterCsv.getInstance().enableDatalogging(false, false, false);
        LogReaderAction.getInstance().dispose();
        hantune.connectProtocol(false);

        closeLayout();
        closeASAP2();
        DbcManager.removeAllDbcs();
        ScriptingManager.getInstance().removeAllScripts();
        hantune.getMainMenuBar().updateItemsServiceModeVisibility();

        currentConfig.setProject(null, null);
        hantune.updateDatalogGuiComponents();
        hantune.updateDAQlistGUI();
        hantune.updateProjectTree();
    }

    /**
     * Saves the specified project.
     *
     * @param projectFile
     * @param fileType
     * @param writeToDisk false: write to memory. Data will be stored in:
     * currentConfig.setProject(projectFile, hantuneDocument);
     * @return null if not saved.
     */
    public boolean saveProject(File projectFile, FileType fileType, boolean writeToDisk) {

        // store the active layout
        storeLayout();
        for (int i = 0; i < hantune.daqLists.size(); i++) {
            if (hantune.daqLists.get(i).isActive()) {
                storeDaqlist(i);
            }
        }
        saveLogList();
        // save filepath in userPrefs for recently used files
        userPrefs.put("lastOpenProject", projectFile.getAbsolutePath());
        addProjectPathToUserPrefs(userPrefs);

        HANtuneDocument hantuneDocument = (HANtuneDocument) currentConfig.getHANtuneDocument().copy();
        HANtuneDocument.HANtune hantuneXML = hantuneDocument.getHANtune();

        hantuneXML.setVersion(VersionInfo.getInstance().getVersionFloat());

        // set default settings
        if (hantuneXML.isSetSettings()) {
            hantuneXML.unsetSettings();
        }
        Settings sett = hantuneXML.addNewSettings();
        addCurrentSettings(sett);

        hantuneXML.setActiveAsap2(hantune.getActiveAsap2Id());
        hantuneXML.setActiveLayout(hantune.getActiveLayoutId());

        hantuneXML.setActiveDAQlistArray(IntStream.range(0, hantune.daqLists.size())
                .filter(daqList -> hantune.daqLists.get(daqList).isActive()).toArray());
        hantuneXML.setActiveDbcArray(DbcManager.getAllDbcNames().toArray(new String[0]));

        if (fileType.equals(FileType.SERVICE_PROJECT_FILE)) {
            ShmlDocument shmlDoc = ShmlHmlConverter.createShmlDocFromHANtuneDoc(hantuneDocument);

            // add ASAP2 data
            ConvertToXml<ASAP2Data, Shml, Layout> filter = new ASAP2ConvertToXmlImpl();
            filter.filterRelevantDataIntoXml(HANtune.getInstance().currentConfig.getASAP2Data(),
                    shmlDoc.getShml(), (Layout) shmlDoc.getShml().getLayout());

            // add Dbc data
            ConvertToXml<DbcObject, Shml, Layout> filterDbc = new DbcConvertToXmlImpl();
            DbcManager.getDbcList().forEach(d -> filterDbc.filterRelevantDataIntoXml(d, shmlDoc.getShml(),
                    (Layout) shmlDoc.getShml().getLayout()));

            DocumentFile<ShmlDocument> docFile = new ServiceToolDocumentFileImpl();
            if (docFile.saveProjectData(projectFile, shmlDoc)) {
                return true;
            } else {
                setError(docFile.getError());
                return false;
            }
        } else {

            DocumentFile<HANtuneDocument> docFile = new HANtuneDocumentFileImpl();

            // Save all the document XML data in file
            if (writeToDisk == true) {
                // replace absolute paths by relative paths.
                Util.relativizePaths(projectFile, hantuneXML.getASAP2FileList());

                hantuneXML.getDbcFileList().stream()
                        .forEach(file -> file.setPath(Util.relativizePath(projectFile.getParentFile(), file.getPath())));

                hantuneXML.getPythonFileList().clear();
                ScriptingManager.getInstance().getScripts().forEach(s -> {
                    HANtuneDocument.HANtune.PythonFile pythonHML = hantuneXML.addNewPythonFile();
                    pythonHML.setName(s.getName());
                    pythonHML.setPath(s.getPath());
                    pythonHML.setRunOnStartup(s.isRunOnStartup());
                });

                hantuneXML.getPythonFileList().stream()
                        .forEach(file -> file.setPath(Util.relativizePath(projectFile.getParentFile(), file.getPath())));

                if (docFile.saveProjectData(projectFile, hantuneDocument)) {
                    // reset paths to absolute paths again
                    Util.resolvePaths(projectFile, hantuneXML.getASAP2FileList());

                    hantuneXML.getDbcFileList().stream()
                            .forEach(file -> file.setPath(Util.resolvePath(projectFile, file.getPath())));

                    hantuneXML.getPythonFileList().stream()
                            .forEach(file -> file.setPath(Util.resolvePath(projectFile, file.getPath())));

                    hantune.updateProjectTree();
                    currentConfig.setProject(projectFile, hantuneDocument);
                    return true;
                } else {
                    return false;
                }
            } else {
                currentConfig.setProject(projectFile, hantuneDocument);
                return true;
            }
        }
    }

    private void addCurrentSettings(Settings sett) {
        // CAN settings
        Setting s1 = sett.addNewSetting();
        s1.setName("can_id_send");
        s1.setStringValue(Integer.toHexString(currentConfig.getXcpsettings().getCANIDTx()));
        Setting s2 = sett.addNewSetting();
        s2.setName("can_id_receive");
        s2.setStringValue(Integer.toHexString(currentConfig.getXcpsettings().getCANIDRx()));
        Setting s3 = sett.addNewSetting();
            s3.setName("can_baudrate");
            s3.setStringValue(Util.baud2string(currentConfig.getXcpsettings().getCANBaudrate()));

        // Timeout prescaler, if necessary
        if (currentConfig.getXcpsettings().getTPrescaler() != 1) {
            Setting s4 = sett.addNewSetting();
            s4.setName("timeout_prescaler");
            s4.setStringValue("" + currentConfig.getXcpsettings().getTPrescaler());
        }

        Setting s5 = sett.addNewSetting();
        s5.setName("ethernet_ip");
        s5.setStringValue(currentConfig.getXcpsettings().getEthernetIP());
        Setting s6 = sett.addNewSetting();
        s6.setName("ethernet_port");
        s6.setStringValue("" + currentConfig.getXcpsettings().getEthernetPort());
        Setting ethernetProtocolSetting = sett.addNewSetting();
        ethernetProtocolSetting.setName("ethernet_protocol");
        ethernetProtocolSetting.setStringValue(currentConfig.getXcpsettings().getEthernetProtocol().toString());
        Setting heartbeatSetting = sett.addNewSetting();
        heartbeatSetting.setName("heartbeat_interval");
        heartbeatSetting.setStringValue("" + currentConfig.getXcpsettings().getHeartbeatInterval());

        Setting s7 = sett.addNewSetting();
        s7.setName("connection_mode");
        s7.setStringValue(currentConfig.getProtocol().name());

        Setting s8 = sett.addNewSetting();
        s8.setName("uart_port");
        s8.setStringValue("" + currentConfig.getXcpsettings().getUARTPort());

        Setting s9 = sett.addNewSetting();
        s9.setName("uart_baudrate");
        s9.setStringValue("" + currentConfig.getXcpsettings().getUARTBaudrate());

        Setting s10 = sett.addNewSetting();
        s10.setName("ethernet_bandwidth");
        s10.setStringValue("" + currentConfig.getXcpsettings().getEthernetBandwidth());

        Setting s11 = sett.addNewSetting();
        s11.setName("uart_usb");
        s11.setStringValue("" + currentConfig.getXcpsettings().isUsbVirtualComPort());

        Setting s12 = sett.addNewSetting();
        s12.setName("can_api");
        s12.setStringValue(currentConfig.getCanApi().name());
    }

    /**
     * Initializes the default parameters values
     */
    public void requestParameters() {
        XCP xcp = currentConfig.getXcp();
        if (xcp != null && xcp.isConnected()) {
            // request all parameters
            for (String characteristic : currentConfig.getASAP2Data().getASAP2Characteristics().keySet()) {
                requestParameter(currentConfig.getASAP2Data().getCharacteristicByName(characteristic));
            }
        }
    }

    /**
     * Retrieves the current parameter value at the slave device using XCP.
     *
     * @param asap2Characteristic The parameter to request
     */
    public void requestParameter(ASAP2Characteristic asap2Characteristic) {
        XCP xcp = currentConfig.getXcp();

        if (xcp != null && xcp.isConnected()) {

            // request information
            ASAP2Characteristic.Type type = asap2Characteristic.getType();
            char byte_cnt = asap2Characteristic.getDatatypeSize();
            int cols = asap2Characteristic.getColumnCount();
            int rows = asap2Characteristic.getRowCount();
            long address = asap2Characteristic.getAddress();

            switch (type) {
                case VALUE: {
                    // VALUE
                    char[] value = xcp.getItem(address, (char) 0, byte_cnt);
                    if (value != null) {
                        // convert value
                        double convertValue = asap2Characteristic.convertValue(value);

                        // store data
                        asap2Characteristic.setValue(convertValue);
                        asap2Characteristic.setInSync(true);
                    }
                    break;
                }
                case AXIS_PTS:
                case CURVE: {
                    // AXIS_PTS or CURVE
                    // number of bytes to request
                    int bytes = cols * byte_cnt;
                    // request item
                    char[] valuea = xcp.getItem(address, (char) 0, bytes);
                    if (valuea != null) {
                        char[] value = new char[byte_cnt];
                        // split into individual values
                        for (int j = 0; j < cols; j++) {
                            System.arraycopy(valuea, j * value.length, value, 0, byte_cnt);
                            double convertValue = asap2Characteristic.convertValue(value);
                            int index = xcp.getInfo().byte_order == 0x00 ? (cols - 1) - j : j;
                            if (type == Type.AXIS_PTS) {
                                asap2Characteristic.setValueAt(convertValue, index);
                                asap2Characteristic.setInSyncAt(true, index);
                            } else if (type == Type.CURVE) {
                                asap2Characteristic.setValueAt(convertValue, 0, index);
                                asap2Characteristic.setInSyncAt(true, 0, index);
                            }
                        }
                    }
                    break;
                }
                case MAP: {
                    // MAP
                    // number of bytes to request
                    int bytes = cols * rows * byte_cnt;
                    // request item
                    char[] valuea = xcp.getItem(address, (char) 0, bytes);

                    if (valuea != null) {
                        char[] value = new char[byte_cnt];
                        for (int k = 0; k < cols; k++) {
                            for (int j = 0; j < rows; j++) {
                                int index = (k * rows + j);
                                System.arraycopy(valuea, index * value.length, value, 0, value.length);
                                double convertValue = asap2Characteristic.convertValue(value);
                                int row = xcp.getInfo().byte_order == 0x00 ? (rows - 1) - j : j;
                                int column = xcp.getInfo().byte_order == 0x00 ? (cols - 1) - k : k;
                                asap2Characteristic.setValueAt(convertValue, row, column);
                                asap2Characteristic.setInSyncAt(true, row, column);
                            }
                        }
                    }
                    break;
                }

                default:
                    break;
            }
        }
    }

    public void printDaqLists() {
        for (int i = 0; i < hantune.daqLists.size(); i++) {
            System.out.println("   daqLists[" + i + "]: " + hantune.daqLists.get(i).getName());
            System.out.println("      .active = " + hantune.daqLists.get(i).isActive());
            System.out.println("      .reload = " + hantune.daqLists.get(i).mustReload());
            System.out.println("      .relativeId = " + hantune.daqLists.get(i).getRelativeId());
            System.out.println("");
        }
    }

    public void addActiveWindowToList(HANtuneWindow window) {
        if (!activeWindows.contains(window)) {
            activeWindows.add(window);
        }
    }

    public void removeActiveWindowFromList(HANtuneWindow window) {
        activeWindows.remove(window);
    }

    public List<HANtuneWindow> getActiveWindows() {
        return activeWindows;
    }

    public void updateAllWindows() {
        for (HANtuneWindow window : activeWindows) {
            window.storeSize();
            window.rebuild();
            window.restoreSize();

        }
    }

    int set64BitSignalsAllowed(boolean allowed) {
        int changedSignals = 0;
        if (currentConfig.getASAP2Data() != null) {
            for (ASAP2Measurement mmt : currentConfig.getASAP2Data().getMeasurements()) {
                if ((mmt.getDatatype().getDataTypeSize() >= ASAP2Datatype.FLOAT64_IEEE.getDataTypeSize())
                        && (mmt.isAllowed() != allowed)) {
                    mmt.setAllowed(allowed);
                    changedSignals++;
                }
            }
        }
        return changedSignals;
    }

    int set64BitParamsActive(boolean active) {
        int changedCharacterists = 0;
        if (currentConfig.getASAP2Data() != null) {
            for (ASAP2Characteristic ch : currentConfig.getASAP2Data().getCharacteristics()) {
                if ((ch.getDatatype().getDataTypeSize() >= ASAP2Datatype.FLOAT64_IEEE.getDataTypeSize())
                        && (active != ch.isActive())) {
                    ch.setActive(active);
                    changedCharacterists++;
                }
            }
        }
        return changedCharacterists;
    }
}
