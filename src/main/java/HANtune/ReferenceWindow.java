/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

import ErrorLogger.AppendToLogfile;
import components.ReferenceLabel;
import datahandling.DescriptionFile;
import datahandling.Reference;
import nl.han.hantune.config.ApplicationProperties;

import static nl.han.hantune.config.ConfigProperties.PARAM_OUTOFSYNC_COLOR;

/**
 *
 * @author Michiel Klifman
 * @param <T> the type of Reference this window handles.
 */
@SuppressWarnings("serial")
public abstract class ReferenceWindow<T extends Reference> extends HANtuneWindow {

    public static final Color ACTIVE_COLOR = Color.WHITE;
    public static final Color ACTIVE_TEXT_COLOR = Color.BLACK;
    public static final Color INACTIVE_COLOR = Color.LIGHT_GRAY;
    public static final Color INACTIVE_TEXT_COLOR = new Color(160, 160, 160);
    public static final Color WARNING_COLOR = ApplicationProperties.getColor(PARAM_OUTOFSYNC_COLOR);
    public static final Color SELECTED_TEXT_COLOR = new Color(51, 153, 255);

    public static final String TITLE_LABEL_POSITION = "titleLabelPosition";

    private final Class<T> type;
    protected List<T> references = new ArrayList<>();
    protected final List<T> inactiveReferences = new ArrayList<>();
    protected final boolean multiReferenceWindow;

    private String defaultLabelPosition = BorderLayout.SOUTH;
    private JLabel titleLabel;

    public ReferenceWindow(Class<T> type) {
        this(type, true);
    }

    public ReferenceWindow(Class<T> type, boolean multiReferenceWindow) {
        this.type = type;
        this.multiReferenceWindow = multiReferenceWindow;

        DataFlavor flavor = new DataFlavor(type, type.getSimpleName());
        addTransferHandler(flavor);
    }

    private void addReferenceMenu() {
        String referenceType = type.getSimpleName();
        JMenu referenceMenu = new JMenu(referenceType + "s");
        JMenuItem addMenuItem = new JMenuItem("Add " + referenceType);
        addMenuItem.addActionListener(e -> showAddReferenceDialog());
        JMenuItem removeMenuItem = new JMenuItem("Remove " + referenceType);
        removeMenuItem.addActionListener(e -> showRemoveReferenceDialog());
        referenceMenu.add(addMenuItem);
        referenceMenu.add(removeMenuItem);
        addMenuToPopupMenu(referenceMenu);
    }

    private void addShowReferenceLabelMenuItem() {
        JMenu referenceNameMenu = new JMenu("Label");
        JCheckBoxMenuItem showReferenceNameItem = new JCheckBoxMenuItem("Show");
        showReferenceNameItem.setSelected(getBooleanWindowSetting("showTitleLabel", true));
        showReferenceNameItem.addActionListener(e -> saveAndShowTitleLabel(showReferenceNameItem.isSelected()));
        referenceNameMenu.add(showReferenceNameItem);

        String currentPosition = getWindowSetting(TITLE_LABEL_POSITION, defaultLabelPosition);
        JMenu positionReferenceNameMenu = new JMenu("Position");
        ButtonGroup group = new ButtonGroup();

        JRadioButtonMenuItem northMenuItem = new JRadioButtonMenuItem("Top");
        northMenuItem.setSelected(BorderLayout.NORTH.equals(currentPosition));
        northMenuItem.addActionListener(e -> saveAndSetTitleLabelPosition(BorderLayout.NORTH));
        group.add(northMenuItem);
        positionReferenceNameMenu.add(northMenuItem);

        JRadioButtonMenuItem southMenuItem = new JRadioButtonMenuItem("Bottom");
        southMenuItem.setSelected(BorderLayout.SOUTH.equals(currentPosition));
        southMenuItem.addActionListener(e -> saveAndSetTitleLabelPosition(BorderLayout.SOUTH));
        group.add(southMenuItem);
        positionReferenceNameMenu.add(southMenuItem);

        referenceNameMenu.add(positionReferenceNameMenu);
        addMenuItemToWindowMenu(referenceNameMenu);
    }

    @Override
    public void rebuild() {
        super.rebuild();
        clearWindow();
        if (references.isEmpty()) {
            buildDefaultReferenceWindow();
        } else {
            createCustomReferenceWindow();
        }
        validate();
    }

    protected void buildDefaultReferenceWindow() {
        JLabel addLabel = new JLabel(getIntroMessage());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        getContentPane().add(addLabel, gridBagConstraints);

        JButton addParameterButton = new JButton("Add " + type.getSimpleName());
        addParameterButton.setEnabled(!currentConfig.getDescriptionFiles().isEmpty());
        addParameterButton.addActionListener(e -> showAddReferenceDialog());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        getContentPane().add(addParameterButton, gridBagConstraints);
    }

    protected String getIntroMessage() {
        return "<html>Drag and drop a " + type.getSimpleName() + " from the sidepanel or press</html>";
    }

    protected void createCustomReferenceWindow() {
        validateReferences();
        initializeDefaultComponents();
        initializePopupMenu();
        addReferenceContent();
        loadSettings();
    }

    /**
     * Iterates over this windows references, validates each reference and
     * updates them if necessary. A reference could be a placeholder dummy and
     * will be replaced if the reference is available. If not it will be added
     * to a list of inactive references.
     */
    private void validateReferences() {
        for (final ListIterator<T> i = references.listIterator(); i.hasNext();) {
            final T reference = i.next();
            if (currentConfig.isReferenceAvailable(reference, type)) {
                T updatedReference = currentConfig.getReferenceOfType(reference, type);
                i.set(updatedReference);
            } else {
                reference.setActive(false);
                inactiveReferences.add(reference);
            }
        }
    }

    @Override
    protected void initializePopupMenu() {
        super.initializePopupMenu();
        if (!references.isEmpty()) {
            if (multiReferenceWindow) {
                addReferenceMenu();
            } else {
                addShowReferenceLabelMenuItem();
            }
        }
    }

    private void initializeDefaultComponents() {
        if (!multiReferenceWindow) {
            createTitleLabel();
            saveAndShowBorder(getWindowSetting("showBorder", "noBorder"));
            saveAndShowBackground(getBooleanWindowSetting("showBackground", false));
        }
    }

    private void createTitleLabel() {
        titleLabel = new ReferenceLabel<>(references.get(0));
        titleLabel.setFont(new Font("Tahoma", 1, 12));
        titleLabel.setHorizontalAlignment(SwingUtilities.CENTER);
        titleLabel.setVisible(getBooleanWindowSetting("showTitleLabel", true));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(0, 3, 0, 3);
        String position = getWindowSetting(TITLE_LABEL_POSITION, defaultLabelPosition);
        this.getSuperContentPane().add(titleLabel, position);
    }

    protected void setTitleText(String text) {
        titleLabel.setText(text);
    }

    protected void saveAndShowTitleLabel(boolean show) {
        showTitleLabel(show);
        windowSettings.put("showTitleLabel", String.valueOf(show));
    }

    protected void setDefaultLabelPosition(String defaultLabelPosition) {
        this.defaultLabelPosition = defaultLabelPosition;
    }

    private void saveAndSetTitleLabelPosition(String position) {
        this.getSuperContentPane().add(titleLabel, position);
        windowSettings.put(TITLE_LABEL_POSITION, position);
        this.validate();
    }

    protected String getReferenceTooltipText(T reference) {
        String name = type.getSimpleName() + ": " + reference.getName();
        String source = "Source: " + reference.getSource();
        String tooltip = "<html>" + name + "<br>" + source + "</html>";
        return tooltip;
    }

    private void showTitleLabel(boolean show) {
        titleLabel.setVisible(show);
        this.validate();
    }

    protected abstract void addReferenceContent();

    @Override
    public void clearWindow() {
        super.clearWindow();
        inactiveReferences.clear();
    }

    public List<T> getReferences() {
        return references;
    }

    public void setReferences(List<T> references) {
        this.references = references;
    }

    public void addReference(T reference) {
        if (!containsReference(reference)) {
            this.references.add(reference);
        }
        rebuild();
    }

    public void addMultipleReferences(List<T> references) {
        for (T reference : references) {
            if (!containsReference(reference)) {
                this.references.add(reference);
            }
        }
        rebuild();
    }

    public void removeReference(T reference) {
        references.remove(reference);
        rebuild();
    }

    public void moveReferenceUp(T reference) {
        int index = references.indexOf(reference);
        if (index > 0) {
            Collections.swap(references, index, index - 1);
        }
        rebuild();
    }

    public void moveReferenceDown(T reference) {
        int index = references.indexOf(reference);
        if (index < references.size() - 1) {
            Collections.swap(references, index, index + 1);
        }
        rebuild();
    }

    private void showAddReferenceDialog() {
        JPanel contentPanel = new JPanel(new BorderLayout(5,5));

        JPanel labels = new JPanel(new GridLayout(0,1,2,2));
        labels.add(new JLabel("Source", SwingConstants.RIGHT));
        labels.add(new JLabel(type.getSimpleName(), SwingConstants.RIGHT));
        contentPanel.add(labels, BorderLayout.WEST);

        JPanel controls = new JPanel(new GridLayout(0,1,2,2));
        DescriptionFile[] descriptionFiles = currentConfig.getDescriptionFiles().stream()
                .filter(d -> !d.getReferencesOfType(type).isEmpty())
                .toArray(DescriptionFile[]::new);
        JComboBox<DescriptionFile> fileComboBox = new JComboBox<>(descriptionFiles);
        JComboBox<Reference> referenceComboBox = new JComboBox<>();
        fileComboBox.addActionListener(e -> {
            DescriptionFile file = (DescriptionFile) fileComboBox.getSelectedItem();
            List<T> referenceList = file.getReferencesOfType(type).stream()
                    .filter(reference -> !references.contains(reference))
                    .collect(Collectors.toList());
            Reference[] referenceArray = referenceList.toArray(new Reference[referenceList.size()]);
            referenceComboBox.setModel(new DefaultComboBoxModel<>(referenceArray));
        });
        if (fileComboBox.getItemCount() > 0) {
            fileComboBox.setSelectedIndex(0);
        }
        controls.add(fileComboBox);
        controls.add(referenceComboBox);
        contentPanel.add(controls, BorderLayout.CENTER);

        int option = JOptionPane.showConfirmDialog(this, contentPanel, "Add " + type.getSimpleName(), JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            addReference(type.cast(referenceComboBox.getSelectedItem()));
        }
    }

    private void showRemoveReferenceDialog() {
        Object reference = JOptionPane.showInputDialog(this, "Select a " + type.getSimpleName().toLowerCase() + " to remove from the window:", "Remove " + type.getSimpleName(), JOptionPane.QUESTION_MESSAGE, null, references.toArray(), null);
        removeReference(type.cast(reference));
    }

    private boolean containsReference(Reference referenceToCompare) {
        for (Reference reference : references) {
            if (compareReferences(reference, referenceToCompare)) {
                return true;
            }
        }
        return false;
    }

    public static boolean compareReferences(Reference reference1, Reference reference2) {
        if (reference1 == null || reference1.getName() == null || reference1.getSource() == null) {
            return false;
        } else {
            return (reference1.getName().equals(reference2.getName()) && (reference1.getSource().equals(reference2.getSource())));
        }
    }

    public Map<String, Map<String, String>> getSettings() {
        return settings;
    }

    public String getStringSetting(Reference reference, String setting) {
        return getSetting(reference, setting, String::toString);
    }

    public int getIntSetting(Reference reference, String setting) {
        return getSetting(reference, setting, Integer::parseInt);
    }

    public double getDoubleSetting(Reference reference, String setting) {
        return getSetting(reference, setting, Double::parseDouble);
    }

    public boolean getBooleanSetting(Reference reference, String setting) {
        return getSetting(reference, setting, Boolean::parseBoolean);
    }

    public <T> T getSetting(Reference reference, String setting, Function<String, T> function) {
        if (hasSetting(reference.getName(), setting)) {
            return function.apply(settings.get(reference.getName()).get(setting));
        }
        return null;
    }

    public JPopupMenu getRemoveReferenceMenu(T reference) {
        JPopupMenu menu = new JPopupMenu();
        JMenuItem removeSignalMenuItem = new JMenuItem("Remove " + reference.getName());
        removeSignalMenuItem.addActionListener((ActionEvent e) -> removeReference(reference));
        menu.add(removeSignalMenuItem);
        return menu;
    }

    private void addTransferHandler(DataFlavor flavor) {
        this.setTransferHandler(new TransferHandler() {
            @Override
            public boolean canImport(TransferHandler.TransferSupport support) {
                return support.isDataFlavorSupported(flavor);
            }

            @Override
            @SuppressWarnings("unchecked")
            public boolean importData(TransferHandler.TransferSupport support) {
                if (!canImport(support)) {
                    return false;
                }

                List<T> data;
                try {
                    data = (List<T>) support.getTransferable().getTransferData(flavor);
                } catch (UnsupportedFlavorException | IOException exception) {
                    AppendToLogfile.appendError(Thread.currentThread(), exception);
                    return false;
                }
                addMultipleReferences(data);
                return true;
            }
        });
    }
}
