/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import ErrorLogger.AppendToLogfile;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;

import haNtuneHML.HANtuneDocument;
import haNtuneHML.Settings.Setting;


public class HANtuneDocumentFileImpl implements DocumentFile<HANtuneDocument> {
    private static String errorString = "";
    private static final String HMLVERSION_SETTING = "HMLversion";
    private static final String HML_VERSION_VALUE = "1.0-1";

    // assume this legacy version for old HML files that do not contain a version setting
    // See also design choices DC_001
    private static final String HML_LEGACY_VERSION_VALUE = "1.0-1"; // Do NOT change

    private HANtuneDocument hantuneDoc;

    public HANtuneDocument loadProjectData(File projectFile) {
        try {
            // parse regular HML file
            hantuneDoc = HANtuneDocument.Factory.parse(projectFile);
        } catch (IOException | XmlException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            errorString = e.getMessage();
            return null;
        }
        return hantuneDoc;
    }


    public boolean saveProjectData(File projectFile, HANtuneDocument hantuneDocument) {
        FileOutputStream os;
        try {
            os = new FileOutputStream(projectFile);

            XmlOptions options = new XmlOptions();
            options.setUseDefaultNamespace();

            addHMLversionSetting(hantuneDocument);

            hantuneDocument.save(os, options);

            os.close();
        } catch (IOException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            HANtune.getInstance().currentConfig.getHANtuneManager().setError(e.getMessage());
            return false;
        }

        return true;
    }


    /**
     * @param hantuneDocument
     */
    private void addHMLversionSetting(HANtuneDocument hantuneDocument) {
        Setting versionSetting = getHMLversion(hantuneDocument);
        if (versionSetting == null) {
            versionSetting = hantuneDocument.getHANtune().getSettings().addNewSetting();
            versionSetting.setName(HMLVERSION_SETTING);
        }
        versionSetting.setStringValue(HML_VERSION_VALUE);
    }


    /**
     * @param hantuneDocument
     * @param versionSetting
     * @return
     */
    private Setting getHMLversion(HANtuneDocument hantuneDocument) {
        for (Setting setting : hantuneDocument.getHANtune().getSettings().getSettingList()) {
            if (setting.getName().equals(HMLVERSION_SETTING)){
                return setting;
            }
        }
        return null;
    }


    static public boolean createBackupOfProjectFile(File file, Float version) {
        String path = file.getPath();
        String backupPath = getBackupPath(path, version);
        try {
            Files.copy(Paths.get(path), Paths.get(backupPath), StandardCopyOption.REPLACE_EXISTING);
            return true;
        } catch (IOException exception) {
            AppendToLogfile.appendError(Thread.currentThread(), exception);
            return false;
        }
    }


    static public String getBackupPath(String path, Float version) {
        if (path.endsWith(".hml")) {
            String backupPath = path.substring(0, path.lastIndexOf('.')) + "(HANtune v" + version + ").hml";
            return appendIndexIfFileExists(backupPath);
        } else {
            return path;
        }
    }


    static private String appendIndexIfFileExists(String path) {
        String validPath = path;
        int index = 1;
        while (new File(validPath).exists()) {
            validPath = path.substring(0, path.lastIndexOf('.')) + "(" + index + ").hml";
            index++;
        }
        return validPath;
    }

    /**
     * @param hmlDoc
     */
    @Override
    public boolean isValidDocument() {
        // Create an XmlOptions instance and set the error listener.
        XmlOptions validateOptions = new XmlOptions();
        ArrayList<XmlError> errorList = new ArrayList<XmlError>();
        validateOptions.setErrorListener(errorList);

        // Validate the XML.
        boolean isValid = hantuneDoc.validate(validateOptions);

        // If the XML isn't valid, loop through the listener's contents and put result in errorString
        if (!isValid) {
            StringBuilder stringBldr = new StringBuilder();
            for (int i = 0; i < errorList.size(); i++) {
                XmlError error = errorList.get(i);
                stringBldr.append("\n" + error.getMessage());
                stringBldr
                    .append("\nLocation of invalid XML: " + error.getCursorLocation().xmlText() + "\n");
            }
            errorString = stringBldr.toString();
        }

        return isValid;
    }



    /**
     * @param applicVersion version string of that belongs to current application
     * @param docVersion version string found in document
     * @return true: application version is greater than or equal to document version
     */
    public static boolean compareVersionStrings(String applicVersion, String docVersion) {
        int applIdx = applicVersion.indexOf('-');
        int docIdx = docVersion.indexOf('-');
        Float applFlt = Float.valueOf(applicVersion.substring(0, applIdx));

        Float docFlt =  Float.valueOf(docVersion.substring(0, docIdx));
        boolean versionOK = false;
        if (applIdx != -1 && docIdx != -1) {
            versionOK = (applFlt >= docFlt);
        }
        return versionOK;
    }


    @Override
    public boolean isDocVersionOK() {

        // get SHML version from xsd file
        String applVersion = HML_VERSION_VALUE;
        Setting versionSetting = getHMLversion(hantuneDoc);
        String docVersion = HML_LEGACY_VERSION_VALUE;
        if (versionSetting != null) {
            docVersion = versionSetting.getStringValue();
        }

        if (!compareVersionStrings(applVersion, docVersion)) {
            String docHANtuneVersion = Float.toString(hantuneDoc.getHANtune().getVersion());
            errorString = "Warning: current HML file does not match the version this HANtune expects.\n"
                + "HML file version: " + docVersion
                + ", this HANtune expects HML version: " + applVersion + "\n"
                + "Please obtain HANtune version " + docHANtuneVersion + " or higher\n" + "\n"
                + "Opening this HML project might give unexpected results.\n" + "Continue anyway?";
            return false;
        }

        return true;
    }

    public String getError() {
        return errorString;
    }



}
