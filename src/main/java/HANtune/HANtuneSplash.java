/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.SplashScreen;
import java.awt.geom.Rectangle2D;

import nl.han.hantune.config.VersionInfo;


public class HANtuneSplash {
    private static SplashScreen Splash;
    private static Rectangle2D.Double splashTextArea;
    private static Graphics2D splashGraphics;

    public static void splashInit() {
        Splash = SplashScreen.getSplashScreen();
        if (Splash != null) { // if there are any problems displaying the
                                     // splash this will be null
            Dimension ssDim = Splash.getSize();
            int height = ssDim.height;
            int width = ssDim.width;
            // stake out some area for our status information
            splashTextArea = new Rectangle2D.Double(25., height * 0.40, width * .40, 32.);

            // create the Graphics environment for drawing status info
            splashGraphics = Splash.createGraphics();
            Font fontForVersion = new Font("Arial", Font.BOLD, 20);
            Font fontForLicence = new Font("Arial", Font.BOLD, 15);

            // initialize the status info
            splashGraphics.setFont(fontForVersion);
            splashText(VersionInfo.getInstance().getBuildCommitCount(), "version_line1");
            splashGraphics.setFont(fontForLicence);
            splashText(VersionInfo.getInstance().getBranchBranchName(), "version_line2");

            splashText("Open source version", "expiryDate");
        }
    }


    /**
     * This method sets text in the splash screen
     *
     * @param str the string to display
     * @param textOnSplash the string indicating the position
     */
    private static void splashText(String str, String textOnSplash) {
        // important to check here so no other methods need to know if there
        if (Splash != null && Splash.isVisible()) {
            // set color of the text
            Color colorText = Color.BLACK;
            splashGraphics.setPaint(colorText);

            switch (textOnSplash) {
                case "version_line1":
                    splashGraphics.drawString(str, (int)(splashTextArea.getX() + 10),
                        (int)(splashTextArea.getY() + 55));
                    break;
                case "version_line2":
                    splashGraphics.drawString(str, (int)(splashTextArea.getX() + 10),
                        (int)(splashTextArea.getY() + 75));
                    break;
                case "company":
                    splashGraphics.drawString(str, (int)(splashTextArea.getX() + 10),
                        (int)(splashTextArea.getY() + 85));
                    break;
                case "expiryDate":
                    splashGraphics.drawString(str, (int)(splashTextArea.getX() + 10),
                        (int)(splashTextArea.getY() + 105));
                    break;
                default:
                    // nothing;
                    break;

            }

            // make sure it's displayed
            Splash.update();
        }
    }
}
