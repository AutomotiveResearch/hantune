/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import static HANtune.HANtuneWindowFactory.HANtuneEditorType;
import static HANtune.HANtuneWindowFactory.HANtuneTableEditorType;
import static HANtune.HANtuneWindowFactory.HANtuneViewerType;
import HANtune.HANtuneWindowFactory.HANtuneWindowType;
import datahandling.DefaultParameter;
import datahandling.DefaultScript;
import datahandling.DefaultSignal;
import datahandling.DefaultTable;
import datahandling.Reference;

/**
 *
 * @author Michiel Klifman
 */
public class DummyReferenceFactory {

    public static Reference createReferenceForWindow(String name, String source, HANtuneWindowType type) {
        Reference reference = null;
        if (HANtuneViewerType.contains(type)) {
            reference = new DefaultSignal(name, source);
        } else if (HANtuneEditorType.contains(type)) {
            reference = new DefaultParameter(name, source);
        } else if (HANtuneTableEditorType.contains(type)) {
            reference = new DefaultTable(name, source);
        } else if (type == HANtuneWindowType.ScriptWindow) {
            reference = new DefaultScript(name, source);
        }
        return reference;

    }

    public static Reference createReferenceFromType(Class<? extends Reference> type) {
        //to be implemented
        return null;
    }

}
