/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.LinkedHashMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.text.DefaultFormatterFactory;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Datatype;
import ASAP2.ASAP2Measurement;
import ErrorLogger.AppendToLogfile;
import XCP.XCPSettings.EthernetProtocol;
import datahandling.CurrentConfig;
import peak.can.basic.TPCANBaudrate;
import util.MessagePane;
import util.Util;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class CommunicationSettings extends HanTuneDialog {

    private static final long serialVersionUID = 1L;

    private CurrentConfig currentConfig = CurrentConfig.getInstance();
    private final HANtune hantune = HANtune.getInstance();
    private LinkedHashMap<String, TPCANBaudrate> br = new LinkedHashMap<>();
    private LinkedHashMap<String, String> nets = new LinkedHashMap<>();
    private LinkedHashMap<String, String> UARTports = new LinkedHashMap<>();
    private LinkedHashMap<String, Integer> UARTbaudrates = new LinkedHashMap<>();
    private LinkedHashMap<String, Integer> ethernetBandwidths = new LinkedHashMap<>();
    private EthernetProtocol ethernetProtocol = EthernetProtocol.TCP;

    public CommunicationSettings() {
        super(true);

        // use System LookAndFeel
        initDialog();
    }

    private void initDialog() {
        // set program window icon
        Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/HANlogo.png"));
        this.setIconImage(img);

        initComponents();

        // init spinner models
        spinnerIDtx.setEditor(new HexNumberEditor(spinnerIDtx));
        spinnerIDtx.setValue(currentConfig.getXcpsettings().getCANIDTx());
        spinnerIDrx.setEditor(new HexNumberEditor(spinnerIDrx));
        spinnerIDrx.setValue(currentConfig.getXcpsettings().getCANIDRx());
        spinnerPort.setValue(currentConfig.getXcpsettings().getEthernetPort());
        spinnerTPrescaler.setValue(currentConfig.getXcpsettings().getTPrescaler());
        textFieldIP.setText(currentConfig.getXcpsettings().getEthernetIP());
        comboDriver.setSelectedIndex(currentConfig.getProtocol().ordinal());

        br.clear();
        br.put("1 MBit/s", TPCANBaudrate.PCAN_BAUD_1M);
        br.put("500 kBit/s", TPCANBaudrate.PCAN_BAUD_500K);
        br.put("250 kBit/s", TPCANBaudrate.PCAN_BAUD_250K);
        br.put("125 kBit/s", TPCANBaudrate.PCAN_BAUD_125K);
        br.put("100 kBit/s", TPCANBaudrate.PCAN_BAUD_100K);
        br.put("50 kBit/s", TPCANBaudrate.PCAN_BAUD_50K);
        br.put("20 kBit/s", TPCANBaudrate.PCAN_BAUD_20K);
        br.put("10 kBit/s", TPCANBaudrate.PCAN_BAUD_10K);
        br.put("5 kBit/s", TPCANBaudrate.PCAN_BAUD_5K);
        comboBaudrate.setModel(new javax.swing.DefaultComboBoxModel<Object>(
                br.keySet().toArray()
        ));
        if (Util.isLinux()) {
            labelBaudrate.setText("CAN baudrate. Change outside HANtune, use SocketCAN params");
            comboBaudrate.setEnabled(false);
        } else {
            labelBaudrate.setText("CAN baudrate");
        }



        // select baudrate
        for (int i = 0; i < br.size(); i++) {
            if (br.values().toArray()[i].equals(currentConfig.getXcpsettings().getCANBaudrate())) {
                comboBaudrate.setSelectedIndex(i);
            }
        }
        addComPorts(UARTports);

        comboUARTPort.setModel(new javax.swing.DefaultComboBoxModel<>(UARTports.keySet().toArray()));

        // select baudrate
        for (int i = 0; i < UARTports.size(); i++) {
            if (UARTports.values().toArray()[i].equals(currentConfig.getXcpsettings().getUARTPort())) {
                comboUARTPort.setSelectedIndex(i);
            }
        }

        // add COM baudrates
        UARTbaudrates.put("9600 bps", 9600);
        UARTbaudrates.put("19200 bps", 19200);
        UARTbaudrates.put("38400 bps", 38400);
        UARTbaudrates.put("57600 bps", 57600);
        UARTbaudrates.put("115200 bps", 115200);
        comboUARTBaudrate.setModel(new javax.swing.DefaultComboBoxModel<Object>(
                UARTbaudrates.keySet().toArray()
        ));

        // select baudrate
        for (int i = 0; i < UARTbaudrates.size(); i++) {
            if ((int) UARTbaudrates.values().toArray()[i] == currentConfig.getXcpsettings().getUARTBaudrate()) {
                comboUARTBaudrate.setSelectedIndex(i);
            }
        }

        usbCheckbox.setSelected(currentConfig.getXcpsettings().isUsbVirtualComPort());
        comboUARTBaudrate.setEnabled(!currentConfig.getXcpsettings().isUsbVirtualComPort());

        // add Ethernet bandwidths
        ethernetBandwidths.put("10 Mbit/s", 10);
        ethernetBandwidths.put("54 Mbit/s", 54);
        ethernetBandwidths.put("100 Mbit/s", 100);
        comboEthernetBandwidth.setModel(new javax.swing.DefaultComboBoxModel<Object>(
                ethernetBandwidths.keySet().toArray()
        ));

        // select Ethernet bandwidth
        for (int i = 0; i < ethernetBandwidths.size(); i++) {
            if ((int) ethernetBandwidths.values().toArray()[i] == currentConfig.getXcpsettings().getEthernetBandwidth()) {
                comboEthernetBandwidth.setSelectedIndex(i);
            }
        }
        
        ethernetProtocol = currentConfig.getXcpsettings().getEthernetProtocol();
        tcpRadioButton.setSelected(ethernetProtocol == EthernetProtocol.TCP);
        udpRadioButton.setSelected(ethernetProtocol == EthernetProtocol.UDP);
        heartbeatSpinner.setEnabled(ethernetProtocol == EthernetProtocol.UDP);
        heartbeatSpinner.setValue(currentConfig.getXcpsettings().getHeartbeatInterval());

        // set title
        setTitle("Communication Settings [" + currentConfig.getProtocol()+ "]");

        if (Util.isWindows()) {
            // enable buttons if possible. Only available in windows
            if ((new File("C:\\Program Files\\PCAN\\NetCfg32.exe")).exists()) {
                buttonNets.setEnabled(true);
            }
            if ((new File("C:\\Program Files\\PCAN\\PcanStat.exe")).exists()) {
                buttonStatus.setEnabled(true);
            }
            if ((new File("C:\\Program Files\\PCAN\\PcanView.exe")).exists()) {
                buttonView.setEnabled(true);
            }
        }

        // set location
        setLocationRelativeTo(null);
    }

    private static void addComPorts(LinkedHashMap<String, String> UARTports) {
        // add COM ports for windows
        if (Util.isWindows()) {
            for (int i = 1; i <= 30; i++) {
                UARTports.put("COM" + i, "COM" + i);
            }
        } else {
            // add tty names for linux
            if (Util.isLinux()) {
                for (int idx = 0; idx < 10; idx++) {
                    UARTports.put("ttyUSB" + idx, "/dev/ttyUSB" + idx);
                }
                for (int idx = 0; idx < 10; idx++) {
                    UARTports.put("ttyACM" + idx, "/dev/ttyACM" + idx);
                }
            }
        }
    }

    public void showProtocolTab(CurrentConfig.Protocol protocol) {
        switch (protocol) {
            case CAN:
            case XCP_ON_CAN:
                tabbedPaneSettings.setSelectedIndex(1);
                break;
            case XCP_ON_ETHERNET:
                tabbedPaneSettings.setSelectedIndex(2);
                break;
            case XCP_ON_UART:
                tabbedPaneSettings.setSelectedIndex(3);
                break;
        }
    }

    private void handleProtocolRadioButtonAction(EthernetProtocol protocol) {
        ethernetProtocol = protocol;
        heartbeatSpinner.setEnabled(protocol == EthernetProtocol.UDP);
    }

    private void showError(String error) {
        infoLabel.setForeground(Color.RED);
        infoLabel.setText(error);
    }
    private void handleProtocolComboBoxAction(ActionEvent e) {
        if (comboDriver.getSelectedItem() == CurrentConfig.Protocol.XCP_ON_CAN && currentConfig.canUseDoubles()) {
            int cnt = countChangedSignals(false);
            int cnt2 = countChangedParams(false);
            if (cnt > 0 || cnt2 > 0) {
                showError(ConnectionDialog.getDoublesWarningString(CurrentConfig.Protocol.XCP_ON_CAN.toString(), cnt, cnt2));
            }
        }
    }

    private int countChangedSignals(boolean allowed) {
        int changedSignals = 0;
        if (currentConfig.getASAP2Data() != null) {
            for (ASAP2Measurement mmt : currentConfig.getASAP2Data().getMeasurements()) {
                if (mmt.getDatatype().getDataTypeSize() >= ASAP2Datatype.FLOAT64_IEEE.getDataTypeSize() &&
                    (mmt.isAllowed() != allowed)) {
                    changedSignals++;
                }
            }
        }
        return changedSignals;
    }


    private int countChangedParams(boolean active) {
        int changedParams = 0;
        if (currentConfig.getASAP2Data() != null) {
            for (ASAP2Characteristic ch : currentConfig.getASAP2Data().getCharacteristics()) {
                if (ch.getDatatype().getDataTypeSize() >= ASAP2Datatype.FLOAT64_IEEE.getDataTypeSize() &&
                    (ch.isActive() != active)) {
                    changedParams++;
                }
            }
        }
        return changedParams;
    }

    class HexNumberEditor extends JSpinner.NumberEditor {

        private static final long serialVersionUID = 7526471155622776147L;

        public HexNumberEditor(JSpinner spinner) {
            super(spinner);
            JFormattedTextField ftf = getTextField();
            ftf.setEditable(true);
            ftf.setFormatterFactory(new HexNumberFormatterFactory());
        }
    }

    class HexNumberFormatterFactory extends DefaultFormatterFactory {

        private static final long serialVersionUID = 7526471155622776148L;

        public HexNumberFormatterFactory() {
            super(new HexNumberFormatter());
        }
    }

    class HexNumberFormatter extends JFormattedTextField.AbstractFormatter {

        private static final long serialVersionUID = 7526471155622776149L;

        /**
         * Converts a hex-coded String to an Integer
         */
        @Override
        public Object stringToValue(String text) throws ParseException {
            return Integer.parseInt(text, 16);
        }

        /**
         * Converts a Number to a hex-coded String (8 digits long)
         */
        @Override
        public String valueToString(Object value) throws ParseException {
            Number number = (Number) value;
            String s = Integer.toHexString(number.intValue()).toUpperCase();
            while (s.length() < 2) {
                s = "0" + s;
            }
            return s;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("All")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        protocolButtonGroup = new javax.swing.ButtonGroup();
        tabbedPaneSettings = new javax.swing.JTabbedPane();
        panelGeneral = new javax.swing.JPanel();
        comboDriver = new JComboBox<>(CurrentConfig.Protocol.values());
        labelDriver = new javax.swing.JLabel();
        infoLabel = new javax.swing.JTextArea();
        infoLabelPane = new javax.swing.JScrollPane();
        spinnerTPrescaler = new javax.swing.JSpinner();
        labelTPrescaler = new javax.swing.JLabel();
        panelCAN = new javax.swing.JPanel();
        comboBaudrate = new javax.swing.JComboBox<>();
        labelBaudrate = new javax.swing.JLabel();
        buttonView = new javax.swing.JButton();
        buttonNets = new javax.swing.JButton();
        buttonStatus = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        labelIDtx = new javax.swing.JLabel();
        labelIDrx = new javax.swing.JLabel();
        spinnerIDrx = new javax.swing.JSpinner();
        spinnerIDtx = new javax.swing.JSpinner();
        jComboBox1 = new javax.swing.JComboBox<>(CurrentConfig.CanApi.values());
        jLabel1 = new javax.swing.JLabel();
        panelEthernet = new javax.swing.JPanel();
        textFieldIP = new javax.swing.JTextField();
        labelIP = new javax.swing.JLabel();
        spinnerPort = new javax.swing.JSpinner();
        labelPort = new javax.swing.JLabel();
        comboEthernetBandwidth = new javax.swing.JComboBox<>();
        labelEthernetBandwidth = new javax.swing.JLabel();
        tcpRadioButton = new javax.swing.JRadioButton();
        udpRadioButton = new javax.swing.JRadioButton();
        protocolLabel = new javax.swing.JLabel();
        heartbeatSpinner = new javax.swing.JSpinner();
        heartbeatLabel = new javax.swing.JLabel();
        panelUART = new javax.swing.JPanel();
        comboUARTPort = new javax.swing.JComboBox<>();
        labelUARTPort = new javax.swing.JLabel();
        labelUARTBaudrate = new javax.swing.JLabel();
        comboUARTBaudrate = new javax.swing.JComboBox<>();
        usbCheckbox = new javax.swing.JCheckBox();
        buttonCancel = new javax.swing.JButton();
        buttonOK = new javax.swing.JButton();

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);

        labelDriver.setText("Communication driver");

        infoLabelPane.setBorder(null);
        infoLabelPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        infoLabelPane.setWheelScrollingEnabled(false);
        infoLabel.setEditable(false);
        infoLabel.setHighlighter(null);
        infoLabel.setRows(4);
        infoLabel.setOpaque(false);
        infoLabel.setLineWrap(true);
        infoLabel.setWrapStyleWord(true);
        infoLabel.setFont(new Font("Tahoma", 0, 11));
        infoLabel.setText("");
        infoLabelPane.setViewportView(infoLabel);

        spinnerTPrescaler.setModel(new javax.swing.SpinnerNumberModel(1, 1, 500, 1));
        spinnerTPrescaler.setPreferredSize(new java.awt.Dimension(85, 20));

        labelTPrescaler.setText("Timeout prescaler (use 1 for default timeouts)");

        javax.swing.GroupLayout panelGeneralLayout = new javax.swing.GroupLayout(panelGeneral);
        panelGeneral.setLayout(panelGeneralLayout);
        panelGeneralLayout.setHorizontalGroup(
            panelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelGeneralLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(panelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(infoLabelPane)
                        .addGroup(panelGeneralLayout.createSequentialGroup()
                            .addGroup(panelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panelGeneralLayout.createSequentialGroup()
                                    .addComponent(comboDriver, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(labelDriver))
                                .addGroup(panelGeneralLayout.createSequentialGroup()
                                    .addComponent(spinnerTPrescaler, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(labelTPrescaler)))
                            .addGap(0, 138, Short.MAX_VALUE)))
                    .addContainerGap())
        );

        panelGeneralLayout.setVerticalGroup(
            panelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelGeneralLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(panelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(comboDriver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelDriver))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(panelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(spinnerTPrescaler, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelTPrescaler))
                    .addGap(20, 20, 20)
                    .addComponent(infoLabelPane, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(65, Short.MAX_VALUE))
        );

        tabbedPaneSettings.addTab("General", panelGeneral);

        comboBaudrate.setPreferredSize(new java.awt.Dimension(85, 20));
        comboBaudrate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboBaudrateFocusGained(evt);
            }
        });

        labelBaudrate.setText("CAN baudrate");

        buttonView.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pcan.png"))); // NOI18N
        buttonView.setText("PCAN-View");
        buttonView.setEnabled(false);
        if (Util.isLinux()) {
            // pcan tool not available under linux
            buttonView.setVisible(false);
        }
        buttonView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonViewActionPerformed(evt);
            }
        });

        buttonNets.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pcan.png"))); // NOI18N
        buttonNets.setText("Nets Configuration");
        buttonNets.setEnabled(false);
        if (Util.isLinux()) {
            // pcan tool not available under linux
            buttonNets.setVisible(false);
        }
        buttonNets.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNetsActionPerformed(evt);
            }
        });

        buttonStatus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pcan.png"))); // NOI18N
        buttonStatus.setText("Status Display");
        buttonStatus.setEnabled(false);
        if (Util.isLinux()) {
            // pcan tool not available under linux
            buttonStatus.setVisible(false);
        }
        buttonStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonStatusActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("XCP on CAN"));

        labelIDtx.setText("CAN identifier used for XCP transmission (hexadecimal)");

        labelIDrx.setText("CAN identifier used for XCP reception (hexadecimal)");

        spinnerIDrx.setModel(new javax.swing.SpinnerNumberModel(102, 0, 2047, 1));
        spinnerIDrx.setPreferredSize(new java.awt.Dimension(85, 20));

        spinnerIDtx.setModel(new javax.swing.SpinnerNumberModel(101, 0, 2047, 1));
        spinnerIDtx.setPreferredSize(new java.awt.Dimension(85, 20));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(spinnerIDtx, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(labelIDtx))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(spinnerIDrx, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(labelIDrx)))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(spinnerIDtx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelIDtx))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(spinnerIDrx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelIDrx))
                    .addContainerGap())
        );

        jLabel1.setText("API");

        javax.swing.GroupLayout panelCANLayout = new javax.swing.GroupLayout(panelCAN);
        panelCAN.setLayout(panelCANLayout);
        panelCANLayout.setHorizontalGroup(
            panelCANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelCANLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(panelCANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelCANLayout.createSequentialGroup()
                            .addGroup(panelCANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panelCANLayout.createSequentialGroup()
                                    .addComponent(buttonView, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(buttonNets, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(buttonStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panelCANLayout.createSequentialGroup()
                                    .addGroup(panelCANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(comboBaudrate, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(panelCANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1)
                                        .addComponent(labelBaudrate))))
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap())
        );
        panelCANLayout.setVerticalGroup(
            panelCANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelCANLayout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelCANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1))
                    .addGap(4, 4, 4)
                    .addGroup(panelCANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(comboBaudrate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelBaudrate))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(panelCANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(buttonView)
                        .addComponent(buttonNets)
                        .addComponent(buttonStatus))
                    .addContainerGap())
        );

        tabbedPaneSettings.addTab("CAN", panelCAN);

        textFieldIP.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldIP.setText("127.0.0.1");

        labelIP.setText("Destination IP address (e.g. 169.254.19.63)");

        spinnerPort.setModel(new javax.swing.SpinnerNumberModel(1000, 0, 65535, 1));

        labelPort.setText("Destination port (e.g. 1000)");

        comboEthernetBandwidth.setPreferredSize(new java.awt.Dimension(85, 20));
        comboEthernetBandwidth.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboEthernetBandwidthFocusGained(evt);
            }
        });

        labelEthernetBandwidth.setText("Ethernet bandwidth (for busload calculation)");

        protocolButtonGroup.add(tcpRadioButton);
        tcpRadioButton.setText("TCP");
        tcpRadioButton.addActionListener(e -> handleProtocolRadioButtonAction(EthernetProtocol.TCP));

        protocolButtonGroup.add(udpRadioButton);
        udpRadioButton.setText("UDP");
        udpRadioButton.addActionListener(e -> handleProtocolRadioButtonAction(EthernetProtocol.UDP));
        
        protocolLabel.setText("Protocol");        
        heartbeatLabel.setText("Heartbeat interval (in seconds)");

        javax.swing.GroupLayout panelEthernetLayout = new javax.swing.GroupLayout(panelEthernet);
        panelEthernet.setLayout(panelEthernetLayout);
        panelEthernetLayout.setHorizontalGroup(
            panelEthernetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEthernetLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelEthernetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelEthernetLayout.createSequentialGroup()
                        .addComponent(textFieldIP, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelIP))
                    .addGroup(panelEthernetLayout.createSequentialGroup()
                        .addComponent(spinnerPort, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelPort))
                    .addGroup(panelEthernetLayout.createSequentialGroup()
                        .addComponent(comboEthernetBandwidth, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelEthernetBandwidth))
                    .addGroup(panelEthernetLayout.createSequentialGroup()
                        .addComponent(tcpRadioButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(udpRadioButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(protocolLabel))
                    .addGroup(panelEthernetLayout.createSequentialGroup()
                        .addComponent(heartbeatSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(heartbeatLabel)))
                .addContainerGap(152, Short.MAX_VALUE))
        );
        panelEthernetLayout.setVerticalGroup(
            panelEthernetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEthernetLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelEthernetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldIP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelIP))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEthernetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spinnerPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPort))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEthernetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboEthernetBandwidth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelEthernetBandwidth))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEthernetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tcpRadioButton)
                    .addComponent(udpRadioButton)
                    .addComponent(protocolLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEthernetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(heartbeatSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(heartbeatLabel))
                .addContainerGap(110, Short.MAX_VALUE))
        );

        tabbedPaneSettings.addTab("Ethernet", panelEthernet);

        comboUARTPort.setPreferredSize(new java.awt.Dimension(85, 20));
        comboUARTPort.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboUARTPortFocusGained(evt);
            }
        });
        comboUARTPort.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboUARTPortActionPerformed(evt);
            }
        });

        labelUARTPort.setText(Util.isWindows() ? "UART port" : Util.isLinux() ? "UART tty device" : "  ");

        labelUARTBaudrate.setText("UART baudrate");

        comboUARTBaudrate.setPreferredSize(new java.awt.Dimension(85, 20));
        comboUARTBaudrate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboUARTBaudrateFocusGained(evt);
            }
        });

        usbCheckbox.setSelected(true);
        usbCheckbox.setText("Connection via USB (for busload calculation)");
        usbCheckbox.setToolTipText("<html>When using a USB Virtual COM Port, the data rate is not limited by the baud rate. Instead it is limited by other factors,<br/>\nsuch as the used hardware and other active USB devices. Therefore a baud rate cannot be set when this option is <br/>\nchecked and a data rate of 1 Mbit/s is used to calculate the busload instead.</html>");
        usbCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usbCheckboxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelUARTLayout = new javax.swing.GroupLayout(panelUART);
        panelUART.setLayout(panelUARTLayout);
        panelUARTLayout.setHorizontalGroup(
            panelUARTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelUARTLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(panelUARTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(usbCheckbox)
                        .addGroup(panelUARTLayout.createSequentialGroup()
                            .addComponent(comboUARTPort, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(labelUARTPort))
                        .addGroup(panelUARTLayout.createSequentialGroup()
                            .addComponent(comboUARTBaudrate, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(labelUARTBaudrate)))
                    .addContainerGap(291, Short.MAX_VALUE))
        );
        panelUARTLayout.setVerticalGroup(
            panelUARTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelUARTLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(panelUARTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(comboUARTPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelUARTPort))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(panelUARTLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(comboUARTBaudrate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelUARTBaudrate))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(usbCheckbox)
                    .addContainerGap(135, Short.MAX_VALUE))
        );

        tabbedPaneSettings.addTab("USB/UART", panelUART);

        buttonCancel.setText("Cancel");
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelActionPerformed(evt);
            }
        });

        buttonOK.setText("OK");
        buttonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonOKActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(tabbedPaneSettings)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(buttonOK)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(buttonCancel)))
                    .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(tabbedPaneSettings)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(buttonCancel)
                        .addComponent(buttonOK))
                    .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonOKActionPerformed
        // disconnect if necessary
        if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()) {
            int result = MessagePane.showConfirmDialog("Proceeding will disconnect HANtune. Do you wish to continue?", "Save communication settings");
            if (result == JOptionPane.OK_OPTION) {
                hantune.connectProtocol(false);
            } else {
                return;
            }
        }

        // save settings
        currentConfig.setProtocol((CurrentConfig.Protocol) comboDriver.getSelectedItem());
        currentConfig.getXcpsettings().setCANIDRx((Integer) spinnerIDrx.getValue());
        currentConfig.getXcpsettings().setCANIDTx((Integer) spinnerIDtx.getValue());
        currentConfig.getXcpsettings().setCANBaudrate(br.get((String) comboBaudrate.getSelectedItem()));
        currentConfig.getXcpsettings().setEthernetIP(textFieldIP.getText());
        currentConfig.getXcpsettings().setEthernetPort((Integer) spinnerPort.getValue());
        currentConfig.getXcpsettings().setEthernetBandwidth(ethernetBandwidths.get((String) comboEthernetBandwidth.getSelectedItem()));
        currentConfig.getXcpsettings().setEthernetProtocol(ethernetProtocol);
        currentConfig.getXcpsettings().setHeartbeatInterval((Integer) heartbeatSpinner.getValue());
        currentConfig.getXcpsettings().setUARTPort(UARTports.get((String) comboUARTPort.getSelectedItem()));
        currentConfig.getXcpsettings().setUARTBaudrate(UARTbaudrates.get((String) comboUARTBaudrate.getSelectedItem()));
        currentConfig.getXcpsettings().setUsbVirtualComPort(usbCheckbox.isSelected());
        currentConfig.getXcpsettings().setTPrescaler((Integer) spinnerTPrescaler.getValue());
        //Check whether the combination of Time-out Prescaler, DAQlist prescaler and ECU cycle time can cause time-outs
        currentConfig.getHANtuneManager().checkDaqListTimeOut();

        currentConfig.getHANtuneManager().set64BitSignalsAllowed(currentConfig.canUseDoubles());
        currentConfig.getHANtuneManager().set64BitParamsActive(currentConfig.canUseDoubles());
        currentConfig.getHANtuneManager().updateDefaultDaqList();

        currentConfig.updateAsap2Listeners();
        hantune.updateDaqListListeners();
        dispose();
    }//GEN-LAST:event_buttonOKActionPerformed

    private void buttonStatusActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler C:\\Program Files\\PCAN\\PcanStat.exe");
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            MessagePane.showError("Could not start PCAN Status Display");
        }
    }

    private void buttonNetsActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler C:\\Program Files\\PCAN\\NetCfg32.exe");
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            MessagePane.showError("Could not start PCAN Nets Configuration");
        }
    }

    private void buttonViewActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler C:\\Program Files\\PCAN\\PcanView.exe");
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            MessagePane.showError("Could not start PCAN-View");
        }
    }

    private void buttonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelActionPerformed
        dispose();
    }//GEN-LAST:event_buttonCancelActionPerformed

    private void comboUARTPortFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboUARTPortFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_comboUARTPortFocusGained

    private void comboUARTBaudrateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboUARTBaudrateFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_comboUARTBaudrateFocusGained

    private void comboUARTPortActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboUARTPortActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboUARTPortActionPerformed

    private void comboEthernetBandwidthFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboEthernetBandwidthFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_comboEthernetBandwidthFocusGained

    private void usbCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usbCheckboxActionPerformed
        JCheckBox checkbox = (JCheckBox) evt.getSource();
        comboUARTBaudrate.setEnabled(!checkbox.isSelected());
    }//GEN-LAST:event_usbCheckboxActionPerformed

    private void comboBaudrateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboBaudrateFocusGained
    }//GEN-LAST:event_comboBaudrateFocusGained


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonCancel;
    private javax.swing.JButton buttonNets;
    private javax.swing.JButton buttonOK;
    private javax.swing.JButton buttonStatus;
    private javax.swing.JButton buttonView;
    private javax.swing.JComboBox<Object> comboBaudrate;
    private javax.swing.JComboBox<CurrentConfig.Protocol> comboDriver;
    private javax.swing.JComboBox<Object> comboEthernetBandwidth;
    private javax.swing.JComboBox<Object> comboUARTBaudrate;
    private javax.swing.JComboBox<Object> comboUARTPort;
    private javax.swing.JComboBox<CurrentConfig.CanApi> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel labelBaudrate;
    private javax.swing.JLabel labelDriver;
    private javax.swing.JTextArea infoLabel;
    private javax.swing.JScrollPane infoLabelPane;

    private javax.swing.JLabel labelEthernetBandwidth;
    private javax.swing.JLabel labelIDrx;
    private javax.swing.JLabel labelIDtx;
    private javax.swing.JLabel labelIP;
    private javax.swing.JLabel labelPort;
    private javax.swing.JLabel labelTPrescaler;
    private javax.swing.JLabel labelUARTBaudrate;
    private javax.swing.JLabel labelUARTPort;
    private javax.swing.JPanel panelCAN;
    private javax.swing.JPanel panelEthernet;
    private javax.swing.JPanel panelGeneral;
    private javax.swing.JPanel panelUART;
    private javax.swing.ButtonGroup protocolButtonGroup;
    private javax.swing.JSpinner spinnerIDrx;
    private javax.swing.JSpinner spinnerIDtx;
    private javax.swing.JSpinner spinnerPort;
    private javax.swing.JSpinner spinnerTPrescaler;
    private javax.swing.JTabbedPane tabbedPaneSettings;
    private javax.swing.JTextField textFieldIP;
    private javax.swing.JRadioButton tcpRadioButton;
    private javax.swing.JRadioButton udpRadioButton;
    private javax.swing.JLabel protocolLabel;
    private javax.swing.JSpinner heartbeatSpinner;
    private javax.swing.JLabel heartbeatLabel;
    private javax.swing.JCheckBox usbCheckbox;
    // End of variables declaration//GEN-END:variables

}

