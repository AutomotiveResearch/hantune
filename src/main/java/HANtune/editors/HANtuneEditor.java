/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

import HANtune.ReferenceWindow;
import datahandling.Parameter;
import datahandling.ParameterListener;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *  A base class for all windows that handle Parameters.
 *
 * @author Michiel Klifman
 * @param <T>
 */
@SuppressWarnings("serial")
public abstract class HANtuneEditor<T extends ParameterListener> extends ReferenceWindow<Parameter> {

    protected final Map<Parameter, T> parameters = new LinkedHashMap<>();

    public HANtuneEditor() {
        this(true);
    }

    public HANtuneEditor(boolean multiReferenceWindow) {
        super(Parameter.class, multiReferenceWindow);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/settings.png")));
    }

    @Override
    protected void addReferenceContent() {
        for (Parameter parameter : references) {
            T listener = getListenerForParameter(parameter);
            parameter.addListener(listener);
            parameter.addStateListener(listener);
            listener.stateUpdate();
            parameters.put(parameter, listener);
            if (!multiReferenceWindow) break;
        }
    }

    @Override
    public void clearWindow() {
        super.clearWindow();
        for (Map.Entry<Parameter, T> entry : parameters.entrySet()) {
            entry.getKey().removeListener(entry.getValue());
        }
        parameters.clear();
    }

    protected abstract T getListenerForParameter(Parameter parameter);


    /**
     * Sends a value for a parameter in this window, but only if the parameter is active within this window.
     * Due to the nature of ChangeListener's for certain editors (i.e. slider) this method might be called
     * multiple times after an adjustment in the editor. In this case a value might also be tried to send
     * multiple times when there is no connection because the lastSentValue of a parameter never changes.
     *
     * @param parameter - the parameter of which a value must be send.
     * @param value - the value to send.
     */
    protected void send(Parameter parameter, double value) {
        if (!inactiveReferences.contains(parameter)) {
            parameter.send(value);
        }
    }
}
