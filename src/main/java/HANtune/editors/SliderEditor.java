/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

import HANtune.DisplayrangeDialog;
import HANtune.editors.SliderEditor.SliderEditorElement;
import components.ReferenceLabel;
import datahandling.CurrentConfig;
import datahandling.Parameter;
import datahandling.ParameterListener;
import util.Util;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;
import javax.swing.*;

/**
 *
 * @author Mathijs, Michel de Beijer, Mike van Laar, Michiel Klifman
 */
@SuppressWarnings("serial")
public class SliderEditor extends HANtuneEditor<SliderEditorElement> {

    private DisplayrangeDialog displayrangeDialog;
    public double default_lower = 0;
    public double default_upper = 100;

    /**
     * The constructor of the SliderEditor class
     */
    public SliderEditor() {
        setTitle("SliderEditor");
        setVerticalResizable(false);
    }

    @Override
    public void rebuild() {
        super.rebuild();
        packAndSetMinMax();
    }

    @Override
    protected SliderEditorElement getListenerForParameter(Parameter parameter) {
        return createSliderEditorElement(parameter);
    }

    protected SliderEditorElement createSliderEditorElement(Parameter parameter) {
        int row = getContentPane().getComponentCount();
        GridBagConstraints gridBagConstraints;
        SliderEditorElement element = new SliderEditorElement(parameter);

        JLabel nameLabel = element.nameLabel;
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = row;
        gridBagConstraints.insets = new Insets(7, 7, 7, 7);
        getContentPane().add(nameLabel, gridBagConstraints);

        JTextField valueJLabel = element.valueField;
        valueJLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        valueJLabel.setColumns(10);
        valueJLabel.setEditable(false);
        String valueText = Integer.toString((int) parameter.validateValue(parameter.getValue()));
        valueJLabel.setText(valueText);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = row;
        gridBagConstraints.insets = new Insets(7, 7, 7, 7);
        getContentPane().add(valueJLabel, gridBagConstraints);

        JSlider slider = element.slider;
        slider.setName(parameter.getName());
        slider.setPreferredSize(new Dimension(80, 22));
        slider.setMinimum((int) parameter.getMinimum());
        slider.setMaximum((int) parameter.getMaximum());
        slider.setValue((int) parameter.validateValue(parameter.getValue()));
        slider.setMajorTickSpacing((int) (Util.calcMajorTick(slider.getMinimum(), slider.getMaximum())));
        slider.addChangeListener(e -> {
            element.updateTextField();
            if(slider.getValueIsAdjusting()){
               double value = element.getValue();
                send(parameter, value);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = row;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(7, 7, 7, 7);
        getContentPane().add(slider, gridBagConstraints);

        if (CurrentConfig.getInstance().isServiceToolMode()) {
            slider.setComponentPopupMenu(null);
        } else {
            addPopupMenuItems(parameter, slider);
        }

        element.setComponentColors();

        if (!settings.containsKey(parameter.getName()) || !settings.get(parameter.getName()).containsKey("decimals")) {
            if (parameter.hasDecimals() && parameter.getFactor() == 1) {
                addSetting(parameter.getName(), "decimals", "" + 2);
            } else if (parameter.hasDecimals() && parameter.getFactor() != 1) {
                addSetting(parameter.getName(), "decimals", "" + 4);
            }
        }
        return element;
    }

    private void addPopupMenuItems(Parameter parameter, JSlider slider) {
        JPopupMenu sliderPopup = new JPopupMenu();
        JMenuItem modDisplayrangeMenuItem = new JMenuItem("Modify range");
        modDisplayrangeMenuItem.addActionListener(e -> {
            displayrangeDialog = new DisplayrangeDialog(SliderEditor.this, parameter, false);
            loadSettings();
        });
        sliderPopup.add(modDisplayrangeMenuItem);

        if (parameter.hasDecimals()) {
            JMenuItem editDecimalsMenuItem = new JMenuItem("Modify decimals");
            editDecimalsMenuItem.addActionListener(e -> {
                actionDecimals(parameter.getName());
                loadSettings();
            });
            sliderPopup.add(editDecimalsMenuItem);
        }

        slider.setComponentPopupMenu(sliderPopup);
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     */
    @Override
    public void loadSettings() {
        repaint();

        for (Map.Entry<Parameter, SliderEditorElement> entry : parameters.entrySet()) {
            Parameter parameter = entry.getKey();
            SliderEditorElement element = entry.getValue();
            JSlider slider = element.slider;
            if (settings.containsKey(parameter.getName()) && settings.get(parameter.getName()).containsKey("limit_lower")) {
                element.minimum = Double.parseDouble(settings.get(parameter.getName()).get("limit_lower"));
            }
            if (settings.containsKey(parameter.getName()) && settings.get(parameter.getName()).containsKey("limit_upper")) {
                element.maximum = Double.parseDouble(settings.get(parameter.getName()).get("limit_upper"));
            }

            //Since JSlider can only handle integers, the min and max have to be
            //recalculated for fractional values. Only half of Integer.MAX_VALUE
            //is used as a base because Integer.MAX_VALUE can cause overflow problems.
            if (parameter.hasDecimals()) {
                int minBase = Integer.MIN_VALUE / 2;
                int maxBase = Integer.MAX_VALUE / 2;
                if (Math.abs(element.minimum) > Math.abs(element.maximum)) {
                    slider.setMinimum(minBase);
                    slider.setMaximum((int) (maxBase * (element.maximum / element.minimum) * -1));
                    element.factor = element.minimum / minBase;
                } else if (Math.abs(element.minimum) <= Math.abs(element.maximum)) {
                    slider.setMinimum((int) (minBase * (element.minimum / element.maximum) * -1));
                    slider.setMaximum(maxBase);
                    element.factor = element.maximum / maxBase;
                }
            } else {
                slider.setMinimum((int) element.minimum);
                slider.setMaximum((int) element.maximum);
            }

            element.decimalFormat = getFormat(parameter.getName(), element.getHighestLimit());
            element.updateTextField();
            element.valueUpdate(parameter.getValue());
        }
    }

    /**
     * Contains and updates components for a given Parameter.
     */
    protected class SliderEditorElement implements ParameterListener {

        private final Parameter parameter;
        private final JLabel nameLabel;
        private final JTextField valueField = new JTextField();
        private final JSlider slider = new JSlider();
        private NumberFormat decimalFormat = new DecimalFormat();

        private double minimum;
        private double maximum;
        private double factor;

        private SliderEditorElement(Parameter parameter) {
            this.parameter = parameter;
            minimum = parameter.getMinimum();
            maximum = parameter.getMaximum();
            nameLabel = new ReferenceLabel<>(parameter, SliderEditor.this);
        }

        private double getValue() {
            if (parameter.hasDecimals()) {
                return getFractionalValue();
            } else {
                return (double) slider.getValue();
            }
        }

        private double getFractionalValue() {
            double value = slider.getValue() * factor;
            if (parameter.hasDecimals() && parameter.getFactor() != 1) {
                //adjust the value for fixed point numbers
                value = Math.round(value / parameter.getFactor()) * parameter.getFactor();
            }
            return value;
        }

        private void updateTextField() {
            String text = decimalFormat.format(getValue());
            valueField.setText(text);
        }

        private double getHighestLimit() {
            return Math.abs(minimum) > Math.abs(maximum) ? minimum : maximum;
        }

        @Override
        public void valueUpdate(double value) {
            if (!slider.getValueIsAdjusting()) {
                if (!parameter.hasDecimals()) {
                    slider.setValue((int) value);
                } else {
                    slider.setValue((int) (value / factor));
                }
            }
        }

        @Override
        public void stateUpdate() {
            setComponentColors();
        }

        private void setComponentColors() {
            if (!parameter.isActive() || inactiveReferences.contains(parameter)) {
                nameLabel.setForeground(INACTIVE_TEXT_COLOR);
                slider.setBackground(INACTIVE_COLOR);
                slider.setToolTipText("N/A");
                slider.setEnabled(false);
            } else if (!parameter.isInSync()) {
                nameLabel.setForeground(ACTIVE_TEXT_COLOR);
                slider.setBackground(WARNING_COLOR);
                slider.setToolTipText("Value could not be sent");
                slider.setEnabled(true);
            } else {
                nameLabel.setForeground(ACTIVE_TEXT_COLOR);
                slider.setBackground(ACTIVE_COLOR);
                slider.setToolTipText(null);
                slider.setEnabled(true);
            }
        }
    }
}
