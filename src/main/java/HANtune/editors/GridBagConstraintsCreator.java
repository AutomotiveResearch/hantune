/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 *
 * @author Mike
 */
public class GridBagConstraintsCreator {
    /**
     * This method creates a new GridBagConstraints object with the properties set to the given parameters.
     * @param gridX :The position on the x axis.
     * @param gridY :The position on the y axis.
     * @return
     */
    public GridBagConstraints createGridBagConstraints(final int gridX, final int gridY) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = gridX;
        gridBagConstraints.gridy = gridY;

        return gridBagConstraints;
    }

    /**
     * This method is an overloaded implementation of createGridBagConstraints.
     * Creates a new GridBagConstraints object with the properties set to the given parameters.
     * @param gridX :The position on the x axis.
     * @param insets :The insets of the content.
     * @return
     */
    public GridBagConstraints createGridBagConstraints(final int gridX, final Insets insets) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = insets;
        gridBagConstraints.gridx = gridX;

        return gridBagConstraints;
    }

    /**
     * This method is an overloaded implementation of createGridBagConstraints.
     * Creates a new GridBagConstraints object with the properties set to the given parameters.
     * @param gridX :The position on the x axis.
     * @param gridY :The position on the y axis.
     * @param insets :The insets of the content.
     * @return
     */
    public GridBagConstraints createGridBagConstraints(final int gridX, final int gridY, final Insets insets) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = insets;
        gridBagConstraints.gridx = gridX;
        gridBagConstraints.gridy = gridY;

        return gridBagConstraints;
    }

    /**
     * This method is an overloaded implementation of createGridBagConstraints.
     * creates a new GridBagConstraints object with the properties set to the given parameters.
     * @param gridX :The position on the x axis.
     * @param gridY :The position on the y axis.
     * @param weightX :The weight on the x axis.
     * @param fill :The fill type of the gridBagConstraints(Default is NONE). example: HORIZONTAL.
     * @return
     */
    public GridBagConstraints createGridBagConstraints(final int gridX, final int gridY, final int weightX, final int fill) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = gridX;
        gridBagConstraints.gridy = gridY;
        gridBagConstraints.weightx = weightX;
        gridBagConstraints.fill = fill;
        gridBagConstraints.insets = new Insets(7, 7, 7, 7);

        return gridBagConstraints;
    }
}
