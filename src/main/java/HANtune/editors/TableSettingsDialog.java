/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

import HANtune.HanTuneDialog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import nl.han.hantune.config.ApplicationProperties;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MAX;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MID;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MIN;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class TableSettingsDialog extends HanTuneDialog {

    private final TableEditor tableEditor;

    private Box valueAndColorBox;
    private Box midBox;

    private JLabel minLabel;
    private JTextField minField;
    private JLabel minColorLabel;
    private Box minColorBox;
    private JButton minColorButton;

    private JLabel midLabel;
    private JTextField midField;
    private JLabel midColorLabel;
    private Box midColorBox;
    private JButton midColorButton;

    private JLabel maxLabel;
    private JTextField maxField;
    private Box maxColorBox;
    private JLabel maxColorLabel;
    private JButton maxColorButton;

    private Box workingPointColorBox;

    private ButtonGroup interpolationButtonGroup;
    private JRadioButton noneButton;
    private JRadioButton rgbButton;
    private JRadioButton hueButton;

    private ButtonGroup calculationButtonGroup;
    private JRadioButton autoButton;
    private JRadioButton customButton;

    private JCheckBox useMidValueCheckbox;
    private JCheckBox showWorkingPointCheckBox;

    private String minFieldValue;
    private String midFieldValue;
    private String maxFieldValue;

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public TableSettingsDialog(TableEditor tableEditor) {
        super(true);
        this.tableEditor = tableEditor;
        setResizable(false);
        createBody();
        createFooter();
        setTitle("Edit Table Settings");
        setLocationRelativeTo(null);

        pack();
        loadSettings();
        updateComponentsEnabledState();
    }

    private void createBody() {
        Box body = Box.createVerticalBox();
        body.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));

        Box colorInterpolatioBox = Box.createHorizontalBox();
        colorInterpolatioBox.setBorder(BorderFactory.createTitledBorder("Cell Color Interpolation"));
        interpolationButtonGroup = new ButtonGroup();

        noneButton = new JRadioButton("None");
        noneButton.setActionCommand("none");
        noneButton.setToolTipText("No interpolation");
        noneButton.setSelected(true);
        noneButton.addActionListener(e -> updateComponentsEnabledState());
        interpolationButtonGroup.add(noneButton);
        colorInterpolatioBox.add(noneButton);

        rgbButton = new JRadioButton("RGB");
        rgbButton.setActionCommand("rgb");
        rgbButton.setToolTipText("Interpolate using the 'red', 'green' and 'blue' of the selected colors");
        rgbButton.addActionListener(e -> updateComponentsEnabledState());
        interpolationButtonGroup.add(rgbButton);
        colorInterpolatioBox.add(rgbButton);

        hueButton = new JRadioButton("HSB");
        hueButton.setActionCommand("hsb");
        hueButton.setToolTipText("Interpolate using the 'hue', 'saturation' and 'brightness' of the selected colors");
        hueButton.addActionListener(e -> updateComponentsEnabledState());
        interpolationButtonGroup.add(hueButton);
        colorInterpolatioBox.add(hueButton);

        colorInterpolatioBox.add(Box.createHorizontalGlue());

        body.add(colorInterpolatioBox);

        body.add(Box.createVerticalStrut(10));

        valueAndColorBox = Box.createVerticalBox();
        valueAndColorBox.setBorder(BorderFactory.createTitledBorder("Cell Values and Colors"));

        Box rangeSettingBox = Box.createHorizontalBox();
        calculationButtonGroup = new ButtonGroup();

        autoButton = new JRadioButton("Auto");
        autoButton.setActionCommand("auto");
        autoButton.setToolTipText("Use minimum and maximum based on the lowest and highest value in the table");
        autoButton.setSelected(true);
        autoButton.addActionListener(e -> updateComponentsEnabledState());
        calculationButtonGroup.add(autoButton);
        rangeSettingBox.add(autoButton);

        customButton = new JRadioButton("Custom");
        customButton.setActionCommand("custom");
        customButton.setToolTipText("Use minimum and maximum as defined by user");
        customButton.addActionListener(e -> updateComponentsEnabledState());
        calculationButtonGroup.add(customButton);
        rangeSettingBox.add(customButton);

        rangeSettingBox.add(Box.createHorizontalGlue());

        valueAndColorBox.add(rangeSettingBox);

        Box midOptionalContainer = Box.createHorizontalBox();
        useMidValueCheckbox = new JCheckBox("Use middle value and color");
        useMidValueCheckbox.addActionListener(e -> updateComponentsEnabledState());
        midOptionalContainer.add(useMidValueCheckbox);
        midOptionalContainer.add(Box.createHorizontalGlue());
        valueAndColorBox.add(midOptionalContainer);

        Box minBox = Box.createHorizontalBox();
        minBox.add(Box.createHorizontalGlue());
        minLabel = new JLabel("Minimum:");
        minBox.add(minLabel);

        minField = createTextField(String.valueOf(0.0));
        minBox.add(minField);

        minColorLabel = new JLabel("Color:");
        minBox.add(minColorLabel);

        minColorBox = Box.createVerticalBox();
        minColorBox.setPreferredSize(new Dimension(21, 21));
        minColorBox.setMaximumSize(minColorBox.getPreferredSize());
        minColorBox.setBackground(Color.WHITE);
        minColorBox.setOpaque(true);
        minBox.add(minColorBox);

        minColorButton = new JButton("Edit");
        minColorButton.addActionListener(e -> editColor(minColorBox));
        minBox.add(minColorButton);

        valueAndColorBox.add(minBox);

        midBox = Box.createHorizontalBox();
        midBox.add(Box.createHorizontalGlue());

        midLabel = new JLabel("Middle:");
        midBox.add(midLabel);

        midField = createTextField(String.valueOf(0.0));
        midBox.add(midField);

        midColorLabel = new JLabel("Color:");
        midBox.add(midColorLabel);

        midColorBox = Box.createVerticalBox();
        midColorBox.setPreferredSize(new Dimension(21, 21));
        midColorBox.setMaximumSize(minColorBox.getPreferredSize());
        midColorBox.setBackground(Color.WHITE);
        midColorBox.setOpaque(true);
        midBox.add(midColorBox);

        midColorButton = new JButton("Edit");
        midColorButton.addActionListener(e -> editColor(midColorBox));
        midBox.add(midColorButton);

        valueAndColorBox.add(midBox);

        Box maxBox = Box.createHorizontalBox();
        maxBox.add(Box.createHorizontalGlue());
        maxLabel = new JLabel("Maximum:");
        maxBox.add(maxLabel);

        maxField = createTextField(String.valueOf(0.0));
        maxBox.add(maxField);

        maxColorLabel = new JLabel("Color:");
        maxBox.add(maxColorLabel);

        maxColorBox = Box.createVerticalBox();
        maxColorBox.setPreferredSize(new Dimension(21, 21));
        maxColorBox.setMaximumSize(minColorBox.getPreferredSize());
        maxColorBox.setBackground(Color.WHITE);
        maxColorBox.setOpaque(true);
        maxBox.add(maxColorBox);

        maxColorButton = new JButton("Edit");
        maxColorButton.addActionListener(e -> editColor(maxColorBox));
        maxBox.add(maxColorButton);

        valueAndColorBox.add(maxBox);

        body.add(valueAndColorBox);

        body.add(Box.createVerticalStrut(10));

        Box workingPointBox = Box.createHorizontalBox();
        workingPointBox.setBorder(BorderFactory.createTitledBorder("Working Point"));
        showWorkingPointCheckBox = new JCheckBox("Show");
        showWorkingPointCheckBox.addActionListener(e -> updateWorkingPoint());
        workingPointBox.add(showWorkingPointCheckBox);
        workingPointBox.add(Box.createHorizontalGlue());

        workingPointBox.add(Box.createHorizontalGlue());
        JLabel workingPointColorLabel = new JLabel("Color:");
        workingPointBox.add(workingPointColorLabel);

        workingPointColorBox = Box.createVerticalBox();
        workingPointColorBox.setPreferredSize(new Dimension(21, 21));
        workingPointColorBox.setMaximumSize(minColorBox.getPreferredSize());
        workingPointColorBox.setBackground(Color.WHITE);
        workingPointColorBox.setOpaque(true);
        workingPointBox.add(workingPointColorBox);

        JButton editWorkingPointColorButton = new JButton("Edit");
        editWorkingPointColorButton.addActionListener(e -> editColor(workingPointColorBox));
        workingPointBox.add(editWorkingPointColorButton);

        body.add(workingPointBox);

        body.add(Box.createVerticalStrut(10));

        this.add(body, BorderLayout.CENTER);

    }

    private void updateComponentsEnabledState() {
        boolean noneSelected = noneButton.isSelected();
        boolean rgbSelected = rgbButton.isSelected();
        boolean customSelected = customButton.isSelected();
        boolean useMiddleValueSelected = useMidValueCheckbox.isSelected();

        autoButton.setEnabled(!noneSelected);
        customButton.setEnabled(!noneSelected);

        useMidValueCheckbox.setEnabled(rgbSelected);

        minLabel.setEnabled(!noneSelected && customSelected);
        minField.setEnabled(!noneSelected && customSelected);
        minColorLabel.setEnabled(!noneSelected);
        minColorButton.setEnabled(!noneSelected);

        midLabel.setEnabled(!noneSelected && customSelected);
        midField.setEnabled(!noneSelected && customSelected);
        midColorLabel.setEnabled(!noneSelected);
        midColorButton.setEnabled(!noneSelected);

        maxLabel.setEnabled(!noneSelected && customSelected);
        maxField.setEnabled(!noneSelected && customSelected);
        maxColorLabel.setEnabled(!noneSelected);
        maxColorButton.setEnabled(!noneSelected);

        midBox.setVisible((noneSelected || rgbSelected) && useMiddleValueSelected);

        updateTableColors();
    }

    private void updateWorkingPoint() {
        tableEditor.getPanel().setPointColor(workingPointColorBox.getBackground());
        tableEditor.getPanel().setShowPoint(showWorkingPointCheckBox.isSelected());
        tableEditor.repaint();
    }

    private void createFooter() {
        Box footer = Box.createHorizontalBox();
        footer.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
        footer.add(Box.createHorizontalGlue());

        JButton okButton = new JButton("Ok");
        okButton.addActionListener(e -> saveAndDispose());
        footer.add(okButton);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> dispose());
        footer.add(cancelButton);

        this.add(footer, BorderLayout.SOUTH);
    }

    private void editColor(Box colorBox) {
        Color newColor = JColorChooser.showDialog(this, "Choose font color", colorBox.getBackground());
        if (newColor != null) {
            colorBox.setBackground(newColor);
            updateTableColors();
            updateWorkingPoint();
        }
    }

    private void updateTableColors() {

        double minValue = 0;
        double midValue = 0;
        double maxValue = 0;
        if (autoButton.isSelected()) {
            tableEditor.findMinAndMax();
        } else {
            try {
                minValue = Double.parseDouble(minField.getText());
            } catch (NumberFormatException exception) {
            }
            try {
                midValue = Double.parseDouble(midField.getText());
            } catch (NumberFormatException exception) {
            }
            try {
                maxValue = Double.parseDouble(maxField.getText());
            } catch (NumberFormatException exception) {
            }
            tableEditor.setMinTableValue(minValue);
            tableEditor.setMidTableValue(midValue);
            tableEditor.setMaxTableValue(maxValue);
        }
        tableEditor.setInterpolation(interpolationButtonGroup.getSelection().getActionCommand());
        TableFieldColor.initColors(minColorBox.getBackground(), midColorBox.getBackground(), maxColorBox.getBackground(), useMidValueCheckbox.isSelected(), hueButton.isSelected(), autoButton.isSelected(), minValue, midValue, maxValue);
        tableEditor.stateUpdate();

    }

    private JTextField createTextField(String text) {
        JTextField textField = new JTextField(text);
        textField.setPreferredSize(new Dimension(75, 21));
        textField.setMaximumSize(textField.getPreferredSize());
        textField.setHorizontalAlignment(SwingConstants.RIGHT);
        textField.addActionListener(e -> updateTableColors());
        return textField;
    }

    private void saveAndDispose() {
        saveSettings();
        dispose();
    }

    private void loadSettings() {
        String interpolation = tableEditor.getWindowSetting("interpolation", "none");
        switch (interpolation) {
            default:
            case "none":
                noneButton.setSelected(true);
                break;
            case "rgb":
                rgbButton.setSelected(true);
                break;
            case "hsb":
                hueButton.setSelected(true);
                break;
        }

        String calculation = tableEditor.getWindowSetting("calculation", "auto");
        switch (calculation) {
            default:
            case "auto":
                autoButton.setSelected(true);
                break;
            case "custom":
                customButton.setSelected(true);
                break;
        }

        boolean useMid = tableEditor.getBooleanWindowSetting("useMid", false);
        useMidValueCheckbox.setSelected(useMid);

        minFieldValue = tableEditor.getWindowSetting("min", "auto");
        minField.setText(minFieldValue);

        midFieldValue = tableEditor.getWindowSetting("mid", "auto");
        midField.setText(midFieldValue);

        maxFieldValue = tableEditor.getWindowSetting("max", "auto");
        maxField.setText(maxFieldValue);

        String minColorString = tableEditor.getWindowSetting("minColor", "");
        Color minColor = !minColorString.isEmpty() ? Color.decode(minColorString) : ApplicationProperties.getColor(TABLE_FIELD_COLOR_MIN);
        minColorBox.setBackground(minColor);

        String midColorString = tableEditor.getWindowSetting("midColor", "");
        Color midColor = !midColorString.isEmpty() ? Color.decode(midColorString) : ApplicationProperties.getColor(TABLE_FIELD_COLOR_MID);
        midColorBox.setBackground(midColor);

        String maxColorString = tableEditor.getWindowSetting("maxColor", "");
        Color maxColor = !maxColorString.isEmpty() ? Color.decode(maxColorString) : ApplicationProperties.getColor(TABLE_FIELD_COLOR_MAX);
        maxColorBox.setBackground(maxColor);

        String workingPointColorString = tableEditor.getWindowSetting("workingPointColor", "");
        Color workingPointColor = !workingPointColorString.isEmpty() ? Color.decode(workingPointColorString) : new Color(20, 20, 20);
        workingPointColorBox.setBackground(workingPointColor);

        boolean enableWorkingPoint = tableEditor.getBooleanWindowSetting("showWorkingPoint", false);
        showWorkingPointCheckBox.setSelected(enableWorkingPoint);
    }

    private void saveSettings() {
        tableEditor.addWindowSetting("interpolation", interpolationButtonGroup.getSelection().getActionCommand());
        tableEditor.addWindowSetting("calculation", calculationButtonGroup.getSelection().getActionCommand());

        tableEditor.addWindowSetting("useMid", String.valueOf(useMidValueCheckbox.isSelected()));

        tableEditor.addWindowSetting("min", minField.getText());
        tableEditor.addWindowSetting("mid", midField.getText());
        tableEditor.addWindowSetting("max", maxField.getText());

        tableEditor.addWindowSetting("minColor", String.valueOf(minColorBox.getBackground().hashCode()));
        tableEditor.addWindowSetting("midColor", String.valueOf(midColorBox.getBackground().hashCode()));
        tableEditor.addWindowSetting("maxColor", String.valueOf(maxColorBox.getBackground().hashCode()));

        tableEditor.addWindowSetting("showWorkingPoint", String.valueOf(showWorkingPointCheckBox.isSelected()));
        tableEditor.addWindowSetting("workingPointColor", String.valueOf(workingPointColorBox.getBackground().hashCode()));
    }

}
