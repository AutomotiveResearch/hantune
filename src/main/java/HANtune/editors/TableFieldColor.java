/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import nl.han.hantune.config.ApplicationProperties;

import static java.lang.Integer.max;
import static java.lang.Integer.min;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_BOOLEAN_USE_HUE;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_BOOLEAN_USE_MID_VALUE;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MAX;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MID;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MIN;


public class TableFieldColor {
    public static final int MAX_COLOR_VALUES = 100;
    private static final int MID_COLOR_IDX = MAX_COLOR_VALUES / 2; // index in the center of the fieldColors
    private static List<Color> fieldColors = null;

    static {
        initColors();
    }


    public static Color getColor(int index) {
        return fieldColors.get(max(0, min(index, MAX_COLOR_VALUES - 1)));
    }

    // Linear interpolation for fieldColors
    private static void initColors() {

        Color minColor = ApplicationProperties.getColor(TABLE_FIELD_COLOR_MIN);
        Color maxColor = ApplicationProperties.getColor(TABLE_FIELD_COLOR_MAX);
        Color midColor = ApplicationProperties.getColor(TABLE_FIELD_COLOR_MID);

        boolean useMidValue = ApplicationProperties.getBoolean(TABLE_FIELD_BOOLEAN_USE_MID_VALUE);
        boolean useHue = ApplicationProperties.getBoolean(TABLE_FIELD_BOOLEAN_USE_HUE);

        initColors(minColor, midColor, maxColor, useMidValue, useHue, false, 0, 0, 0);
    }

    public static void initColors(Color minColor, Color midColor, Color maxColor, boolean useMidColor, boolean useHSB, boolean autoCalculation, double min, double mid, double max) {
        int midColorIndex;
        if (autoCalculation) {
            midColorIndex = MID_COLOR_IDX;
        } else {
            midColorIndex = (int) (((mid - min) / (max - min)) * (MAX_COLOR_VALUES - 1));
        }
        fieldColors = new ArrayList<>();
        if (useHSB) {
            // calculate full range, no middle value
            addColorsByHSB(minColor, maxColor, MAX_COLOR_VALUES);
        } else if (useMidColor) {
            // calculate bottom half
            addColorsNew(minColor, midColor, midColorIndex);
            // calculate top half, including middle value
            addColorsNew(midColor, maxColor, MAX_COLOR_VALUES - midColorIndex);
        } else {
            // calculate full range, no middle value
            addColorsNew(minColor, maxColor, MAX_COLOR_VALUES);
        }
    }

    private static void addColorsNew(Color startColor, Color endColor, int count) {
        int offsetRed = startColor.getRed();
        int offsetGreen = startColor.getGreen();
        int offsetBlue = startColor.getBlue();
        double factorRed = calculateFactor(startColor.getRed(), endColor.getRed(), count - 1);
        double factorGreen = calculateFactor(startColor.getGreen(), endColor.getGreen(), count - 1);
        double factorBlue = calculateFactor(startColor.getBlue(), endColor.getBlue(), count - 1);

        for (int idx = 0; idx < count; idx++) {
            fieldColors.add(new Color(calcColorValue(factorRed, offsetRed, idx),
                calcColorValue(factorGreen, offsetGreen, idx),
                calcColorValue(factorBlue, offsetBlue, idx)));
        }
    }

    private static double calculateFactor(int valLo, int valHi, int sizeDenominator) {
        return ((double) (valHi - valLo)) / sizeDenominator;
    }

    private static int calcColorValue(double factor, int offset, int idx) {
        return (int) Math.round(factor * idx) + offset;
    }

    private static void addColorsByHSB(Color startColor, Color endColor, int count) {
        float startHue = Color.RGBtoHSB(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), null)[0];
        float endHue = Color.RGBtoHSB(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), null)[0];

        float startSaturation = Color.RGBtoHSB(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), null)[1];
        float endSaturation = Color.RGBtoHSB(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), null)[1];

        float startBrightness = Color.RGBtoHSB(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), null)[2];
        float endBrightness = Color.RGBtoHSB(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), null)[2];

        float hfactor = calculateHueFactor(startHue, endHue, count - 1);
        float sfactor = calculateHueFactor(startSaturation, endSaturation, count - 1);
        float bfactor = calculateHueFactor(startBrightness, endBrightness, count - 1);

        for (int i = 0; i < count; i++) {
            float hue = calculateHueValue(hfactor, startHue, i);
            float saturation = calculateHueValue(sfactor, startSaturation, i);
            float brightness = calculateHueValue(bfactor, startBrightness, i);
            fieldColors.add(Color.getHSBColor(hue, saturation, brightness));
        }
    }

    private static float calculateHueFactor(float valLo, float valHi, int sizeDenominator) {
        return (valHi - valLo) / sizeDenominator;
    }

    private static float calculateHueValue(float factor, float offset, int idx) {
        return (factor * idx) + offset;
    }
}
