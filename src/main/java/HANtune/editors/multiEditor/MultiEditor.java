/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors.multiEditor;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.text.NumberFormatter;

import HANtune.editors.GridBagConstraintsCreator;
import HANtune.editors.HANtuneEditor;
import HANtune.editors.HANtuneEditorElement;
import HANtune.editors.multiEditor.MultiEditor.MultiEditorElement;
import datahandling.CurrentConfig;
import datahandling.Parameter;
import datahandling.ParameterListener;
import nl.han.hantune.gui.components.NumpadDecimalLocaleConverter;
import util.Util;

/**
 * Shows multiple asap2Data parameters to edit in a list with name and spinner-box.
 *
 * @author Aart-Jan, Michel de Beijer, Mike van Laar, Michiel Klifman
 */
@SuppressWarnings("serial")
public class MultiEditor extends HANtuneEditor<MultiEditorElement> {

    public static final String MULTI_EDITOR_NAME = "MultiEditor";
    private final GridBagConstraintsCreator gridBagConstraintsCreator;
    private final Insets insets = new Insets(7, 7, 7, 7);
    private final int SENDTIMEINTERVALMS = 5;


    /**
     * The constructor of the MultiEditor class
     */
    public MultiEditor() {
        gridBagConstraintsCreator = new GridBagConstraintsCreator();
        setTitle(MULTI_EDITOR_NAME);
        setVerticalResizable(false);
    }


    @Override
    public void rebuild() {
        super.rebuild();
        packAndSetMinMax();
    }


    @Override
    protected MultiEditorElement getListenerForParameter(Parameter parameter) {
        return createMultiEditorElement(parameter);
    }


    /**
     *
     * @param parameter
     * @return the element/
     */

    private MultiEditorElement createMultiEditorElement(Parameter parameter) {
        final MultiEditorElement element = new MultiEditorElement(parameter);
        int index = getContentPane().getComponentCount();
        createNameContentPane(element, parameter.getName(), index);

        if (parameter.getUnit() != null && !parameter.getUnit().equals("")) {
            createUnitContentPane(element, parameter.getUnit(), index);
        }

        JSpinner spinner = createNewJSpinner(element, parameter);
        addKeyListenersToSpinner(spinner, parameter);
        addChangeListenerToSpinner(spinner, parameter);
        addFocusListenerToSpinner(spinner);
        createJSpinnerContentPane(element, index);

        if (CurrentConfig.getInstance().isServiceToolMode()) {
            element.getValueComponent().setComponentPopupMenu(null);
        } else {
            addPopupMenuItems(parameter, element, spinner);
        }

        element.stateUpdate();
        return element;
    }


    private void addPopupMenuItems(Parameter parameter, final MultiEditorElement element,
                                   JSpinner spinner) {
        // add popup menu for floats and fixed point data types
        if (parameter.hasDecimals()) {

            JPopupMenu popupMenu = new JPopupMenu();
            JMenuItem modDecimalsMenu = new JMenuItem("Modify decimals");
            modDecimalsMenu.addActionListener(e -> {
                actionDecimals(parameter.getName());
                setJSpinnerFormat(spinner);
            });
            popupMenu.add(modDecimalsMenu);

            // add Toggle Scientific Notation option
            JCheckBoxMenuItem forceScientificNotation = new JCheckBoxMenuItem("Force scientific notation");
            forceScientificNotation.addActionListener(e -> {
                forceScientificNotation(parameter.getName());
                setJSpinnerFormat(spinner);
            });
            popupMenu.add(forceScientificNotation);
            element.getValueComponent().setComponentPopupMenu(popupMenu);
        }
    }


    /**
     * This method creates a new name content pane.
     *
     * @param element :The objects of interest per asap2 reference
     * @param name :The asap2Ref of the element that needs to be created.
     * @param index :The position of the element in the row.
     */
    private void createNameContentPane(final MultiEditorElement element, final String name,
                                       final int index) {

        GridBagConstraints gridBagConstraints =
            gridBagConstraintsCreator.createGridBagConstraints(0, index, insets);
        getContentPane().add(element.getNameLabel(), gridBagConstraints);
    }


    /**
     * This method creates a new unit content pane.
     *
     * @param element :The objects of interest per asap2 reference
     * @param unit :The asap2Ref of the element that needs to be created.
     * @param index :The position of the element in the row.
     */
    private void createUnitContentPane(final MultiEditorElement element, final String unit,
                                       final int index) {
        GridBagConstraints gridBagConstraints =
            gridBagConstraintsCreator.createGridBagConstraints(1, index, insets);
        getContentPane().add(element.getUnitLabel(), gridBagConstraints);
    }


    /**
     * This method creates a new JSpinner content pane.
     *
     * @param element :The objects of interest per asap2 reference
     * @param asap2Ref :The asap2Ref of the element that needs to be created.
     * @param index :The position of the element in the row.
     */
    private void createJSpinnerContentPane(final MultiEditorElement element, final int index) {
        int fill = GridBagConstraints.HORIZONTAL;
        GridBagConstraints gridBagConstraints =
            gridBagConstraintsCreator.createGridBagConstraints(2, index, 1, fill);
        getContentPane().add(element.getValueComponent(), gridBagConstraints);
    }


    /**
     * This method creates a new JSpinner.
     *
     * @param element :The objects of interest per asap2 reference
     * @param asap2Ref :The asap2Ref of the element that needs to be created.
     */
    private JSpinner createNewJSpinner(final MultiEditorElement element, final Parameter parameter) {
        JSpinner spinner = new JSpinner();
        element.setValueComponent(spinner);
        spinner.setUI(new HANtuneSpinnerUI());
        spinner.setName(parameter.getName());
        spinner.setPreferredSize(new Dimension(130, 20));
        spinner.setModel(getSpinnerModel(parameter));

        setDecimalCount(element, parameter);

        element.addStateComponent(spinner);
        element.addStateComponent(getTextField(spinner));
        return spinner;
    }


    /**
     * Sets the correct amount of decimals to be displayed by the spinner. The default is 0. A setting will
     * only be added if there are no settings present. Decimals values will be set to 2 decimals, fractional
     * values to 4.
     *
     * @param element :The objects of interest per asap2 reference
     * @param asap2Ref :The asap2Ref of the element that needs to be created.
     */
    private void setDecimalCount(final MultiEditorElement element, final Parameter parameter) {
        if (!settings.containsKey(parameter.getName())
            || !settings.get(parameter.getName()).containsKey("decimals")) {
            if (parameter.hasDecimals()) {
                addSetting(parameter.getName(), "decimals", "" + parameter.getDecimalCount());
            }
        }
        setJSpinnerFormat((JSpinner)element.getValueComponent());
    }


    /**
     * Adds a FocusListener to the JFormattedTextField of the provided JSpinner, to select its text when it
     * gains focus. When a value has been entered, the value needs to be confirmed by pressing enter or the
     * value will revert back when focus is lost. *
     *
     * @param spinner - the spinner to which a FocusListener must be added.
     */
    private void addFocusListenerToSpinner(JSpinner spinner) {
        JFormattedTextField textField = getTextField(spinner);
        textField.setFocusLostBehavior(JFormattedTextField.REVERT);
        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                if (!e.isTemporary()) {
                    textField.setText(textField.getText());
                    textField.selectAll();
                }
            }
        });
    }


    /**
     * This method adds a new changelistener on the HANtuneSpinnerUI.
     *
     * @param spinner :The JSpinner that needs the mouseListeners.
     */
    private void addChangeListenerToSpinner(final JSpinner spinner, Parameter parameter) {

        spinner.addChangeListener(new SpinnerListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                HANtuneSpinnerUI hantuneSpinnerUI = (HANtuneSpinnerUI)spinner.getUI();
                if (hantuneSpinnerUI.getValueIsAdjusting()) {
                    setJSpinnerFormat(spinner);
                    double value = (double)spinner.getValue();
                    send(parameter, value);
                }
            }
        });
    }


    /**
     * This method adds a keyPressed and a keyReleased listener to the JSpinner.
     *
     * @param spinner :The JSpinner that needs the keyListeners.
     */
    private void addKeyListenersToSpinner(final JSpinner spinner, Parameter parameter) {
        JFormattedTextField text = getTextField(spinner);
        final HANtuneSpinnerUI spinnerUI = (HANtuneSpinnerUI)spinner.getUI();

        text.addKeyListener(new KeyAdapter() {
            NumpadDecimalLocaleConverter cnvLocale = new NumpadDecimalLocaleConverter();

            SpinnerModel spinnerModel = (SpinnerModel)spinner.getModel();
            double normalStepSize = spinnerModel.getStepSize().doubleValue();
            double multipliedStepSize = normalStepSize * 10;
            List<Integer> keys = new ArrayList<>();
            final int SHIFT = KeyEvent.VK_SHIFT;
            final int UP = KeyEvent.VK_UP;
            final int DOWN = KeyEvent.VK_DOWN;
            final int ENTER = KeyEvent.VK_ENTER;


            @Override
            public void keyPressed(KeyEvent event) {
                int key = event.getKeyCode();

                cnvLocale.handleKeyPressedEvent(event);

                if (key == ENTER || key == UP || key == DOWN) {
                    spinnerUI.setValueIsAdjusting(true);
                }
                if (key == SHIFT || key == UP || key == DOWN) {
                    if (!keys.contains(key)) {
                        keys.add(key);
                        if (key == SHIFT) {
                            spinnerModel.setStepSize(multipliedStepSize);
                        }
                    }

                    if (keys.contains(SHIFT) && keys.contains(UP) && spinnerModel.getNextValue() != null) {
                        spinnerModel.setValue(spinnerModel.getNextValue());
                    } else if (keys.contains(SHIFT) && keys.contains(DOWN)
                        && spinnerModel.getPreviousValue() != null) {
                        spinnerModel.setValue(spinnerModel.getPreviousValue());
                    }

                } else if (key == ENTER) {
                    JFormattedTextField field = getTextField(spinner);
                    JFormattedTextField.AbstractFormatter formatter = field.getFormatter();
                    try {
                        Object newValue = formatter.stringToValue(field.getText());
                        Object oldValue = field.getValue();
                        if (oldValue.equals(newValue)) {
                            spinnerModel.fireStateChanged();
                        }
                    } catch (ParseException ex) {
                        setJSpinnerFormat(spinner);
                    }
                }
            }


            @Override
            public void keyReleased(final KeyEvent event) {
                int key = event.getKeyCode();
                if (key == ENTER || key == UP || key == DOWN) {
                    spinnerUI.setValueIsAdjusting(false);
                }
                if (key == SHIFT || key == UP || key == DOWN) {
                    keys.remove(Integer.valueOf(key));
                    if (key == SHIFT) {
                        spinnerModel.setStepSize(normalStepSize);
                    }
                }
            }


            @Override
            public void keyTyped(final KeyEvent event) {
                cnvLocale.handleKeyTypedEvent(event);
            }

        });
    }


    /**
     * This method gets the textField of the JSpinner
     *
     * @param spinner the spinner that holds the text field.
     * @return the text field of the specified spinner.
     */
    private JFormattedTextField getTextField(JSpinner spinner) {
        JComponent editor = spinner.getEditor();
        if (editor instanceof JSpinner.DefaultEditor) {
            return ((JSpinner.DefaultEditor)editor).getTextField();
        } else {
            return null;
        }
    }


    /**
     * This method load the HANtuneWindow specific settings for each asap2 reference. This method should be
     * called only if settings have been changed. The method automatically triggers a repaint after the
     * settings are loaded.
     *
     * NOTE: Currently Nothing to be done here!
     */
    @Override
    public void loadSettings() {
        repaint();
    }


    /**
     * Creates the appropriate spinner model for the given asap2Data parameter.
     *
     * @param asap2ref :Asap2Data reference to create the SpinnerModel.
     * @return A spinnerNumberModel
     */
    private SpinnerNumberModel getSpinnerModel(Parameter parameter) {
        double value = parameter.validateValue(parameter.getValue());
        double minimum = parameter.getMinimum();
        double maximum = parameter.getMaximum();
        double factor = parameter.getFactor();
        if (factor == 1 && parameter.hasDecimals()) {
            return getFloatingPointModel(value, minimum, maximum);
        } else if (parameter.hasDecimals()) {
            return getFractionModel(value, minimum, maximum, factor);
        } else {
            return getIntegerModel(value, minimum, maximum);
        }
    }


    /**
     * Creates a SpinnerNumberModel for fractional values. Every value that is set in this model will be
     * rounded to a multiple of the fraction. (e.g. with a fraction of 0.5 a value of 1.4 will be rounded to
     * 1.5).
     *
     * @param minimum - the minimum value for this model
     * @param maximum - the maximum value for this model
     * @return a SpinnerNumberModel for decimal values
     */
    private SpinnerNumberModel getFractionModel(double value, double minimum, double maximum,
                                                double fraction) {
        return new SpinnerModel(value, minimum, maximum, fraction) {
            private static final long serialVersionUID = 2526471155622776173L;


            @Override
            public void setValue(Object value) {
                // round the value to the nearest multitple of the fraction
                double roundedValue = Math.round((double)value / fraction) * fraction;
                super.setValue(roundedValue);
            }
        };
    }


    /**
     * Creates a SpinnerNumberModel for integer values
     *
     * @param minimum - the minimum value for this model
     * @param maximum - the maximum value for this model
     * @return a SpinnerNumberModel for integer values
     */
    private SpinnerNumberModel getIntegerModel(double value, double minimum, double maximum) {
        return new SpinnerModel(value, minimum, maximum, 1) {
            private static final long serialVersionUID = 3526471155622776173L;


            @Override
            public void setValue(Object value) {
                double roundedValue = Math.round((double)value);
                super.setValue(roundedValue);
            }
        };
    }


    /**
     * Creates a SpinnerNumberModel for floating point values
     *
     * @param minimum - the minimum value for this model
     * @param maximum - the maximum value for this model
     * @return a SpinnerNumberModel for floating point values
     */
    private SpinnerNumberModel getFloatingPointModel(double value, double minimum, double maximum) {
        return new SpinnerModel(value, minimum, maximum, 1) {
            private static final long serialVersionUID = 4526471155622776173L;


            @Override
            public void setValue(Object value) {
                super.setValue(value);
            }
        };
    }


    /**
     * This method sets the format for the NumberEditor of a JSpinner. it checks the value present in the
     * JSpinner and adapts to Scientific Notation when this value isn't readable.
     *
     * @param JSpinner :the JSpinner where the format has to be set
     *
     */
    private void setJSpinnerFormat(JSpinner spinner) {
        Double value = Util.getDoubleValue(spinner.getValue());
        String asap2Ref = spinner.getName();

        JComponent comp = spinner.getEditor();
        JFormattedTextField field = (JFormattedTextField)comp.getComponent(0);
        NumberFormatter formatter = (NumberFormatter)field.getFormatter();
        formatter.setFormat(getFormat(asap2Ref, value));
        field.setValue(field.getValue());
    }

    public class MultiEditorElement extends HANtuneEditorElement implements ParameterListener {

        public MultiEditorElement(Parameter parameter) {
            super(parameter, MultiEditor.this);
        }


        @Override
        public void valueUpdate(double value) {
            JSpinner spinner = (JSpinner)getValueComponent();
            HANtuneSpinnerUI spinnerUI = (HANtuneSpinnerUI)spinner.getUI();
            if (!spinnerUI.getValueIsAdjusting()) {
                spinner.setValue(value);
            }
        }
    }

    public class SpinnerModel extends SpinnerNumberModel {

        public SpinnerModel(double value, double minimum, double maximum, double stepSize) {
            super(value, minimum, maximum, stepSize);
        }


        @Override
        public void fireStateChanged() {
            super.fireStateChanged();
        }
    }
}
