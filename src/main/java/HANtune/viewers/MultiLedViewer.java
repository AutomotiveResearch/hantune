/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import HANtune.ReferenceWindow;
import HANtune.SubrangeDialog;
import HANtune.viewers.MultiLedViewer.MultiLedElement;
import components.ReferenceLabel;
import datahandling.CurrentConfig;
import datahandling.Signal;
import datahandling.SignalListener;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.*;

/**
 * Displays multiple signals in a list with name, unit and 5 color, 4 threshold led
 * @author Sander
 */
@SuppressWarnings("serial")
public class MultiLedViewer extends HANtuneViewer<MultiLedElement> {

    private SubrangeDialog subrangeDialog;
    public static final int MINIMUMRANGES = 1;
    public static final int MAXIMUMRANGES = 4;

    /**
     * The constructor of the MultiLedViewer class
     */
    public MultiLedViewer(){
        setTitle("MultiLedViewer");
        setVerticalResizable(false);
    }

    @Override
    public void rebuild() {
        super.rebuild();
        packAndSetMinMax();
    }

    @Override
    protected MultiLedElement getListenerForSignal(Signal signal) {
        return createMultiLedRow(signal);
    }

    /**
     * This method rebuilds the content of this HANtuneWindow and should be called only
     * if the content changed (adding or removing asap2 references). There is no need to call
     * this method after a value or setting has changed.
     *
     * The method loops through all asap2 references and will build the components required for
     * each asap2 reference.
     * @param signal
     * @return
     */
    private MultiLedElement createMultiLedRow(Signal signal) {
        GridBagConstraints gridBagConstraints;

        // add labels and progressbar for each signal
        int rowIndex = getContentPane().getComponentCount();
        MultiLedElement element = new MultiLedElement(signal);

        JLabel nameLabel = element.nameLabel;
        nameLabel.setText(signal.getName());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = rowIndex;
        gridBagConstraints.insets = new Insets(7, 7, 7, 7);
        getContentPane().add(nameLabel, gridBagConstraints);

        // value label
        JLabel valueLabel = element.valueLabel;
        valueLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        valueLabel.setPreferredSize(new Dimension(50, 18));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = rowIndex;
        gridBagConstraints.insets = new Insets(7, 7, 7, 7);
        gridBagConstraints.weightx = 1;
        getContentPane().add(valueLabel, gridBagConstraints);

        // unit label
        if (signal.getUnit() != null && !signal.getUnit().equals("")) {
            JLabel unitLabel = new JLabel("[" + signal.getUnit() + "]");
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 2;
            gridBagConstraints.gridy = rowIndex;
            gridBagConstraints.insets = new Insets(7, 7, 7, 7);
            getContentPane().add(unitLabel, gridBagConstraints);

        }

        // 4 threshold led, depicting value
        LED valueLed = element.led;
        valueLed.setValue(null);
        valueLed.setPreferredSize(new Dimension(25, 25));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = rowIndex;
        gridBagConstraints.gridwidth = 27;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        getContentPane().add(valueLed, gridBagConstraints);

        // add popup menus
        if (CurrentConfig.getInstance().isServiceToolMode()) {
            setComponentPopupMenu(null);
            nameLabel.setComponentPopupMenu(null);
            valueLabel.setComponentPopupMenu(null);
            valueLed.setComponentPopupMenu(null);
        }
        else {
            addPopupMenuItems(signal, nameLabel, valueLabel, valueLed);
        }


        loadSettings();
        return element;
    }


    private void addPopupMenuItems(Signal signal, JLabel nameLabel, JLabel valueLabel, LED valueLed) {
        JPopupMenu modifyDecimalsPopup = new JPopupMenu();
        JMenuItem mi1 = new JMenuItem("Modify decimals");
        mi1.addActionListener(e -> actionDecimals(signal.getName()));
        modifyDecimalsPopup.add(mi1);
        nameLabel.setComponentPopupMenu(modifyDecimalsPopup);
        valueLabel.setComponentPopupMenu(modifyDecimalsPopup);

        // led popupmenu
        // Menu item for that calls SubrangeDialog, copy and edit this for viewers that use the dialog
        JMenuItem ledMenuItem = new JMenuItem("Modify ranges");
        ledMenuItem.addActionListener((ActionEvent e) -> {
            subrangeDialog =
                new SubrangeDialog(MultiLedViewer.this, signal, MINIMUMRANGES, MAXIMUMRANGES, true);
            subrangeDialog.setVisible(true);
        });
        JPopupMenu ledPopupMenu = new JPopupMenu();
        ledPopupMenu.add(ledMenuItem);
        valueLed.setComponentPopupMenu(ledPopupMenu);
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2 reference. This method
     * should be called only if settings have been changed. The method automatically triggers
     * a repaint after the settings are loaded.
     *
     * The method loops through all asap2 references and will load the settings for
     * each asap2 reference.
     */
    @Override
    public void loadSettings() {
        // loadSettings for each reference
        for (Map.Entry<Signal, MultiLedElement> entry : signals.entrySet()) {
            Signal signal = entry.getKey();
            MultiLedElement multiLedRow = entry.getValue();
            LED led = multiLedRow.led;
            // read settings

            // read settings for subranges, copy and edit this into viewers that use the SubrangeDialog
            List<Color> colors = new ArrayList<>();
            List<Double> limits = new ArrayList<>();
            for(int loopcount=0; loopcount<=MAXIMUMRANGES; loopcount++){
                // get colors settings
                if(settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("color"+loopcount)){
                   colors.add(loopcount, Color.decode(settings.get(signal.getName()).get("color"+loopcount)));
                }
                // if limit0 is not present in settings, get limit setting for limit0
                if(!(settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit0"))){
                    if(settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit")){
                        limits.add(loopcount, Double.parseDouble(settings.get(signal.getName()).get("limit")));
                    }

                }
                // get limits settings
                if(settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit"+loopcount) && loopcount<MAXIMUMRANGES){
                    limits.add(loopcount, Double.parseDouble(settings.get(signal.getName()).get("limit"+loopcount)));
                }
                // break if no more limits or colors are found
                else{
                    break;
                }
            }
            // END

            // update ranges
            led.clearRanges();
            for(int loopcount=0; loopcount<colors.size(); loopcount++){
                led.setColor(loopcount, colors.get(loopcount));
            }
            for(int loopcount=0; loopcount<limits.size(); loopcount++){
                led.setLimit(loopcount, limits.get(loopcount));
            }
        }
        repaint();
    }

    /**
     * Contains and updates components for a given Signal.
     */
    public class MultiLedElement implements SignalListener {

        private final Signal signal;
        private final LED led = new LED();
        private final JLabel valueLabel = new JLabel();
        private final JLabel nameLabel;

        public MultiLedElement(Signal signal) {
            this.signal = signal;
            this.nameLabel = new ReferenceLabel<>(signal, MultiLedViewer.this);
        }

        @Override
        public void valueUpdate(double value, long timeStamp) {
            led.setValue(value);
            led.repaint();
            valueLabel.setText(getFormat(signal.getName(), value).format(value));
        }

        @Override
        public void stateUpdate() {
            setLabelColor();
        }

        private void setLabelColor() {
            if (!signal.isActive() || inactiveReferences.contains(signal)) {
                nameLabel.setForeground(ReferenceWindow.INACTIVE_TEXT_COLOR);
            } else {
                nameLabel.setForeground(ACTIVE_TEXT_COLOR);
            }
        }
    }

    /**
     * Class that draws a led with an infinite number of thresholds and corresponding colors.
     */
    public class LED extends JPanel {

        private static final long serialVersionUID = 7526471155622776181L;

        private int width, height;
        private Double value = null;
        private Graphics2D g2d;

        private final List<Color> colors = new ArrayList<>();
        private final List<Double> limits = new ArrayList<>();

        /**
         * The constuctor for the LED class
         */
        public LED(){

        }
        /**
         * Returns if the component is opaque
         * @return boolean
         */
        @Override public boolean isOpaque() {
            return false;
        }

        /**
         * The method that paints the led, gets called on a repaint
         * @param g The Graphics object the led is placed in
         */
        @Override
        protected void paintComponent(Graphics g) {
            g2d = (Graphics2D) g;

            // get dimensions
            width = getWidth();
            height = getHeight();

            // draw led
            g2d.setColor(getStateColor());
            g2d.fill3DRect(0, 0, width, height, true);
        }

        /**
         * Returns the correct color for the current state, is used by paintComponent
         * @return Color The correct color
         */
        private Color getStateColor(){

           Color returncolor = Color.gray;

           if (value == null){
                    return returncolor;
                }
           if(!(limits.size()<colors.size())) return returncolor;
           if(limits.isEmpty() || colors.isEmpty()) return returncolor;
           for (int loopcount = 0; loopcount<limits.size(); loopcount++){
                if (value <=limits.get(loopcount)){
                    returncolor = colors.get(loopcount);
                    return returncolor;
                }
                else {
                    returncolor = colors.get(loopcount+1);
                }
            }
            return returncolor;
        }


        /**
         * Setter for limit array
         * @param index The index of the limit to be set
         * @param newValue The value of the limit to be set
         */
        public void setLimit(int index, Double newValue) {
            if(limits.size()<(index+1)){
                limits.add(index, newValue);
            }
            else{
                limits.set(index, newValue);
            }
        }

        /**
         * Setter for color array
         * @param index The index of the color to be set
         * @param newColor The value of the limit to be set
         */
        public void setColor (int index, Color newColor){
            if(colors.size()<(index+1)){
                colors.add(index, newColor);
            }
            else{
                colors.set(index, newColor);
            }
        }

        /**
         * Setter for value
         * @param newValue the value to set
         */
        public void setValue(Double newValue){
            value = newValue;
        }

        /**
         * Getter for limit list
         * @param index The index of the limit to get
         * @return Double The value of limit
         */
        public Double getLimit(int index){
            if(limits.size()<(index+1)){
                return null;
            }
            else{
                return limits.get(index);
            }
        }

        /**
         * Getter for color list
         * @param index The index of the color to get
         * @return Color The color
         */
        public Color getColor (int index){
            if(colors.size()<(index+1)){
                return null;
            }
            else{
                return colors.get(index);
            }
        }

        /**
         * A method that clears the ranges
         */
        public void clearRanges(){
            limits.clear();
            colors.clear();
        }

    }
}
