/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import HANtune.ReferenceWindow;
import HANtune.viewers.MultiViewer.MultiViewerElement;
import components.ReferenceLabel;
import datahandling.CurrentConfig;
import datahandling.Signal;
import datahandling.SignalListener;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Map;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

/**
 * Displays multiple asap2Data signals in a list with name, unit and relative
 * value-bar
 *
 * @author Aart-Jan, Michiel Klifman
 */
@SuppressWarnings("serial")
public class MultiViewer extends HANtuneViewer<MultiViewerElement> {

    public MultiViewer() {
        setTitle("Multiviewer");
        setVerticalResizable(false);
    }

    @Override
    public void rebuild() {
        super.rebuild();
        packAndSetMinMax();
    }

    @Override
    protected MultiViewerElement getListenerForSignal(Signal signal) {
        return createMultiViewerElement(signal);
    }

    /**
     * @param signal
     * @return
     */
    protected MultiViewerElement createMultiViewerElement(Signal signal) {
        int row = getContentPane().getComponentCount();
        GridBagConstraints gridBagConstraints;
        MultiViewerElement element = new MultiViewerElement(signal);

        JLabel nameLabel = element.nameLabel;
        nameLabel.setToolTipText(signal.getSource());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = row;
        gridBagConstraints.insets = new Insets(6, 7, 6, 7);
        getContentPane().add(nameLabel, gridBagConstraints);

        // value label
        JLabel valueLabel = element.valueLabel;
        valueLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        valueLabel.setPreferredSize(new Dimension(60, 18));
        Border emptyBorder = BorderFactory.createEmptyBorder(0, 2, 0, 2);
        Border etchedBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
        Border compoundBorder = BorderFactory.createCompoundBorder(etchedBorder, emptyBorder);
        valueLabel.setBorder(compoundBorder);
        String initialValue = getDecimalNotation(signal.getDecimalCount()).format(0);
        if (hasSetting(signal.getName(), "decimals")) {
            Map<String, String> value = getSetting(signal.getName(), "decimals");
            int decimals = Integer.parseInt(value.get("decimals"));
            initialValue = getDecimalNotation(decimals).format(0);
        }
        valueLabel.setText(inactiveReferences.contains(signal) ? "N/A" : initialValue);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = row;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.insets = new Insets(6, 7, 6, 7);
        getContentPane().add(valueLabel, gridBagConstraints);

        // unit label
        if (signal.getUnit() != null && !signal.getUnit().equals("")) {
            JLabel unitLabel = new JLabel("[" + signal.getUnit() + "]");
            if (!unitLabel.getText().equals("[]")) {
                gridBagConstraints = new GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = row;
                gridBagConstraints.insets = new Insets(7, 7, 7, 7);
                getContentPane().add(unitLabel, gridBagConstraints);
            }
        }

        // progressbar showing value between given bounds
        /*       asap2Row.valueJProgressBar = new JProgressBar();
        asap2Row.valueJProgressBar.setValue(0);
        if (currentConfig.getASAP2Data().getASAP2Measurements().get(asap2Ref).isLimitEnabled()) {
        if (currentConfig.getASAP2Data().getASAP2Measurements().get(asap2Ref).getDatatype().equals(Datatype.valueOf("FLOAT32_IEEE")) || currentConfig.getASAP2Data().getASAP2Measurements().get(asap2Ref).getFactor() != 1) {
        asap2Row.valueJProgressBar.setMinimum(0);
        asap2Row.valueJProgressBar.setMaximum(p_size);
        } else {
        asap2Row.valueJProgressBar.setMinimum((int) currentConfig.getASAP2Data().getASAP2Measurements().get(asap2Ref).getLowerLimit()+100);
        asap2Row.valueJProgressBar.setMaximum((int) currentConfig.getASAP2Data().getASAP2Measurements().get(asap2Ref).getUpperLimit()-100);
        }
        } else {
        asap2Row.valueJProgressBar.setVisible(false);
        }
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = row;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(7, 7, 7, 7);
        getContentPane().add(asap2Row.valueJProgressBar, gridBagConstraints);
        */
        // add popup menu, if applicable
        if (CurrentConfig.getInstance().isServiceToolMode()) {
            valueLabel.setComponentPopupMenu(null);
        } else {
            addPopupMenuItems(signal, valueLabel);
        }
        return element;
    }


    private void addPopupMenuItems(Signal signal, JLabel valueLabel) {
        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem modifyMenuItem = new JMenuItem("Modify decimals");
        modifyMenuItem.addActionListener(e -> actionDecimals(signal.getName()));
        popupMenu.add(modifyMenuItem);
        valueLabel.setComponentPopupMenu(popupMenu);
    }

    @Override
    public void actionDecimals(String asap2Ref) {
        super.actionDecimals(asap2Ref);
        super.rebuild();
    }

    @Override
    public void loadSettings() {
    }

    /**
     * Contains and updates components for a given Signal.
     */
    public class MultiViewerElement implements SignalListener {

        private final Signal signal;
        private final JLabel valueLabel = new JLabel();
        private JLabel nameLabel;


        public MultiViewerElement(Signal signal) {
            this.signal = signal;
            nameLabel = new ReferenceLabel<>(signal, MultiViewer.this);
            nameLabel.setText(signal.getName());
        }


        @Override
        public void valueUpdate(double value, long timeStamp) {
            valueLabel.setText(getFormat(signal.getName(), value).format(value));
        }


        @Override
        public void stateUpdate() {
            valueLabel.setEnabled(signal.isActive());
        }


        private void setLabelColor() {
            if (!signal.isActive() || inactiveReferences.contains(signal)) {
                nameLabel.setForeground(ReferenceWindow.INACTIVE_TEXT_COLOR);
            } else {
                nameLabel.setForeground(ReferenceWindow.ACTIVE_TEXT_COLOR);
            }
        }
    }
}
