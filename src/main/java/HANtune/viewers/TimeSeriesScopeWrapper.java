/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import datahandling.SignalListener;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.TimeSeries;

public class TimeSeriesScopeWrapper implements SignalListener {

    private static final long REFRESH_RATE_MICROS = 50000; //In microseconds (currently 50 milliSec = 20 fps)

    private final TimeSeries timeSeries;
    private long lastTimestampMicros = 0;
    private ScopeViewer scopeViewer;

    TimeSeriesScopeWrapper(TimeSeries timeSeries, ScopeViewer scope) {
        this.scopeViewer = scope;
        this.timeSeries = timeSeries;
    }

    @Override
    public void valueUpdate(double value, long timestampMicros) {
        if (timestampMicros < lastTimestampMicros) {
            timeSeries.clear();
            lastTimestampMicros = 0;
        }

        addValueToTimeSeries(timestampMicros, value);

        if (timestampMicros > lastTimestampMicros + REFRESH_RATE_MICROS) {
            timeSeries.setNotify(scopeViewer.isPlaying());
            lastTimestampMicros = timestampMicros;
        }
        timeSeries.setNotify(false);
    }

    private synchronized void addValueToTimeSeries(long timestamp, double value) {
        FixedMillisecond period = new FixedMillisecond(timestamp / 1000);
        if (timeSeries.getDataItem(period) == null) {
            timeSeries.add(period, value, false);
        }
    }

    @Override
    public void stateUpdate() {

    }
}
