/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import java.util.LinkedHashMap;
import java.util.Map;

import HANtune.ReferenceWindow;
import datahandling.Signal;
import datahandling.SignalListener;

/**
 * A base class for all windows that handle Signals.
 *
 * @author Michiel Klifman
 * @param <T>
 */
@SuppressWarnings("serial")
public abstract class HANtuneViewer<T extends SignalListener> extends ReferenceWindow<Signal> {

    protected final Map<Signal, T> signals = new LinkedHashMap<>();


    public HANtuneViewer() {
        this(true);
    }


    public HANtuneViewer(boolean multiReferenceWindow) {
        super(Signal.class, multiReferenceWindow);
    }


    @Override
    protected void addReferenceContent() {
        for (Signal signal : references) {
            T listener = getListenerForSignal(signal);
            signal.addListener(listener);
            signal.addStateListener(listener);
            listener.stateUpdate();
            signals.put(signal, listener);
            if (!multiReferenceWindow)
                break;
        }
    }


    @Override
    public void clearWindow() {
        super.clearWindow();
        for (Map.Entry<Signal, T> entry : signals.entrySet()) {
            entry.getKey().removeListener(entry.getValue());
        }
        signals.clear();
    }


    protected abstract T getListenerForSignal(Signal signal);
}
