/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.TimeZone;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.JSpinner;
import javax.swing.JSpinner.NumberEditor;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ErrorLogger.AppendToLogfile;
import HANtune.CustomFileChooser;
import datahandling.CurrentConfig;
import datahandling.Signal;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleEdge;
import util.MessagePane;

/**
 * Displays the value of signals using a oscilloscope display
 *
 * @author Aart-Jan, Michiel Klifman
 */
public class ScopeViewer extends HANtuneViewer<TimeSeriesScopeWrapper> {

    private static final long serialVersionUID = 7526471155622776184L;

    private enum TimeSpan {
        SPAN_1_MIN("1 min", 60000),
        SPAN_30_SEC("30 sec", 30000),
        SPAN_15_SEC("15 sec", 15000),
        SPAN_10_SEC("10 sec", 10000),
        SPAN_5_SEC("5 sec", 5000),
        SPAN_2_SEC("2 sec", 2000),
        SPAN_1_SEC("1 sec", 1000),
        SPAN_500_MSEC("500ms", 500);

        private final String label;
        private final int millisecond;

        TimeSpan(String label, int millisecond) {
            this.label = label;
            this.millisecond = millisecond;
        }

        public int getMillisecond() {
            return millisecond;
        }

        @Override
        public String toString() {
            return label;
        }
    }

    private ChartPanel scopePanel;
    private JButton saveButton;
    private JToggleButton autoscaleToggle;
    private JToggleButton holdOrRunToggle;
    private JSpinner lowerLimitSpinner;
    private JSpinner upperLimitSpinner;
    private JComboBox<TimeSpan> timespanBox;
    private static final double DEFAULT_LOWER = 0;
    private static final double DEFAULT_UPPER = 100;
    private static final TimeSpan DEFAULT_TIMESPAN = TimeSpan.SPAN_5_SEC;
    private static final long MAXIMUM_ITEM_AGE = 60000; //In milliseconds (currently 1 minute)
    private TimeSeriesCollection timeSeriesCollection;
    private boolean isPlaying = true;

    /**
     * The constructor of the MultiLedViewer class
     */
    public ScopeViewer() {
        defaultMinimumSize = new Dimension(350, 250);
        getContentPane().setPreferredSize(new Dimension(450, 350));
        setTitle("HANtune - ScopeViewer");
    }

    @Override
    protected TimeSeriesScopeWrapper getListenerForSignal(Signal signal) {
        return createScopeViewer(signal);
    }

    boolean isPlaying() {
        return isPlaying;
    }

    private TimeSeriesScopeWrapper createScopeViewer(Signal signal) {
        TimeSeries timeSeries = new TimeSeries(signal.getName());

        timeSeries.setMaximumItemAge(MAXIMUM_ITEM_AGE);
        timeSeriesCollection.addSeries(timeSeries);

        return new TimeSeriesScopeWrapper(timeSeries, this);
    }

    /**
     * This method rebuilds the content of this HANtuneWindow and should be
     * called only if the content changed (adding or removing asap2 references).
     * There is no need to call this method after a value or setting has
     * changed.
     *
     * The method loops through all asap2 references and will build the
     * components required for each asap2 reference.
     */
    @Override
    public void createCustomReferenceWindow() {
        GridBagConstraints gridBagConstraints;
        timeSeriesCollection = new TimeSeriesCollection();

        // chart
        JFreeChart chart = ChartFactory.createTimeSeriesChart(null, null, null, timeSeriesCollection, true, false, false);
        chart.setTextAntiAlias(false);
        chart.getLegend().setPosition(RectangleEdge.TOP);
        chart.setBackgroundPaint(new Color(0, 0, 0, 0));

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.BLACK);
        plot.setRangeGridlinePaint(Color.BLACK);

        DateAxis domainAxis = (DateAxis) plot.getDomainAxis();
        domainAxis.setTimeZone(TimeZone.getTimeZone("GMT"));
        domainAxis.setAutoRange(true);
        domainAxis.setFixedAutoRange(DEFAULT_TIMESPAN.getMillisecond());

        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setAutoRangeIncludesZero(false);

        scopePanel = new ChartPanel(chart, false);
        scopePanel.setRangeZoomable(false);
        scopePanel.setDomainZoomable(false);
        scopePanel.setMinimumDrawWidth(400);
        scopePanel.setMinimumDrawHeight(200);
        scopePanel.setOpaque(false);
        if (CurrentConfig.getInstance().isServiceToolMode()) {
            scopePanel.setPopupMenu(null);
        } else {
            scopePanel.setPopupMenu(getMenu());
        }

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 6;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        getContentPane().add(scopePanel, gridBagConstraints);

        // play / pause button
        holdOrRunToggle = new JToggleButton("Hold");
        holdOrRunToggle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                isPlaying = !holdOrRunToggle.isSelected();
                if (isPlaying) {
                    holdOrRunToggle.setText("Hold");
                    saveButton.setEnabled(false);
                } else {
                    holdOrRunToggle.setText("Run");
                    saveButton.setEnabled(true);
                }
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        getContentPane().add(holdOrRunToggle, gridBagConstraints);

        // lower limit spinner
        lowerLimitSpinner = new JSpinner();
        lowerLimitSpinner.setToolTipText("Lower Limit");
        lowerLimitSpinner.setModel(new SpinnerNumberModel(DEFAULT_LOWER, null, null, Double.valueOf(1.0d)));
        lowerLimitSpinner.setEditor(new NumberEditor(lowerLimitSpinner, "#.#"));
        lowerLimitSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (Double.parseDouble(lowerLimitSpinner.getModel().getValue().toString()) != DEFAULT_LOWER) {
                    windowSettings.put("limit_lower", lowerLimitSpinner.getModel().getValue().toString());
                } else {
                    windowSettings.remove("limit_lower");
                }
                loadSettings();
            }
        });
        lowerLimitSpinner.getEditor().setPreferredSize(new Dimension(40, 14));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        gridBagConstraints.fill = GridBagConstraints.NONE;
        getContentPane().add(lowerLimitSpinner, gridBagConstraints);

        // upper limit spinner
        upperLimitSpinner = new JSpinner();
        upperLimitSpinner.setToolTipText("Upper Limit");
        upperLimitSpinner.setModel(new SpinnerNumberModel(DEFAULT_UPPER, null, null, 1.0d));
        upperLimitSpinner.setEditor(new NumberEditor(upperLimitSpinner, "#.#"));
        upperLimitSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (Double.parseDouble(upperLimitSpinner.getModel().getValue().toString()) != DEFAULT_UPPER) {
                    windowSettings.put("limit_upper", upperLimitSpinner.getModel().getValue().toString());
                } else {
                    windowSettings.remove("limit_upper");
                }
                loadSettings();
            }
        });
        upperLimitSpinner.getEditor().setPreferredSize(new Dimension(40, 14));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        gridBagConstraints.fill = GridBagConstraints.NONE;
        getContentPane().add(upperLimitSpinner, gridBagConstraints);

        // timespan dropdown box
        timespanBox = new JComboBox<>();
        timespanBox.setModel(new DefaultComboBoxModel<TimeSpan>(TimeSpan.values()));
        timespanBox.setToolTipText("Timespan");
        timespanBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (timespanBox.isEnabled()) {
                    if (!(timespanBox.getSelectedItem() == DEFAULT_TIMESPAN)) {
                        windowSettings.put("timespan", "" + ((TimeSpan) timespanBox.getSelectedItem()).getMillisecond());

                    } else {
                        windowSettings.remove("timespan");
                    }
                    loadSettings();
                }
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        getContentPane().add(timespanBox, gridBagConstraints);

        // auto scale
        autoscaleToggle = new JToggleButton("Auto scale", false);
        autoscaleToggle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (autoscaleToggle.isSelected()) {
                    ScopeViewer.this.scopePanel.getChart().getXYPlot().getRangeAxis().setAutoRange(true);
                    ((NumberAxis) ScopeViewer.this.scopePanel.getChart().getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
                    upperLimitSpinner.setEnabled(false);
                    lowerLimitSpinner.setEnabled(false);
                } else {
                    ScopeViewer.this.scopePanel.getChart().getXYPlot().getRangeAxis().setAutoRange(false);
                    upperLimitSpinner.setValue(((XYPlot) scopePanel.getChart().getPlot()).getRangeAxis().getUpperBound());
                    lowerLimitSpinner.setValue(((XYPlot) scopePanel.getChart().getPlot()).getRangeAxis().getLowerBound());
                    upperLimitSpinner.setEnabled(true);
                    lowerLimitSpinner.setEnabled(true);
                }
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        getContentPane().add(autoscaleToggle, gridBagConstraints);

        // save image button
        saveButton = new JButton("Save image");
        saveButton.setToolTipText("Only available when paused");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                CustomFileChooser fileChooser = new CustomFileChooser("Save", CustomFileChooser.FileType.PNG_FILE);
                File pngFile = fileChooser.chooseSaveDialogConfirmOverwrite(hantune, "Save");

                if (pngFile != null) {
                    try {
                        ChartUtilities.saveChartAsPNG(pngFile, scopePanel.getChart(), scopePanel.getWidth(),
                                scopePanel.getHeight());
                    } catch (Exception e) {
                        AppendToLogfile.appendError(Thread.currentThread(), e);
                        MessagePane.showError("Error while saving PNG image: " + e.getMessage());
                    }
                }
            }
        });
        saveButton.setEnabled(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        getContentPane().add(saveButton, gridBagConstraints);

        JMenuItem invertBackgroundMenuItem = new JMenuItem("Invert background");
        invertBackgroundMenuItem.addActionListener(e -> invertBackgroundColor(plot));
        addMenuItemToPopupMenu(invertBackgroundMenuItem);

        loadSettings();
        super.createCustomReferenceWindow();
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     *
     * The method loops through all asap2 references and will load the settings
     * for each asap2 reference.
     */
    @Override
    public void loadSettings() {
        XYPlot plot = scopePanel.getChart().getXYPlot();
        if (windowSettings.containsKey("limit_lower")) {
            plot.getRangeAxis().setLowerBound(Double.parseDouble(windowSettings.get("limit_lower")));
            lowerLimitSpinner.setValue(Double.parseDouble(windowSettings.get("limit_lower")));
        } else {
            plot.getRangeAxis().setLowerBound(DEFAULT_LOWER);
            lowerLimitSpinner.setValue(DEFAULT_LOWER);
        }

        if (windowSettings.containsKey("limit_upper")) {
            plot.getRangeAxis().setUpperBound(Double.parseDouble(windowSettings.get("limit_upper")));
            upperLimitSpinner.setValue(Double.parseDouble(windowSettings.get("limit_upper")));
        } else {
            plot.getRangeAxis().setUpperBound(DEFAULT_UPPER);
            upperLimitSpinner.setValue(DEFAULT_UPPER);
        }

        if (windowSettings.containsKey("plotBackgroundColor")) {
            Color backgroundColor = Color.decode(windowSettings.get("plotBackgroundColor"));
            plot.setBackgroundPaint(backgroundColor);
        }

        if (windowSettings.containsKey("gridColor")) {
            Color gridColor = Color.decode(windowSettings.get("gridColor"));
            plot.setDomainGridlinePaint(gridColor);
            plot.setRangeGridlinePaint(gridColor);
        }

        timespanBox.setEnabled(false);
        if (windowSettings.containsKey("timespan")) {
            plot.getDomainAxis().setFixedAutoRange(Integer.parseInt(windowSettings.get("timespan")));
            for (TimeSpan timespan : TimeSpan.values()) {
                if (timespan.getMillisecond() == Integer.parseInt(windowSettings.get("timespan"))) {
                    timespanBox.setSelectedItem(timespan);
                }
            }
        } else {
            plot.getDomainAxis().setFixedAutoRange(DEFAULT_TIMESPAN.getMillisecond());
            timespanBox.setSelectedItem(DEFAULT_TIMESPAN);
        }

        if (autoscaleToggle.isSelected()) {
            plot.getRangeAxis().setAutoRange(true);
            ((NumberAxis) plot.getRangeAxis()).setAutoRangeIncludesZero(false);
        }

        timespanBox.setEnabled(true);
        repaint();
    }

    private void invertBackgroundColor(XYPlot plot) {
        Paint gridColor = plot.getBackgroundPaint();
        Paint backgroundColor = gridColor == Color.WHITE ? Color.BLACK : Color.WHITE;
        plot.setBackgroundPaint(backgroundColor);
        plot.setDomainGridlinePaint(gridColor);
        plot.setRangeGridlinePaint(gridColor);
        windowSettings.put("plotBackgroundColor", Integer.toString(backgroundColor.hashCode()));
        windowSettings.put("gridColor", Integer.toString(gridColor.hashCode()));
    }
}
