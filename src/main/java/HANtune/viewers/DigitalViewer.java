/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.io.IOException;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingConstants;

import ErrorLogger.AppendToLogfile;
import datahandling.Signal;
import datahandling.SignalListener;
import nl.han.hantune.config.ApplicationProperties;

import static nl.han.hantune.config.ConfigProperties.DIGITALVIEWER_LABEL_POSITION;

/**
 * Displays the value of an asap2Data signal using a quartz font type
 *
 * @author Aart-Jan, Michiel Klifman
 */
public class DigitalViewer extends HANtuneViewer<DigitalViewer> implements SignalListener {

    private static final long serialVersionUID = 7526471155622776178L;

    private static final Color BACKGROUND_COLOR = new Color(46, 0, 0);
    private static final Color FOREGROUND_COLOR = new Color(240, 0, 0);
    private static final Dimension DEFAULT_SIZE = new Dimension(130, 70);
    private static final int BINARY = 2;
    private static final int DECIMAL = 10;
    private static final int HEXADECIMAL = 16;
    private int numeralSystem = DECIMAL;
    private Signal signal;
    private JLabel valueLabel;

    public DigitalViewer() {
        super(false);
        defaultMinimumSize = new Dimension(70, 60);
        getContentPane().setPreferredSize(DEFAULT_SIZE);
        setTitle("DigitalViewer");
        saveAndShowBackground(getBooleanWindowSetting("showBackground", true));
        setDefaultLabelPosition(ApplicationProperties.getTopBottomLabelPos(DIGITALVIEWER_LABEL_POSITION).getLabelPositionString());


        // add listener to the window that is activated during movements of the window
        addComponentListener(new ComponentAdapter() {

            /**
             * changes the font size after a resizing
             */
            @Override
            public void componentResized(java.awt.event.ComponentEvent evt) {
                updateLabelFont();
            }
        });
    }

    @Override
    protected DigitalViewer getListenerForSignal(Signal signal) {
        return createDigitalViewer(signal);
    }

    /**
     * @param signal
     * @return
     */
    private DigitalViewer createDigitalViewer(Signal signal) {
        this.signal = signal;
        GridBagConstraints gridBagConstraints;
        valueLabel = new JLabel();

        setTitle(signal.getName());
        setTitleTooltip(signal.getSource());

        valueLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        valueLabel.setOpaque(false);
        valueLabel.setBackground(BACKGROUND_COLOR);
        valueLabel.setForeground(FOREGROUND_COLOR);
        valueLabel.setText("0");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        getContentPane().add(valueLabel, gridBagConstraints);

        String storedBackground = windowSettings.get("background");
        Color background = storedBackground != null ? Color.decode(storedBackground) : BACKGROUND_COLOR;
        saveAndSetBackground(background);
        if (signal.hasDecimals() && !hasSetting(signal.getName(), "decimals")) {
            addSetting(signal.getName(), "decimals", "2");
        }


        loadSettings();
        initializePopupMenu();
        addPopupMenuItems();
        validate();
        updateLabelFont();
        return this;
    }

    private void addPopupMenuItems() {
        JMenuItem modDecimalsMenu = new JMenuItem("Modify decimals");
        modDecimalsMenu.addActionListener(e -> actionDecimals(signal.getName()));
        addMenuItemToPopupMenu(modDecimalsMenu);

        JMenu numberSystemMenu = new JMenu("Edit numeral system");
        ButtonGroup numberSystemButtonGroup = new ButtonGroup();
        JRadioButtonMenuItem decimalMenuItem = new JRadioButtonMenuItem("Decimal (physical value)");
        decimalMenuItem.addActionListener((ActionEvent e) -> {
            addSetting(signal.getName(), "numeralSystem", Integer.toString(10));
            loadSettings();
        });
        decimalMenuItem.setSelected(numeralSystem == DECIMAL);
        numberSystemButtonGroup.add(decimalMenuItem);
        numberSystemMenu.add(decimalMenuItem);

        JRadioButtonMenuItem hexadecimalMenuItem = new JRadioButtonMenuItem("Hexadecimal (raw value)");
        hexadecimalMenuItem.setToolTipText("only for byte (UBYTE/SBYTE), short (UWORD/SWORD), int (A_UINT64/A_INT64) and long (ULONG/SLONG)");
        hexadecimalMenuItem.addActionListener((ActionEvent e) -> {
            addSetting(signal.getName(), "numeralSystem", Integer.toString(16));
            loadSettings();
        });
        hexadecimalMenuItem.setSelected(numeralSystem == HEXADECIMAL);
        numberSystemButtonGroup.add(hexadecimalMenuItem);
        numberSystemMenu.add(hexadecimalMenuItem);

        JRadioButtonMenuItem binaryMenuItem = new JRadioButtonMenuItem("Binary (raw value)");
        binaryMenuItem.setToolTipText("only for byte (UBYTE/SBYTE) and short (UWORD/SWORD)");
        binaryMenuItem.addActionListener((ActionEvent e) -> {
            addSetting(signal.getName(), "numeralSystem", Integer.toString(2));
            loadSettings();
        });
        binaryMenuItem.setSelected(numeralSystem == BINARY);
        numberSystemButtonGroup.add(binaryMenuItem);
        numberSystemMenu.add(binaryMenuItem);

        addMenuItemToPopupMenu(numberSystemMenu);

        JMenuItem resetSizeMenuItem = new JMenuItem("Reset to default size");
        resetSizeMenuItem.addActionListener((ActionEvent e) -> {
            Dimension defaultWindowSize = (new Dimension(((getSize().width - getContentPane().getSize().width) + DEFAULT_SIZE.width + 9), ((getSize().height - getContentPane().getSize().height) + DEFAULT_SIZE.height + 9)));
            setSize(defaultWindowSize);
            repaint();
        });
        addMenuItemToWindowMenu(resetSizeMenuItem);

        //add Toggle Scientific Notation option
        JCheckBoxMenuItem forceScientificNotation = new JCheckBoxMenuItem("Force scientific notation");
        forceScientificNotation.addActionListener((ActionEvent e) -> {
            forceScientificNotation(signal.getName());
        });
        addMenuItemToPopupMenu(forceScientificNotation);
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     *
     * The method loops through all asap2 references and will load the settings
     * for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet supported. This HANtuneWindow allows for just a single asap2
     * reference to be present. The developer will informed by a AssertException
     * otherwise
     */
    @Override
    public void loadSettings() {
        if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("numeralSystem")) {
            numeralSystem = Integer.decode(settings.get(signal.getName()).get("numeralSystem"));
        }
        if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("numeralSystem")) {
            if (!(Integer.decode(settings.get(signal.getName()).get("numeralSystem")) == 2)) {
                numeralSystem = Integer.decode(settings.get(signal.getName()).get("numeralSystem"));
            }
        }

        valueUpdate(signal.getValue(), 0);
        repaint();
    }

    @Override
    public void valueUpdate(double value, long timeStamp) {
        String formattedValue;
        switch (numeralSystem) {
            case BINARY:
                formattedValue = signal.getBitString(signal.getRawValue());
                break;
            case HEXADECIMAL:
                formattedValue = signal.getHexString(signal.getRawValue());
                break;
            default:
                formattedValue = getFormat(signal.getName(), value).format(value);
                break;
        }
        valueLabel.setText(formattedValue);
    }

    public String formatValue(double value) {
        if (value == Double.POSITIVE_INFINITY || value == Double.NEGATIVE_INFINITY || Double.isNaN(value)) {
            return String.valueOf(value);
        } else {
            return getFormat(signal.getName(), value).format(value);
        }
    }

    @Override
    public void stateUpdate() {
    }

    public void updateLabelFont() {
        if (valueLabel != null) {
            Font font;
            int height = getFontHeight(valueLabel);
            try {
                font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/fonts/quartz.ttf"));
                font = font.deriveFont(Font.PLAIN, height);
            } catch (FontFormatException | IOException exception) {
                font = new Font("Arial", Font.PLAIN, height);
                AppendToLogfile.appendError(Thread.currentThread(), exception);
            }
            valueLabel.setFont(font);
        }
    }

    public int getFontHeight(JLabel label) {
        Insets insets = label.getInsets();
        Rectangle space = new Rectangle(insets.left, insets.top,
                label.getWidth() - insets.left - insets.right,
                label.getHeight() - insets.top - insets.bottom);
        return (int) space.getHeight();
    }

    @Override
    public void restoreSize() {
        super.restoreSize();
        updateLabelFont();
    }
}
