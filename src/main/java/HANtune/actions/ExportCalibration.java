/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.io.File;
import javax.swing.tree.DefaultMutableTreeNode;

import ErrorLogger.AppendToLogfile;
import HANtune.CustomFileChooser;
import HANtune.HANtune;
import HANtune.ProjectInfo;
import datahandling.CalibrationExportWriter;
import datahandling.CalibrationExportWriterDCM;
import datahandling.CalibrationExportWriterMatlab;
import haNtuneHML.Calibration;
import util.MessagePane;

public class ExportCalibration {

    private HANtune hanTune = null;

    public ExportCalibration(HANtune hantune) {
        this.hanTune = hantune;
    }


    public void doActionExportCalibrationMatlab(DefaultMutableTreeNode node) {
        if (node == null) {
            return;
        }

        CustomFileChooser.FileType ft = CustomFileChooser.FileType.MATLAB_FILE;
        CalibrationExportWriter writer = new CalibrationExportWriterMatlab();

        doActionExport(node, ft, writer);
    }


    public void doActionExportCalibrationDCM(DefaultMutableTreeNode node) {
        if (node == null) {
            return;
        }

        CustomFileChooser.FileType ft = CustomFileChooser.FileType.DCM_FILE;
        CalibrationExportWriter writer = new CalibrationExportWriterDCM();

        doActionExport(node, ft, writer);
    }


    private void doActionExport(DefaultMutableTreeNode node, CustomFileChooser.FileType ft, CalibrationExportWriter writer) {
        int id = ((ProjectInfo) node).getId();
        Calibration calib = hanTune.currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id);

        CustomFileChooser fileChooser = new CustomFileChooser(ft, "Export", calib.getTitle());
        File file = fileChooser.chooseSaveDialogConfirmOverwrite(hanTune, "Export");

        // Do nothing if no file selected
        if (file != null) {
            try {
                // do actual writing to export file here, using appropriate method
                writer.writeCalibration(calib, file);
                if (writer.getWarningLines().length() > 0) {
                    MessagePane.showWarning("Calibration data exported to " + file.getName() + "\n" + writer.getWarningLines());
                } else {
                    MessagePane.showInfo("Successfully exported calibration data to " + file.getName());
                }
            } catch (Exception e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                hanTune.currentConfig.getHANtuneManager().setError(e.getMessage());

                MessagePane.showError("Error while exporting " + ft.getDescription() + ":\n\n"
                    + hanTune.currentConfig.getHANtuneManager().getError());
            }
        }
    }

}
