/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import ASAP2.ASAP2Data;
import ErrorLogger.AppendToLogfile;
import HANtune.CustomFileChooser;
import HANtune.ExportFlashDialog;
import HANtune.HANtune;
import datahandling.CalibrationImportReader;
import datahandling.CalibrationMerger;
import haNtuneHML.Calibration;
import util.MessagePane;
import util.Util;

@SuppressWarnings("java:S110") // suppress: Inheritance tree of classes should not be too deep
public abstract class ExportFlashDialogAction extends ExportFlashDialog {

    private String prevBaseName = null;
    private File baseFile = null;
    private File exportFile = null;
    private String calibrationName = null;
    private String flashTypeText = "";
    private boolean flashButtonOk = true;  // all preconditions for flashing are OK
    private CustomFileChooser.FileType flashFileType = null;
    private ExportFlashActionReport exportReport = null;
    private boolean succesfullReport = false;
    private boolean succesfullExport = false;
    private CalibrationMerger merger = null;
    protected ASAP2Data asap = null;
    protected Calibration calib = null;

    private static boolean isLinuxFlashNotAvailableMessageDisplayed = false;

    protected abstract boolean isExportFlashPreconditionsOk(ASAP2Data asap);
    protected abstract CalibrationMerger getCalibrationMerger(ASAP2Data asap);
    protected abstract boolean handleExportAction(CalibrationMerger merger, File exportFile) throws IOException;
    protected abstract boolean handleExportFlashAction(CalibrationMerger merger, File exportFile) throws IOException;

    protected abstract String getSuccessfullString(boolean doOptionalFlash);

    ExportFlashDialogAction(HANtune hanTune) {
        super(hanTune, true);
    }

    void handleExportFlashDialog() {
        setLocationRelativeTo(null);

        setTitle("Export / Flash " + flashTypeText + " " + calibrationName);

        checkPreviousBaseFile();
        setCreateReportButtonEnabled(baseFile != null);
        merger = null;
        setReportText("");
        succesfullReport = false;
        succesfullExport = false;

        setSelectExportFileButtonEnabled(true);
        setExportButtonEnabled(false);
        setExportFlashButtonEnabled(false);

        setVisible(true);
    }

    boolean initExportFlashDialogAction(int calibId, String calibName, String prevBaseName, String flashTypeText,
            CustomFileChooser.FileType flashFileType) {
        this.calibrationName = calibName;
        this.flashTypeText = flashTypeText;
        this.flashFileType = flashFileType;
        this.prevBaseName = prevBaseName == null ? "" : prevBaseName;

        asap = ((HANtune) super.getParent()).currentConfig.getASAP2Data();
        calib = ((HANtune) super.getParent()).currentConfig.getHANtuneDocument().getHANtune()
                .getCalibrationArray(calibId);

        exportReport = new ExportFlashActionReport(asap, calib);

        ifLinuxDisplayOnceFlashNotAvailable();

        return isExportFlashPreconditionsOk(asap);
    }

    @Override
    protected void handleChooseBaseFile() {
        CustomFileChooser fileChooser = new CustomFileChooser("Open " + flashTypeText + " base file", flashFileType);
        File newBase = fileChooser.chooseOpenDialog(getParent(), "Open");
        if (newBase == null) {
            return;  // cancel button has been pressed, do nothing
        }

        baseFile = newBase;
        setBaseFileNameField(baseFile.getAbsolutePath());
        merger = null;
        setReportText("");
        succesfullReport = false;
        succesfullExport = false;

        setCreateReportButtonEnabled(true);
        setExportButtonsEnabledIfOk();
    }

    @Override
    protected void handleCreateReport() {
        try {
            succesfullReport = false;
            setReportText("");

            merger = getCalibrationMerger(asap);

            merger.loadBasefileData(baseFile);
            if (!isLoadBaseFileOk(merger.getReadResult())) {
                return;
            }
            // do actual merge of file here
            merger.mergeCalibrationData(calib);

            exportReport.buildMergeReport(baseFile, merger.getMergeReport(), merger.getMergeResult());
            setReportText(exportReport.getMergeReport());

            if (merger.getMergeResult() == CalibrationImportReader.ImportReaderResult.READER_RESULT_ERROR) {
                return;
            }
            succesfullReport = true;
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            ((HANtune) super.getParent()).currentConfig.getHANtuneManager().setError(e.getMessage());
        } finally {
            setExportButtonsEnabledIfOk();
        }
    }

    @Override
    protected void handleChooseExportFile() {
        String name = Util.removeExtension(baseFile.getName());
        CustomFileChooser fileChooser
                = new CustomFileChooser(flashFileType, "Select export", name + "-" + calibrationName);
        File newExportFile = fileChooser.chooseSaveDialogConfirmOverwrite(super.getParent(), "Select");

        if (newExportFile == null) {
            return;  // cancel button has been pressed, do nothing
        }

        exportFile = newExportFile;
        setExportFileNameField(exportFile.getAbsolutePath());

        succesfullExport = false;
        setExportButtonsEnabledIfOk();

        appendReportText("\n\n" + "Selected file: " + exportFile.getName());
        appendReportText("\n" + "Press 'Export' or 'Export and Flash' to continue...");
    }

    @Override
    protected void handleExportAndOptionalFlash(boolean doOptionalFlash) {
        succesfullExport = false;
        try {
            if (!doOptionalFlash) {
                if (handleExportAction(merger, exportFile)) {
                    succesfullExport = true;
                } else {
                    exportReport.buildFinalReport("Export not complete.", exportFile);
                    return;
                }
            } else {
                if (handleExportFlashAction(merger, exportFile)) {
                    succesfullExport = true;
                } else {
                    if (merger.getMergeResult() == CalibrationImportReader.ImportReaderResult.READER_RESULT_ERROR) {
                        exportReport.buildFinalReport("Export not complete.", exportFile);
                        return;
                    } else {
                        exportReport.buildFinalReport("Export successful, Flashing has been cancelled.", exportFile);
                        return;
                    }
                }
            }
            exportReport.buildFinalReport(getSuccessfullString(doOptionalFlash), exportFile);
        } catch (IOException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            String errString = e.getMessage();
            ((HANtune) super.getParent()).currentConfig.getHANtuneManager().setError(errString);
            exportReport.buildFinalReport(
                    "Error while exporting" + (doOptionalFlash ? "/flashing " : " ") + "\n" + errString, exportFile);
        } finally {
            MessagePane.showInfo(super.getParent(), exportReport.getFinalMessage());
            setReportText(exportReport.getFinalReport());
        }
    }


    private static void ifLinuxDisplayOnceFlashNotAvailable() {
        if (Util.isLinux() && !isLinuxFlashNotAvailableMessageDisplayed) {
            MessagePane.showInfo("Flashing is currently not available in Linux.\n"
                    + "Only export functionality available here.\n\n"
                    + "Please use Windows version of HANtune for flashing when file has been exported");
            isLinuxFlashNotAvailableMessageDisplayed = true;
        }
    }


    private boolean isLoadBaseFileOk(CalibrationImportReader.ImportReaderResult mrgRslt) {
        boolean rtn = false;
        switch (mrgRslt) {
            case READER_RESULT_OK:
                return true;

            case READER_RESULT_WARNING:
                return MessagePane.showConfirmDialog(super.getParent(), merger.getReadResultTxt(), "Continue OK?")
                        == JOptionPane.OK_OPTION;

            case READER_RESULT_ERROR:
                MessagePane.showError(super.getParent(), merger.getReadResultTxt());
                return false;
        }

        return rtn;
    }

    private void setExportButtonsEnabledIfOk() {
        setExportButtonEnabled(false);
        setExportFlashButtonEnabled(false);
        if (succesfullReport && exportFile != null) {
            setExportButtonEnabled(true);
            setExportFlashButtonEnabled(flashButtonOk);
        }
    }

    private void checkPreviousBaseFile() {
        File file = new File(prevBaseName);
        if (file.exists()) {
            baseFile = file;
            setBaseFileNameField(baseFile.getAbsolutePath());
        }
    }

    protected ASAP2Data getAsap() {
        return asap;
    }

    void setFlashButtonOk(boolean flashButtonOk) {
        this.flashButtonOk = flashButtonOk;
    }

    boolean isSuccesfullExport() {
        return succesfullExport;
    }

    String getBaseFileAbsolutePath() {
        if (baseFile == null) {
            return "";
        } else {
            return baseFile.getAbsolutePath();
        }
    }

}
