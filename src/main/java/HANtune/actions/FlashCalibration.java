/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.io.File;

import javax.swing.JOptionPane;

import ErrorLogger.AppendToLogfile;
import HANtune.CustomFileChooser;
import HANtune.HANtune;
import java.io.IOException;

import util.MessagePane;
import util.Util;

public class FlashCalibration {

    boolean doReConnect = false;

    private static final String FLASH_COMMAND
            = "." + File.separator + "tools" + File.separator + "MicroBoot" + File.separator + "MicroBoot.exe";
    // Location of Flash tool

    private HANtune hanTune = null;
    private boolean crc32AdressessPresent = false;

    public FlashCalibration(HANtune hantune) {
        this.hanTune = hantune;
        if (hanTune.currentConfig.getASAP2Data() != null && hanTune.currentConfig.getASAP2Data().getModPar() != null
                && hanTune.currentConfig.getASAP2Data().getModPar().isCrc32AdressesPresent()) {
            this.crc32AdressessPresent = true;
        }
    }

    /**
     * Request for filename and Execute flash command.
     *
     * @return true if flash command has been executed
     */
    public boolean chooseFilenameAndFlash() {
        CustomFileChooser.FileType fType1;
        CustomFileChooser.FileType fType2;
        if (Util.isLinux()) {
            MessagePane.showInfo("Flashing is currently not available in Linux.\n"
                    + "Please use Flash from within Windows version of HANtune");
            return false;
        }

        // use presence of CRC32 addresses to decide order of extensions in FileChooser
        if (crc32AdressessPresent) {
            fType1 = CustomFileChooser.FileType.IHEX_FILE;
            fType2 = CustomFileChooser.FileType.SREC_FILE;
        } else {
            fType1 = CustomFileChooser.FileType.SREC_FILE;
            fType2 = CustomFileChooser.FileType.IHEX_FILE;
        }
        CustomFileChooser fileChooser = new CustomFileChooser("Flash into target", fType1, fType2);

        File file = fileChooser.chooseOpenDialog(hanTune, "Flash");
        if (file != null) {
            if (Util.getExtension(file.getName()).equalsIgnoreCase(CustomFileChooser.FileType.SREC_FILE.getExtension())) {
                return doActionFlashSRec(file.getAbsolutePath());
            } else if (Util.getExtension(file.getName()).equalsIgnoreCase(CustomFileChooser.FileType.IHEX_FILE.getExtension())) {
                return doActionFlashIHex(file.getAbsolutePath());

            }
        }
        return false;
    }

    /**
     * Execute flash command using given fileName
     *
     * @param fileName holds filename to use for flash command
     * @return true if flash command has been executed
     */
    boolean doActionFlashSRec(String fileName) {
        if (!checkContinueReConnectOK()) {
            return false;
        }

        if (!doFlashCommand(fileName)) {
            return false;
        }

        // re-connect when necessary
        if (doReConnect) {
            hanTune.connectProtocol(true);
        }

        return true;
    }

    /**
     * Execute flash command using given fileName
     *
     * @param fileName holds filename to use for flash command
     * @return true if flash command has been executed
     */
    boolean doActionFlashIHex(String fileName) {
        if (!ExportFlashIHexAction.isFlashConditionsOk(hanTune.currentConfig.getASAP2Data())) {
            return false;
        }

        if (!checkContinueReConnectOK()) {
            return false;
        }

        try {
            BatchIHexCalibration batch = new BatchIHexCalibration(hanTune);
            batch.doActionFlash(fileName);
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            hanTune.currentConfig.getHANtuneManager().setError(e.getMessage());
            MessagePane.showError(hanTune,
                    "Error while flashing " + CustomFileChooser.FileType.IHEX_FILE.getDescription() + ":\n\n" + "Filename: " + fileName
                    + "\n\n" + hanTune.currentConfig.getHANtuneManager().getError());
        }

        // re-connect when necessary
        if (doReConnect) {
            hanTune.connectProtocol(true);
        }

        return true;
    }

    private boolean checkContinueReConnectOK() {
        doReConnect = false;
        // disconnect during flash if necessary
        if (hanTune.isConnected()) {

            if (MessagePane.showConfirmDialog(hanTune, "HANtune is currently connected to the target.\n "
                    + "Connection will be closed during flash operation. Continue?", "Continue Flash?")
                    != JOptionPane.OK_OPTION) {
                return false;
            }

            doReConnect = true;
            hanTune.connectProtocol(false);

        }
        return true;
    }

    /**
     * Execute flash command using given fileName
     *
     * @param fileName holds filename to use for flash command
     * @return true if flash command has been executed
     */
    boolean doActionPatchFlashIHex(String fileName) {

        if (!ExportFlashIHexAction.isFlashConditionsOk(hanTune.currentConfig.getASAP2Data())) {
            return false;
        }

        if (!checkContinueReConnectOK()) {
            return false;
        }

        boolean rtn = false;
        try {
            BatchIHexCalibration batch = new BatchIHexCalibration(this.hanTune);
            // A Dos CMD is displayed during flash, therefore no message after completion.
            rtn = batch.doActionPatchFlash(fileName);
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            hanTune.currentConfig.getHANtuneManager().setError(e.getMessage());
            MessagePane.showError(hanTune,
                    "Error while flashing " + CustomFileChooser.FileType.IHEX_FILE.getDescription() + ":\n\n" + "Filename: " + fileName
                    + "\n\n" + hanTune.currentConfig.getHANtuneManager().getError());
        }

        // re-connect when necessary
        if (doReConnect) {
            hanTune.connectProtocol(true);
        }

        return rtn;
    }

    private boolean doFlashCommand(String fileName) {
        boolean flashed = false;
        try {
            flashed = Util.executeCommand(FLASH_COMMAND, fileName) == 0;
        } catch (IOException e) {
            MessagePane.showError("Microboot could not be found. Download OpenBLT from https://sourceforge.net/projects/openblt/, "
                    + "unzip it and place the contents of the 'hosts' folder in 'HANtune/tools/MicroBoot'.");
        } catch (InterruptedException e) {
            MessagePane.showError("An error occurred during the flasing process: " + e);
        }
        return flashed;
    }
}
