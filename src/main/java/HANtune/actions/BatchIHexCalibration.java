/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import ASAP2.ASAP2Crc32ModPar;
import ASAP2.ASAP2ModPar;
import HANtune.HANtune;
import datahandling.CurrentConfig;
import util.MessagePane;
import util.Util;

public class BatchIHexCalibration {

    private static final String CRC32_PATCHFLASH_BATCH_COMMAND
            = "cmd /C start \"Patch and Flash\" /WAIT /D\"tools\\Crc32\" cmd.exe /C \"crc32PatchFlash \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \" ";

    private static final String CRC32_PATCH_BATCH_COMMAND
            = "cmd /C start \"Patch\" /WAIT /D\"tools\\Crc32\" cmd.exe /C \"crc32Patch \"%s\" \"%s\"  \" ";

    private static final String CRC32_FLASH_BATCH_COMMAND
            = "cmd /C start \"Flash\" /WAIT /D\"tools\\Crc32\" cmd.exe /C \"crc32Flash \"%s\" \"%s\" \"%s\" \"%s\"  \" ";

    private HANtune hanTune = null;

    public BatchIHexCalibration(HANtune hantune) {
        this.hanTune = hantune;
    }

    /**
     * Execute patch batch command using given fileName
     *
     * @param fileName holds filename to use for patch command
     */
    public boolean doActionPatch(String fileName) throws IOException {
        boolean rtn = true;

        ASAP2ModPar mp = this.hanTune.currentConfig.getASAP2Data().getModPar();
        if (mp == null) {
            throw new IOException("Error MOD_PAR section not found");
        }
        // create mini dump:
        File miniDump = createMiniDumpFile(mp);

        // and execute it
        String cmdStr = String.format(CRC32_PATCH_BATCH_COMMAND, miniDump.getPath(), fileName);
        rtn = executeFlashCommand(cmdStr);

        // delete temporary dump file
        miniDump.delete();
        return rtn;
    }

    /**
     * Execute PatchFlash command using given fileName
     *
     * @param fileName holds filename to use for flash command
     */
    public boolean doActionPatchFlash(String fileName) throws IOException {
        boolean rtn = true;

        ASAP2ModPar mp = this.hanTune.currentConfig.getASAP2Data().getModPar();
        if (mp == null) {
            throw new IOException("Error MOD_PAR section not found");
        }
        // create mini dump:
        File miniDump = createMiniDumpFile(mp);

        // create commandline parameters
        String bodasPath = CurrentConfig.getInstance().getBodasPath();
        String hardwareDevice = mp.getEcu();

        // TODO remove hardcoding of interfaceName
        String interfaceName = "PEAK can-usb,1";

        // and execute it
        String cmdStr
                = String.format(CRC32_PATCHFLASH_BATCH_COMMAND, miniDump.getPath(), fileName, bodasPath, hardwareDevice,
                        interfaceName);
        rtn = executeFlashCommand(cmdStr);

        // delete temporary dump file
        miniDump.delete();
        return rtn;
    }

    /**
     * Execute flash command using given fileName
     *
     * @param fileName holds filename to use for flash command
     */
    public boolean doActionFlash(String fileName) {
        ASAP2ModPar mp = this.hanTune.currentConfig.getASAP2Data().getModPar();

        // create commandline parameters
        String bodasPath = CurrentConfig.getInstance().getBodasPath();
        String hardwareDevice = mp.getEcu();

        // TODO remove hardcoding of interfaceName
        String interfaceName = "PEAK can-usb,1";

        // and execute it
        String cmdStr = String.format(CRC32_FLASH_BATCH_COMMAND, fileName, bodasPath, hardwareDevice, interfaceName);

        return executeFlashCommand(cmdStr);
    }

    private boolean executeFlashCommand(String cmdStr) {
        boolean flashed = false;
        try {
            Util.executeCommand(cmdStr);
            flashed = true;
        } catch (IOException e) {
            MessagePane.showError("CRC32 could not be found. Please contact us at hantune@han.nl for more information.");
        } catch (InterruptedException e) {
            MessagePane.showError("An error occurred during the flasing process: " + e);
        }
        return flashed;
    }

    // Create a mini dump file in temp directory
    private File createMiniDumpFile(ASAP2ModPar mp) throws IOException {
        File dumpFile = null;
        try {
            dumpFile = File.createTempFile("HANmini", ".dump");

            if (!mp.isCrc32AdressesPresent()) {
                throw new IOException("Error: CRC32 addresses missing");
            }

            try (BufferedWriter bw = new BufferedWriter(new FileWriter(dumpFile))) {
                String outStr = createMinidumpStr(mp.getCrc32Addresses());
                bw.write(outStr);
                bw.flush();
            }
        } catch (IOException ex) {
            throw new IOException("Error: could not create temporary dump file. " + ex.getMessage());
        }
        return dumpFile;
    }

    private String createMinidumpStr(ASAP2Crc32ModPar crc32) {
        String first = "                0x";
        String secnd = "                __";
        return first + Long.toHexString(crc32.getEndAddress()) + secnd + "CRC_END_ADDR" + Util.LF + first
                + Long.toHexString(crc32.getPatchAddress()) + secnd + "CRC_PATCH_ADDR" + Util.LF + first
                + Long.toHexString(crc32.getStartAddress()) + secnd + "CRC_START_ADDR" + Util.LF + first
                + Long.toHexString(crc32.getpCodeStart()) + secnd + "PCODE_START" + Util.LF + first
                + Long.toHexString(crc32.getpCodeEnd()) + secnd + "PCODE_END" + Util.LF;
    }
}
