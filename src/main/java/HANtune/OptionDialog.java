/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Object;
import can.CanSignal;
import can.DbcManager;
import datahandling.CurrentConfig;
import nl.han.hantune.gui.components.NumpadDecimalLocaleConverter;
import util.MessagePane;
import util.Util;

/**
 *
 * @author obwlf
 */
public class OptionDialog extends JFrame {
    /**
     * A return status code - returned if OK button has been pressed
     */
    private static final int RET_OK = 1;
    private static final int RET_CANCEL = 0;

    private static final String INPUT_VALUE = "inputValue";
    private static final String TEXT_VALUE = "textValue";
    private static final String NUMBER_OF_OPTIONS = "numberOfOptions";

    private static final int MAX_NUM_OPTIONS = 5;
    private static final int MIN_NUM_OPTIONS = 2;
    private static final long serialVersionUID = 7526471155622776157L;

    private HANtuneWindow hantuneWindow;

    private String asap2Ref = null;
    private Integer numberOfOptions = MIN_NUM_OPTIONS;

    private double minInputVal = Integer.MIN_VALUE;
    private double maxInputValue = Integer.MAX_VALUE;
    private String formatStr = "0.##########";
    private double factor = ASAP2Object.DEFAULT_CONVERSION_FACTOR;


    /**
     * NumberFormat for the parsing of numbers from the jTextField
     */
    NumberFormat inputFormat;
    private CurrentConfig currentConfig = CurrentConfig.getInstance();


    /**
     * Constructor
     */
    public OptionDialog(HANtuneWindow hantuneWindow, String asap2Ref) {
        this.hantuneWindow = hantuneWindow;
        this.asap2Ref = asap2Ref;
        setTitle("Set options " + this.asap2Ref);
        this.setIconImage(
            Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/HANlogo.png")));
        setLocationRelativeTo(null);
        setUpFormat();
        loadSettings();
        initComponents();
        closeOnCancelKey();
        setVisible(true);
        addKeyListeners();
        rebuild(true);
    }


    // For unit testing ONLY!!!
    protected OptionDialog() {
    }


    private void closeOnCancelKey() {
        // Close the dialog when Esc is pressed
        String cancelAction = "escapePressed";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelAction);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelAction, new AbstractAction() {
            private static final long serialVersionUID = 7526471155622776151L;

            @Override
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });
    }


    private void rebuild(boolean fillAll) {
        enableComponents();
        if (fillAll) {
            fillOptionValueFields();
        }
    }


    private void enableComponents() {
        // Enable or disable fields according to current number of options
        for (int idx = 0; idx < optionValuePairs.size(); idx++) {
            OptionValuePair ovp = optionValuePairs.get(idx);
            ovp.optionField.setEnabled(idx < numberOfOptions);
            ovp.valueField.setEnabled(idx < numberOfOptions);
        }

        addButton.setEnabled(numberOfOptions < MAX_NUM_OPTIONS);
        removeButton.setEnabled(numberOfOptions > MIN_NUM_OPTIONS);
    }


    private void fillOptionValueFields() {
        // Fill Option and Value fields with current data from hantuneWindow.settings
        if (hantuneWindow.settings.containsKey(asap2Ref)) {
            for (int idx = 0; idx < optionValuePairs.size(); idx++) {
                String optStr = TEXT_VALUE + (idx + 1);
                OptionValuePair ovp = optionValuePairs.get(idx);
                if (hantuneWindow.settings.get(asap2Ref).containsKey(optStr)) {
                    ovp.optionField.setText(hantuneWindow.settings.get(asap2Ref).get(optStr));
                }
                String valStr = INPUT_VALUE + (idx + 1);
                if (hantuneWindow.settings.get(asap2Ref).containsKey(valStr)) {
                    ovp.valueField.setText(hantuneWindow.settings.get(asap2Ref).get(valStr)
                        .replace(Util.DEC_SEPARATOR_HML, Util.DEC_SEPARATOR_LOCALE)); // for compatibility
                    // reasons: hml files should ALWAYS use a dot (.) as a decimal separator, therefore
                    // replace it here for current locale separator (might be same)
                }
            }
        }
    }


    private void addKeyListeners() {
        for (OptionValuePair ovp : optionValuePairs) {
            ovp.valueField.addKeyListener(new HandleKeys());
        }
    }


    private void doClose(final int status) {
        if (status == RET_OK) {
            updateSettings();
        }

        hantuneWindow.loadSettings();
        hantuneWindow.rebuild();
        setVisible(false);
        dispose();
    }// end of doClose


    private void updateSettings() {
        // write texts and values to settings according to the number of options, remove others
        int textNumber = 1; // 'textValue' starts at 1: textValue1
        for (OptionValuePair ovp : optionValuePairs) {
            String optStr = TEXT_VALUE + textNumber;
            String valStr = INPUT_VALUE + textNumber;
            if (textNumber <= numberOfOptions) {
                hantuneWindow.addSetting(asap2Ref, optStr, ovp.optionField.getText());

                // for compatibility reasons: hml files should ALWAYS use a dot (.) as a decimal
                // separator,
                // therefore replace current locale separator for HML separator
                hantuneWindow.addSetting(asap2Ref, valStr, ovp.valueField.getText()
                    .replace(Util.DEC_SEPARATOR_LOCALE, Util.DEC_SEPARATOR_HML));
            } else {
                hantuneWindow.removeSetting(asap2Ref, optStr);
                hantuneWindow.removeSetting(asap2Ref, valStr);
            }
            textNumber++;
        }
        // write numberOfOptions to the settings
        hantuneWindow.addSetting(asap2Ref, NUMBER_OF_OPTIONS, numberOfOptions.toString());
    }


    private void loadSettings() {
        if (hantuneWindow.settings.containsKey(asap2Ref)
            && hantuneWindow.settings.get(asap2Ref).containsKey(NUMBER_OF_OPTIONS)) {
            numberOfOptions = Integer.parseInt(hantuneWindow.settings.get(asap2Ref).get(NUMBER_OF_OPTIONS));
        }

        ASAP2Characteristic asap2Char = currentConfig.getASAP2Data().getASAP2Characteristics().get(asap2Ref);

        if (asap2Char != null) {

            minInputVal = asap2Char.getMinimum();
            maxInputValue = asap2Char.getMaximum();
            formatStr = asap2Char.getDecFormat().replace('0', '#');
            factor = asap2Char.getFactor();

        } else if (DbcManager.containsSignalByName(asap2Ref)) {
            CanSignal sig = DbcManager.getSignalByName(asap2Ref);

            minInputVal = sig.getMinimum();
            maxInputValue = sig.getMaximum();
            int decCnt = sig.getDecimalCount();
            if (decCnt == 0) {
                formatStr = "#";
            } else {
                StringBuilder builder = new StringBuilder("#.");
                for (int icnt = 0; icnt < sig.getDecimalCount(); icnt++) {
                    builder.append('#');
                }
                formatStr = builder.toString();
            }
            factor = sig.getFactor();
        }
    }


    public class OptionValueVerifier extends InputVerifier implements ActionListener {

        public double convertToNearestDiscreteValue(double factor, double inRaw) {
             if (factor == ASAP2Object.DEFAULT_CONVERSION_FACTOR) {
                return inRaw;
            } else {
                // Always rounds DOWN, don't use it.
                // dontusereturn as2Char.convertValue(as2Char.convertValue((Object)inRaw))

                // Use this one, proper rounding
                return (Math.round(inRaw / factor)) * factor;
            }
        }


        @Override
        public boolean shouldYieldFocus(JComponent input) {
            boolean inputOK = verify(input);
            if (inputOK) {
                return true;
            } else {
                Toolkit.getDefaultToolkit().beep();

                MessagePane.showError(input.getParent(),
                    "Incorrect value: " + ((JTextField)input).getText()
                        + "\n Please enter a number between " + Util.getStringValLocale(minInputVal)
                        + " and " + Util.getStringValLocale(maxInputValue));
            }
            return false;
        }


        @Override
        public boolean verify(JComponent input) {
            boolean isValid = false;
            JTextField textField = (JTextField)input;

            try {
                double raw = Util.getDoubleValueLocale(textField.getText());
                isValid = raw >= minInputVal && raw <= maxInputValue;
                if (isValid) {
                    textField.setText(
                        Util.getStringValue(convertToNearestDiscreteValue(factor, raw), formatStr));
                } else {
                    textField.setText(Util.getStringValLocale(raw));
                }
            } catch (ParseException e) {
                // do nothing here
            }

            return isValid;
        }


        @Override
        public void actionPerformed(ActionEvent e) {
            JTextField source = (JTextField)e.getSource();
            shouldYieldFocus(source);
            source.selectAll();
        }
    }


    private void setUpFormat() {
        inputFormat = NumberFormat.getNumberInstance(Locale.FRENCH);
        if (currentConfig.getASAP2Data().getASAP2Characteristics().get(asap2Ref) != null) {
            inputFormat.setMaximumFractionDigits(hantuneWindow.currentConfig.getASAP2Data()
                .getASAP2Characteristics().get(asap2Ref).getDecimalCount());
        } else {
            inputFormat.setMaximumFractionDigits(0);
        }
    }


    private class HandleKeys extends KeyAdapter {
        // Convert numpad '.' to decimal for current locale
        NumpadDecimalLocaleConverter cnvLocale = new NumpadDecimalLocaleConverter();


        @Override
        public void keyPressed(KeyEvent event) {
            cnvLocale.handleKeyPressedEvent(event);
        }


        @Override
        public void keyTyped(final KeyEvent event) {
            if (event.getKeyChar() == '\n') { // Handle Enter key here
                JTextField source = (JTextField)event.getSource();
                source.getInputVerifier().shouldYieldFocus(source);
            } else {
                cnvLocale.handleKeyTypedEvent(event);
            }
        }

    }


    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this
     * code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        optionValuePairs.clear();
        for (int i = 0; i < MAX_NUM_OPTIONS; i++) {
            optionValuePairs.add(new OptionValuePair());
        }

        addButton = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        cancelButton = new javax.swing.JButton();
        okButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        int cnt = 1;
        for (OptionValuePair ovp : optionValuePairs) {
            String optStr = "Option " + cnt;
            ovp.optionField.setText(optStr);
            ovp.valueField.setText("0");
            ovp.valueField.setInputVerifier(new OptionValueVerifier());
            cnt++;
        }
        addButton.setText("Add option");
        addButton.addActionListener(this::addButtonActionPerformed);

        removeButton.setText("Remove option");
        removeButton.addActionListener(this::removeButtonActionPerformed);

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(this::cancelButtonActionPerformed);
        cancelButton.setVerifyInputWhenFocusTarget(false); // Do not verify when canceling

        okButton.setText("OK");
        okButton.addActionListener(this::okButtonActionPerformed);

        jLabel1.setText("Desciption");
        jLabel2.setText("Value");


        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout
            .setHorizontalGroup(layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout
                    .createSequentialGroup().addGap(10, 10, 10).addGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                layout.createSequentialGroup().addComponent(jLabel1).addGap(130, 130, 130)
                                    .addComponent(jLabel2).addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout
                                .createSequentialGroup().addGap(0, 30, Short.MAX_VALUE)
                                .addComponent(addButton).addGap(18, 18, 18).addComponent(removeButton))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout
                                    .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(optionValuePairs.get(0).optionField)
                                    .addComponent(optionValuePairs.get(1).optionField)
                                    .addComponent(optionValuePairs.get(2).optionField)
                                    .addComponent(optionValuePairs.get(3).optionField)
                                    .addComponent(optionValuePairs.get(4).optionField,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE))
                                .addGap(18, 18, 18).addGroup(
                                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(optionValuePairs.get(0).valueField,
                                            javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(optionValuePairs.get(1).valueField,
                                            javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(optionValuePairs.get(2).valueField,
                                            javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(optionValuePairs.get(3).valueField,
                                            javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(optionValuePairs.get(4).valueField,
                                            javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))))
                    .addGap(10, 10, 10))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                    layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(okButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cancelButton).addGap(9, 9, 9)));

        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup().addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1).addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(optionValuePairs.get(0).optionField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(optionValuePairs.get(0).valueField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(optionValuePairs.get(1).optionField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(optionValuePairs.get(1).valueField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(optionValuePairs.get(2).optionField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(optionValuePairs.get(2).valueField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(optionValuePairs.get(3).optionField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(optionValuePairs.get(3).valueField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(optionValuePairs.get(4).optionField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(optionValuePairs.get(4).valueField,
                        javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addButton).addComponent(removeButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10,
                    javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(okButton).addComponent(cancelButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        pack();
        setMinimumSize(getSize());
    }// </editor-fold>//GEN-END:initComponents


    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_okButtonActionPerformed
        doClose(RET_OK);
    }// GEN-LAST:event_okButtonActionPerformed


    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_removeOptionButtonActionPerformed
        if (numberOfOptions > MIN_NUM_OPTIONS) {
            numberOfOptions--;
            rebuild(false);
        }
    }// GEN-LAST:event_removeOptionButtonActionPerformed


    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_addOptionButtonActionPerformed
        if (numberOfOptions < MAX_NUM_OPTIONS) {
            numberOfOptions++;
            rebuild(false);
        }
    }// GEN-LAST:event_addOptionButtonActionPerformed


    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cancelButtonActionPerformed
        doClose(RET_CANCEL);
    }// GEN-LAST:event_cancelButtonActionPerformed


    private void formWindowClosing(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_formWindowClosing
        doClose(RET_CANCEL);
    }// GEN-LAST:event_formWindowClosing


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton okButton;
    private javax.swing.JButton addButton;
    private javax.swing.JButton removeButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;

    private class OptionValuePair {
        JTextField optionField;
        JTextField valueField;


        OptionValuePair() {
            optionField = new JTextField();
            valueField = new JTextField();
        }
    }

    private final List<OptionValuePair> optionValuePairs = new ArrayList<>();
    // End of variables declaration//GEN-END:variables
}
