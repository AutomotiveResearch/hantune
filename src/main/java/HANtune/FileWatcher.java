/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import ErrorLogger.AppendToLogfile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.SwingUtilities;

/**
 *
 * @author Michiel Klifman
 */
public class FileWatcher implements Runnable {

    private static WatchService watcher;
    private static final List<Path> paths = new ArrayList<>();
    private static final List<Path> modifiedFiles = Collections.synchronizedList(new ArrayList<Path>());


    public static void addFile(File file) {
        file = file.getAbsoluteFile();
        Path path = Paths.get(file.getParent());
        if (!paths.contains(path)) {
            paths.add(path);
            try {
                if (watcher == null) {
                    watcher = FileSystems.getDefault().newWatchService();
                    Thread thread = new Thread(new FileWatcher());
                    thread.setName("FileWatcherThread");
                    thread.start();
                }
                path.register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);
            } catch (IOException exception) {
                AppendToLogfile.appendError(Thread.currentThread(), exception);
            }
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
            WatchKey key = watcher.take();

            List<WatchEvent<?>> events = key.pollEvents();
            for (WatchEvent<?> event : events) {
                if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
                    Path dir = (Path)key.watchable();
                    Path fullPath = dir.resolve((Path)event.context());
                    handleModification(fullPath);
                }
            }
            key.reset();
            }
        } catch (InterruptedException exception) {
            AppendToLogfile.appendError(Thread.currentThread(), exception);
        }
    }

    private void handleModification(final Path filePath) {
        SwingUtilities.invokeLater(() -> {
            if (!modifiedFiles.contains(filePath)) {
                modifiedFiles.add(filePath);
                HANtune.getInstance().updateProjectTree();
            }
        });
    }

    public static boolean isFileModified(File file) {
        return modifiedFiles.contains(getPathFromFile(file));
    }

    public static void removeFileFromList(File file) {
        modifiedFiles.remove(getPathFromFile(file));
    }

    private static Path getPathFromFile(File file) {
        return Paths.get(file.getPath());
    }
}
