/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.markup;
import HANtune.HANtuneWindow;
import components.JFontChooser;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;

/**
 * Displays a text label for placing additional information. The following can
 * be configured to the label: Font, text color, hide frame, show background and
 * reset size
 *
 * @author Mathijs,
 * @Revised Roel van den Boom
 */
public class TextMarkup extends HANtuneWindow implements FocusListener{
    private static final long serialVersionUID = 1L;
    private static final Dimension DEFAULT_SIZE = new Dimension(100, 100);
    private Container container;
    private JEditorPane editorPane = new JEditorPane();
    private Highlighter highLighter = editorPane.getHighlighter();
    private Boolean border = true;

    public TextMarkup() {
        defaultMinimumSize = new Dimension(70, 50);
        this.getContentPane().setPreferredSize(DEFAULT_SIZE);
        initializePopupMenu();
        addPopupMenuItems();
        initializeEditorField();
        showBorder(getWindowSetting("showBorder", DEFAULT_BORDER));
    }

    /**
     * This method rebuilds the content of this HANtuneWindow and should be
     * called only if the content changed (adding or removing asap2 references).
     * There is no need to call this method after a value or setting has
     * changed.
     *
     * The method loops through all asap2 references and will build the
     * components required for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet supported. This HANtuneWindow allows for just a single asap2 reference
     * to be present. The developer will informed by a AssertException otherwise
     *
     */
    @Override
    public void rebuild() {
        super.loadWindowSettings();
        getContentPane().removeAll();
        GridBagConstraints gridBagConstraints;
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        getContentPane().add(editorPane, gridBagConstraints);
        setAllOpaque(container, false);
        editorPane.setBorder(null);
        loadSettings();
    }

    private void addPopupMenuItems(){

            JMenuItem modifyText = new JMenuItem("Modify Text");
            modifyText.addActionListener(e -> setEditable());
            addMenuItemToPopupMenu(modifyText);

            JMenuItem modifyFont = new JMenuItem("Modify Font");
            modifyFont.addActionListener(e -> showFontChooser());
            addMenuItemToPopupMenu(modifyFont);

            JMenuItem modifyColor = new JMenuItem("Modify Text Color");
            modifyColor.addActionListener(e -> showColorChooser());
            addMenuItemToPopupMenu(modifyColor);

            editorPane.addMouseListener(new MouseAdapter(){
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (SwingUtilities.isRightMouseButton(e)) {
                        showPopupMenu(editorPane, e.getX(), e.getY());
                    }
                }
            });
    }

    public void setSelected(){
        editorPane.setEditable(false);
    }

    private void showFontChooser() {
        JFontChooser fontChooser = new JFontChooser();
        if (fontChooser.showDialog(null) == JFontChooser.OK_OPTION) {
            editorPane.setFont(fontChooser.getSelectedFont());
        }
        String style;
        int s = editorPane.getFont().getStyle();
        switch (s) {
            case 1:
                style = "BOLD";
                break;
            case 2:
                style = "ITALIC";
                break;
            case 3:
                style = "BOLDITALIC";
                break;
            default:
                style = "PLAIN";
                break;
        }
        String xmlFont = editorPane.getFont().getName()
                + "-" + style
                + "-" + Integer.toString(editorPane.getFont().getSize());
        addWindowSetting("font", xmlFont);
        repaint();
    }

    private void showColorChooser() {
        Font font = editorPane.getFont();
        Color newColor = JColorChooser.showDialog(TextMarkup.this, "Choose color", Color.RED);
        if (!(newColor == null)) {
            editorPane.setForeground(newColor);
            editorPane.setFont(font);
            addWindowSetting("textColor", Integer.toString(newColor.hashCode()));
            loadSettings();
        }
    }

    private void setEditable(){
        editorPane.setEditable(true);
        editorPane.setEnabled(true);
        editorPane.setSelectionColor(Color.BLUE);
        editorPane.setHighlighter(highLighter);
        editorPane.setHighlighter(new DefaultHighlighter());
        editorPane.grabFocus();
    }

    public void setText(String string){
        editorPane.setText(string);
    }

    public void saveAndSetText(){
        addWindowSetting("text", editorPane.getText());
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     *
     * The method loops through all asap2 references and will load the settings
     * for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet supported. This HANtuneWindow allows for just a single asap2 reference
     * to be present. The developer will informed by a AssertException otherwise
     */
    @Override
    public void loadSettings() {
        if (super.windowSettings.containsKey("text")) {
            editorPane.setText(super.windowSettings.get("text"));
        }

            if (super.windowSettings.containsKey("font")) {
                editorPane.setFont(Font.decode(super.windowSettings.get("font")));
            }
            if (super.windowSettings.containsKey("backgroundColor")) {
                editorPane.setBackground(Color.decode(super.windowSettings.get("backgroundColor")));
            }

            if (super.windowSettings.containsKey("showBackground")) {
                if (Boolean.parseBoolean(super.windowSettings.get("visible")) == false) {
                    editorPane.setOpaque(false);
                } else {
                    editorPane.setOpaque(true);
                }
            }

            if (super.windowSettings.containsKey("textColor")) {
                editorPane.setForeground((Color.decode(super.windowSettings.get("textColor"))));
            }
        super.loadWindowSettings();
        repaint();
    }

    @Override
    protected void saveAndShowBackground(boolean show) {
        addWindowSetting("showBackground", String.valueOf(show));
        showBackground(show);
    }

    @Override
    protected void saveAndSetBackground(Color background) {
        addWindowSetting("backgroundColor", Integer.toString(background.hashCode()));
        editorPane.setBackground(background);
        repaint();
    }


    /**
     * Sets opaque of all the components in the container to true or false
     *
     * @param c the container on witch to perform the action
     * @param opaque boolean to use
     */
    public final void setAllOpaque(Container c, boolean opaque) {
        if (c instanceof JComponent) {
            ((JComponent) c).setOpaque(opaque);
            for (int i = 0; i < c.getComponentCount(); i++) {
                Component comp = c.getComponent(i);
                if (comp instanceof Container) {
                    setAllOpaque((Container) comp, opaque);
                }
            }
        }
    }

    @Override
        protected void editBackgroundColor() {
        Color newColor = JColorChooser.showDialog(this, "Choose background color", this.getBackground());
        if (newColor != null) {
            saveAndSetBackground(newColor);
            editorPane.setBackground(newColor);
        }
    }

    @Override
    protected void showBackground(boolean show) {
        super.showBackground(show);
        editorPane.setOpaque(show);
        repaint();
    }

    @Override
    public void clearWindow() {
    }

    private void initializeEditorField() {
        editorPane.setEditable(false);
        editorPane.setEnabled(true);
        editorPane.addFocusListener(this);
        editorPane.setHighlighter(null);
    }

    @Override
    public void focusGained(FocusEvent e) {
        //Do nothing
    }

    @Override
    public void focusLost(FocusEvent e) {
        editorPane.setEditable(false);
        editorPane.setHighlighter(null);
        saveAndSetText();
        rebuild();
    }
}
