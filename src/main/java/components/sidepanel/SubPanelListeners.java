/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;

/**
 *
 * @author Michiel
 */
public class SubPanelListeners extends MouseInputAdapter {

    private final List<SubPanel> subPanels = new ArrayList<>();
    private SubPanel upperComponent;
    private SubPanel lowerComponent;
    private SubPanel clickedComponent;
    private int resizeDelta;
    private boolean isResizing;
    private boolean mouseOnPanel;
    private int lastMouseY = 0;
    private static final int NORTH = 0;
    private static final int SOUTH = 1;
    private int direction = 0;

    public void addSubpanel(SubPanel subPanel) {
        subPanels.add(subPanel);
        if (subPanel.getResizeButton() != null){
        subPanel.getResizeButton().setButtonActionPerformed((ActionEvent e) -> toggleSize(subPanel));
        }
        subPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent event) {
                SubPanel subPanel = (SubPanel) event.getSource();
                if(subPanel.isMinimized()) {
                    if (direction == NORTH) {
                        if (subPanel == upperComponent) {
                            upperComponent = getPreviousPanel(subPanel);
                        }
                    } else if (direction == SOUTH) {
                        if (subPanel == lowerComponent) {
                            lowerComponent = getNextPanel(subPanel);
                            if (subPanel != lowerComponent) lowerComponent.setMaximizedSize(lowerComponent.getSize());
                            resizeDelta = MouseInfo.getPointerInfo().getLocation().y + lowerComponent.getHeight();
                        }
                    }
                }
            }
        });
    }

    private void toggleSize(SubPanel subPanel) {
        if (subPanel.getHeight() != subPanel.getMinimumSize().height) {
            subPanel.setMaximizedSize(subPanel.getSize());
            subPanel.minimize();

        } else {

            SubPanel tallestPanel = getTallestPanel();
            if (subPanel.getMaximizedSize().height > tallestPanel.getHeight() - tallestPanel.getMinimumSize().height) {
                subPanel.setMaximizedSize(new Dimension(subPanel.getMaximizedSize().width, subPanel.getHeight() + (tallestPanel.getHeight() - tallestPanel.getMinimumSize().height)));
            }
            subPanel.maximize();
            resizeSupPanel(tallestPanel, tallestPanel.getWidth(), tallestPanel.getHeight() - (subPanel.getMaximizedSize().height - subPanel.getMinimumSize().height));
        }
        subPanel.getParent().validate();
    }

    @Override
    public void mouseEntered(MouseEvent event) {
        setResizeCursor();
        mouseOnPanel = true;
    }

    @Override
    public void mouseExited(MouseEvent event) {
        mouseOnPanel = false;
        if (!isResizing) {
            setDefaultCursor();
        }
    }

    @Override
    public void mousePressed(MouseEvent event) {
        clickedComponent = (SubPanel) event.getSource();
        upperComponent = clickedComponent.isMinimized() ? getPreviousPanel(clickedComponent) : clickedComponent;
        lowerComponent = getNextOpenPanel(clickedComponent) != null ? getNextOpenPanel(clickedComponent) : getNextPanel(clickedComponent);
        resizeDelta = MouseInfo.getPointerInfo().getLocation().y + lowerComponent.getHeight();
        lastMouseY = MouseInfo.getPointerInfo().getLocation().y;
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        isResizing = false;
        if (!mouseOnPanel) {
            setDefaultCursor();
        }
    }

    @Override
    public void mouseDragged(MouseEvent event) {
        isResizing = true;
        setDirection();
        if (direction == NORTH) {
            if (lowerComponent != getNextPanel(clickedComponent)) {
                lowerComponent = getNextPanel(clickedComponent);
                resizeDelta = MouseInfo.getPointerInfo().getLocation().y + lowerComponent.getHeight();
            }
        } else if (direction == SOUTH) {
            if (upperComponent != clickedComponent) {
                upperComponent = getPreviousPanel(lowerComponent);
            }
        }

        int lowerComponentHeight = resizeDelta - MouseInfo.getPointerInfo().getLocation().y;
        int adjustedSize = getResizeAdjustment(lowerComponentHeight - lowerComponent.getHeight());
        resizeSupPanel(upperComponent, upperComponent.getWidth(), upperComponent.getHeight() - adjustedSize);
        resizeSupPanel(lowerComponent, lowerComponent.getWidth(), lowerComponent.getHeight() + adjustedSize);
        upperComponent.getParent().validate();
        lastMouseY = MouseInfo.getPointerInfo().getLocation().y;
    }

    private void resizeSupPanel(SubPanel subPanel, int width, int height) {
        subPanel.setSize(width, height);
        subPanel.setPreferredSize(subPanel.getSize());
        subPanel.validate();
        if (!subPanel.manuallyValidate()) subPanel.attemptToCorrectSize();
    }

    private int getResizeAdjustment(int mouseDistance) {
        int adjustment = mouseDistance;
        if (upperComponent.getHeight() - mouseDistance <= upperComponent.getMinimumSize().height) {
            adjustment = upperComponent.getHeight() - upperComponent.getMinimumSize().height;
        } else if (lowerComponent.getHeight() + mouseDistance <= lowerComponent.getMinimumSize().height) {
            adjustment = -(lowerComponent.getHeight() - lowerComponent.getMinimumSize().height);
        }
        return adjustment;
    }

    private void setDefaultCursor() {
        for (SubPanel subPanel : subPanels) {
            subPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    private void setResizeCursor() {
        for (SubPanel subPanel : subPanels) {
            subPanel.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
        }
    }

    private void setDirection() {
        if (lastMouseY > MouseInfo.getPointerInfo().getLocation().y) {
            direction = NORTH;
        } else if (lastMouseY < MouseInfo.getPointerInfo().getLocation().y) {
            direction = SOUTH;
        }
    }

    private SubPanel getNextPanel(SubPanel subPanel) {
        int index = subPanels.indexOf(subPanel);
        if (index >= 0 && index + 1 < subPanels.size()) {

            return subPanels.get(index + 1);
        }
        return subPanel;
    }

    private SubPanel getPreviousPanel(SubPanel subPanel) {
        int index = subPanels.indexOf(subPanel);
        if (index > 0 && index - 1 <= subPanels.size()) {

            return subPanels.get(index - 1);
        }
        return subPanel;
    }

    private SubPanel getNextOpenPanel(SubPanel subPanel) {
        int index = subPanels.indexOf(subPanel);
        if (index >= 0 && index + 1 < subPanels.size()) {
            for (int i = index+1; i < subPanels.size(); i++) {
                if (!subPanels.get(i).isMinimized()) {
                    return subPanels.get(i);
                }
            }
        }
        return null;
    }

    private SubPanel getTallestPanel() {
        SubPanel tallestSubPanel = subPanels.get(0);
        for (SubPanel subPanel : subPanels) {
            if (subPanel.getHeight() > tallestSubPanel.getHeight()) {
                tallestSubPanel = subPanel;
            }
        }
        return tallestSubPanel;
    }

    private void printSizes() {
        JPanel contentPanel = (JPanel) subPanels.get(0).getParent();
        int totalHeight = 0;
        for (SubPanel subPanel : subPanels) {
            System.out.println(subPanel.getTitle() + " height: " + subPanel.getHeight());
            totalHeight += subPanel.getHeight();
        }
        System.out.println("total height: " + totalHeight);
        System.out.println("content panel height: " + contentPanel.getHeight() + "\n");
    }
}
