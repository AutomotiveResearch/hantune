/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class AbstractTree extends JTree implements Searchable {

    private DefaultMutableTreeNode root = new DefaultMutableTreeNode();
    private final List<TreePath>expandedNodes = new ArrayList<>();
    private boolean filtered;
    private final boolean showLeafCount = true;
    private Timer tooltipTimer;
    private MouseEvent mouseEvent;
    private DefaultMutableTreeNode selectedNode;
    private final List<AbstractTreeListener> listeners = new ArrayList<>();

    public AbstractTree() {
        addEventListeners();
    }

    @Override
    public void find(String filter) {
        if (this.getLastSelectedPathComponent() != null) {
            selectedNode = (DefaultMutableTreeNode) this.getLastSelectedPathComponent();
        }
        if (filter.isEmpty()) {
            if(filtered) {
                resetTree();
            }
        } else {
            filterTree(filter);
        }
        if (selectedNode != null) {
           TreePath path = findObject(selectedNode.getUserObject());
        this.setSelectionPath(path);
        }
    }

    private TreePath findObject(Object object) {
        Enumeration<?> nodes = ((DefaultMutableTreeNode) this.getModel().getRoot()).preorderEnumeration();
        while (nodes.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) nodes.nextElement();
            if (node.getUserObject() == object) {
                return new TreePath(node.getPath());
            }
        }
        return null;
    }

    private void resetTree() {
        this.setModel(new DefaultTreeModel(root));
        restoreExpansionState();
        filtered = false;
        if (selectedNode != null) {
            this.setSelectionPath(new TreePath(selectedNode.getPath()));
        }
    }

    private void filterTree(String filter) {
        if (!filtered) {
            root = (DefaultMutableTreeNode) this.getModel().getRoot();
        }

        DefaultMutableTreeNode filteredRoot = new DefaultMutableTreeNode();
        filterNode(root, filteredRoot, filter);

        if (filteredRoot.isLeaf()) {
            filteredRoot.add(new DefaultMutableTreeNode("No matches found."));
        }
        this.setModel(new DefaultTreeModel(filteredRoot));
        filtered = true;
        expandTree();
    }

    public void filterNode(DefaultMutableTreeNode unfilteredNode, DefaultMutableTreeNode filteredNode, String filter) {
        for (int i = 0; i < unfilteredNode.getChildCount(); i++) {
            DefaultMutableTreeNode unfilteredChildNode = (DefaultMutableTreeNode) unfilteredNode.getChildAt(i);
            DefaultMutableTreeNode filteredChildNode = (DefaultMutableTreeNode) unfilteredChildNode.clone();

            if (unfilteredChildNode.toString().toLowerCase().contains(filter.toLowerCase())) {
                filterNode(unfilteredChildNode, filteredChildNode, "");
                filteredNode.add(filteredChildNode);
            } else {
                filterNode(unfilteredChildNode, filteredChildNode, filter);
                if(!filteredChildNode.isLeaf()) {
                    filteredNode.add(filteredChildNode);
                }
            }
        }
    }

    public void expandTree() {
        for (int i = 0; i < this.getRowCount(); i++) {
            this.expandRow(i);
        }
    }

    public void restoreExpansionState() {
        for (TreePath expandedNode : expandedNodes) {
            this.expandPath(expandedNode);
        }
    }

    private void addEventListeners() {
        this.addTreeExpansionListener(new TreeExpansionListener() {
            @Override
            public void treeExpanded(TreeExpansionEvent event) {
                if (!expandedNodes.contains(event.getPath()) && !filtered) {
                    expandedNodes.add(event.getPath());
                }
            }

            @Override
            public void treeCollapsed(TreeExpansionEvent event) {
                expandedNodes.remove(event.getPath());
            }
        });
    }

    public String getNodeSuffix(DefaultMutableTreeNode node) {
        String suffix = "";
        if (showLeafCount) {
            suffix = " (" + node.getLeafCount() + ")";
        }
        return suffix;
    }

    public ImageIcon createImageIcon(String path) {
        URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            return null;
        }
    }

    public void setDynamicToolTip() {
        tooltipTimer = new Timer(1000, (ActionEvent e) -> showTooltip());
        tooltipTimer.setRepeats(false);
        this.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent event) {
                mouseEvent = event;
                startTimer();
            }
        });
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent event) {
                tooltipTimer.stop();
                ToolTipManager.sharedInstance().setEnabled(true);
            }
        });
    }

    private void startTimer() {
        if (tooltipTimer.isRunning()) {
            tooltipTimer.restart();
            ToolTipManager.sharedInstance().setEnabled(false);
        } else {
            tooltipTimer.start();
        }
    }

    private void showTooltip() {
        ToolTipManager.sharedInstance().setEnabled(true);
        DefaultMutableTreeNode node = getLastPathComponent();
        if (node != null) {
            this.setToolTipText(getToolTipTextForNode(node));
            ToolTipManager.sharedInstance().mouseMoved(mouseEvent);
        }
    }

    public DefaultMutableTreeNode getLastPathComponent() {
        TreePath path = this.getPathForLocation(mouseEvent.getX(), mouseEvent.getY());
        return path != null ? (DefaultMutableTreeNode) path.getLastPathComponent() : null;
    }

    public String getToolTipTextForNode(DefaultMutableTreeNode node) {
        return node.toString();
    }

    /**
     * Gets all user objects of the currently selected nodes in this tree for a
     * give type (i.e. Signal.class). This includes children of a node one level
     * down.
     * @param <T> - the type of the user objects to get.
     * @param type- the type of the user objects to get.
     * @return a list of user objects of the give type.
     */
    public <T> List<T> getSelectedUserObjects(Class<T> type) {
        List<T> userObjects = new ArrayList<>();
        TreePath[] paths = this.getSelectionPaths();
        for (TreePath path : paths) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
            if (type.isInstance(node.getUserObject())) {
                userObjects.add(type.cast(node.getUserObject()));
            } else if (!node.isLeaf()) {
                List<T> children = getUserObjectsFromParent(node, type);
                userObjects.addAll(children);
            }
        }
        return userObjects;
    }

    /**
     * Gets all user objects of the children of the given node for a given type.
     * @param <T> - the type of the user objects to get.
     * @param parent - the node to get its children's user objects from.
     * @param type - the type of the user objects to get.
     * @return a list of user objects of the give type.
     */
    public <T> List<T> getUserObjectsFromParent(DefaultMutableTreeNode parent, Class<T> type) {
        List<T> userObjects = new ArrayList<>();
        for (int i = 0; i < parent.getChildCount(); i++) {
            T userObject = getUserObjectFromNode((DefaultMutableTreeNode) parent.getChildAt(i), type);
            if (userObject != null) {
                userObjects.add(userObject);
            }
        }
        return userObjects;
    }

    /**
     * Gets the user object of the given node for a given type.
     * @param <T> - the type of user object to get.
     * @param node - the node of the user object to get.
     * @param type - the type of user object to get.
     * @return  a user object of the given type.
     */
    public <T> T getUserObjectFromNode(DefaultMutableTreeNode node, Class<T> type) {
        Object object = node.getUserObject();
        if (type.isInstance(node.getUserObject())) {
            return type.cast(object);
        } else {
            return null;
        }
    }

    public DefaultMutableTreeNode getSelectedNode() {
        TreePath path = this.getSelectionPath();
        if (path != null) {
            return (DefaultMutableTreeNode) path.getLastPathComponent();
        } else {
            return null;
        }
    }

    public void addListener(AbstractTreeListener listener) {
        listeners.add(listener);
    }

    public void updateListeners(DescriptionNode node) {
        for(AbstractTreeListener listener : listeners) {
            listener.treeUpdate(node);
        }
    }
}
