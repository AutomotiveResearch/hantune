/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import components.CustomButton;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;

/**
 * Subpanel at bottom of side panel
 * @author Michiel
 */
@SuppressWarnings("serial")
public class SubPanel extends Box {

    private CustomButton resizeButton;
    private Dimension oldSize;
    private String title = "";

    public SubPanel() {
        super(BoxLayout.Y_AXIS);
    }

    public SubPanel(String title) {
        super(BoxLayout.Y_AXIS);
        this.title = title;
        Box header = createSubPanelHeader(title);
        add(header);
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent event) {
                SubPanel subPanel = (SubPanel) event.getSource();
                if(subPanel.isMinimized()) {
                    resizeButton.setImage("/images/sidepanel/maximize_vertical2.png");
                    resizeButton.setMouseOverImage("/images/sidepanel/maximize_vertical_hover2.png");
                } else {
                    resizeButton.setImage("/images/sidepanel/minimize_vertical2.png");
                    resizeButton.setMouseOverImage("/images/sidepanel/minimize_vertical_hover2.png");
                }
            }
        });
    }

    private Box createSubPanelHeader(String title) {
        Box header = SidePanel.createSidePanelHeader(title);
        resizeButton = new CustomButton("/images/sidepanel/minimize_vertical2.png");
        resizeButton.setMouseOverImage("/images/sidepanel/minimize_vertical_hover2.png");
        header.add(resizeButton);
        return header;
    }

    public void addSearchField(Searchable searchable, String hint) {
        this.add(SidePanel.createSearchField(searchable, hint));
    }

    public CustomButton getResizeButton() {
        return resizeButton;
    }

    public Dimension getMaximizedSize() {
        if (oldSize == null) return this.getSize();
        return oldSize;
    }

    public void setMaximizedSize(Dimension oldSize) {
        this.oldSize = oldSize;
    }

    public void minimize() {
        this.setSize(this.getWidth(), this.getMinimumSize().height);
        this.setPreferredSize(this.getSize());
    }

    public void maximize() {
        this.setSize(oldSize);
        this.setPreferredSize(this.getSize());
    }

    public boolean isMinimized() {
        if (this.getHeight() == this.getMinimumSize().height) {
            return true;
        }
        return false;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

     public boolean manuallyValidate() {
        int totalComponentHeight = 0;
        for (Component component : this.getComponents()) {
            totalComponentHeight += component.getHeight();
        }
        return this.getHeight() - (this.getInsets().top + this.getInsets().bottom) == totalComponentHeight;
    }

    public void attemptToCorrectSize() {
        for (Component component : this.getComponents()) {
            if (component instanceof JScrollPane) {
                component.setSize(new Dimension(component.getWidth(), component.getHeight() + 1));
                component.setPreferredSize(component.getSize());
                component.validate();
                this.validate();
            }
        }
    }
}
