/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel.asap2;

import ASAP2.ASAP2Measurement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Michiel
 */
@SuppressWarnings("serial")
public class DaqListTableModel extends AbstractTableModel {

    private final List<Object[]> data = new ArrayList<>();
    private final List<Integer> disabledColumns = new ArrayList<>();
    private List<Object> columnNames = new ArrayList<>();

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames.get(column).toString();
    }

    public void setColumnNames(List<Object> columnNames) {
        this.columnNames = columnNames;
    }

    public void enableColumn(int column) {
        if (disabledColumns.contains(column)) {
            disabledColumns.remove(column);
        }
    }

    public void disableColumn(int column) {
        if (!disabledColumns.contains(column)) {
            disabledColumns.add(column);
        }
    }

    public boolean isColumnEnabled(int column) {
        return !disabledColumns.contains(column);
    }

    @Override
    public Object getValueAt(int row, int col) {
        return data.get(row)[col];
    }

    @Override
    public Class<?> getColumnClass(int column) {
        return column == 0 ? ASAP2Measurement.class : Boolean.class;
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        data.get(row)[column] = value;
        fireTableCellUpdated(row, column);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public void addRow(Object[] row) {
        data.add(row);
    }

    public Object getColumnObject(int column) {
        return columnNames.get(column);
    }

    public void clear() {
        data.clear();
    }
}
