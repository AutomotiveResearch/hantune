/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel.asap2;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import DAQList.DAQList;
import DAQList.DaqListListener;
import HANtune.HANtune;
import HANtune.HANtuneManager;
import components.IntegerEditor;
import datahandling.CurrentConfig;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class DaqListInfoTable extends JTable implements DaqListListener {

    DefaultTableModel tableModel;
    private static final int ID_COLUMN = 0;
    private static final int NAME_COLUMN = 1;
    private static final int PRESCALER_COLUMN = 2;
    private static final int FREQUENCY_COLUMN = 3;
    private static final int ACTIVE_COLUMN = 4;

    public DaqListInfoTable() {

        createTable();
        addPopupMenu();
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                int row = convertRowIndexToModel(rowAtPoint(event.getPoint()));
                int column = columnAtPoint(event.getPoint());
                if (column == ACTIVE_COLUMN) {
                    boolean value = !(boolean) tableModel.getValueAt(row, column);
                    tableModel.setValueAt(value, row, column);
                }
            }
        });

    }

    private void createTable() {
        tableModel = new DefaultTableModel();
        String[] columns = {"ID", "Name", "Prescaler", "Frequency", "Active"};
        tableModel.setColumnIdentifiers(columns);
        this.setModel(tableModel);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        this.getColumnModel().getColumn(ID_COLUMN).setPreferredWidth(30);
        this.getColumnModel().getColumn(PRESCALER_COLUMN).setCellRenderer(this.getDefaultRenderer(Integer.class));
        this.getColumnModel().getColumn(PRESCALER_COLUMN).setCellEditor(new IntegerEditor(1, 255));
        this.getColumnModel().getColumn(FREQUENCY_COLUMN).setCellRenderer(rightRenderer);
        this.getColumnModel().getColumn(ACTIVE_COLUMN).setCellRenderer(this.getDefaultRenderer(Boolean.class));
        this.getColumnModel().getColumn(ACTIVE_COLUMN).setCellEditor(this.getDefaultEditor(Boolean.class));

        tableModel.addTableModelListener(e -> tableChangedAt(e.getFirstRow(), e.getColumn(), e.getType()));
    }

    private void addPopupMenu() {
        JPopupMenu menu = new JPopupMenu();
        JMenuItem addMenuItem = new JMenuItem("Add new DAQ list");
        addMenuItem.addActionListener(e -> CurrentConfig.getInstance().getHANtuneManager().newDaqlist("", false));
        menu.add(addMenuItem);

        JMenuItem removeMenuItem = new JMenuItem("Remove selected DAQ lists");
        removeMenuItem.addActionListener(e -> removeSelectedDaqLists());
        menu.add(removeMenuItem);
        this.setComponentPopupMenu(menu);
    }

    private void removeSelectedDaqLists() {
        HANtune hantune = HANtune.getInstance();
        HANtuneManager hantuneManager = CurrentConfig.getInstance().getHANtuneManager();
        int[] selectedRows = this.getSelectedRows();
        for (int i = selectedRows.length - 1; i >= 0; i--) {
            int row = convertRowIndexToModel(selectedRows[i]);
            if (row != 0) {
                DAQList daqList = (DAQList) tableModel.getValueAt(row, 0);
                int id = hantune.daqLists.indexOf(daqList);
                hantuneManager.removeDaqlist(id);
            }
        }
    }

    private void tableChangedAt(int row, int column, int type) {
        if (type == TableModelEvent.UPDATE) {
            HANtune hantune = HANtune.getInstance();
            HANtuneManager hantuneManager = CurrentConfig.getInstance().getHANtuneManager();

            DAQList daqList = (DAQList) tableModel.getValueAt(row, 0);
            int id = hantune.daqLists.indexOf(daqList);

            if (column == NAME_COLUMN) {
                String value = (String) tableModel.getValueAt(row, column);
                hantuneManager.renameDaqlist(id, value);
            } else if (column == PRESCALER_COLUMN) {
                int value = (int) tableModel.getValueAt(row, column);
                hantune.changePrescaler(id, (char) value, true);
            } else if (column == ACTIVE_COLUMN) {
                if ((boolean) tableModel.getValueAt(row, column)) {
                    hantuneManager.loadDaqlist(id, true);
                    if (!hantune.daqLists.get(id).isActive()) {
                        tableModel.setValueAt(false, row, column);
                    }
                } else {
                    hantuneManager.unloadDaqlist(id, true, true);
                }
            }
        }
        CurrentConfig.getInstance().getHANtuneManager().updateProjectDataTab();
    }

    private void fillTable() {
        HANtune hantune = HANtune.getInstance();
        for (DAQList daqList : hantune.daqLists) {
            Object[] row = {daqList, daqList.getName(), (int) daqList.getPrescaler(), daqList.getSampleFrequency(), daqList.isActive()};
            tableModel.addRow(row);
        }
    }

    @Override
    public void daqListUpdate() {
        tableModel.setRowCount(0);
        fillTable();
        this.setAutoCreateRowSorter(true);
    }

    @Override
    public boolean isCellEditable(int viewRow, int viewColumn) {
        int row = convertRowIndexToModel(viewRow);
        int column = convertColumnIndexToModel(viewColumn);
        return (column == NAME_COLUMN || column == PRESCALER_COLUMN) && !(column == NAME_COLUMN && row == 0);
    }
}
