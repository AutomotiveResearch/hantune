/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import datahandling.Parameter;
import datahandling.Signal;
import datahandling.Table;
import java.awt.datatransfer.DataFlavor;
import java.util.EnumSet;
import nl.han.hantune.gui.sidepanel.tree.TransferableTreeNode;

/**
 * A simple tree node that extends DefaultMutableTreeNode. Used by trees that
 * display the contents (e.g. signals and parameters) of a description file
 * (i.e. ASAP2 and DBC). This allows to set a type to DefaultMutableTreeNode to
 * ease later handling of this node and its UserObject.
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class DescriptionNode extends TransferableTreeNode {

    public enum Type {
        VISIBLE_ROOT, SIGNAL, PARAMETER, SIGNAL_PARENT, PARAMETER_PARENT, TABLE, AXIS_POINT
    }

    private final EnumSet<Type> types = EnumSet.noneOf(Type.class);
    private boolean enabled = true; // field has been added to allow for disabled CANmessages and CANsignals
                                    // when DBC message length (DLC) is too long. Can be removed when
                                    // long DBC messages can be handled properly

    public DescriptionNode() {
        super();
    }

    public DescriptionNode(Object userObject) {
        super(userObject);
    }

    public EnumSet<Type> getTypes() {
        return types;
    }

    public void setType(Type type) {
        types.add(type);
    }

    public <T> void setType(Class<T> type) {
        if (Signal.class.isAssignableFrom(type)) {
            types.add(Type.SIGNAL);
        }
        if (Parameter.class.isAssignableFrom(type)) {
            types.add(Type.PARAMETER);
        }
        if (Table.class.isAssignableFrom(type)) {
            types.add(Type.TABLE);
        }
    }

    public boolean isType(Type type) {
        return types.contains(type);
    }


    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }


    public boolean isEnabled() {
        return enabled;
    }


    @Override
    public DataFlavor[] getDataFlavors() {
        if (isType(Type.SIGNAL_PARENT)) {
            return new DataFlavor[]{Signal.DATA_FLAVOR};
        } else if (isType(Type.PARAMETER_PARENT)) {
            return new DataFlavor[]{Parameter.DATA_FLAVOR};
        } else {
            return super.getDataFlavors();
        }
    }

}
