/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.TransferHandler;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import nl.han.hantune.gui.sidepanel.tree.TransferableTreeNode;

/**
 * Transfers user objects from the nodes in a tree to a transfer handler that
 * can import the corresponding flavor.
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class TreeTransferHandler extends TransferHandler {

    private final AbstractTree tree;
    private Point mousePosition;

    public TreeTransferHandler(AbstractTree tree) {
        this.tree = tree;
        tree.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                mousePosition = tree.getMousePosition();
            }
        });
    }

    @Override
    protected Transferable createTransferable(JComponent c) {
        Transferable transferable = new Transferable() {

            @Override
            public DataFlavor[] getTransferDataFlavors() {
                return getSupportedFlavors();
            }

            @Override
            public boolean isDataFlavorSupported(DataFlavor flavor) {
                for (DataFlavor supportedFlavor : getSupportedFlavors()) {
                    if (supportedFlavor == flavor) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public Object getTransferData(DataFlavor flavor) {
                return getUserObjects(flavor);
            }

        };
        return transferable;
    }

    /**
     * Gets all user objects of the given data flavors from the current
     * selection in a tree.
     *
     * @return a list of user objects for the given flavors
     */
    private List<?> getUserObjects(DataFlavor... flavors) {
        List<Object> userObjects = new ArrayList<>();
        TreePath[] paths = tree.getSelectionPaths();
        if (paths != null) {
            for (TreePath path : paths) {
                Object selectedNode = path.getLastPathComponent();
                if (selectedNode instanceof TransferableTreeNode) {
                    getUserObjectsOfFlavor((TransferableTreeNode) selectedNode, userObjects, flavors);
                }
            }
        }
        return userObjects;
    }

    /**
     * Gets all user objects of the given data flavors from the given node and
     * its children.
     *
     * @return a list of user objects for the given flavors
     */
    private List<?> getUserObjectsOfFlavor(TransferableTreeNode node, List<Object> objects, DataFlavor... flavors) {
        if (node.isLeaf() && node.isDragEnabled()) {
            Object userObject = node.getUserObject();
            if (userObject != null && node.hasFlavor(flavors)) {
                objects.add(userObject);
            }
        } else {
            for (int i = 0; i < node.getChildCount(); i++) {
                TreeNode childNode = node.getChildAt(i);
                if (childNode instanceof TransferableTreeNode) {
                    getUserObjectsOfFlavor((TransferableTreeNode) childNode, objects, flavors);
                }
            }
        }
        return objects;
    }

    /**
     * Gets the flavors this transfer action supports. Some nodes have multiple
     * types and can therefor have multiple flavors.
     *
     * @return the flavors this transfer action supports.
     */
    private DataFlavor[] getSupportedFlavors() {
        TreeNode node = tree.getSelectedNode();
        if (node instanceof TransferableTreeNode && ((TransferableTreeNode)node).isDragEnabled()) {
            return ((TransferableTreeNode) node).getDataFlavors();
        }
        return new DataFlavor[]{};
    }

    /**
     * Gets the type of action the transferring entails (i.e. COPY or MOVE).
     * Also sets the drag image and its offset.
     *
     * @param component - the component from where data is transfered. In this
     * case a tree.
     * @return a "MOVE" action.
     */
    @Override
    public int getSourceActions(JComponent component) {
        setDragImageAndOffset();
        return MOVE;
    }

    /**
     * Sets the drag image and offset when dragging a node. The image is a copy
     * of the currently selected node in a tree.
     */
    private void setDragImageAndOffset() {
        if (mousePosition != null) {
            TreePath path = tree.getPathForLocation(mousePosition.x, mousePosition.y);
            if (path != null) {
                Object draggedNode = path.getLastPathComponent();
                boolean isExpanded = tree.isExpanded(path);
                boolean isLeaf = tree.getModel().isLeaf(path.getLastPathComponent());
                JLabel label = (JLabel) tree.getCellRenderer().getTreeCellRendererComponent(tree, draggedNode, false, isExpanded, isLeaf, 0, false);
                int selectionCount = getUserObjects(getSupportedFlavors()).size();
                if (selectionCount > 1 && isLeaf) {
                    label.setText(label.getText() + " (+" + (selectionCount - 1) + ")");
                }
                label.setSize(new Dimension(label.getPreferredSize().width, label.getPreferredSize().height));
                BufferedImage image = getGhostImageOfComponent(label);
                setDragImage(image);
                Rectangle pathBounds = tree.getPathBounds(path);
                setDragImageOffset(new Point(mousePosition.x - pathBounds.x, mousePosition.y - pathBounds.y));
            }
        }
    }

    /**
     * Creates a semi-transparent image of the given component.
     *
     * @param component - the component of which a ghost image must me made.
     * @return a semi-transparent (50%) image.
     */
    private BufferedImage getGhostImageOfComponent(JComponent component) {
        BufferedImage image = new BufferedImage(component.getWidth(), component.getHeight(), BufferedImage.TYPE_INT_ARGB_PRE);
        Graphics2D graphics = image.createGraphics();
        graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
        component.setOpaque(false);
        component.paint(graphics);
        graphics.dispose();
        return image;
    }
}
