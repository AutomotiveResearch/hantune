/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class OpaqueTitledBorder extends TitledBorder {

    private final JLabel label = new JLabel();
    private final Component target;

    public OpaqueTitledBorder(Border border, String title, Component target) {
        super(border, title);
        this.target = target;
    }

    public OpaqueTitledBorder(Border border, String title) {
        this(border, title, null);
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        Border baseBorder = getBorder();
        String borderTitle = " " + getTitle();
        if ((borderTitle != null) && !borderTitle.isEmpty()) {
            int edge = (baseBorder instanceof TitledBorder) ? 0 : EDGE_SPACING;
            JLabel borderLabel = getLabel(c);
            Dimension size = borderLabel.getPreferredSize();
            Insets insets = getBorderInsets(baseBorder, c, new Insets(0, 0, 0, 0));

            int borderX = x + edge;
            int borderY = y + edge;
            int borderW = width - edge - edge;
            int borderH = height - edge - edge;

            int labelY = y;
            int labelH = size.height;
            insets.top = edge + insets.top / 2 - labelH / 2;
            if (insets.top < edge) {
                borderY -= insets.top;
                borderH += insets.top;
            } else {
                labelY += insets.top;
            }
            insets.left += edge + TEXT_INSET_H;
            insets.right += edge + TEXT_INSET_H;

            int labelX = x;
            int labelW = width - insets.left - insets.right;
            if (labelW > size.width) {
                labelW = size.width;
            }
            labelX += insets.left;
            if (target.isOpaque()) {
                g.setColor(target.getBackground());
                g.fillRect(borderX+1, borderY+1, borderW-2, borderY + labelH / 2);
            }
            if (baseBorder != null) {
                Graphics g2 = g.create();
                baseBorder.paintBorder(c, g2, borderX, borderY, borderW, borderH);
                g2.dispose();
            }

            g.translate(labelX, labelY);
            borderLabel.setSize(labelW, labelH);
            borderLabel.paint(g);
            g.translate(-labelX, -labelY);
        } else if (baseBorder != null) {
            baseBorder.paintBorder(c, g, x, y, width, height);
        }
    }

    private static Insets getBorderInsets(Border border, Component c, Insets insets) {
        if (border == null) {
            insets.set(0, 0, 0, 0);
        } else if (border instanceof AbstractBorder) {
            AbstractBorder ab = (AbstractBorder) border;
            insets = ab.getBorderInsets(c, insets);
        } else {
            Insets i = border.getBorderInsets(c);
            insets.set(i.top, i.left, i.bottom, i.right);
        }
        return insets;
    }

    private JLabel getLabel(Component c) {
        this.label.setBackground(Color.WHITE);
        this.label.setOpaque(true);
        this.label.setText(" " + getTitle());
        this.label.setFont(getFont(c));
        this.label.setForeground(Color.BLACK);
        this.label.setComponentOrientation(c.getComponentOrientation());
        this.label.setEnabled(c.isEnabled());
        return this.label;
    }
}
