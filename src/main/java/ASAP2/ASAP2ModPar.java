/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

/**
 * Contains information about a MOD_PAR
 */
public class ASAP2ModPar {

    private static final long EMPTY_RAMFLASH_OFFSET = Long.MAX_VALUE;
    private static final long EMPTY_ADDRES_EPK = Long.MAX_VALUE;

    // asap example
    //  /begin MOD_PAR "MOD PAR Comment Goes Here"
    //      ADDR_EPK 0x0801e004
    //      CPU_TYPE "STM32F103"
    //      ECU "Olimexino"
    //      EPK "HANcoder_Olimexino_softwareID_v1_161116_133409"
    //      SYSTEM_CONSTANT "XcpStationID string length" "0x0000002e"
    //      SYSTEM_CONSTANT "RAM-FlashOffset" "-402612064"
    //      SYSTEM_CONSTANT "CRC32-EndAddress" "0x80055f3f"
    //      SYSTEM_CONSTANT "CRC32-StartAddress" "0x80055f3f"
    //      SYSTEM_CONSTANT "CRC32-PatchAddress" "0x80055f3f"
    //      SYSTEM_CONSTANT "CRC32-PcodeStartAddress" "0x80055f3f"
    //      SYSTEM_CONSTANT "CRC32-PcodeEndAddress" "0x80055f3f"
    //  /end MOD_PAR
    // contains offset of ram address to flash address (to be used when data is flashed)
    private long ramFlashOffset = EMPTY_RAMFLASH_OFFSET; // converted from: SYSTEM_CONSTANT
    // "RAM-FlashOffset" "-402612064"

    private String description = null;
    private long addrEpk = EMPTY_ADDRES_EPK; // start address where epk string is stored in target memory
    private String cpuType = null; // String that identifies the CPU.
    private String ecu = null; // String that describes the control unit.
    private String epk = null; // String that describes the EPROM.
    private ASAP2Crc32ModPar crc32Addresses = null;

    public long getAddrEpk() {
        return addrEpk;
    }

    public boolean isAddrEpkPresent() {
        return (addrEpk != EMPTY_ADDRES_EPK);
    }

    public void setAddrEpk(long addrEpk) {
        this.addrEpk = addrEpk;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCpuType() {
        return cpuType;
    }

    public boolean isCpuTypePresent() {
        return (cpuType != null);
    }

    public void setCpuType(String cpuType) {
        this.cpuType = cpuType;
    }

    public String getEcu() {
        return ecu;
    }

    public boolean isEcuPresent() {
        return (ecu != null);
    }

    public void setEcu(String ecu) {
        this.ecu = ecu;
    }

    // prefer to use ASAP2Data.getEpromID()
    protected String getEpk() {
        return epk;
    }

    public boolean isEpkPresent() {
        return (epk != null) && (epk.length() > 0);
    }

    public void setEpk(String epk) {
        this.epk = epk;
    }

    public long getRamFlashOffset() {
        return ramFlashOffset;
    }

    public void setRamFlashOffset(long ramFlashOffset) {
        this.ramFlashOffset = ramFlashOffset;
    }

    public boolean isRamFlashOffsetPresent() {
        return (ramFlashOffset != EMPTY_RAMFLASH_OFFSET);
    }

    public ASAP2Crc32ModPar getCrc32Addresses() {
        return crc32Addresses;
    }

    public void setCrc32Addresses(ASAP2Crc32ModPar cr32Addresses) {
        this.crc32Addresses = cr32Addresses;
    }

    public boolean isCrc32AdressesPresent() {
        return crc32Addresses != null;
    }
}
