/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import ASAP2.ASAP2Data.Data;
import datahandling.ReferenceStateListener;
import datahandling.Signal;
import datahandling.SignalListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains information about a MEASUREMENT.
 *
 * @author Aart-Jan
 */
public class ASAP2Measurement extends ASAP2Object implements Signal {

    private int resolution = 0;
    private float accuracy = 0;
    private double value;
    private long timeStamp;
    private final List<SignalListener> listeners = new ArrayList<>();
    private final List<ReferenceStateListener> stateListeners = new ArrayList<>();
    private boolean active = true;
    private boolean allowed = true; // can be communicated with target (doubles for example are NOT allowed on XCP_CAN)

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    @Override
    public void setValueAndTime(double value, long timestamp) {
        this.value = value;
        this.timeStamp = timestamp;
        updateListeners();
    }

    @Override
    public void addListener(SignalListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeListener(SignalListener listener) {
        listeners.remove(listener);
    }

    public void updateListeners() {
        for(SignalListener listener : listeners) {
            listener.valueUpdate(value, timeStamp);
        }
    }

    @Override
    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public double getRawValue() {
        return value / factor;
    }

    @Override
    public long getTimestamp() {
        return timeStamp;
    }


    /**
     * Only used by calibration management. Do not use this method. Use
     * getValue() instead.
     * @return
     * @deprecated
     */
    @Deprecated
    public Data getData() {
        return new Data(timeStamp, value);
    }

    @Override
    public boolean isActive() {
        return active && allowed;
    }

    public void setActive(boolean active) {
        this.active = active;
        updateStateListeners();
    }

    public boolean isAllowed() {
        return allowed;
    }

    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
        updateStateListeners();
    }

    @Override
    public void addStateListener(ReferenceStateListener listener) {
        if (!stateListeners.contains(listener)) {
            stateListeners.add(listener);
        }
    }

    @Override
    public void removeStateListener(ReferenceStateListener listener) {
        stateListeners.remove(listener);
    }

    public void updateStateListeners() {
        for (ReferenceStateListener listener : stateListeners) {
            listener.stateUpdate();
        }
    }
}
