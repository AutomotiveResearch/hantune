/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import java.nio.ByteBuffer;

import util.Util;

public class ASAP2Object {
    public static final double DEFAULT_CONVERSION_FACTOR = 1.0;
    public static final String DEFAULT_SOURCE = "ASAP2";

    /**
     * Returns the size of one individual element, in number of bytes.
     *
     * @return
     */
    public char getDatatypeSize() {
        return (char)datatype.dataTypeSize;
    }


    public enum ASAP2ObjType {

//        FOLDER, ASCII,
        SIGNAL,
        CHARACTERISTIC,
        COMPU_METHOD
    };

    private ASAP2ObjType asap2ObjType = null;
    private String name = "";
    private String description = "";
    private int address = 0;
    protected ASAP2Datatype datatype = ASAP2Datatype.UWORD;
    protected double factor = 1;
    protected int decimals = 0;
    private String decFormat = "0";
    protected String conversion = "";
    protected double lowerLimit = 0;
    protected double upperLimit = 0;
    protected boolean lowerLimitEnabled = false;
    protected boolean upperLimitEnabled = false;
    protected boolean limitEnabled = false;
    private String unit = "";
    private String source = "";


    /**
     * Returns the type of the element
     *
     * @return
     */
    public ASAP2ObjType getASAP2ObjType() {
        return asap2ObjType;
    }


    /**
     * Sets the type of the element.
     *
     * @param type
     */
    public void setASAP2ObjType(ASAP2ObjType type) {
        this.asap2ObjType = type;
    }


    /**
     * Returns the name of the conversion method
     *
     * @return
     */
    public String getConversion() {
        return conversion;
    }


    /**
     * Sets the name of the conversion method
     *
     * @param conversion
     */
    public void setConversion(String conversion) {
        this.conversion = conversion;
    }


    /**
     * Converts a float value to a generic value object with the correct datatype.
     *
     * @param value
     * @return
     */
    public Object convertValue(double value) {
        Class<? extends Number> dataclass = getDataclass();
        Object val = value;

        // Byte
        if (dataclass == Byte.class) {
            val = Byte.valueOf((byte)value);
        }
        // Short
        if (dataclass == Short.class) {
            val = Short.valueOf((short)value);
        }
        // Integer
        if (dataclass == Integer.class) {
            val = Integer.valueOf((int)value);
        }
        // Long
        if (dataclass == Long.class) {
            val = Long.valueOf((long)value);
        }
        // Float
        if (dataclass == Float.class) {
            val = Float.valueOf((float)value);
        }
        // Double
        if (dataclass == Double.class) {
            val = Double.valueOf(value);
        }

        return val;
    }


    /**
     * Converts a string value to the correct number of bytes[]
     *
     * @param value
     * @return
     */
    public byte[] convertValue(String valStr) {
        byte[] bytes = new byte[getDatatypeSize()];

        switch (datatype) {
            case BOOLEAN:
            case SBYTE:
                bytes[0] = (byte)(Byte.parseByte(valStr) / factor);
                break;

            case UBYTE:
                bytes[0] = (byte)(Short.parseShort(valStr) / factor);
                break;

            case SWORD:
                ByteBuffer.wrap(bytes).putShort((short)(Short.parseShort(valStr) / factor));
                break;

            case UWORD:
                ByteBuffer.wrap(bytes).putShort((short)(Integer.parseInt(valStr) / factor));
                break;

            case SLONG:
                ByteBuffer.wrap(bytes).putInt((int)(Integer.parseInt(valStr) / factor));
                break;

            case ULONG:
                ByteBuffer.wrap(bytes).putInt((int)(Integer.parseUnsignedInt(valStr) / factor));
                break;

            case A_INT64:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    ByteBuffer.wrap(bytes).putLong(Long.parseLong(valStr));
                } else {
                    // this conversion will give inaccuracy regardless of the value of factor
                    ByteBuffer.wrap(bytes).putLong((long)(Long.parseLong(valStr) / factor));
                }
                break;

            case A_UINT64:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    ByteBuffer.wrap(bytes).putLong(Long.parseUnsignedLong(valStr));
                } else {
                    // this conversion will give inaccuracy regardless of the value of factor
                    ByteBuffer.wrap(bytes).putLong((long)(Long.parseUnsignedLong(valStr) / factor));
                }
                break;

            case FLOAT32_IEEE:
                ByteBuffer.wrap(bytes).putFloat((float)(Float.parseFloat(valStr) / factor));
                break;

            case FLOAT64_IEEE:
                ByteBuffer.wrap(bytes).putDouble(Double.parseDouble(valStr) / factor);
                break;

            // NO default here. java will give a warning when a new enum has been added.
        }
        return bytes;
    }


    /**
     * Converts a generic value object to an array of characters (databytes) and performs the related data
     * formatting.
     *
     * @param value
     * @return
     */
    public char[] convertValue(Object value) {
        char[] data = null;

        if (factor != DEFAULT_CONVERSION_FACTOR) {
            value = (Util.getDoubleValue(value) / factor);
        } else {
            // when value gets VERY big, (around 2^63), using doubleValue() will give inaccuracy
            // (about 107). Therefore use longValue() on converting Long datatype
            if (value instanceof Long) {
                value = ((Number)value).longValue();
            } else {
                value = ((Number)value).doubleValue();
            }
        }


        switch (datatype) {
            case BOOLEAN:
            case UBYTE: {
                data = new char[1];
                short val = ((Double)value).shortValue();
                data[0] = (char)(val & 0xFF);
                break;
            }
            case SBYTE: {
                data = new char[1];
                byte val = ((Double)value).byteValue();
                data[0] = (char)(val & 0xFF);
                break;
            }
            case UWORD: {
                data = new char[2];
                int val = ((Double)value).intValue();
                data[1] = (char)(val & 0xFF);
                data[0] = (char)((val & 0xFF00) >> 8);
                break;
            }
            case SWORD: {
                data = new char[2];
                short val = ((Double)value).shortValue();
                data[1] = (char)(val & 0xFF);
                data[0] = (char)((val & 0xFF00) >> 8);
                break;
            }
            case ULONG: {
                data = new char[4];
                long val = ((Double)value).longValue();
                data[3] = (char)(val & 0xFF);
                data[2] = (char)((val & 0xFF00) >> 8);
                data[1] = (char)((val & 0xFF0000) >> 16);
                data[0] = (char)((val & 0xFF000000) >> 24);
                break;
            }
            case SLONG: {
                data = new char[4];
                int val = ((Double)value).intValue();
                data[3] = (char)(val & 0xFF);
                data[2] = (char)((val & 0xFF00) >> 8);
                data[1] = (char)((val & 0xFF0000) >> 16);
                data[0] = (char)((val & 0xFF000000) >> 24);
                break;
            }
            case FLOAT32_IEEE: {
                data = new char[4];
                float val_f = ((Double)value).floatValue();
                int val = Float.floatToIntBits(val_f);
                data[3] = (char)(val & 0xFF);
                data[2] = (char)((val & 0xFF00) >> 8);
                data[1] = (char)((val & 0xFF0000) >> 16);
                data[0] = (char)((val & 0xFF000000) >> 24);
                break;
            }
            case FLOAT64_IEEE: {
                data = new char[8];
                long val = Double.doubleToLongBits((Double)value);
                data[7] = (char)(val & 0xFF);
                data[6] = (char)((val & 0xFF00) >> 8);
                data[5] = (char)((val & 0xFF0000) >> 16);
                data[4] = (char)((val & 0xFF000000) >> 24);
                data[3] = (char)((val & 0xFF00000000L) >> 32);
                data[2] = (char)((val & 0xFF0000000000L) >> 40);
                data[1] = (char)((val & 0xFF000000000000L) >> 48);
                data[0] = (char)((val & 0xFF00000000000000L) >> 56);
                break;
            }
            case A_INT64: {
                data = new char[8];

                long val = ((Number)value).longValue();

                data[7] = (char)((Long)val & 0xFF);
                data[6] = (char)(((Long)val & 0xFF00) >> 8);
                data[5] = (char)(((Long)val & 0xFF0000) >> 16);
                data[4] = (char)(((Long)val & 0xFF000000) >> 24);
                data[3] = (char)(((Long)val & 0xFF00000000L) >> 32);
                data[2] = (char)(((Long)val & 0xFF0000000000L) >> 40);
                data[1] = (char)(((Long)val & 0xFF000000000000L) >> 48);
                data[0] = (char)(((Long)val & 0xFF00000000000000L) >> 56);
                break;
            }
            case A_UINT64: {
                data = new char[8];

                long val = ((Number)value).longValue();

                data[7] = (char)((Long)val & 0xFF);
                data[6] = (char)(((Long)val & 0xFF00) >> 8);
                data[5] = (char)(((Long)val & 0xFF0000) >> 16);
                data[4] = (char)(((Long)val & 0xFF000000) >> 24);
                data[3] = (char)(((Long)val & 0xFF00000000L) >> 32);
                data[2] = (char)(((Long)val & 0xFF0000000000L) >> 40);
                data[1] = (char)(((Long)val & 0xFF000000000000L) >> 48);
                data[0] = (char)(((Long)val & 0xFF00000000000000L) >> 56);
                break;
            }

            default:
                throw new IllegalStateException("Unsupported ASAP2 datatype: " + datatype.toString());
        }

        return data;
    }


    /**
     * Converts an array of characters (databytes) to a generic value object with the correct datatype.
     *
     * @param d
     * @return
     */
    public double convertValue(char[] d) {
        long[] data = new long[d.length];
        for (int i = 0; i < d.length; i++) {
            data[i] = d[i] & 0xFF;
        }

        Object value = null;
        switch (datatype) {
            case BOOLEAN:
            case UBYTE:
                value = (short)data[0];
                break;
            case SBYTE:
                value = (byte)data[0];
                break;
            case UWORD:
                value = (int)((data[0] << 8 | data[1]) & 0x0000FFFF);
                break;
            case SWORD:
                value = (short)((data[0] << 8 | data[1]) & 0x0000FFFF);
                break;
            case ULONG:
                value = (data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3]) & 0xFFFFFFFF;
                break;
            case SLONG:
                value = (int)((data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3]) & 0xFFFFFFFF);
                break;
            case FLOAT32_IEEE:
                value = Float.intBitsToFloat(
                    (int)((data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3]) & 0xFFFFFFFF));
                break;
            case FLOAT64_IEEE:
                value =
                    Double.longBitsToDouble(((data[0] << 56 | data[1] << 48 | data[2] << 40 | data[3] << 32
                        | data[4] << 24 | data[5] << 16 | data[6] << 8 | data[7]) & 0xFFFFFFFFFFFFFFFFL));
                break;

            case A_INT64:
            case A_UINT64:
                // not supported
                break;
        }
        double doubleValue = Util.getDoubleValue(value);
        if (factor != 1) {
            return factor * doubleValue;
        }
        return doubleValue;
    }


    /**
     * Returns the name of the element.
     *
     * @return
     */
    public String getName() {
        return name;
    }


    /**
     * Sets the name of the element.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Returns the description of the element.
     *
     * @return
     */
    public String getDescription() {
        return description;
    }


    /**
     * Sets the description of the element.
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Returns the memory address of the element.
     *
     * @return
     */
    public int getAddress() {
        return address;
    }


    /**
     * Sets the memory address of the element at the ECU.
     *
     * @param address
     */
    public void setAddress(int address) {
        this.address = address;
    }


    /**
     * Returns the datatype of the element.
     *
     * @return
     */
    public ASAP2Datatype getDatatype() {
        return datatype;
    }


    /**
     * Sets the datatype of the element at the ECU.
     *
     * @param datatype
     */
    public void setDatatype(ASAP2Datatype datatype) {
        this.datatype = datatype;
        double min = 0;
        double max = 0;
        switch (datatype) {
            case BOOLEAN:
                min = 0;
                max = 1;
                break;
            case UBYTE:
                min = 0;
                max = 255;
                break;
            case SBYTE:
                min = -128;
                max = 127;
                break;
            case UWORD:
                min = 0;
                max = 65535;
                break;
            case SWORD:
                min = -32768;
                max = 32767;
                break;
            case ULONG:
                min = 0;
                max = 4.294967296E9;
                break;
            case SLONG:
                min = -2147483648;
                max = 2147483647;
                break;
            case A_UINT64:
                min = 0;
                max = 1.8446744073709551615E19;
                break;
            case A_INT64:
                min = -9.223372036854775808E18;
                max = 9.223372036854775807E18;
                break;
            case FLOAT32_IEEE:
                min = -Float.MAX_VALUE;
                max = Float.MAX_VALUE;
                break;
            case FLOAT64_IEEE:
                min = -Double.MAX_VALUE;
                max = Double.MAX_VALUE;
                break;
        } // end of switch
        if (lowerLimitEnabled != true || this.lowerLimit < min) {
            this.lowerLimit = min;
            lowerLimitEnabled = true;
            limitEnabled = true;
        }
        if (upperLimitEnabled != true || this.upperLimit > max) {
            this.upperLimit = max;
            upperLimitEnabled = true;
            limitEnabled = true;
        }
    }


    /**
     * Returns the datatype class for this element.
     *
     * @return
     */
    public Class<? extends Number> getDataclass() {
        return datatype.dataTypeClass;
    }


    /**
     * Returns the number of decimals, needed to represent a value of this element.
     *
     * @return
     */
    public int getDecimalCount() {
        return decimals;
    }


    /**
     * Sets the amount of decimals used in the displayed value
     *
     * @param decimals
     */
    public void setDecimals(int decimals) {
        this.decimals = decimals;
        String decFormat = decimals > 0 ? "0." : "0";
        for (int i = 0; i < decimals; i++) {
            decFormat += "0";
        }
        setDecFormat(decFormat);
    }


    /**
     * Returns the conversion factor between internal calib value from data and physical format.
     *
     * @return
     */
    public double getFactor() {
        return factor;
    }


    /**
     * Sets the conversion factor between value from data and displayed value. Also determines the related
     * number of decimals and printformat.
     *
     * @param factor
     */
    public void setFactor(double factor) {
        this.factor = factor;
        // decimals = Math.getExponent(1/factor);// 1 = 0, 2 = 1, 4 = 2, 8 = 3, 16 = 4 ... etc
        // if (decimals < 0) decimals = 0;
        // if (decimals > 0) {
        // setDecFormat("0.");
        // for(int i=0; i<decimals; i++){
        // setDecFormat(getDecFormat() + "0");
        // }
        // }
    }


    /**
     * Returns the lower value limit of this element.
     *
     * @return
     */
    public double getMinimum() {
        return lowerLimit;
    }


    /**
     * Sets the lower value limit of this element.
     *
     * @param lowerLimit
     */
    public void setMinimum(double lowerLimit) {
        this.lowerLimit = lowerLimit;
    }


    /**
     * Returns the upper value limit of this element.
     *
     * @return
     */
    public double getMaximum() {
        return upperLimit;
    }


    /**
     * Sets the upper value limit of this element.
     *
     * @param upperLimit
     */
    public void setMaximum(double upperLimit) {
        this.upperLimit = upperLimit;
    }


    /**
     * Returns whether this element uses a lower value limit.
     *
     * @return
     */
    public boolean isLowerLimitEnabled() {
        return lowerLimitEnabled;
    }


    /**
     * Sets whether this element uses a lower value limit.
     *
     * @param lowerLimitEnabled
     */
    public void setLowerLimitEnabled(boolean lowerLimitEnabled) {
        this.lowerLimitEnabled = lowerLimitEnabled;
        if (isLowerLimitEnabled() && isUpperLimitEnabled() && upperLimit > lowerLimit) {
            limitEnabled = true;
        }
    }


    /**
     * Returns whether this element uses an upper value limit.
     *
     * @return
     */
    public boolean isUpperLimitEnabled() {
        return upperLimitEnabled;
    }


    /**
     * Sets whether this element uses an upper value limit.
     *
     * @param upperLimitEnabled
     */
    public void setUpperLimitEnabled(boolean upperLimitEnabled) {
        this.upperLimitEnabled = upperLimitEnabled;
        if (isLowerLimitEnabled() && isUpperLimitEnabled() && upperLimit > lowerLimit) {
            limitEnabled = true;
        }
    }


    /**
     * Returns whether this element uses any value limiting.
     *
     * @return
     */
    public boolean isLimitEnabled() {
        return limitEnabled;
    }


    /**
     * Returns the printformat to be used to display a value of this element.
     *
     * @return
     */
    public String getDecFormat() {
        return decFormat;
    }


    /**
     * @param decFormat the decFormat to set
     */
    public void setDecFormat(String decFormat) {
        this.decFormat = decFormat;
    }


    public boolean hasDecimals() {
        return decimals > 0;
    }


    public String getUnit() {
        return unit;
    }


    public void setUnit(String unit) {
        this.unit = unit;
    }


    /**
     * Returns DEFAULT_SOURCE
     * Reasons for this are backwards compatability and to allow
     * viewers/editors not to be connected to a specific asap2 file. This
     * (coupled with getDescriptionFileByName(String name) in CurrentConfig)
     * could be changed to return the file name (this.source) but comes with
     * several pros and cons.
     * Pros:
     *  - Allows multiple asap2 files to be loaded at the same time.
     * Cons:
     *  - HANtune will not be able to open pre 1.2 projects any more.
     *  - The references in a window will be "tied" to a specific asap2.
     *
     * @return the source of this object
     */
    public String getSource() {
        return DEFAULT_SOURCE;
    }

    public String getProtocolName() {
        return "XCP";
    }

    public String getDataString(ByteBuffer buffer) {
        String value = null;
        switch (this.getDatatype()) {
            case FLOAT32_IEEE:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Float.toString(buffer.getFloat());
                } else {
                    value = Float.toString((float)(buffer.getFloat() * factor));
                }
                break;

            case FLOAT64_IEEE:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Double.toString(buffer.getDouble());
                } else {
                    value = Double.toString((buffer.getDouble() * factor));
                }
                break;

            case BOOLEAN:
            case SBYTE:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Byte.toString(buffer.get());
                } else {
                    value = Byte.toString((byte)(buffer.get() * factor));
                }
                break;

            case UBYTE:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Short.toString((short)(buffer.get() & 0xff));
                } else {
                    value = Short.toString((short)((buffer.get() & 0xff) * factor));
                }
                break;


            case UWORD:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Integer.toUnsignedString(Short.toUnsignedInt(buffer.getShort()));
                } else {
                    value =
                        Integer.toUnsignedString(Short.toUnsignedInt((short)(buffer.getShort() * factor)));
                }
                break;

            case SWORD:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Short.toString(buffer.getShort());
                } else {
                    value = Short.toString((short)(buffer.getShort() * factor));
                }
                break;

            case ULONG:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Integer.toUnsignedString(buffer.getInt());
                } else {
                    value = Integer.toUnsignedString((int)(buffer.getInt() * factor));
                }
                break;

            case SLONG:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Integer.toString(buffer.getInt());
                } else {
                    value = Integer.toString((int)(buffer.getInt() * factor));
                }
                break;

            case A_INT64:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Long.toString(buffer.getLong());
                } else {
                    value = Long.toString((long)(buffer.getLong() * factor));
                }
                break;

            case A_UINT64:
                if (factor == DEFAULT_CONVERSION_FACTOR) {
                    value = Long.toUnsignedString(buffer.getLong());
                } else {
                    value = Long.toUnsignedString((long)(buffer.getLong() * factor));
                }
                break;

            // default:
            // value = Integer.toString(buffer.getInt());
            // break;
        }
        return value;
    }


    public String getBitString(double value) {
        String bitString;
        switch (datatype) {
            case FLOAT32_IEEE:
                bitString = Long.toBinaryString(Float.floatToRawIntBits((float)value));
                break;
            case FLOAT64_IEEE:
                bitString = Long.toBinaryString(Double.doubleToRawLongBits(value));
                break;
            default:
                bitString = Long.toBinaryString((long)value);
                break;
        }
        bitString = value < 0 ? trimLeadingBits(bitString) : addLeadingBits(bitString);
        return Util.divideWithWhitespace(bitString, 4);
    }


    public String getHexString(double value) {
        String hexString;
        switch (datatype) {
            case FLOAT32_IEEE:
                hexString = Integer.toHexString(Float.floatToRawIntBits((float)value));
                break;
            case FLOAT64_IEEE:
                hexString = Long.toHexString(Double.doubleToRawLongBits(value));
                break;
            default:
                hexString = Long.toHexString((long)value);
                break;
        }
        hexString = value < 0 ? trimLeadingBytes(hexString) : addLeadingBytes(hexString);
        return Util.divideWithWhitespace(hexString, 2);
    }


    private String trimLeadingBits(String bits) {
        return Util.trimLeadingCharacters(bits, getDatatypeSize() * 8);
    }


    private String addLeadingBits(String bits) {
        return Util.addLeadingCharacters(bits, '0', getDatatypeSize() * 8);
    }


    private String trimLeadingBytes(String bytes) {
        return Util.trimLeadingCharacters(bytes, getDatatypeSize() * 2);
    }


    private String addLeadingBytes(String bytes) {
        return Util.addLeadingCharacters(bytes, '0', getDatatypeSize() * 2);
    }


    @Override
    public String toString() {
        return this.getName();
    }
}
