/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import java.util.Arrays;

/**
 * Contains information about a COMPU_METHOD.
 *
 * @author Aart-Jan
 */
public class ASAP2CompuMethod extends ASAP2Object {
    private String conversionType = "IDENTICAL";
    private String unit = "";
    private float[] coeffs = new float[6];

    public String getConversionType() {
        return conversionType;
    }

    public void setConversionType(String conversionType) {
        String[] ConversionTypes = {"IDENTICAL", "FORM", "LINEAR", "RAT_FUNC", "TAB_INTP", "TAB_NOINTP", "TAB_VERB"};
        boolean found = false;
        for(int i=0; i<ConversionTypes.length; i++){
            if(ConversionTypes[i].equals(conversionType)){
                found = true;
                break;
            }
        }
        if(found) this.conversionType = conversionType;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float[] getCoeffs() {
        return coeffs;
    }

    public void setCoeffs(float[] coeffs) {
        if(coeffs.length == 6){
            this.coeffs = Arrays.copyOf(coeffs, coeffs.length);
            setFactor(coeffs[5] / coeffs[1]);
        }
    }
}
