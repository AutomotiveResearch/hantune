/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ASAP2.ASAP2Characteristic.Type;
import ASAP2.ASAP2Object.ASAP2ObjType;
//import ASAP2.Datatype;
import ErrorLogger.AppendToLogfile;
import XCP.XCPSettings;
import datahandling.CurrentConfig;
import util.MessagePane;

/**
 * ASAP2 import parser. This class is used to parse the content of an A2L /
 * ASAP2 file into objects that can be used inside a Java environment. This
 * parser is based on ASAM MCD-2MC version 1.6.
 *
 * @author Aart-Jan
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class ASAP2Import {

    private static final int MAX_ASAP2_STRUCTURAL_LEVEL = 8;
    private static final int MAX_LINES_DISPLAYED = 7;
    // error message
    private String error = "";

    private class UnsupportedType {

        private String objTypeName;
        private String unsupportedTypeName;
        private String parameterName;

        UnsupportedType(String type, String unsupType, String paramName) {
            this.objTypeName = type;
            this.unsupportedTypeName = unsupType;
            this.parameterName = paramName;
        }

        public String getObjTypeName() {
            return objTypeName;
        }

        public String getUnsupportedTypeName() {
            return unsupportedTypeName;
        }

        public String getParameterName() {
            return parameterName;
        }
    }
    private List<UnsupportedType> unsupportedTypes = new ArrayList<>();

    // ASAP2File contains the double datatype (64 bits)
    private boolean containsDoubles = false;
    private ArrayList<String> doubleNames = new ArrayList<>();

    // array for the splitted file parts
    protected ArrayList<String>[] part = new ArrayList[MAX_ASAP2_STRUCTURAL_LEVEL];
    protected ArrayList<Integer>[] part_parent = new ArrayList[MAX_ASAP2_STRUCTURAL_LEVEL];
    protected HashMap<Integer, String> part_parent_name = new HashMap<>();

    // asap2 main data array
    protected ASAP2Data data = new ASAP2Data();

    // treemap arrays for the various asap2 elements
    private Map<String, Asap2Function> unstructuredFunctions = new HashMap<>();
    // private TreeMap<String, Integer> sf_map = new TreeMap<>();
    protected TreeMap<String, ASAP2RecordLayout> record_layout = new TreeMap<>();
    protected TreeMap<String, ASAP2Characteristic> characteristic = new TreeMap<>();
    protected List<ASAP2Characteristic> characteristics = new ArrayList<>();
    protected TreeMap<String, ASAP2AxisPts> axis_pts = new TreeMap<>();
    protected TreeMap<String, ASAP2Measurement> measurementsMap = new TreeMap<>();
    protected List<ASAP2Measurement> measurementsList = new ArrayList<>();
    protected TreeMap<String, ASAP2CompuMethod> compu_method = new TreeMap<>();
    private Map<String, Asap2Group> unstructuredGroups = new HashMap<>();
    private List<Asap2Group> groups = new ArrayList<>();
    private List<Asap2Function> functions = new ArrayList<>();
    protected ASAP2ModPar modPar = new ASAP2ModPar();
    private boolean generatedBySimulink = false;

    /**
     * Returns the ASAP2 import error message, if available.
     *
     * @return error message
     */
    public String getError() {
        return this.error;
    }

    public Boolean hasWarning() {
        return !unsupportedTypes.isEmpty() ||
                (!doubleNames.isEmpty() && !CurrentConfig.getInstance().canUseDoubles());
    }

    public String getWarning() {
        if (hasWarning()) {
            String warning = "";
            if (!unsupportedTypes.isEmpty()) {
                warning += createUnsupportedTypeWarning();
            }
            if (!doubleNames.isEmpty()  && !CurrentConfig.getInstance().canUseDoubles()) {
                warning += createDoubleDatatypeWarning();
            }
            return warning;
        } else {
            return "";
        }
    }

    private String createUnsupportedTypeWarning() {
        StringBuilder warn = new StringBuilder("<html><b>Warning when opening file: " + data.getName()
                + ".  Item(s) will be skipped.</b></html>\n"
                + "Unsupported type used in the following parameter(s).\n");

        int originalSize = unsupportedTypes.size();
        int sublistSize = originalSize > MAX_LINES_DISPLAYED ? MAX_LINES_DISPLAYED : originalSize;

        unsupportedTypes.subList(0, sublistSize).stream()
                .forEach(e -> warn.append(" - type: " + e.getUnsupportedTypeName() + " used by parameter: "
                + e.getObjTypeName() + " name: " + e.getParameterName() + "\n"));
        if (sublistSize < originalSize) {
            warn.append("... and " + (originalSize - sublistSize) + " more.");
        }
        warn.append("\n\n"); // leave some space for possible next warning
        return warn.toString();
    }

    private String createDoubleDatatypeWarning() {
        StringBuilder warn = new StringBuilder(
                "<html><b>Warning, double datatype used in file: " + data.getName() + "</b></html>\n"
                + "The loaded ASAP2 file contains the double datatype which is not supported on the CAN Transport layer.\n"
                + "This will cause the signals and parameters consisting of the double datatype not to update.\n"
                + "The following signals/parameters use the double datatype:\n");

        int originalSize = doubleNames.size();
        int sublistSize = originalSize > MAX_LINES_DISPLAYED ? MAX_LINES_DISPLAYED : originalSize;

        doubleNames.subList(0, sublistSize).stream().forEach(e -> warn.append(" - " + e + "\n"));
        if (sublistSize < originalSize) {
            warn.append("... and " + (originalSize - sublistSize) + " more.");
        }
        warn.append("\n\n"); // leave some space for possible next warning
        return warn.toString();
    }

    /**
     * returns true if the ASAP2File just loaded contains double (64 bits)
     */
    public boolean getContainsDoubles() {
        return containsDoubles;
    }

    /**
     * Returns a list of all asap2 elements containing doubles
     *
     * @return doubleNames
     */
    public ArrayList<String> getDoubleNames() {
        return doubleNames;
    }

    /**
     * Sets the ASAP2 import error message
     *
     * @param error error message
     */
    private void setError(String error) {
        this.error = error;
    }

    /**
     * Imports the specified ASAP2 file by splitting the file into a number of
     * parts and then parsing each different part.
     *
     * @param asap2File ASAP2 file to be imported
     * @return ASAP2 data object
     */
    public ASAP2Data importASAP2(File asap2File) {
        // store filename
        data.setName(asap2File.getName());
        data.setPath(asap2File.getPath());

        doubleNames.clear();
        try {
            ASAP2ImportPart importPart = new ASAP2ImportPart(asap2File);

            // First stage: parse all data chunks between '/begin' and '/end' and put them in
            // part/part_parent
            part = importPart.parseDelimitedDataChunks();
            part_parent = importPart.getParsedPartParent();

            // Second stage: evaluate data in part and place in ASAP2 Data object
            if (!parseGeneral()) {
                this.setError(this.getError() + "\n" + data.getName() + "\nSecond stage parsing failed ");
                return null;
            }

        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            // return error, if occuring
            this.setError(this.getError() + "\n" + data.getName() + "\n" + e.getMessage());
            return null;
        }

        return data;
    }

    /**
     * Process ASAP2 string
     *
     * @param ASAP2 String to process
     * @return processed string
     */
    private static String processMatch(String ASAP2) {
        // remove multi line comments
        ASAP2 = ASAP2.replaceAll("\\x2F\\x2A([\\x00-\\x29\\x2b-\\x7f]*?)\\x2A\\x2F", "");

        // remove single line comments
        ASAP2 = ASAP2.replaceAll("\\x2F\\x2F([\\x00-\\x7f]*?)\\x0a", "");

        // remove excess whitespace (including tabs)
        ASAP2 = ASAP2.replaceAll("\\s+", " ");

        // remove leading and trailing whitespace
        ASAP2 = ASAP2.trim();

        return ASAP2;
    }

    /**
     * Parses the information that is present in the data arrays.
     *
     * @return
     */
    private boolean parseGeneral() {
        Pattern p;
        Matcher m;

        // Parse ASAP2 version information
        p = Pattern.compile("ASAP2_VERSION\\s+([0-9]{1,2})\\s+([0-9]{1,3})", Pattern.CASE_INSENSITIVE);
        m = p.matcher(part[0].get(0));
        if (m.find()) {
            data.setVersionNo(Integer.parseInt(m.group(1)));
            data.setUpgradeNo(Integer.parseInt(m.group(2)));
        }

        if (part[0].get(0).toLowerCase().contains("simulink")) {
            generatedBySimulink = true;
        }

        // Parse project information
        p = Pattern.compile("/begin PROJECT ([A-Za-z0-9_\\.\\[\\]]+) \"(.*)\"", Pattern.CASE_INSENSITIVE);
        m = p.matcher(part[1].get(0));
        if (m.find()) {
            data.setProjectName(m.group(1));
            data.setProjectComments(m.group(2));
        }

        // Parse content of module
        // LEVEL 3
        p = Pattern.compile("^/begin ([A-Za-z0-9_\\.\\[\\]]+)\\s+(.*)", Pattern.CASE_INSENSITIVE);
        for (int i = 0; i < part[3].size(); i++) {
            m = p.matcher(part[3].get(i));
            while (m.find()) {
                switch (m.group(1)) {
                    case "IF_DATA":
                        if (m.group(2).equals("XCP")) {
                            parseCommunicationSettings(i);
                        }
                        break;

                    case "MOD_COMMON":
                        parseByteOrder(i);
                        break;

                    case "FUNCTION":
                        parseFunction(i);
                        break;

                    case "RECORD_LAYOUT":
                        parseRecordLayout(i);
                        break;

                    case "CHARACTERISTIC":
                        parseCharacteristic(i);
                        break;

                    case "AXIS_PTS":
                        parseAxisPts(i);
                        break;

                    case "MEASUREMENT":
                        parseMeasurement(i);
                        break;

                    case "COMPU_METHOD":
                        parseCompuMethod(i);
                        break;

                    case "GROUP":
                        parseGroup(i);
                        break;

                    case "MOD_PAR":
                        parseModPar(i);
                        break;

                    default:
                        break;
                }
            }
        }
        // LEVEL 4
        p = Pattern.compile("^/begin ([A-Za-z0-9_\\.\\[\\]]+)", Pattern.CASE_INSENSITIVE);
        for (int i = 0; i < part[4].size(); i++) {
            m = p.matcher(part[4].get(i));
            while (m.find()) {
                switch (m.group(1)) {
                    case "AXIS_DESCR":
                        // Parse AXIS_DESCR (for CHARACTERISTIC)
                        parseAxisDescr(i);
                        break;

                    case "SUB_FUNCTION":
                        parseSubFunction(i);
                        break;

                    case "SUB_GROUP":
                        parseSubGroup(i);
                        break;

                    case "DEF_CHARACTERISTIC":
                        parseDefCharacteristic(i);
                        break;

                    case "IN_MEASUREMENT":
                        parseInMeasurement(i);
                        break;

                    case "OUT_MEASUREMENT":
                        parseOutMeasurement(i);
                        break;

                    case "REF_MEASUREMENT":
                    case "REF_CHARACTERISTIC)":
                        parseGroupReference(i, m.group(1));
                        break;

                    default:
                        break;
                }
            }
        }

        data.setASAP2RecordLayouts(record_layout);
        data.setASAP2Characteristics(characteristic);
        data.setCharacteristics(characteristics);
        data.setASAP2AxisPts(axis_pts);
        data.setASAP2Measurement(measurementsMap);
        data.setMeasurements(new ArrayList<ASAP2Measurement>(measurementsMap.values()));
        data.setASAP2CompuMethods(compu_method);
        data.setGroups(groups);
        data.setFunctions(functions);
        data.setModPar(modPar);

        // post-processing of data
        postprocessMeasurements();
        postprocessAxisPts();
        postprocessCharacteristics();
        validateFunctions(data.getFunctionRoot().getSubFunctions());
        validateGroups(data.getGroupRoot().getSubGroups());
        setSimulinkRoot(data.getGroupRoot().getSubGroups());

        return true;
    }

    /**
     * @param id - the index of the text to parse in "parts array"
     */
    private void parseCommunicationSettings(int id) {
        for (int i = 0; i < part_parent[4].size(); i++) {
            if (part_parent[4].get(i) == id) {
                String settings = processMatch(part[4].get(i));
                if (settings.contains("XCP_ON_TCP_IP")) {
                    parseXCPonTCPSettings(settings);
                } else if (settings.contains("XCP_ON_CAN")) {
                    parseXCPonCANSettings(settings);
                }
            }
        }
    }

    /**
     * @param settings - the text containing the settings
     */
    private void parseXCPonTCPSettings(String settings) {
        Pattern p = Pattern.compile(
                "0[xX]([0-9a-fA-F]+)\\s+(\\d+).+\"ADDRESS\" (\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(settings);
        if (m.find()) {
            int port = Integer.parseInt(m.group(2));
            String ip = m.group(3);
            XCPSettings xcpSettings = CurrentConfig.getInstance().getXcpsettings();
            xcpSettings.setEthernetPort(port);
            xcpSettings.setEthernetIP(ip);
        }
    }

    /**
     * @param settings - the text containing the settings
     */
    private void parseXCPonCANSettings(String settings) {
        String hex = "0[xX]([0-9a-fA-F]+)";
        Pattern p = Pattern.compile(hex + "\\s+CAN_ID_BROADCAST\\s+" + hex + "\\s+CAN_ID_MASTER\\s+" + hex
                + "\\s+CAN_ID_SLAVE\\s+" + hex + "\\s+BAUDRATE\\s+" + hex, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(settings);
        if (m.find()) {
            int transmissionID = Integer.parseInt(m.group(3), 16);
            int receptionID = Integer.parseInt(m.group(4), 16);
            String baudRate = m.group(5);
            XCPSettings xcpSettings = CurrentConfig.getInstance().getXcpsettings();
            xcpSettings.setCANIDTx(transmissionID);
            xcpSettings.setCANIDRx(receptionID);
            xcpSettings.setCANBaudrate(XCPSettings.BAUD_RATES.get(baudRate));
        }
    }

    /**
     * Parses the specified byte order in the ASAP2 file
     *
     * @param id
     * @return
     */
    private boolean parseByteOrder(int id) {
        // prepare regular expression
        Pattern p = Pattern.compile("BYTE_ORDER\\s+(MSB_FIRST|MSB_LAST)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(part[3].get(id));

        // process a possible match
        if (m.find()) {
            data.setByteOrder(ASAP2Data.ByteOrder.valueOf(m.group(1)));
        }
        return true;
    }

    /**
     * Parses a FUNCTION element.
     *
     * @param id
     * @return
     */
    private boolean parseFunction(int id) {
        Asap2Function function = new Asap2Function();

        // prepare regular expression
        Pattern p = Pattern.compile("/begin FUNCTION ([A-Za-z0-9_\\.\\[\\]]*?) \"(.*)\" (.*)/end FUNCTION",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[3].get(id)));

        // process a possible match
        if (m.find()) {
            function.setName(m.group(1));
            function.setDescription(m.group(2));
            functions.add(function);
            if (!unstructuredFunctions.containsKey(function.getName())) {
                unstructuredFunctions.put(function.getName(), function);
            }
            part_parent_name.put(id, function.getName());

        }

        return true;
    }

    /**
     * Parses a SUB_FUNCTION element.
     *
     * @param id
     * @return
     */
    private boolean parseSubFunction(int id) {
        // prepare regular expression
        Pattern p = Pattern.compile("/begin SUB_FUNCTION (.*) /end SUB_FUNCTION", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[4].get(id)));
        String pname = part_parent_name.get((int) part_parent[4].get(id));
        // process a possible match
        if (m.find()) {

            String[] sf = m.group(1).split("\\s");
            for (int i = 0; i < sf.length; i++) {
                if (!pname.equals(sf[i])) {
                    unstructuredFunctions.get(pname).addSubFunction(unstructuredFunctions.get(sf[i]));
                    functions.remove(unstructuredFunctions.get(sf[i]));
                } else {
                    AppendToLogfile
                            .appendMessage("Cross references within functions not allowed. Subfunction \""
                                    + sf[i] + "\" will be ignored.");
                }
            }

        }

        return true;
    }

    private boolean parseGroup(int id) {
        Asap2Group group = new Asap2Group();

        // prepare regular expression
        Pattern p = Pattern.compile("/begin GROUP ([A-Za-z0-9_\\.\\[\\]]*?) \"(.*)\" (ROOT)?(.*)/end GROUP",
                Pattern.CASE_INSENSITIVE);
        String bla = processMatch(part[3].get(id));
        Matcher m = p.matcher(bla);
        // process a possible match
        if (m.find()) {
            group.setName(m.group(1));
            group.setDescription(m.group(2));
            if (m.group(3) != null) {
                group.setRoot(true);
            }
            group.setSimulinkStructure(generatedBySimulink);
            unstructuredGroups.put(group.getName(), group);
            groups.add(group);
            part_parent_name.put(id, group.getName());
        }

        return true;
    }

    private boolean parseSubGroup(int id) {
        // prepare regular expression
        Pattern p = Pattern.compile("/begin SUB_GROUP (.*) /end SUB_GROUP", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[4].get(id)));
        String pname = part_parent_name.get((int) part_parent[4].get(id));
        // process a possible match
        if (m.find()) {

            String[] sf = m.group(1).split("\\s");
            for (int i = 0; i < sf.length; i++) {
                unstructuredGroups.get(pname).addSubGroup(unstructuredGroups.get(sf[i]));
                groups.remove(unstructuredGroups.get(sf[i]));
            }

        }

        return true;
    }

    private void parseGroupReference(int index, String type) {
        Pattern p = Pattern.compile(
                "/begin REF_(CHARACTERISTIC|MEASUREMENT) (.*) /end REF_(CHARACTERISTIC|MEASUREMENT)",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[4].get(index)));

        if (m.find()) {

            String pname = part_parent_name.get((int) part_parent[4].get(index));
            Asap2Group parent = unstructuredGroups.get(pname);
            String[] reference = m.group(2).split("\\s");
            for (String ref : reference) {
                switch (type) {
                    case "REF_CHARACTERISTIC":

                        ASAP2Characteristic chara = characteristic.get(ref);
                        if (parent == null || chara == null) {
                            // System.out.println("parent = null:" + pname);
                            // System.out.println("characteristic = null: " + ref);
                        } else {
                            unstructuredGroups.get(pname).addCharacteristic(characteristic.get(ref));
                        }
                        Asap2Function parent2 = unstructuredFunctions.get(pname);
                        if (parent2 == null || chara == null) {
                            // System.out.println("parent = null:" + pname);
                            // System.out.println("characteristic = null: " + ref);
                        } else {
                            unstructuredFunctions.get(pname).addCharacteristic(characteristic.get(ref));
                        }
                        break;
                    case "REF_MEASUREMENT":
                        if (measurementsMap.containsKey(ref)) {
                            unstructuredGroups.get(pname).addMeasurement(measurementsMap.get(ref));
                        }
                        break;
                }
            }
        }
    }

    /**
     * Parses a DEF_CHARACTERISTIC element.
     *
     * @param id
     * @return
     */
    private boolean parseDefCharacteristic(int id) {
        // prepare regular expression
        Pattern p = Pattern.compile("/begin DEF_CHARACTERISTIC (.*) /end DEF_CHARACTERISTIC",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[4].get(id)));

        // process a possible match
        if (m.find()) {
            // does parent exist?
            String pname = part_parent_name.get((int) part_parent[4].get(id));
            if (pname != null && unstructuredFunctions.containsKey(pname)) {
                // split into separate def_characteristic names
                String[] ch = m.group(1).split("\\s");
                for (String ref : ch) {
                    if (characteristic.containsKey(ref)) {
                        unstructuredFunctions.get(pname).addCharacteristic(characteristic.get(ref));
                    }
                }
            }
        }

        return true;
    }

    /**
     * Parses a IN_MEASUREMENT element.
     *
     * @param id
     * @return
     */
    private boolean parseInMeasurement(int id) {
        // prepare regular expression
        Pattern p
                = Pattern.compile("/begin IN_MEASUREMENT (.*) /end IN_MEASUREMENT", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[4].get(id)));

        // process a possible match
        if (m.find()) {
            // find parent
            String pname = part_parent_name.get((int) part_parent[4].get(id));

            // does parent exist?
            if (pname != null && unstructuredFunctions.containsKey(pname)) {
                // split into separate in_measurement names
                String[] mm = m.group(1).split("\\s");
                for (String ref : mm) {
                    if (measurementsMap.containsKey(ref)) {
                        unstructuredFunctions.get(pname).addInputMeasurement(measurementsMap.get(ref));
                    }
                }
            }
        }

        return true;
    }

    /**
     * Parses a OUT_MEASUREMENT element.
     *
     * @param id
     * @return
     */
    private boolean parseOutMeasurement(int id) {
        // prepare regular expression
        Pattern p
                = Pattern.compile("/begin OUT_MEASUREMENT (.*) /end OUT_MEASUREMENT", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[4].get(id)));

        // process a possible match
        if (m.find()) {
            // find parent
            String pname = part_parent_name.get((int) part_parent[4].get(id));

            // does parent exist?
            if (pname != null && unstructuredFunctions.containsKey(pname)) {
                // split into separate out_measurement names
                String[] mm = m.group(1).split("\\s");
                for (String ref : mm) {
                    if (measurementsMap.containsKey(ref)) {
                        unstructuredFunctions.get(pname).addOutputMeasurement(measurementsMap.get(ref));
                    }
                }
            }
        }

        return true;
    }

    /**
     * Parses a RECORD_LAYOUT element.
     *
     * @param id
     * @return
     */
    private boolean parseRecordLayout(int id) {
        ASAP2RecordLayout rl = new ASAP2RecordLayout();

        // prepare regular expression
        Pattern p = Pattern.compile("/begin RECORD_LAYOUT ([A-Za-z0-9_\\.\\[\\]]*?) (.*)/end RECORD_LAYOUT",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[3].get(id)));

        // process a possible match
        if (m.find()) {
            rl.setName(m.group(1));
            String content = processMatch(m.group(2));
            p = Pattern.compile(
                    "FNC_VALUES ([0-9]*?) (UBYTE|SBYTE|UWORD|SWORD|ULONG|SLONG|A_UINT64|AINT64|FLOAT32_IEEE|FLOAT64_IEEE) (ALTERNATE_CURVES|ALTERNATE_WITH_X|ALTERNATE_WITH_Y|COLUMN_DIR|ROW_DIR) (PBYTE|PWORD|PLONG|DIRECT)",
                    Pattern.CASE_INSENSITIVE);
            m = p.matcher(content);
            if (m.find()) {
                try {
                    rl.setPosition(Integer.parseInt(m.group(1)));
                } catch (NumberFormatException e) {
                    AppendToLogfile.appendError(Thread.currentThread(), e);
                }
                rl.setDatatype(ASAP2Datatype.valueOf(m.group(2)));
                rl.setIndexMode(m.group(3));
                rl.setAddresstype(m.group(4));
            }
            p = Pattern.compile(
                    "AXIS_PTS_X ([0-9]*?) (UBYTE|SBYTE|UWORD|SWORD|ULONG|SLONG|A_UINT64|AINT64|FLOAT32_IEEE|FLOAT64_IEEE) (INDEX_INCR|INDEX_DECR) (PBYTE|PWORD|PLONG|DIRECT)",
                    Pattern.CASE_INSENSITIVE);
            m = p.matcher(content);
            if (m.find()) {
                try {
                    rl.setPosition(Integer.parseInt(m.group(1)));
                } catch (NumberFormatException e) {
                    AppendToLogfile.appendError(Thread.currentThread(), e);
                }
                rl.setDatatype(ASAP2Datatype.valueOf(m.group(2)));
                rl.setIndexIncr(m.group(3));
                rl.setAddressing(m.group(4));
            }

            record_layout.put(rl.getName(), rl);
        }

        return true;
    }

    /**
     * Parses a CHARACTERISTIC element.
     *
     * @param id
     * @return
     */
    private boolean parseCharacteristic(int id) {
        ASAP2Characteristic ch = null;

        // prepare regular expression
        Pattern p = Pattern.compile(
                "/begin CHARACTERISTIC ([A-Za-z0-9_\\.\\[\\]]*?) \"(.*)\" (ASCII|CURVE|MAP|CUBOID|CUBE_4|CUBE_5|VAL_BLK|VALUE) ([\\w]*?) ([A-Za-z0-9_\\.\\[\\]]*?) ([\\w\\x2d\\x2e]*?) ([A-Za-z0-9_\\.\\[\\]]*?) ([\\w\\x2d\\x2e\\+]*?) ([\\w\\x2d\\x2e\\+]*?) (.*)/end CHARACTERISTIC",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[3].get(id)));

        // process a possible match
        if (m.find()) {
            ASAP2Characteristic.Type type = ASAP2Characteristic.Type.valueOf(m.group(3));
            if (type == Type.VALUE) {
                ch = new Asap2Parameter();
            } else if (type == Type.CURVE || type == Type.MAP) {
                ch = new Asap2Table();
            } else {
                // HANtune currently only supports single values, one dimensional
                // and two dimensional tables. The asap2 standard however also
                // supports four- and five-dimensional tables.
                unsupportedTypes.add(new UnsupportedType(ASAP2ObjType.CHARACTERISTIC.toString(),
                        type.toString(), m.group(1)));
                return false;
            }

            ch.setASAP2ObjType(ASAP2ObjType.CHARACTERISTIC);

            ch.setName(m.group(1));
            ch.setDescription(m.group(2));
            ch.setType(type);
            try {
                ch.setAddress((int) Long.parseLong(m.group(4).replaceAll("0x", ""), 16));
            } catch (NumberFormatException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                return false;
            }
            if (ch.getAddress() == 0) {
                return false;
            }
            ch.setDeposit(m.group(5));
            if ("Scalar_FLOAT64_IEEE".equalsIgnoreCase(m.group(5))) {
                containsDoubles = true;
                doubleNames.add(m.group(1));
            }
            try {
                ch.setMaxDiff(Float.parseFloat(m.group(6)));
            } catch (NumberFormatException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
            }
            ch.setConversion(m.group(7));
            try {

                String lowerLimit = m.group(8);
                if (lowerLimit.contains("0x")) { // If the lower limit value is represented hexadecimal;
                    double doubleValue;
                    lowerLimit = lowerLimit.replaceAll("0x", ""); // Remove "0x" from the hex representation
                    // because BigInteger doesn't like this
                    BigInteger bigInteger = new BigInteger(lowerLimit, 16); // Parse the hex number to a
                    // bigInteger
                    doubleValue = bigInteger.doubleValue(); // Parse bigInteger to double
                    ch.setMinimum(doubleValue); // Set lower limit with double value
                    ch.setLowerLimitEnabled(true);
                } else {
                    ch.setMinimum(Double.parseDouble(lowerLimit));
                    ch.setLowerLimitEnabled(true);
                }

            } catch (NumberFormatException e) {
                AppendToLogfile.appendMessage(
                        "Unable to parse the Lower Limit of Characteristic \"" + m.group(1) + "\" " + data.getName());
                AppendToLogfile.appendMessage("Characteristic Lower Limit value: " + m.group(8));
            }
            try {

                String upperLimit = m.group(9);
                if (upperLimit.contains("0x")) { // If the upper limit value is represented hexadecimal;
                    double doubleValue;
                    upperLimit = upperLimit.replaceAll("0x", ""); // Remove "0x" from the hex representation
                    // because BigInteger doesn't like this
                    BigInteger bigInteger = new BigInteger(upperLimit, 16); // Parse the hex number to a
                    // bigInteger
                    doubleValue = bigInteger.doubleValue(); // Parse bigInteger to double
                    ch.setMaximum(doubleValue); // Set upper limit with double value
                    ch.setUpperLimitEnabled(true);
                } else {
                    ch.setMaximum(Double.parseDouble(upperLimit));
                    ch.setUpperLimitEnabled(true);
                }

            } catch (NumberFormatException e) {
                AppendToLogfile.appendMessage(
                        "Unable to parse the Upper Limit of Characteristic \"" + m.group(1) + "\" " + data.getName());
                AppendToLogfile.appendMessage("Characteristic Upper Limit value: " + m.group(9));
            }

            // no address specified?
            if (ch.getAddress() == 0) {
                return false;
            }

            // non-supported datatype? (64bit)
            if (ch.getDatatype() == ASAP2Datatype.A_UINT64 || ch.getDatatype() == ASAP2Datatype.A_INT64
                    || ch.getDatatype() == ASAP2Datatype.FLOAT64_IEEE) {
                return false;
            }

            characteristic.put(ch.getName(), ch);
            characteristics.add(ch);
            part_parent_name.put(id, ch.getName());
        } else {
            return false;
        }

        return true;
    }

    /**
     * Parses a AXIS_DESCR element.
     *
     * @param id
     * @return
     */
    private boolean parseAxisDescr(int id) {
        // prepare regular expression
        Pattern p = Pattern.compile(
                "/begin AXIS_DESCR (CURVE_AXIS|COM_AXIS|FIX_AXIS|RES_AXIS|STD_AXIS) ([A-Za-z0-9_\\.\\[\\]]*?) ([A-Za-z0-9_\\.\\[\\]]*?) ([0-9]*?) ([\\w\\x2d\\x2e\\+\\.]*?) ([\\w\\x2d\\x2e\\+\\.]*?) (.*)/end AXIS_DESCR",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[4].get(id)));

        // process a possible match
        if (m.find()) {
            // does parent exist?
            String pname = part_parent_name.get((int) part_parent[4].get(id));
            if (pname != null && characteristic.containsKey(pname)) {
                // correct element for HANtune?
                if (m.group(1).equals("COM_AXIS")) {
                    p = Pattern.compile("AXIS_PTS_REF ([A-Za-z0-9_\\.\\[\\]]*?)\\s",
                            Pattern.CASE_INSENSITIVE);
                    m = p.matcher(m.group(7));
                    if (m.find()) {
                        // add reference at characteristic
                        // 1st call to getAxisDescr().add() denotes COLUMN description,
                        // 2nd call ROW description
                        characteristic.get(pname).getAxisDescr().add(m.group(1));
                    }
                }
            }
        }

        return true;
    }

    /**
     * Parses a AXIS_PTS element.
     *
     * @param id
     * @return
     */
    private boolean parseAxisPts(int id) {
        ASAP2AxisPts ap = new ASAP2AxisPts();

        // prepare regular expression
        Pattern p = Pattern.compile(
                "/begin AXIS_PTS ([A-Za-z0-9_\\.\\[\\]]*?) \"(.*)\" ([\\w]*?) ([A-Za-z0-9_\\.\\[\\]]*?) ([A-Za-z0-9_\\.\\[\\]]*?) ([\\w\\x2d\\x2e]*?) ([A-Za-z0-9_\\.\\[\\]]*?) ([0-9]*?) ([\\w\\x2d\\x2e\\+\\.]*?) ([\\w\\x2d\\x2e\\+\\.]*?) (.*)/end AXIS_PTS",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[3].get(id)));

        // process a possible match
        if (m.find()) {
            ap.setType(ASAP2Characteristic.Type.AXIS_PTS);
            ap.setName(m.group(1));
            ap.setDescription(m.group(2));

            try {
                ap.setAddress((int) Long.parseLong(m.group(3).replaceAll("0x", ""), 16));
            } catch (NumberFormatException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                return false;
            }
            if (ap.getAddress() == 0) {
                return false;
            }
            ap.setInputQuantity(m.group(4));
            ap.setDeposit(m.group(5));
            try {
                ap.setMaxDiff(Float.parseFloat(m.group(6)));
            } catch (NumberFormatException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
            }
            ap.setConversion(m.group(7));
            try {
                ap.setMaxAxisPoints(Integer.parseInt(m.group(8)));
                ap.setColumnCount(ap.getMaxAxisPoints());
            } catch (NumberFormatException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
            }
            try {
                ap.setMinimum(Double.parseDouble(m.group(9)));
                ap.setLowerLimitEnabled(true);
            } catch (NumberFormatException e) {
                AppendToLogfile
                        .appendMessage("Unable to parse the Lower Limit of Axis Points \"" + m.group(1) + "\" " + data.getName());
                AppendToLogfile.appendMessage("Axis Points Lower Limit value: " + m.group(9));
            }
            try {
                ap.setMaximum(Double.parseDouble(m.group(10)));
                ap.setUpperLimitEnabled(true);
            } catch (NumberFormatException e) {
                AppendToLogfile
                        .appendMessage("Unable to parse the Upper Limit of Axis Points \"" + m.group(1) + "\" " + data.getName());
                AppendToLogfile.appendMessage("Axis Points Upper Limit value: " + m.group(10));
            }

            axis_pts.put(ap.getName(), ap);
            characteristic.put(ap.getName(), (ASAP2Characteristic) ap);
        }

        return true;
    }

    /**
     * Parses a MEASUREMENT element.
     *
     * @param id
     * @return
     */
    private boolean parseMeasurement(int id) {
        ASAP2Measurement mm = new ASAP2Measurement();

        // prepare regular expression
        Pattern p = Pattern.compile(
                "/begin MEASUREMENT ([A-Za-z0-9_\\.\\[\\]]*?) \"(.*)\" (UBYTE|SBYTE|UWORD|SWORD|ULONG|SLONG|A_UINT64|AINT64|FLOAT32_IEEE|FLOAT64_IEEE) ([A-Za-z0-9_\\.\\[\\]]*?) ([\\d]*?) ([\\w\\x2d\\x2e]*?) ([\\w\\x2d\\x2e\\+\\.]*?) ([\\w\\x2d\\x2e\\+\\.]*?) (.*)/end MEASUREMENT",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[3].get(id)));

        // process possible match
        if (m.find()) {
            mm.setASAP2ObjType(ASAP2Object.ASAP2ObjType.SIGNAL);
            mm.setName(m.group(1));
            mm.setDescription(m.group(2));
            mm.setDatatype(ASAP2Datatype.valueOf(m.group(3)));
            mm.setConversion(m.group(4));
            // When using CAN and doubles occur, warn the user that doubles cannot be used on the CAN
            // transport layer.
            if ("FLOAT64_IEEE".equalsIgnoreCase(m.group(3))) {
                containsDoubles = true;
                doubleNames.add(m.group(1));
            }
            try {
                mm.setResolution(Integer.parseInt(m.group(5), 16));
            } catch (NumberFormatException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
            }
            try {
                mm.setAccuracy(Float.parseFloat(m.group(6)));
            } catch (NumberFormatException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
            }
            try {
                String lowerLimit = m.group(7);
                if (lowerLimit.contains("0x")) { // If the lower limit is represented hexadecimal;
                    double doubleValue;
                    lowerLimit = lowerLimit.replaceAll("0x", ""); // Remove "0x" from the hex representation
                    // because BigInteger doesn't like this
                    BigInteger bigInteger = new BigInteger(lowerLimit, 16); // Parse the hex number to a
                    // bigInteger
                    doubleValue = bigInteger.doubleValue(); // Parse bigInteger to double
                    mm.setMinimum(doubleValue); // Set lower limit with double value
                    mm.setLowerLimitEnabled(true);
                } else {
                    mm.setMinimum(Double.parseDouble(lowerLimit));
                    mm.setLowerLimitEnabled(true);
                }

            } catch (NumberFormatException e) {
                AppendToLogfile
                        .appendMessage("Unable to parse the Lower Limit of Measurement \"" + m.group(1) + "\" " + data.getName());
                AppendToLogfile.appendMessage("Measurement Lower Limit value: " + m.group(7));
            }
            try {
                String upperLimit = m.group(8);
                if (upperLimit.contains("0x")) { // If the upper limit value is represented hexadecimal;
                    double doubleValue;
                    upperLimit = upperLimit.replaceAll("0x", ""); // Remove "0x" from the hex representation
                    // because BigInteger doesn't like this
                    BigInteger bigInteger = new BigInteger(upperLimit, 16); // Parse the hex number to a
                    // bigInteger
                    doubleValue = bigInteger.doubleValue(); // Parse bigInteger to double
                    mm.setMaximum(doubleValue); // Set upper limit with double value
                    mm.setUpperLimitEnabled(true);
                } else {
                    mm.setMaximum(Double.parseDouble(upperLimit));
                    mm.setUpperLimitEnabled(true);
                }

            } catch (NumberFormatException e) {
                AppendToLogfile
                        .appendMessage("Unable to parse the Upper Limit of Measurement \"" + m.group(1) + "\" " + data.getName());
                AppendToLogfile.appendMessage("Measurement Upper Limit value: " + m.group(8));
            }
            p = Pattern.compile("ECU_ADDRESS ([\\w]*?)\\s", Pattern.CASE_INSENSITIVE);
            m = p.matcher(m.group(9));
            if (m.find()) {
                try {
                    mm.setAddress((int) Long.parseLong(m.group(1).replaceAll("0x", ""), 16));
                } catch (NumberFormatException e) {
                    AppendToLogfile.appendError(Thread.currentThread(), e);
                    return false;
                }
            }

            // no address specified?
            if (mm.getAddress() == 0) {
                return false;
            }

            // non-supported datatype? (64bit)
            // if (mm.getDatatype() == Datatype.A_UINT64 || mm.getDatatype() == Datatype.A_INT64 ||
            // mm.getDatatype() == Datatype.FLOAT64_IEEE) {
            // return false;
            // }
            measurementsMap.put(mm.getName(), mm);
        }

        return true;
    }

    private boolean parseModPar(int id) {
        ASAP2ModPar mp = new ASAP2ModPar();
        // pattern to process possible match (up to SYSTEM_CONSTANT)
        Pattern p = Pattern.compile(
                "/begin MOD_PAR \"(.*)\" " + "ADDR_EPK ([xXa-fA-F0-9]*)" + " CPU_TYPE \"(.*)\""
                + " ECU \"(.*)\"" + " EPK \"([^\" ]*)\"",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[3].get(id)));

        if (m.find()) {
            try {
                mp.setAddrEpk(Long.parseUnsignedLong(m.group(2).toLowerCase().replace("0x", ""), 16));
                mp.setCpuType(m.group(3));
                mp.setEcu(m.group(4));
                mp.setEpk(m.group(5));

                parseSystemConstants(mp, part[3].get(id).substring(m.end()));

            } catch (NumberFormatException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                return false;
            }
        }
        modPar = mp;
        return true;
    }

    private void parseSystemConstants(ASAP2ModPar mp, String sysConsts) {
        // search consecutive groups for presence of an unknown number of System constants
        Pattern psc = Pattern.compile(" (?:(SYSTEM_CONSTANT(?: \"([^\"]*)\"|) (?:\"([^\"]*)\")|)|)",
                Pattern.CASE_INSENSITIVE);
        Matcher msc = psc.matcher(processMatch(sysConsts));
        ASAP2Crc32ModPar cmp = new ASAP2Crc32ModPar();
        while (msc.find()) {
            if (!msc.group(1).isEmpty()) {
                distributeSysConstParams(mp, cmp, msc.group(2), msc.group(3));
            }
        }
        if (cmp.isAllFieldsPresent()) {
            mp.setCrc32Addresses(cmp);
        }
    }

    private void distributeSysConstParams(ASAP2ModPar mp, ASAP2Crc32ModPar cmp, String key, String val) {
        if (key == null || val == null) {
            return;
        }
        val = val.toLowerCase().replace("0x", "");
        switch (key) {
            case "RAM-FlashOffset":
                mp.setRamFlashOffset(Long.parseLong(val)); // offset in file as decimal
                break;

            case "CRC_START_ADDR":
                cmp.setStartAddress(Long.parseLong(val, 16));
                break;

            case "CRC_END_ADDR":
                cmp.setEndAddress(Long.parseLong(val, 16));
                break;

            case "CRC_PATCH_ADDR":
                cmp.setPatchAddress(Long.parseLong(val, 16));
                break;

            case "PCODE_START":
                cmp.setpCodeStart(Long.parseLong(val, 16));
                break;

            case "PCODE_END":
                cmp.setpCodeEnd(Long.parseLong(val, 16));
                break;

            default:
                break;
        }
    }

    /**
     * Parses a COMPU_METHOD element.
     *
     * @param id
     * @return
     */
    private boolean parseCompuMethod(int id) {
        ASAP2CompuMethod cm = new ASAP2CompuMethod();

        // prepare regular expression
        Pattern p = Pattern.compile(
                "/begin COMPU_METHOD ([A-Za-z0-9_\\.\\[\\]]*?) \"(.*)\" (IDENTICAL|FORM|LINEAR|RAT_FUNC|TAB_INTP|TAB_NOINTP|TAB_VERB) \"(.*)\" \"(.*)\" (.*)/end COMPU_METHOD",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(processMatch(part[3].get(id)));

        // process a possible match
        if (m.find()) {
            cm.setASAP2ObjType(ASAP2Object.ASAP2ObjType.COMPU_METHOD);
            cm.setName(m.group(1));
            cm.setDescription(m.group(2));
            cm.setConversionType(m.group(3));
            String decFormat = m.group(4).replace("%", "");
            if (decFormat.indexOf(".") > 0) {
                int decimals = Integer.parseInt(decFormat.substring(decFormat.indexOf(".") + 1));
                cm.setDecimals(decimals);
            }
            // cm.setDecFormat(decFormat);
            cm.setUnit(m.group(5));
            p = Pattern.compile(
                    "COEFFS ([0-9\\.]*?) ([0-9\\.]*?) ([0-9\\.]*?) ([0-9\\.]*?) ([0-9\\.]*?) ([0-9\\.]*?)\\s",
                    Pattern.CASE_INSENSITIVE);
            m = p.matcher(m.group(6));
            if (m.find()) {
                try {
                    float[] coeffs = {Float.parseFloat(m.group(1)), Float.parseFloat(m.group(2)),
                        Float.parseFloat(m.group(3)), Float.parseFloat(m.group(4)),
                        Float.parseFloat(m.group(5)), Float.parseFloat(m.group(6))};
                    cm.setCoeffs(coeffs);
                } catch (NumberFormatException e) {
                    AppendToLogfile.appendError(Thread.currentThread(), e);
                    return false;
                }
            }

            compu_method.put(cm.getName(), cm);
        }

        return true;
    }

    /**
     * Converts the parsed MEASUREMENT elements into the correct data objects
     * for HANtune.
     */
    protected void postprocessMeasurements() {
        String[] names = data.getASAP2MeasurementsNames();
        if (names.length > 0) {
            for (int i = 0; i < names.length; i++) {
                ASAP2CompuMethod c = data.getASAP2CompuMethods()
                        .get(data.getASAP2Measurements().get(names[i]).getConversion());
                ASAP2Measurement m = data.getASAP2Measurements().get(names[i]);
                if (c != null && m != null) {
                    m.setFactor(c.getFactor());
                    m.setDecimals(c.getDecimalCount());
                    m.setUnit(c.getUnit());
                }
            }
        }
    }

    /**
     * Converts the parsed CHARACTERISTIC elements into the correct data objects
     * for HANtune.
     */
    protected void postprocessCharacteristics() {
        // loop through asap2 characteristics
        List<String> toRemove = new ArrayList();
        for (Iterator<ASAP2Characteristic> iterator
                = data.getASAP2Characteristics().values().iterator(); iterator.hasNext();) {
            ASAP2Characteristic asap2Characteristic = iterator.next();
            ASAP2CompuMethod asap2CompuMethod
                    = data.getASAP2CompuMethods().get(asap2Characteristic.getConversion());
            if (asap2CompuMethod != null) {
                asap2Characteristic.setFactor(asap2CompuMethod.getFactor());
                asap2Characteristic.setDecimals(asap2CompuMethod.getDecimalCount());
                asap2Characteristic.setUnit(asap2CompuMethod.getUnit());
            }
            // ticket #276 a if statement could be added here to ensure proper processing of Booleans, the
            // effect in the rest of the software has to be evaluated
            ASAP2RecordLayout asap2RecordLayout
                    = data.getASAP2RecordLayouts().get(asap2Characteristic.getDeposit());
            if (asap2RecordLayout != null) {
                if (asap2Characteristic.getDeposit().equals("Scalar_BOOLEAN")) {
                    asap2Characteristic.setDatatype(ASAP2Datatype.BOOLEAN);
                } else {
                    asap2Characteristic.setDatatype(asap2RecordLayout.getDatatype());
                }
            }

            if (asap2Characteristic.getType() == Type.CURVE) {
                // determine the number of columns (values) of a curve
                if (asap2Characteristic.getAxisDescr().size() == 1
                        && data.getASAP2AxisPts().containsKey(asap2Characteristic.getAxisColumnDescr())) {
                    asap2Characteristic.setColumnCount(data.getASAP2AxisPts()
                            .get(asap2Characteristic.getAxisColumnDescr()).getMaxAxisPoints());
                    asap2Characteristic
                            .addAxisPoint(data.getASAP2AxisPts().get(asap2Characteristic.getAxisColumnDescr()));
                } else {
                    // remove element when not all information is provided
                    data.getCharacteristics().remove(asap2Characteristic);
                    iterator.remove();
                }
            } else if (asap2Characteristic.getType() == Type.MAP) {
                // determine the number of columns and rows of a map (table)
                if (asap2Characteristic.getAxisDescr().size() == 2
                        && data.getASAP2AxisPts().containsKey(asap2Characteristic.getAxisColumnDescr())
                        && data.getASAP2AxisPts().containsKey(asap2Characteristic.getAxisRowDescr())) {
                    asap2Characteristic.setColumnCount(data.getASAP2AxisPts()
                            .get(asap2Characteristic.getAxisColumnDescr()).getMaxAxisPoints());
                    asap2Characteristic.setRowCount(data.getASAP2AxisPts()
                            .get(asap2Characteristic.getAxisRowDescr()).getMaxAxisPoints());
                    asap2Characteristic
                            .addAxisPoint(data.getASAP2AxisPts().get(asap2Characteristic.getAxisColumnDescr()));
                    asap2Characteristic
                            .addAxisPoint(data.getASAP2AxisPts().get(asap2Characteristic.getAxisRowDescr()));
                } else {
                    // remove element when not all information is provided
                    data.getCharacteristics().remove(asap2Characteristic);
                    iterator.remove();
                }
            } else if (asap2Characteristic.getDatatype() == ASAP2Datatype.FLOAT64_IEEE) {
                // 64bit floating point parameter
                // toRemove.add(asap2Characteristic.getName());
            }
        }

        if (toRemove.size() > 0) {
            String message = "The following characteristics were removed because they were a double: ";
            for (String key : toRemove) {
                data.getASAP2Characteristics().remove(key);
                message += key + " ";
            }
            MessagePane.showWarning(message);
        }
    }

    /**
     * Converts the parsed AXIS_PTS elements into the correct data objects for
     * HANtune.
     */
    protected void postprocessAxisPts() {
        String[] names = data.getASAP2AxisPtsNames();
        if (names.length > 0) {
            for (int i = 0; i < names.length; i++) {
                ASAP2CompuMethod c
                        = data.getASAP2CompuMethods().get(data.getASAP2AxisPts().get(names[i]).getConversion());
                ASAP2AxisPts a = data.getASAP2AxisPts().get(names[i]);
                if (c != null) {
                    a.setFactor(c.getFactor());
                    a.setDecimals(c.getDecimalCount());
                }
                ASAP2RecordLayout rl
                        = data.getASAP2RecordLayouts().get(data.getASAP2AxisPts().get(names[i]).getDeposit());
                if (rl != null) {
                    a.setDatatype(rl.getDatatype());
                }
            }
        }
    }

    private void validateFunctions(List<Asap2Function> functions) {
        for (Asap2Function function : functions) {
            validateFunctions(function.getSubFunctions());
            for (Iterator<ASAP2Characteristic> iterator = function.getCharacteristics().iterator(); iterator
                    .hasNext();) {
                ASAP2Characteristic asap2Characteristic = iterator.next();
                if (!data.getCharacteristics().contains(asap2Characteristic)) {
                    iterator.remove();
                }
            }
        }
    }

    private void validateGroups(List<Asap2Group> functions) {
        for (Asap2Group function : functions) {
            validateGroups(function.getSubGroups());
            for (Iterator<ASAP2Characteristic> iterator = function.getCharacteristics().iterator(); iterator
                    .hasNext();) {
                ASAP2Characteristic asap2Characteristic = iterator.next();
                if (!data.getCharacteristics().contains(asap2Characteristic)) {
                    iterator.remove();
                }
            }
        }
    }

    private void setSimulinkRoot(List<Asap2Group> groups) {
        if (generatedBySimulink && !groups.isEmpty() && !groups.get(0).getSubGroups().isEmpty()) {
            Asap2Group oldRoot = groups.get(0);
            Asap2Group newRoot = oldRoot.getSubGroups().get(0);
            groups.remove(oldRoot);
            groups.add(newRoot);
        }
    }

}
