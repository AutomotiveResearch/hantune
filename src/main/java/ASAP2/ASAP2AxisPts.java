/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains information about a AXIS_PTS.
 *
 * @author Aart-Jan
 */
public class ASAP2AxisPts extends ASAP2Characteristic {

    private double[] data;
    private boolean[] syncs;
    private long timeStamp;
    private final List<AxisPointsListener> listeners = new ArrayList<>();
    private String inputQuantity = "";
    private int maxAxisPoints = 0;

    public String getInputQuantity() {
        return inputQuantity;
    }

    public void setInputQuantity(String InputQuantity) {
        this.inputQuantity = InputQuantity;
    }

    public int getMaxAxisPoints() {
        return maxAxisPoints;
    }

    public void setMaxAxisPoints(int MaxAxisPoints) {
        this.maxAxisPoints = MaxAxisPoints;
        data = new double[MaxAxisPoints];
        syncs = new boolean[MaxAxisPoints];
    }

    public double getValueAt(int index) {
        return data[index];
    }

    @Override
    public void setValueAt(double value, int index) {
        this.timeStamp = System.currentTimeMillis();
        if (data[index] != value) {
            data[index] = value;
        }
        updateListeners(value, index);
    }

    public boolean isInSync() {
        for (boolean sync : syncs) {
            if (sync == false) return false;
        }
        return true;
    }

    public boolean isInSyncAt(int index) {
        return syncs[index];
    }

    @Override
    public void setInSyncAt(boolean inSync, int index) {
        if (syncs[index] != inSync) {
            syncs[index] = inSync;
            updateStateListeners();
        }
    }

    public void addListener(AxisPointsListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(AxisPointsListener listener) {
        listeners.remove(listener);
    }

    public void updateListeners(double value, int index) {
        for (AxisPointsListener listener : listeners) {
            listener.axisPointsUpdate(this, value, index);
        }
    }

    @Override
    public void updateStateListeners() {
        for (AxisPointsListener listener : listeners) {
            listener.axisPointsStateUpdate();
        }
    }

    public boolean isConnectedToEditor() {
        for (AxisPointsListener listener : listeners) {
            if (listener.isConnectedToEditor()) return true;
        }
        return false;
    }

    /**
     * Only used by calibration management. Do not use this method. Use
     * getValueAt() instead.
     * @return
     * @deprecated
     */
    @Deprecated
    @Override
    public ASAP2Data.Data getData() {
        if (isInSync() || isConnectedToEditor()) {
            Object[] axisdata = new Object[maxAxisPoints];
            for (int i = 0; i < maxAxisPoints; i++) {
                axisdata[i] = data[i];
            }
            return new ASAP2Data.Data(timeStamp, axisdata);
        } else {
            return null;
        }
    }
}
