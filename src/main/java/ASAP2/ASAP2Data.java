/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import datahandling.BasicDescriptionFile;
import datahandling.Reference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;
import util.CompositeList;

/**
 * Contains parsed information of an ASAP2 file.
 *
 * @author Aart-Jan, Michiel Klifman
 */
public class ASAP2Data extends BasicDescriptionFile {

    public enum ByteOrder {MSB_FIRST,MSB_LAST}

    /**
     * Contains the version number of the ASAM MCD-2MC standard.
     */
    private int versionNo = 0;
    /**
     * Contains the upgrade number of the ASAM MCD-2MC standard.
     */
    private int upgradeNo = 0;
    /**
     * Contains the name of the ASAP2 project.
     */
    private String projectName;
    /**
     * Contains the comments added for the ASAP2 project.
     */
    private String projectComments;
    /**
     * Indicates the byte order as used by the XCP slave device.
     */
    private ByteOrder byteOrder;
    /**
     * Root of this asap2 file's Group hierarchy
     */
    private final Asap2Group group = new Asap2Group();
    /**
     * Root of this asap2 file's Function hierarchy
     */
    private final Asap2Function function = new Asap2Function();
    /**
     * Contains an array of RECORD_LAYOUT's.
     */
    private TreeMap<String, ASAP2RecordLayout> asap2RecordLayouts = new TreeMap<>();
    /**
     * Contains an array of CHARACTERISTIC's. These are the tunable parameters
     * out of the ASAP2 file.
     */
    private TreeMap<String, ASAP2Characteristic> asap2Characteristics = new TreeMap<>();
    private List<ASAP2Characteristic> characteristics = new ArrayList<>();
    /**
     * Contains an array of AXIS_PTS's.
     */
    private TreeMap<String, ASAP2AxisPts> asap2AxisPts = new TreeMap<>();
    /**
     * Contains an array of MEASUREMENT's. These are the viewable elements
     * out of the ASAP2 file.
     */
    private TreeMap<String, ASAP2Measurement> asap2Measurements = new TreeMap<>();
    private List<ASAP2Measurement> measurements = new ArrayList<>();
    /**
     * Contains an array of COMPU_METHOD's.
     */
    private TreeMap<String, ASAP2CompuMethod> asap2CompuMethods = new TreeMap<>();

    private ASAP2ModPar modPar = new ASAP2ModPar();

    public int getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(int versionNo) {
        this.versionNo = versionNo;
    }

    public int getUpgradeNo() {
        return upgradeNo;
    }

    public void setUpgradeNo(int upgradeNo) {
        this.upgradeNo = upgradeNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectComments() {
        return projectComments;
    }

    public void setProjectComments(String projectComments) {
        this.projectComments = projectComments;
    }

    public ByteOrder getByteOrder() {
        return byteOrder;
    }

    public void setByteOrder(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    public String getEpromID() {
        if (modPar != null && modPar.isEpkPresent()) {
            return modPar.getEpk(); // EPK holds the eprom ID.
        } else {
            return getProjectComments();  // Use project comment only for compatibility with older a2l
        }
    }

    /**
     * @return the root of all groups
     */
    public Asap2Group getGroupRoot() {
        return group;
    }

    /**
     * @param groups the groups to set
     */
    public void setGroups(List<Asap2Group> groups) {
        group.setSubGroups(groups);
    }

    /**
     * @return the root of all functions
     */
    public Asap2Function getFunctionRoot() {
        return function;
    }

    /**
     * @param functions the functions to set
     */
    public void setFunctions(List<Asap2Function> functions) {
        function.setSubFunctions(functions);
    }

    public TreeMap<String, ASAP2RecordLayout> getASAP2RecordLayouts() {
        return asap2RecordLayouts;
    }

    public String[] getASAP2RecordLayoutNames(){
        return asap2RecordLayouts.keySet().toArray(new String[asap2RecordLayouts.size()]);
    }

    public void setASAP2RecordLayouts(TreeMap<String, ASAP2RecordLayout> asap2RecordLayouts) {
        this.asap2RecordLayouts = asap2RecordLayouts;
    }

    public TreeMap<String, ASAP2Characteristic> getASAP2Characteristics() {
        return asap2Characteristics;
    }

    public void setASAP2Characteristics(TreeMap<String, ASAP2Characteristic> asap2Characteristics) {
        this.asap2Characteristics = asap2Characteristics;
    }

    public TreeMap<String, ASAP2AxisPts> getASAP2AxisPts() {
        return asap2AxisPts;
    }

    public String[] getASAP2AxisPtsNames(){
        return asap2AxisPts.keySet().toArray(new String[asap2AxisPts.size()]);
    }

    public void setASAP2AxisPts(TreeMap<String, ASAP2AxisPts> asap2AxisPts) {
        this.asap2AxisPts = asap2AxisPts;
    }

    public TreeMap<String, ASAP2Measurement> getASAP2Measurements() {
        return asap2Measurements;
    }

    public String[] getASAP2MeasurementsNames() {
        return asap2Measurements.keySet().toArray(new String[asap2Measurements.size()]);
    }

    public String getASAP2MeasurementName(int id) {
        return asap2Measurements.keySet().toArray(new String[asap2Measurements.size()])[id];
    }

    public void setASAP2Measurement(TreeMap<String, ASAP2Measurement> asap2Measurements) {
        this.asap2Measurements = asap2Measurements;
    }

    public List<ASAP2Characteristic> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(List<ASAP2Characteristic> characteristics) {
        sortAsap2Objects(characteristics);
        this.characteristics = characteristics;
    }

    public ASAP2Characteristic getCharacteristicByName(String name) {
        return asap2Characteristics.get(name);
    }

    public List<ASAP2Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<ASAP2Measurement> measurements) {
        sortAsap2Objects(measurements);
        this.measurements = measurements;
    }

    public ASAP2Measurement getMeasurementByName(String name) {
        return asap2Measurements.get(name);
    }

    public TreeMap<String, ASAP2CompuMethod> getASAP2CompuMethods() {
        return asap2CompuMethods;
    }

    public void setASAP2CompuMethods(TreeMap<String, ASAP2CompuMethod> asap2CompuMethods) {
        this.asap2CompuMethods = asap2CompuMethods;
    }

    public ASAP2ModPar getModPar() {
        return modPar;
    }

    public void setModPar(ASAP2ModPar modPar) {
        this.modPar = modPar;
    }


    public int getMyIndex(String text){
        int value = 0;
        String[] myArray = asap2Measurements.keySet().toArray(new String[asap2Measurements.size()]);
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i].equals(text)) {
                value = i;
            }
        }
        return value;
    }

    public static void sortAsap2Objects(List<? extends ASAP2Object> asap2Objects) {
        Collections.sort(asap2Objects, (ASAP2Object object1, ASAP2Object object2) -> object1.getName().compareTo(object2.getName()));
    }

    @Override
    public List<? extends Reference> getReferences() {
        return new CompositeList<>(characteristics, measurements);
    }

   public void putData(String asap2ref, Object valueObject, long timestamp) {

    }

   public static final class Data{
        public final long timestamp;
        public final Object value;
        public Data(final long timestamp, final Object value) {
            this.timestamp = timestamp;
            this.value = value;
        }
    }

    /**
     * Only used by calibration management. Do not use this method.
     * @param key
     * @return
     * @deprecated
     */
    @Deprecated
    public Data getData(String key) {
        Data data = null;
        if (asap2Characteristics.get(key) != null) {
            data = asap2Characteristics.get(key).getData();
        }
        return data;
    }

   /**
     * This overrides the toString method of Object so that an
     * object of this class can be used as a node in a JTree.
     * @return the name of this object
     */
    @Override
    public String toString() {
        return getName();
    }
}
