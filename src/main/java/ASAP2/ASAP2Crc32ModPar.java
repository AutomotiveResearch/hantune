/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

// A PoJo containing all optional MOD_PAR CRC32 params in ASAP2
public class ASAP2Crc32ModPar {

    private Long startAddress;
    private Long endAddress;
    private Long patchAddress;
    private Long pCodeStart;
    private Long pCodeEnd;

    public ASAP2Crc32ModPar() {
        startAddress = null;
        endAddress = null;
        patchAddress = null;
        pCodeStart = null;
        pCodeEnd = null;
    }

    public Long getStartAddress() {
        return startAddress;
    }

    public Long setStartAddress(Long start) {
        return (startAddress = start);
    }

    public Long getEndAddress() {
        return endAddress;
    }

    public Long setEndAddress(Long endAddress) {
        return this.endAddress = endAddress;
    }

    public Long getPatchAddress() {
        return patchAddress;
    }

    public Long setPatchAddress(Long patchAddress) {
        return this.patchAddress = patchAddress;
    }

    public Long getpCodeStart() {
        return pCodeStart;
    }

    public Long setpCodeStart(Long pCodeStart) {
        return this.pCodeStart = pCodeStart;
    }

    public Long getpCodeEnd() {
        return pCodeEnd;
    }

    public Long setpCodeEnd(Long pCodeEnd) {
        return this.pCodeEnd = pCodeEnd;
    }

    public boolean isAllFieldsPresent() {
        return (this.startAddress != null && this.endAddress != null && this.patchAddress != null
                && this.pCodeStart != null && this.pCodeEnd != null);
    }
}
