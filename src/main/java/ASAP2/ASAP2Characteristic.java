/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ASAP2.ASAP2Data.Data;
import datahandling.Reference;
import datahandling.ReferenceStateListener;

/**
 * Contains information about a CHARACTERISTIC.
 *
 * @author Aart-Jan, Michiel Klifman
 */
public abstract class ASAP2Characteristic extends ASAP2Object implements Reference {
    public enum Type {
        // HANtune (as of now) currently only supports: single values; one dimensional
        // and two dimensional tables: VALUE, CURVE, MAP.
        // The asap2 standard however also supports four- and five-dimensional tables.
        VALUE,
        CURVE,
        AXIS_PTS, // special CURVE used as axis points for a table
        MAP,
        // not supported:
        ASCII, VAL_BLK, CUBOID, CUBE_4, CUBE_5
    }

    public static final int COLUMN_INDEX = 0; // Element 0 in list/array indicates the Column nr
    public static final int ROW_INDEX = 1;// Element 1 in list/array indicates the row nr
    private String deposit = null;
    private float maxDiff = 0;
    private int columnCount = 1;
    private int rowCount = 1;
    private final List<String> axisDescr = new LinkedList<>();
    private boolean active = true;
    private final List<ReferenceStateListener> referenceListeners = new ArrayList<>();
    private Type type = null;


    public Type getType() {
        return type;
    }

    public void setType(Type tp) {
        this.type = tp;
    }

    public int getColumnCount() {
        return columnCount;
    }


    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }


    public int getRowCount() {
        return rowCount;
    }


    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }


    public String getDeposit() {
        return deposit;
    }


    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }


    public float getMaxDiff() {
        return maxDiff;
    }


    public void setMaxDiff(float maxDiff) {
        this.maxDiff = maxDiff;
    }


    public List<String> getAxisDescr() {
        return axisDescr;
    }


    public String getAxisColumnDescr() {
        if (!axisDescr.isEmpty()) {
            return axisDescr.get(COLUMN_INDEX);
        } else {
            return null;
        }
    }


    public String getAxisRowDescr() {
        if (axisDescr.size() >= 2) {
            return axisDescr.get(ROW_INDEX);
        } else {
            return null;
        }
    }


    public double validateValue(double value) {
        if (value < lowerLimit) {
            return lowerLimit;
        } else if (value > upperLimit) {
            return upperLimit;
        } else if (factor != 1 || decimals == 0) {
            return Math.round(value / factor) * factor;
        } else {
            return value;
        }
    }


    /**
     * Only used by calibration management. Do not use this method. Use getValue() in Asap2Parameter,
     * getValueAt() in Asap2Table etc.
     *
     * @return
     * @deprecated
     */
    @Deprecated
    public Data getData() {
        return new Data(0, 0);
    }


    public void setValue(double value) {
        // does nothing at the moment, but is overriden in Asap2Parameter
        // Asap2Characteristic should be refactored to further separate Asap2Table,
        // Asap2Parameter and ASAP2AxisPts.
    }


    public void setInSync(boolean inSync) {
        // See setValue();
    }


    public void setValueAt(double value, int row, int column) {
        // does nothing at the moment, but is overriden in Asap2Table
        // Asap2Characteristic should be refactored to further separate Asap2Table,
        // Asap2Parameter and ASAP2AxisPts.
    }


    public void setInSyncAt(boolean inSync, int row, int column) {
        // See setValueAt(value, row, column);
    }


    public void setValueAt(double value, int index) {
        // does nothing at the moment, but is overriden in ASAP2AxisPts
        // Asap2Characteristic should be refactored to further separate Asap2Table,
        // Asap2Parameter and ASAP2AxisPts.
    }


    public void setInSyncAt(boolean inSync, int index) {
        // See setValueAt(value, index);
    }


    public void addAxisPoint(ASAP2AxisPts axisPoint) {
        // does nothing at the moment, but is overriden in Asap2Table
        // Asap2Characteristic should be refactored to further separate Asap2Table,
        // Asap2Parameter and ASAP2AxisPts.
    }


    /**
     * Check if the given dataSize matches the data according to ASAP2Characteristic
     *
     * @param dataSize in bytes
     * @return true if data contains correct number of bytes for export according to asap2. false if
     * otherwise
     */
    public boolean checkDataSize(int dataSize) {
        int cols = getColumnCount();
        int rows = getRowCount();
        int size = getDatatypeSize();

        return (dataSize == (cols * rows * size));
    }


    @Override
    public boolean isActive() {
        return active;
    }


    @Override
    public void setActive(boolean active) {
        this.active = active;
        updateStateListeners();
    }


    @Override
    public void addStateListener(ReferenceStateListener listener) {
        if (!referenceListeners.contains(listener)) {
            referenceListeners.add(listener);
        }
    }


    @Override
    public void removeStateListener(ReferenceStateListener listener) {
        referenceListeners.remove(listener);
    }


    protected void updateStateListeners() {
        for (ReferenceStateListener listener : referenceListeners) {
            listener.stateUpdate();
        }
    }
}
