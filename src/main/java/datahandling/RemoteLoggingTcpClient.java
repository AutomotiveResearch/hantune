/*
Copyright (c) 2024 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package datahandling;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.Set;

/**
 *
 * @author Michiel Klifman
 */
public class RemoteLoggingTcpClient {
    
    private Socket clientSocket;
    private PrintWriter out;
    private InputStream in;

    public String startConnection(String ip, int port, String name, String frequency, String duration, Set<String> signals) throws IOException {
        clientSocket = new Socket();
        SocketAddress socketAddress = new InetSocketAddress(ip, port); 
        clientSocket.setSoTimeout(5000);
        clientSocket.connect(socketAddress, 5000);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        int signalCount = 0;
        String jsonSignals = "";
        for (String s : signals) {
            jsonSignals += "\"" + s + "\"";
            signalCount++;
            if (signalCount < signals.size()) {
                jsonSignals += ", ";
            }
        }
        String json = "{\"log_request\": {\"name\": \"" + name + "\", \"sampletime\": " + frequency + ", \"duration\": " + duration + ", "
                + "\"signals\": [" + jsonSignals +"]}}";
        out.print(json);
        out.flush();
        
        in = clientSocket.getInputStream();
        byte[] b = new byte[1024];
        in.read(b);        
        String text = new String(b, StandardCharsets.UTF_8);
        out.close();
        in.close();
        clientSocket.close();
        return text;
    }

    public String getStatus(String ip, int port, String name, String frequency, String duration, Set<String> signals) throws IOException {
        clientSocket = new Socket();
        SocketAddress socketAddress = new InetSocketAddress(ip, port); 
        clientSocket.setSoTimeout(5000);
        clientSocket.connect(socketAddress, 5000);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        int signalCount = 0;
        String jsonSignals = "";
        for (String s : signals) {
            jsonSignals += "\"" + s + "\"";
            signalCount++;
            if (signalCount < signals.size()) {
                jsonSignals += ", ";
            }
        }
        String json = "\"status_request\"";
        out.print(json);
        out.flush();
        
        in = clientSocket.getInputStream();
        byte[] b = new byte[1024];
        in.read(b);        
        String text = new String(b, StandardCharsets.UTF_8);
        out.close();
        in.close();
        clientSocket.close();
        return text;
    }

    public String cancel(String ip, int port, String name, String frequency, String duration, Set<String> signals) throws IOException {
        clientSocket = new Socket();
        SocketAddress socketAddress = new InetSocketAddress(ip, port); 
        clientSocket.setSoTimeout(5000);
        clientSocket.connect(socketAddress, 5000);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        int signalCount = 0;
        String jsonSignals = "";
        for (String s : signals) {
            jsonSignals += "\"" + s + "\"";
            signalCount++;
            if (signalCount < signals.size()) {
                jsonSignals += ", ";
            }
        }
        String json = "\"cancel_log\"";
        out.print(json);
        out.flush();
        
        in = clientSocket.getInputStream();
        byte[] b = new byte[1024];
        in.read(b);        
        String text = new String(b, StandardCharsets.UTF_8);
        out.close();
        in.close();
        clientSocket.close();
        return text;
    }
    
}