/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

/*
 * Intel Hex object. Data of Intel Hex file is stored in these objects
 */
public class IHexObject implements LineDataObject {

    static final int IHEX_RADIX = 16;

    public enum RecordType {
        // HANtune (as of now) currently only supports: DATA, End Of File, Extended Linear Address.
        IHEX_DATA(0), IHEX_EOF(1), IHEX_EXTENDED_LINEAR_ADDRESS(4);

        private RecordType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        private final int value;
    };

    private RecordType type;
    private String name;

    private int baseAddress; // Extended linear Address: Upper 16 address bits. Stored as
    // 0x00001234. Shift 16 bits left to use value for REAL address
    private int addressOffset; // : Lower 16 address bits. Start address where data bytes are
    // located

    private byte[] data; // holds array of actual data bits

    public IHexObject(int baseAddr) {
        this.type = RecordType.IHEX_EXTENDED_LINEAR_ADDRESS;
        this.baseAddress = baseAddr;
        this.addressOffset = 0;
        this.data = null;
    }

    public IHexObject(int baseAddr, int offsAddr, byte[] data) {
        this.type = RecordType.IHEX_DATA;
        this.baseAddress = baseAddr;
        this.addressOffset = offsAddr;
        this.data = data;
    }

    public IHexObject(String name, long addr, byte[] data) {
        this.type = RecordType.IHEX_DATA;
        this.name = name;
        this.baseAddress = (int) ((addr >> 16) & 0x000000000000ffff);
        this.addressOffset = (int) (addr & 0x000000000000ffff);
        this.data = data;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public long getStartAddress() {
        return ((long) baseAddress << 16) + addressOffset;
    }

    @Override
    public void addStartAddressFlashOffset(long offset) {
        long tempAdr = ((long) baseAddress << 16) + addressOffset;
        tempAdr += offset;
        this.baseAddress = (int) (tempAdr >> 16);
        this.addressOffset = (int) (tempAdr & 0x000000000000ffff);
    }

    // last memory address holding data
    @Override
    public long getEndAddress() {
        if (type == RecordType.IHEX_DATA) {
            return ((long) baseAddress << 16) + addressOffset + data.length - 1;
        } else {
            return 0;
        }
    }

    public int getBaseAddress() {
        return baseAddress;
    }

    public int getAddressOffset() {
        return addressOffset;
    }

    public byte[] getData() {
        return data;
    }

    public int getDataSize() {
        if (type == RecordType.IHEX_DATA) {
            return data.length;
        } else {
            return 0;
        }
    }

    public RecordType getType() {
        return type;
    }

}
