/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import nl.han.hantune.scripting.Interpreter;
import nl.han.hantune.scripting.Script;

/**
 *
 * @author Michiel
 */
public class DefaultScript extends DefaultReference implements Script {

    private final String name;
    private final String path;

    public DefaultScript(String name, String path) {
        this.name = name;
        this.path = path;
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSource() {
        return SOURCE;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public Interpreter getInterpreter() {
        return null;
    }

    @Override
    public void addStateListener(ReferenceStateListener listener) {
    }

    @Override
    public void removeStateListener(ReferenceStateListener listener) {
    }

    @Override
    public boolean isRunning() {
        return false;
    }

    @Override
    public void setRunning(boolean running) {
    }

    @Override
    public void setPath(String path) {
    }

    @Override
    public boolean isRunOnStartup() {
        return false;
    }

    @Override
    public void setRunOnStartup(boolean runOnStartup) {
    }

}
