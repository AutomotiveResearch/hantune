/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;
import java.util.TreeMap;

import ASAP2.ASAP2Characteristic;
import HANtune.CustomFileChooser.FileType;
import haNtuneHML.Calibration;
import haNtuneHML.Calibration.ASAP2;
import util.Util;

public class CalibrationImportReaderSRec extends CalibrationImportLineReader {

    private CalibReportImportSRec report;
    private boolean swapDataBytes = false; // Only swap data bytes if input is 'INTEL' order: little-endian

    /**
     * Reader implementation for SREC file
     *
     * @param EpromId holds calib eprom ID to be compared with eprom ID in file
     * @param addressEpk holds address where eprom ID of file is located
     * @param ramFlashOffset ofsset from RAM variable to flash location
     * @param swapDataBytes holds true if bytes need to be swapped for correct
     * endianness
     *
     */
    public CalibrationImportReaderSRec(String EpromId, long addressEpk, long ramFlashOffset,
            boolean swapDataBytes) {
        super(EpromId, addressEpk, ramFlashOffset, FileType.SREC_FILE);

        this.swapDataBytes = swapDataBytes;
    }

    /**
     * transfer all relevant SREC data objects into calibration
     *
     * @param calib contains the calibration where the data from SRec needs to
     * be stored
     * @param characteristics contains the characteristic for conversion and
     * size of each element
     */
    public ImportReaderResult transferDataIntoCalibration(Calibration calib,
            TreeMap<String, ASAP2Characteristic> characteristics) {

        // Iterate all memory addresses of each calibration element
        for (int i = 0; i < calib.sizeOfASAP2Array(); i++) {
            ASAP2 calibItem = calib.getASAP2Array(i);
            if (calibItem != null) {
                String calibrationParameterName = calibItem.getName();
                ASAP2Characteristic charact = characteristics.get(calibrationParameterName);

                if (charact != null) {
                    // Search all SRecs for contiguous data of size and starting at address
                    long calibAddress = Integer.toUnsignedLong(charact.getAddress());
                    long calibAddressSRec = calibAddress + getRamFlashOffset();

                    int numBytes = calibItem.getByteArrayValue().length;

                    byte[] calibBytes = getLineDataContainer().getData(calibAddressSRec, numBytes);

                    // if found, then place into calibration
                    if (calibBytes != null) {
                        if (swapDataBytes) {
                            calibBytes = Util.swapEndianness(calibBytes, charact.getDatatypeSize());
                        }

                        calibItem.setByteArrayValue(calibBytes);
                        report.addReportLine(CalibReportImportSRec.ReportLineTypeSRec.SREC_CALIB_IMPORT_OK,
                                calibrationParameterName,
                                CalibReportImportSRec.ReportLineTypeSRec.SREC_CALIB_IMPORT_OK.getDescription());
                    } else {
                        report.addReportLine(CalibReportImportSRec.ReportLineTypeSRec.SREC_CALIB_NOT_FOUND,
                                calibrationParameterName, CalibReportImportSRec.ReportLineTypeSRec.SREC_CALIB_NOT_FOUND
                                        .getDescription(calibAddress, numBytes, calibAddressSRec));
                    }
                } else {
                    report.addReportLine(CalibReportImportSRec.ReportLineTypeSRec.SREC_CALIB_NAME_NOT_FOUND,
                            calibrationParameterName,
                            CalibReportImportSRec.ReportLineTypeSRec.SREC_CALIB_NAME_NOT_FOUND.getDescription());
                }

            }
        }

        return ImportReaderResult.READER_RESULT_OK;
    }

    protected LineDataObject parseSingleLine(String line) {
        if (line.startsWith("S3")) {
            return extractSRecObject(line);
        }

        return null;
    }

    protected void createImportReport(File file) {
        report = new CalibReportImportSRec(file);
    }

    // protected void readAllDataLines(File file) throws FileNotFoundException {
    // report = new CalibReportImportSRec(file);
    // Scanner importFile = new Scanner(file);
    // ArrayList<SRecObject> items = new ArrayList<SRecObject>();
    //
    // while (importFile.hasNextLine()) {
    // String line = importFile.nextLine().trim();
    //
    // if (line.startsWith("S3")) {
    // SRecObject obj = extractSRecObject(line);
    // if (obj != null) {
    // items.add(obj);
    // }
    // }
    // }
    // importFile.close();
    //
    // // then: Sort SREC objects by address
    // Collections.sort(items, new Comparator<SRecObject>() {
    // @Override
    // public int compare(SRecObject item1, SRecObject item2) {
    // if (item1.getStartAddress() < item2.getStartAddress()) {
    // return -1;
    // }
    // if (item1.getStartAddress() == item2.getStartAddress()) {
    // return 0;
    // }
    // return 1;
    // }
    // });
    //
    // srecObjs = items;
    // }
    private SRecObject extractSRecObject(String line) {
        int dataByteCount = Integer.parseUnsignedInt(line.substring(2, 4), SRecObject.SREC_RADIX)
                - SRecObject.EXTRA_SIZE_S3_ADDRESS_CHECKSUM;

        // check Checksum at end of line
        int offset = line.length() - 2;
        int srecChecksum = Short.parseShort(line.substring(offset, offset + 2), SRecObject.SREC_RADIX);

        if (SRecObject.calculateS3CheckSum(line) != (byte) srecChecksum) {
            return null;
        }

        // check line length
        // Line length: 2 pos 'S3', 2 pos count, 8 pos address, 2 pos checksum => 14 pos
        // + 2 * dataByteCount
        if (line.length() != (dataByteCount * 2) + 14) {
            return null;
        }

        // get address value
        long address = Long.parseLong(line.substring(4, 12), SRecObject.SREC_RADIX);

        // fill data bytes
        byte[] data = new byte[dataByteCount];
        offset = 12;
        for (int iCnt = 0; iCnt < dataByteCount; iCnt++) {
            data[iCnt] = (byte) Short.parseShort(line.substring(offset, offset + 2), SRecObject.SREC_RADIX);

            offset += 2;
        }

        return new SRecObject(address, data);
    }

    @Override
    public String getImportReport() {
        return report.buildImportReport();
    }

}
