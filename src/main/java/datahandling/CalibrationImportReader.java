/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;
import java.io.IOException;
import java.util.TreeMap;

import ASAP2.ASAP2Characteristic;
import HANtune.CustomFileChooser.FileType;
import haNtuneHML.Calibration;

public interface CalibrationImportReader {

    enum ImportReaderResult {
        READER_RESULT_OK,
        READER_RESULT_WARNING, // There is a problem: ask user to continue
        READER_RESULT_ERROR // There is an expected error. Cannot continue
    }

    /**
     * Read calibration data from the specified file
     *
     * @param file location including filename and extension
     */
    public ImportReaderResult readCalibration(File file) throws IOException;

    /**
     * After read has successfully been completed, write data into calibration
     * object
     *
     * @param calibration The calibration to put data into
     * @characteristics contains properties of calibration data
     */
    ImportReaderResult transferDataIntoCalibration(Calibration calibration,
            TreeMap<String, ASAP2Characteristic> characteristics);

    public String getReadResultMessage();

    public String getReadResultText();

    /**
     * Import report
     *
     * @return string holding result report of the import
     */
    public String getImportReport();

    /**
     * get fileType associated with reader
     */
    public FileType getFileType();
}
