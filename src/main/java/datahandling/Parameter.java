/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.awt.datatransfer.DataFlavor;

/**
 *
 * @author Michiel Klifman
 */
public interface Parameter extends ValueDescription {

    public static final DataFlavor DATA_FLAVOR = new DataFlavor(Parameter.class, "Parameter");

    public double getValue();

    /**
     * Get the raw value of this Parameters current value;
     * @return the raw value.
     */
    public double getRawValue();

    /**
     * Converts a value to the raw value for this Parameter.
     * @param value the value to convert.
     * @return the raw value.
     */
    public double getRawValue(double value);

    /**
     * Validates a value for this parameter and adjusts the value if necessary.
     * Used by GUI components to validate user input values.
     * @param value - the value that needs to be validated.
     * @return whether or not the provided value is a valid value for this
     * parameter.
     */
    public double validateValue(double value);

    public void send(double value);

    public double getLastSentValue();

    public boolean isInSync();

    public void addListener(ParameterListener listener);

    public void removeListener(ParameterListener listener);

}
