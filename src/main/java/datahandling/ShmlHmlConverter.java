/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import nl.han.hantune.config.VersionInfo;
import haNtuneHML.HANtuneDocument;
import haNtuneHML.Layout;
import haNtuneSHML.ShmlDocument;
import haNtuneSHML.ShmlDocument.Shml;

public class ShmlHmlConverter {
    /**
     * Create a new SHML Document object from active values found in given HANtune Document
     * @param doc HANtuneDocument with objects to be placed in SHML document
     * @return ShmlDocument
     */
    public static ShmlDocument createShmlDocFromHANtuneDoc(HANtuneDocument doc) {
        haNtuneHML.HANtuneDocument.HANtune ht = doc.getHANtune();
        ShmlDocument shmlDoc = ShmlDocument.Factory.newInstance();
        Shml shml = shmlDoc.addNewShml();
        shml.setVersion(VersionInfo.getInstance().getVersionFloat());
        shml.setSHMLVersion(shml.getSHMLVersion()); // SHML version (set in current xsd file)

        shml.setSettings(ht.getSettings());

        // in HML: active daq lists is an array of indexes to the DaqListArray (not in SHML)
        int iCnt = 0;
        for (int  activeElem : ht.getActiveDAQlistList()) {
            shml.addNewDaqlist();
            shml.setDaqlistArray(iCnt, ht.getDaqlistArray(activeElem));
            iCnt++;
        }

        // convert only the active layout
        shml.setLayout(ht.getLayoutArray(ht.getActiveLayout()));

        return shmlDoc;
    }

    public static HANtuneDocument createHANtuneDocFromShmlDoc(ShmlDocument sdoc) {
        Shml shml = sdoc.getShml();

        HANtuneDocument htDoc = HANtuneDocument.Factory.newInstance();
        haNtuneHML.HANtuneDocument.HANtune ht = htDoc.addNewHANtune();
        ht.setVersion(shml.getVersion());

        ht.setSettings(shml.getSettings());

        // in HML: active daq lists is an array of indexes to the DaqListArray (not in SHML)
        for (int iCnt = 0; iCnt < shml.sizeOfDaqlistArray(); iCnt++) {
            ht.addNewDaqlist();
            ht.setDaqlistArray(iCnt, shml.getDaqlistArray(iCnt));
        }

        ht.addNewLayout();
        ht.setLayoutArray(0, (Layout)shml.getLayout());
        ht.setActiveLayout(0);

        return htDoc;
    }



}
