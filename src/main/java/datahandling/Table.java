/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.awt.datatransfer.DataFlavor;
import java.util.List;

/**
 *
 * @author Michiel Klifman
 */
public interface Table extends Reference {

    public static final DataFlavor DATA_FLAVOR = new DataFlavor(Table.class, "Table");

    public double getValueAt(int row, int column);

    public void setValueAt(double value, int row, int column);

    public boolean isInSyncAt(int row, int column);

    public double getAxisPointsValueAt(int axis, int index);

    public boolean isAxisPointInSyncAt(int axis, int index);

    public boolean isValueValid(double value);

    public int getColumnCount();

    public int getRowCount();

    public List<String> getAxisDescr();
    public String getAxisColumnDescr();
    public String getAxisRowDescr();

    public void send(double value, int row, int column);

    public void sendAxisPoint(double value, int axis, int index);

    public void addListener(TableListener listener);

    public void removeListener(TableListener listener);

}
