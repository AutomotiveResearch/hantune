/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

/**
 * DCM Object. Contains data of a single item in a DCM file
 *
 */
public class DCMObject {
    private DCMObjectType type;
    private String name;

    private int rowSize;
    private int colSize;

    // private List<String> data;
    private String[][] data;


    // constructor for single parameter object
    DCMObject(String str) {
        type = DCMObjectType.DCM_PARAMETER;
        name = str;
        rowSize = 1;
        colSize = 1;
        data = new String[1][1];
    }


    // constructor for array object
    DCMObject(String str, int cols) {
        type = DCMObjectType.DCM_ARRAY;
        name = str;
        colSize = cols;
        rowSize = 1;
        data = new String[cols][1];
    }


    // constructor for cols x rows matrix
    DCMObject(String str, int cols, int rows) {
        type = DCMObjectType.DCM_MATRIX;
        name = str;
        colSize = cols;
        rowSize = rows;
        data = new String[cols][rows];
    }


    void setDataString(String str, int col, int row) {
        data[col][row] = str;
    }


    public String getName() {
        return name;
    }


    public int getRows() {
        return rowSize;
    }


    public int getCols() {
        return colSize;
    }


    public String getDataString(int iCol, int iRow) {
        return data[iCol][iRow];
    }

}
