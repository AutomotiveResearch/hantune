/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

/**
 * A default implementation of Parameter. Can be used as a placeholder reference
 * by editors.
 *
 * @author Michiel Klifman
 */
public class DefaultParameter extends DefaultReference implements Parameter {

    private final String name;
    private final String source;

    public DefaultParameter(String name, String source) {
        this.name = name;
        this.source = source;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public double getValue() {
        return 0.0;
    }

    @Override
    public double getRawValue() {
        return 0.0;
    }

    @Override
    public double getRawValue(double value) {
        return 0.0;
    }

    @Override
    public double validateValue(double value) {
        return 0.0;
    }

    @Override
    public void send(double value) {
    }

    @Override
    public double getLastSentValue() {
        return 0.0;
    }

    @Override
    public boolean isInSync() {
        return false;
    }

    @Override
    public void addListener(ParameterListener listener) {
    }

    @Override
    public void removeListener(ParameterListener listener) {
    }

    @Override
    public double getMinimum() {
        return 0.0;
    }

    @Override
    public double getMaximum() {
        return 100.0; //Arbitrary value, but must be larget than minimum.
    }

    @Override
    public String getUnit() {
        return "";
    }

    @Override
    public double getFactor() {
        return 1.0; //Arbitrary value, but must not be 0.
    }

    @Override
    public int getDecimalCount() {
        return 0;
    }

    @Override
    public boolean hasDecimals() {
        return false;
    }

    @Override
    public void addStateListener(ReferenceStateListener listener) {
    }

    @Override
    public void removeStateListener(ReferenceStateListener listener) {
    }
}
