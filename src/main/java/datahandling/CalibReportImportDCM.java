/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;

public class CalibReportImportDCM extends CalibrationReport<CalibReportImportDCM.ReportLineTypeDCM> {

    public enum ReportLineTypeDCM  {
        DCM_CALIB_IMPORT_OK("DCM item successfully imported"),
        DCM_CALIB_NOT_FOUND("DCM name not found in calibration data. Not imported"),
        DCM_ASAP2_NOT_FOUND("DCM name not found in ASAP2 data. Not imported"),
        ASAP2_SIZE_DIFFERENT_CALIB("ASAP2 size %d differs from calibration size %d. Not imported"),
        ASAP2_COLS_DIFFERENT_DCM("ASAP2 number of columns (%d) differs from DCM columns %d. Not imported"),
        ASAP2_ROWS_DIFFERENT_DCM("ASAP2 number of rows (%d) differs from DCM rowss %d. Not imported");

        // enum constructor
        ReportLineTypeDCM(String line) {
            this.description = line;
        }

        private String description;


        public String getDescription() {
            return description;
        }


        public String getDescription(int sizeA, int sizeC) {
            return String.format(description, sizeA, sizeC);
        }

    }

    CalibReportImportDCM(File file) {
        super(file);
    }
     class DCMReportLine {
         ReportLineTypeDCM type;
         String reportString;
     }


    @Override
    public int compareReportElements(ReportLine item1, ReportLine item2) {
        return item1.lineType.compareTo(item2.lineType);
    }


     @Override
     public boolean isOkElement(ReportLineTypeDCM item) {
        return (item.compareTo(ReportLineTypeDCM.DCM_CALIB_IMPORT_OK) == 0) ? true : false;
    }


    @Override
    public String getReportHeaderLine() {
        String header = String.format("%d of %d calibration items successfully imported from file: %s\n",
            super.getOkLineCount(), super.getTotalLineCount(), super.getReportFile().getName());

        return header;
    }


    @Override
    public String getReportRoundup() {
        return "";
    }
}
