/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;

import ASAP2.ASAP2Characteristic;
import HANtune.CustomFileChooser.FileType;
import haNtuneHML.Calibration;
import haNtuneHML.Calibration.ASAP2;

public class CalibrationImportReaderDCM implements CalibrationImportReader {

    private ArrayList<DCMObject> dcmObjs;

    CalibReportImportDCM report;

    @Override
    // read all data blocks from DCM file and put each block in a DCM object
    public ImportReaderResult readCalibration(File file) throws IOException {
        report = new CalibReportImportDCM(file);

        Scanner importFile = new Scanner(file);

        dcmObjs = readAllDataBlocks(importFile);

        importFile.close();

        return ImportReaderResult.READER_RESULT_OK;
    }

    /**
     * Write all DCM objects into calibration
     *
     * @param calib contains the calibration where the data from DCM needs to be
     * stored
     * @param characts contains the characteristic for conversion and size of
     * each element
     * @return
     */
    @Override
    public ImportReaderResult transferDataIntoCalibration(Calibration calib,
            TreeMap<String, ASAP2Characteristic> characts) {

        for (DCMObject obj : dcmObjs) {
            ASAP2 calibItem = getCalibDataItemByName(calib, obj.getName());
            ASAP2Characteristic characteristic = characts.get(obj.getName());

            if (checkValidMetadata(obj, characteristic, calibItem)) {
                setCalibData(obj, characteristic, calibItem);
                report.addReportLine(CalibReportImportDCM.ReportLineTypeDCM.DCM_CALIB_IMPORT_OK, obj.getName(),
                        CalibReportImportDCM.ReportLineTypeDCM.DCM_CALIB_IMPORT_OK.getDescription());
            }

        }
        return ImportReaderResult.READER_RESULT_OK;
    }

    @Override
    public String getImportReport() {
        return report.buildImportReport();
    }

    @Override
    public String getReadResultMessage() {
        return "";
    }

    @Override
    public String getReadResultText() {
        return "";
    }

    @Override
    public FileType getFileType() {
        return FileType.DCM_FILE;
    }

    private ArrayList<DCMObject> readAllDataBlocks(Scanner in) {
        ArrayList<DCMObject> items = new ArrayList<DCMObject>();

        while (in.hasNextLine()) {
            String line = in.nextLine().trim();
            String itemString;
            if (line.startsWith("FESTWERTEBLOCK") || line.startsWith("FESTWERT")) {
                itemString = loadDcmItemString(line, in);
                if (itemString != null) {
                    DCMObject obj = extractDCMObject(itemString);
                    if (obj != null) {
                        items.add(obj);
                    }
                }

            }
        }
        return items;
    }

    // Extract DCM item string: all characters between (and including) FESTWERT(EBLOCK) and END
    private String loadDcmItemString(String startLine, Scanner in) {
        String resultString = new String(startLine.trim());

        while (in.hasNextLine()) {
            String line = in.nextLine().trim();

            resultString += " " + line;
            if (line.startsWith("END")) {
                return resultString;
            }
        }

        return null;
    }

    /**
     * Extracts all relevant data from given str and puts in a DCMObject
     *
     * @param str
     * @return DCMObject filled with relevant data or null;
     */
    private DCMObject extractDCMObject(String str) {

        String words[] = str.split("\\s++"); // split on whitespace

        if (words[0].equals("FESTWERT")) {
            return createDcmParameterObject(words);
        }

        if (words[0].equals("FESTWERTEBLOCK")) {
            if (words[3].equals("@")) {
                return createDcmMatrixObject(words);
            } else {
                return createDcmArrayObject(words);
            }

        }
        return null;
    }

    private DCMObject createDcmParameterObject(String words[]) {
        DCMObject resultObj = null;

        resultObj = new DCMObject(words[1]);
        resultObj.setDataString(words[3], 0, 0);

        return resultObj;
    }

    private DCMObject createDcmArrayObject(String words[]) {
        DCMObject resultObj = null;
        int cols = Integer.parseInt(words[2]);
        if (words.length < (cols) + 4) {
            return null;
        }

        resultObj = new DCMObject(words[1], cols);
        for (int iCol = 0; iCol < cols; iCol++) {
            resultObj.setDataString(words[iCol + 4], iCol, 0);
        }
        return resultObj;
    }

    private DCMObject createDcmMatrixObject(String words[]) {
        DCMObject resultObj = null;
        int cols = Integer.parseInt(words[2]);
        int rows = Integer.parseInt(words[4]);
        if (words.length < (cols * rows) + 9) {
            return null;
        }

        resultObj = new DCMObject(words[1], cols, rows);
        for (int iCol = 0; iCol < cols; iCol++) {
            for (int iRow = 0; iRow < rows; iRow++) {
                resultObj.setDataString(words[iCol + (iRow * (cols + 1)) + 6], iCol, iRow);
            }
        }
        return resultObj;
    }

    private ASAP2 getCalibDataItemByName(Calibration calib, String name) {
        List<ASAP2> list = calib.getASAP2List();
        for (ASAP2 elem : list) {
            if (elem.getName().equals(name)) {
                return elem;
            }
        }
        return null;
    }

    private boolean checkValidMetadata(DCMObject obj, ASAP2Characteristic charact, ASAP2 calItem) {
        if (calItem == null) {

            report.addReportLine(CalibReportImportDCM.ReportLineTypeDCM.DCM_CALIB_NOT_FOUND, obj.getName(),
                    CalibReportImportDCM.ReportLineTypeDCM.DCM_CALIB_NOT_FOUND.getDescription());
            return false;
        }

        if (charact == null) {
            report.addReportLine(CalibReportImportDCM.ReportLineTypeDCM.DCM_ASAP2_NOT_FOUND, obj.getName(),
                    CalibReportImportDCM.ReportLineTypeDCM.DCM_ASAP2_NOT_FOUND.getDescription());
            return false;
        }

        int dataSize = charact.getColumnCount() * charact.getRowCount() * charact.getDatatypeSize();
        if (dataSize != calItem.getByteArrayValue().length) {
            report.addReportLine(CalibReportImportDCM.ReportLineTypeDCM.ASAP2_SIZE_DIFFERENT_CALIB, obj.getName(),
                    CalibReportImportDCM.ReportLineTypeDCM.ASAP2_SIZE_DIFFERENT_CALIB.getDescription(dataSize,
                            calItem.getByteArrayValue().length));
            return false;
        }

        if (charact.getColumnCount() != obj.getCols()) {
            report.addReportLine(CalibReportImportDCM.ReportLineTypeDCM.ASAP2_COLS_DIFFERENT_DCM, obj.getName(),
                    CalibReportImportDCM.ReportLineTypeDCM.ASAP2_COLS_DIFFERENT_DCM.getDescription(charact.getColumnCount(),
                            obj.getCols()));
            return false;
        }

        if (charact.getRowCount() != obj.getRows()) {
            report.addReportLine(CalibReportImportDCM.ReportLineTypeDCM.ASAP2_ROWS_DIFFERENT_DCM, obj.getName(),
                    CalibReportImportDCM.ReportLineTypeDCM.ASAP2_ROWS_DIFFERENT_DCM.getDescription(charact.getRowCount(),
                            obj.getRows()));
            return false;
        }

        return true;
    }

    private void setCalibData(DCMObject obj, ASAP2Characteristic characteristic, ASAP2 item) {
        int rows = characteristic.getRowCount();
        int dataSize = characteristic.getDatatypeSize();
        byte[] calibData = new byte[item.getByteArrayValue().length];

        for (int iCol = 0; iCol < characteristic.getColumnCount(); iCol++) {
            for (int iRow = 0; iRow < characteristic.getRowCount(); iRow++) {
                byte[] dataElem = characteristic.convertValue(obj.getDataString(iCol, iRow));
                System.arraycopy(dataElem, 0, calibData, ((iCol * rows) + iRow) * dataSize, dataSize);
            }
        }
        item.setByteArrayValue(calibData);
    }

}
