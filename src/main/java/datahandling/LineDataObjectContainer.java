/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class LineDataObjectContainer {

    private ArrayList<LineDataObject> lineDataObjects;

    public LineDataObjectContainer() {
        lineDataObjects = new ArrayList<>();
    }

    public void addDataObject(LineDataObject ldo) {
        lineDataObjects.add(ldo);
    }

    public boolean containerElementsIsEmpty() {
        return lineDataObjects.isEmpty();
    }

    public ArrayList<LineDataObject> getLineDataContainerElements() {
        return lineDataObjects;
    }

    public void sortByAddress() {
        // then: Sort SREC objects by address
        Collections.sort(lineDataObjects, new Comparator<LineDataObject>() {
            @Override
            public int compare(LineDataObject item1, LineDataObject item2) {
                if (item1.getStartAddress() < item2.getStartAddress()) {
                    return -1;
                }
                if (item1.getStartAddress() == item2.getStartAddress()) {
                    return 0;
                }
                return 1;
            }
        });

    }

    protected void fillDataSingleLineObject(LineDataObject srcObj, LineDataObject targetObj) {
        int destPos = (int) (srcObj.getStartAddress() - targetObj.getStartAddress());
        int size = srcObj.getData().length;

        System.arraycopy(srcObj.getData(), 0, targetObj.getData(), destPos, size);
    }

    protected void fillDataMultipleMultipleLineObjects(LineDataObject srcObj, int targetIdxStrt,
            int targetIdxEnd) {
        LineDataObject targObj;
        int srcPos = 0;
        byte[] srcData = srcObj.getData();
        int srcLength = srcData.length;

        int copyLength;
        int targPos;
        int targSize;
        byte[] targData;
        for (int idx = targetIdxStrt; idx <= targetIdxEnd; idx++) {
            targObj = lineDataObjects.get(idx);

            targData = targObj.getData();
            targSize = targData.length;
            if ((targPos = (int) (srcObj.getStartAddress() - targObj.getStartAddress())) < 0) {
                targPos = 0;
            }

            if ((copyLength = targSize - targPos) > srcLength - srcPos) {
                copyLength = srcLength - srcPos;
            }
            System.arraycopy(srcObj.getData(), srcPos, targObj.getData(), targPos, copyLength);

            srcPos += copyLength;
        }

    }

    /**
     * Get data from LineDataObjects for given start address and size
     *
     * @param strtAddress
     * @param size
     * @return byte array containing requested data, or null if not found
     */
    public byte[] getData(long strtAddress, int size) {
        long endAddress = strtAddress + size - 1;

        // first: find index containing requested startaddress
        int strtIdx = -1; // index within lineDataObjects
        int idx = 0;
        long objEndAddress = 0;
        for (LineDataObject lineObj : lineDataObjects) {
            if (lineObj.getData() != null) {
                objEndAddress = lineObj.getEndAddress();

                if (strtAddress >= lineObj.getStartAddress() && strtAddress <= objEndAddress) {
                    if (endAddress <= objEndAddress) {
                        return getDataSingleLineDataObj(idx, strtAddress, endAddress);
                    }

                    strtIdx = idx;
                    break;
                }
            }
            idx++;
        }

        if (strtIdx == -1) {
            // start not found
            return null;
        }

        // next continue search at strtIdx + 1: check for last index containing calculated end-address.
        // Also check if all lineData recs contain adjacent data bytes. No gaps between data of 2
        // consecutive recs.
        int endIdx = -1;
        LineDataObject ldo;
        for (idx = strtIdx + 1; idx < lineDataObjects.size(); idx++) {
            ldo = lineDataObjects.get(idx);
            if (ldo.getData() != null) {
                // check for gap.
                if (ldo.getStartAddress() != objEndAddress + 1) {
                    return null;
                }

                if (endAddress >= ldo.getStartAddress()
                        && endAddress <= (objEndAddress = ldo.getEndAddress())) {
                    // found it
                    endIdx = idx;
                    break;
                }
            }
        }

        if (endIdx == -1) {
            // end not found
            return null;
        }

        return getCalibDataMulitpleLineDataObj(strtIdx, endIdx, strtAddress, endAddress);
    }

    private byte[] getDataSingleLineDataObj(int idxLdo, long strtAdr, long endAdr) {

        int size = (int) (endAdr - strtAdr + 1);
        int from = (int) (strtAdr - lineDataObjects.get(idxLdo).getStartAddress());

        return Arrays.copyOfRange(lineDataObjects.get(idxLdo).getData(), from, from + size);
    }

    /**
     * Get byte array from lineDataObjects. Data will span multiple contiguous
     * elements from lineDataObjects.
     *
     * @param strtIdx index within lineDataObjects where first data byte is
     * located.
     * @param endIdx index within lineDataObjects where last data byte is
     * located. Data between strtIdx and endIdx has no gaps!!
     * @param strtAddress start address of requested data bytes
     * @param endAddress end address of requested data bytes (address of last
     * byte)
     * @return array of bytes holding requested data
     */
    private byte[] getCalibDataMulitpleLineDataObj(int strtIdx, int endIdx, long strtAddress,
            long endAddress) {

        int dataPos = 0;
        int ldoPos;
        int copyLength;
        int dataLength = (int) (endAddress - strtAddress + 1);
        byte[] dataBytes = new byte[dataLength];
        LineDataObject ldo;

        for (int ldoIdx = strtIdx; ldoIdx <= endIdx; ldoIdx++) {
            ldo = lineDataObjects.get(ldoIdx);
            byte[] ldoData = ldo.getData();
            if (ldoData != null) {
                if ((ldoPos = (int) (strtAddress - ldo.getStartAddress())) < 0) {
                    ldoPos = 0;
                }
                if ((copyLength = ldoData.length - ldoPos) > dataLength - dataPos) {
                    copyLength = dataLength - dataPos;
                }
                System.arraycopy(ldoData, ldoPos, dataBytes, dataPos, copyLength);
                dataPos += copyLength;
            }
        }

        return dataBytes;
    }

}
