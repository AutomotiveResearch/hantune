/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ASAP2.ASAP2Characteristic;
import haNtuneHML.Calibration;

/**
 * @author Joshua Lettink <joshua_lettink@outlook.com> Implements writing calibration into an Etas DCM-file
 */
public class CalibrationExportWriterDCM implements CalibrationExportWriter {

    private static final String LF = System.lineSeparator(); // use general linefeed for DCM file
    private List<String> notFoundNames = new ArrayList<>();


    /**
     * Export the specified calibration into an Etas DCM-file.
     *
     * @param calibration: Holds the actual calibration data
     * @param file:        The filename of the calibration.
     */
    @Override
    public void writeCalibration(final Calibration calibration, final File file) throws IOException {
        BufferedWriter bw;
        ASAP2Characteristic asap2Characteristic;
        bw = new BufferedWriter(new FileWriter(file));

        writeHeader(file.getName(), bw);

        // write all calibration data
        if (calibration != null) {
            for (int i = 0; i < calibration.sizeOfASAP2Array(); i++) {
                String calibrationParameterName = calibration.getASAP2Array(i).getName();
                asap2Characteristic = CurrentConfig.getInstance().getASAP2Data()
                    .getASAP2Characteristics().get(calibrationParameterName);

                if (asap2Characteristic != null) {
                    writeItem(asap2Characteristic, bw, calibration.getASAP2Array(i).getByteArrayValue());
                } else {
                    notFoundNames.add(calibrationParameterName + ": ASAP2 characteristic not found");
                }
            }
        }

        bw.close();
    }

    public void writeToFile(String headerString, File file, List<LineDataObject> ihObjs) throws IOException {
        // not used, empty function
    }

    public String getWarningLines() {
        if (notFoundNames.isEmpty()) {
            return "";
        } else {
            StringBuilder warn = new StringBuilder(
                "<html><b>Warning: " + notFoundNames.size() + " calibration items could not be exported:</b></html>\n");
            notFoundNames.forEach(e -> warn.append(" - " + e + "\n"));
            return warn.toString();
        }
    }


    private void writeItem(ASAP2Characteristic asap2Characteristic, BufferedWriter bufferedWriter,
                           byte[] data)
        throws IOException {

        // only write dcm data if correct number of bytes in data[]
        if (data != null && asap2Characteristic.checkDataSize(data.length)) {
            bufferedWriter.write(dcmItemString(asap2Characteristic, data));
            bufferedWriter.flush();
        } else {
            notFoundNames.add(asap2Characteristic.getName() + ": ASAP2 data length doesn't match");
        }

    }


    private String valueToDcmString(byte[] data, ASAP2Characteristic asap2) {
        StringBuilder rtn = new StringBuilder("");
        ByteBuffer buffer = ByteBuffer.wrap(data);
        buffer.rewind();

        rtn.append(String.format("FESTWERT %s" + LF, asap2.getName()));
        rtn.append(String.format("  WERT %s" + LF, asap2.getDataString(buffer)));
        rtn.append("END" + LF + LF);

        return rtn.toString();
    }


    private String arrayToDcmString(byte[] data, ASAP2Characteristic asap2) {
        StringBuilder rtn = new StringBuilder("");

        int cols = asap2.getColumnCount();
        int dataSize = asap2.getDatatypeSize();
        ByteBuffer buffer = ByteBuffer.allocate(dataSize);

        rtn.append(String.format("FESTWERTEBLOCK %s %d" + LF, asap2.getName(), cols));
        rtn.append("  WERT");

        for (int i = 0; i < cols; i++) {
            buffer.put(data, i * dataSize, dataSize);
            buffer.rewind();
            rtn.append(String.format(" %s", asap2.getDataString(buffer)));
            buffer.rewind();
        }
        rtn.append(LF + "END" + LF + LF);

        return rtn.toString();
    }


    private String matrixToDcmString(byte[] data, ASAP2Characteristic asap2) {
        StringBuilder rtn = new StringBuilder("");

        int cols = asap2.getColumnCount();
        int rows = asap2.getRowCount();
        int dataSize = asap2.getDatatypeSize();
        ByteBuffer buffer = ByteBuffer.allocate(dataSize);


        rtn.append(String.format("FESTWERTEBLOCK %s %d @ %d" + LF, asap2.getName(), cols, rows));

        for (int iRow = 0; iRow < rows; iRow++) {
            rtn.append("  WERT");
            for (int iCol = 0; iCol < cols; iCol++) {
                buffer.put(data, ((iCol * rows) + iRow) * dataSize, dataSize);
                buffer.rewind();
                rtn.append(String.format(" %s", asap2.getDataString(buffer)));
                buffer.rewind();
            }
            rtn.append(LF);
        }
        rtn.append("END" + LF + LF);

        return rtn.toString();
    }


    private void writeHeader(String fileLocation, BufferedWriter w) throws IOException {
        SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        w.write("* DAMOS format" + LF);
        w.write("* Created by HANTUNE" + LF);
        w.write("* Creation date: " + sd.format(new Date()) + LF);
        w.write("*" + LF);
        w.write("* DamosDataFilePath: " + fileLocation + LF);
        w.write("* DamosExtensionForOutput: 'dcm'" + LF);
        w.write("* DamosFormatVersion: '1'" + LF);
        w.write("* DamosCaseSensitiveNames: true" + LF);
        w.write("* DamosIncludeDependentParameter: true" + LF);
        w.write("* DamosBooleanFormat: 'Integer'" + LF);
        w.write("* DamosV1WriteEnums: false" + LF);
        w.write("* DamosEnumerationFormat: 'String'" + LF);
        w.write("* DamosV1WriteSamplingPoints: false" + LF);
        w.write("* DamosShowInputLogFile: false" + LF);
        w.write("* DamosInputLogFile: false" + LF);
        w.write(LF);
    }


    private String dcmItemString(ASAP2Characteristic asap2Characteristic, final byte[] data) {
        String rtnString = null;
        switch (asap2Characteristic.getType()) {
            case VALUE:
                rtnString = valueToDcmString(data, asap2Characteristic);
                break;

            case CURVE:
            case AXIS_PTS:
                rtnString = arrayToDcmString(data, asap2Characteristic);
                break;

            case MAP:
                rtnString = matrixToDcmString(data, asap2Characteristic);
                break;

            default:
                rtnString = LF + "**** " + asap2Characteristic.getName()
                    + ": Error no conversion for ASAP2 Characteristic type: "
                    + asap2Characteristic.getType().toString() + LF + LF;
                break;
        }
        return rtnString;
    }

}
