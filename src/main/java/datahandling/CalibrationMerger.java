/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Data;
import datahandling.CalibrationImportReader.ImportReaderResult;
import haNtuneHML.Calibration;
import util.Util;

public abstract class CalibrationMerger {

    private String mergerTypeText = "";
    private CalibrationExportWriter writer;
    private CalibrationImportLineReader reader;
    private CalibMergeReport report = null;
    private ImportReaderResult readResult = null;
    private String readResultTxt = "";
    private ImportReaderResult mergeResult = null;
    String readCalibResultTxt = "";
    protected ASAP2Data asap;
    private Calibration calib;

    protected abstract CalibrationImportLineReader createCalibrationImportLineReader();

    protected abstract CalibrationExportWriter createCalibrationExportWriter();

    protected abstract LineDataObject createNewLineDataObject(String name, long addr, byte[] data);

    protected abstract void sortLineDataByAddress(ArrayList<LineDataObject> ldObjs);

    public CalibrationMerger(Calibration calib, ASAP2Data asap, String mergerTypeText) {
        this.mergerTypeText = mergerTypeText;
        this.calib = calib;
        this.asap = asap;
        reader = createCalibrationImportLineReader();
        writer = createCalibrationExportWriter();
    }

    void initCalibrationMerger(CalibrationImportLineReader reader) {
        this.reader = reader;
    }

    /**
     * Load basefile data from given file (holds application AND calibration
     * data)
     *
     * @param file
     * @throws IOException
     */
    public void loadBasefileData(File file) throws IOException {
        report = new CalibMergeReport(file, mergerTypeText, calib.getTitle(), asap.getEpromID());

        readResult = reader.readCalibration(file);
        readResultTxt = reader.getReadResultText();
        report.setReportHeaderLine(reader.getReadResultText());
    }

    /**
     * merge (write into) given calibration data (in memory) with basefile data
     * objects. Overwrite basefile data with new data from calibration
     *
     * @param calib holds calibration data
     */
    public void mergeCalibrationData(Calibration calib) {
        if (readResult == null || readResult == ImportReaderResult.READER_RESULT_ERROR) {
            return;
        }
        ArrayList<LineDataObject> calibLineObjects = new ArrayList<>();
        long ramFlashOffset = asap.getModPar().getRamFlashOffset();

        // first: load data from calibration into line objects
        transferCalibDataToLineData(calib, calibLineObjects);

        long currentCalibAddressNoOffset;
        // next: merge data from line objects into targetIHexObjs
        for (LineDataObject srcObj : calibLineObjects) {

            // keep calibration address (without offset) for possible report line
            currentCalibAddressNoOffset = srcObj.getStartAddress();

            // and add offset here
            srcObj.addStartAddressFlashOffset(ramFlashOffset);

            if (writeSourceIntoExportTarget(srcObj, currentCalibAddressNoOffset)) {
                report.addReportLine(CalibMergeReport.ReportLineType.CALIB_MERGE_OK, srcObj.getName(),
                        CalibMergeReport.ReportLineType.CALIB_MERGE_OK.formatDescription());
            }

        }
        checkReportOk();
    }

    private void transferCalibDataToLineData(Calibration calib, ArrayList<LineDataObject> calibObjs) {
        // Only swap data bytes if output is 'INTEL' order: little-endian
        boolean swapDataBytes = (asap.getByteOrder() == ASAP2Data.ByteOrder.MSB_LAST);

        for (Calibration.ASAP2 calObj : calib.getASAP2List()) {
            String calibrationParameterName = calObj.getName();
            ASAP2Characteristic asap2Charact = asap.getASAP2Characteristics().get(calibrationParameterName);

            // only transfer data if asap2Charact has been found
            if (asap2Charact == null) {
                // name not found, add to report
                report.addReportLine(CalibMergeReport.ReportLineType.ITEM_NAME_NOT_FOUND, calibrationParameterName,
                        CalibMergeReport.ReportLineType.ITEM_NAME_NOT_FOUND.formatDescription());
            } else {
                byte[] dataBytes = calObj.getByteArrayValue();
                // only add ihex data if correct number of bytes in dataBytes[]
                if (dataBytes != null && asap2Charact.checkDataSize(dataBytes.length)) {
                    if (swapDataBytes) {
                        dataBytes = Util.swapEndianness(dataBytes, (int) asap2Charact.getDatatypeSize());
                    }
                    calibObjs.add(createNewLineDataObject(calibrationParameterName,
                            Integer.toUnsignedLong(asap2Charact.getAddress()), dataBytes));
                } else {
                    report.addReportLine(CalibMergeReport.ReportLineType.ITEM_INCORRECT_DATA_SIZE,
                            calibrationParameterName, CalibMergeReport.ReportLineType.ITEM_INCORRECT_DATA_SIZE
                                    .formatDescription(dataBytes == null ? 0 : dataBytes.length));

                }
            }
        }

    }

    private boolean writeSourceIntoExportTarget(LineDataObject ldObj, long currentCalibAddressNoOffset) {
        long ldStrtAddress = ldObj.getStartAddress();
        long ldObjEndAddress = ldObj.getEndAddress();
        int targetIdxStrt = 0;

        LineDataObjectContainer ldContainer = reader.getLineDataContainer();
        Iterator<LineDataObject> iter = ldContainer.getLineDataContainerElements().iterator();
        while (iter.hasNext()) {
            LineDataObject targetObj = iter.next();
            long targetEndAddress = targetObj.getEndAddress();

            if (targetObj.getStartAddress() <= ldStrtAddress && ldStrtAddress <= targetEndAddress) {

                if (ldObjEndAddress <= targetObj.getEndAddress()) {
                    ldContainer.fillDataSingleLineObject(ldObj, targetObj);
                    return true; // found it
                } else {
                    int targetIdxEnd = targetIdxStrt;
                    long lastEndAddress = targetEndAddress;
                    while (iter.hasNext()) {
                        LineDataObject lastObj = iter.next();
                        targetIdxEnd++;

                        // check for gap
                        if (lastObj.getStartAddress() != lastEndAddress + 1) {
                            report.addReportLine(CalibMergeReport.ReportLineType.GAP_IN_BASEFILE, ldObj.getName(),
                                    CalibMergeReport.ReportLineType.GAP_IN_BASEFILE.formatDescription(lastEndAddress));
                            return false; // Not found: gap present
                        }

                        // check if endAddress within range
                        if (lastObj.getStartAddress() <= ldObjEndAddress
                                && ldObjEndAddress <= lastObj.getEndAddress()) {
                            ldContainer.fillDataMultipleMultipleLineObjects(ldObj, targetIdxStrt, targetIdxEnd);
                            return true; // found it
                        }
                        // search for next addresses
                        lastEndAddress = lastObj.getEndAddress();
                    }
                }
                // not found: start address OK, end address NOT OK => range not found
                report.addReportLine(CalibMergeReport.ReportLineType.RANGE_NOT_FOUND, ldObj.getName(),
                        CalibMergeReport.ReportLineType.RANGE_NOT_FOUND
                                .formatDescription(ldStrtAddress, ldObjEndAddress));
                return false;
            }
            targetIdxStrt++;
        }

        // address not found
        report.addReportLine(CalibMergeReport.ReportLineType.ITEM_ADDR_NOT_FOUND, ldObj.getName(),
                CalibMergeReport.ReportLineType.ITEM_ADDR_NOT_FOUND
                        .formatDescription(currentCalibAddressNoOffset, ldObj.getDataSize(), ldObj.getStartAddress()));
        return false;
    }

    private void checkReportOk() {
        mergeResult = readResult;
        if (report.getTotalLineCount() > 0) {
            if (report.getOkLineCount() == 0) {
                mergeResult = ImportReaderResult.READER_RESULT_ERROR;
            } else if (report.getTotalLineCount() > report.getOkLineCount()
                    && mergeResult != ImportReaderResult.READER_RESULT_ERROR) {
                mergeResult = ImportReaderResult.READER_RESULT_WARNING;
            }
        }
    }

    /**
     * Write merged data into given file
     *
     * @param file
     * @throws IOException
     */
    public void writeToFile(File file) throws IOException {
        // last: create/overwrite file with data from ihexObjs
        writer.writeToFile(calib.getTitle(), file, reader.getLineDataContainer().getLineDataContainerElements());
    }

    /**
     * Import report
     *
     * @return string holding result report of the import
     */
    public String getMergeReport() {
        return report.buildImportReport();
    }

    /**
     * Get result of merge action
     *
     * @return OK if activity has been successful
     */
    public ImportReaderResult getMergeResult() {
        return mergeResult;
    }

    public ImportReaderResult getReadResult() {
        return readResult;
    }

    public String getReadResultTxt() {
        return readResultTxt;
    }
}
