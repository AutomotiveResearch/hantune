/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.util.ArrayList;
import java.util.List;

/**
 * A default implementation of Table. Can be used as a placeholder reference by
 * a table editor.
 *
 * @author Michiel Klifman
 */
public class DefaultTable extends DefaultReference implements Table {

    private static final String NOT_AVAILABLE = "Not Availabe";

    private final String name;
    private final String source;

    public DefaultTable(String name, String source) {
        this.name = name;
        this.source = source;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public double getValueAt(int row, int column) {
        return 0;
    }

    @Override
    public void setValueAt(double value, int row, int column) {
    }

    @Override
    public boolean isInSyncAt(int row, int column) {
        return false;
    }

    @Override
    public double getAxisPointsValueAt(int axis, int index) {
        return 0;
    }

    @Override
    public boolean isAxisPointInSyncAt(int axis, int index) {
        return false;
    }

    @Override
    public boolean isValueValid(double value) {
        return false;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    public List<String> getAxisDescr() {
        List<String> axisDescriptions = new ArrayList<>();
        axisDescriptions.add(NOT_AVAILABLE);
        return axisDescriptions;
    }

    @Override
    public String getAxisColumnDescr() {
        return NOT_AVAILABLE;
    }

    @Override
    public String getAxisRowDescr() {
        return NOT_AVAILABLE;
    }

    @Override
    public void send(double value, int column, int row) {
    }

    @Override
    public void sendAxisPoint(double value, int axis, int index) {
    }

    @Override
    public void addListener(TableListener listener) {
    }

    @Override
    public void removeListener(TableListener listener) {
    }

    @Override
    public void addStateListener(ReferenceStateListener listener) {
    }

    @Override
    public void removeStateListener(ReferenceStateListener listener) {
    }

}
