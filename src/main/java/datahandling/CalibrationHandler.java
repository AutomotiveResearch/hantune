/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Data.Data;
import ErrorLogger.AppendToLogfile;
import HANtune.HANtune;
import HANtune.HANtuneManager;
import XCP.XCP;
import haNtuneHML.Calibration;
import haNtuneHML.Calibration.ASAP2;
import util.MessagePane;
import util.ProgressDialog;
import util.Util;

/**
 * This class handles the creation of a calibration, the changes to a
 * calibration, the export and loading of a calibration.
 *
 * @author Mike
 */
public class CalibrationHandler {

    private static final CalibrationHandler INSTANCE = new CalibrationHandler();
    private CurrentConfig currentConfig = CurrentConfig.getInstance();

    /**
     * The constructor is private for singleton.
     */
    private CalibrationHandler() {

    }

    /**
     * gets an instance of CalibrationHandler.
     *
     * @return instance :instance of CalibrationHandler.
     */
    public static CalibrationHandler getInstance() {
        return INSTANCE;
    }

    /**
     * This method adds a new calibration to the project.
     *
     * @param name :The name of the calibration.
     * @return id of calibration just added.
     */
    public int newCalibration(final String name) {
        Calendar now = Calendar.getInstance();

        // add to project
        Calibration calibration = currentConfig.getHANtuneDocument().getHANtune().addNewCalibration();
        calibration.setTitle(name);
        calibration.setDate(now);
        calibration.setDateMod(now);

        // stores created calibration
        int id = currentConfig.getHANtuneDocument().getHANtune().sizeOfCalibrationArray() - 1;
        handleStoreCalibration(id);
        return id;
    }

    /**
     * This method Renames the specified calibration.
     *
     * @param id :The id of the calibration.
     * @param applicationName :The new name of the calibration.
     */
    public void renameCalibration(final int id, final String name) {
        currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id).setTitle(name);
        currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id)
                .setDateMod(Calendar.getInstance());
    }

    /**
     * This method copies the specified calibration within the project.
     *
     * @param id :The id of the calibration.
     */
    public void copyCalibration(final int id) {
        Calibration calibration = currentConfig.getHANtuneDocument().getHANtune().addNewCalibration();
        calibration.set(currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id).copy());
        calibration.setTitle("Copy of " + calibration.getTitle());
    }

    /**
     * This method removes the specified calibration from the project.
     *
     * @param id :The id of the calibration.
     */
    public void removeCalibration(final int id) {
        currentConfig.getHANtuneDocument().getHANtune().removeCalibration(id);
    }

    /**
     * This method updates the current calibration with the values in the
     * controller
     *
     * @param id :The id of the calibration.
     */
    public void updateCalibration(final int id) {
        // store the new values
        String updateCalibrationConfirmMessage
                = "Are you sure you want to update the calibration? \n This will overwrite the initial configuration file";
        if (id != -1
                && MessagePane.showConfirmDialog(updateCalibrationConfirmMessage, "Update calibration") == 0) {
            handleStoreCalibration(id);
        }

        // update last modified date
        currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id)
                .setDateMod(Calendar.getInstance());
    }

    /**
     * This method handles the update of the XML content related to the
     * currently active calibration.
     *
     * @param id :The id of the calibration.
     */
    public void handleStoreCalibration(final int id) {
        // clear calibration
        currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id).getASAP2List().clear();

        // set modification date
        currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id)
                .setDateMod(Calendar.getInstance());

        storeCalibration(id);
    }

    /**
     * This method initializes the default parameters values
     */
    public void handleParameterCalibration() {
        XCP xcp = currentConfig.getXcp();
        if (xcp != null && xcp.isConnected()) {
            // request all parameters
            for (String asap2Ref : currentConfig.getASAP2Data().getASAP2Characteristics().keySet()) {
                calibrateParameter(asap2Ref);
            }
        }
    }

//    /**
//     * This method handles the export of the specified calibration.
//     *
//     * @param calib: Holds the calibration data
//     * @param file: The filename used when exporting a calibration.
//     * @param expWriter: contains the writer method used for export
//     * @return True if the export was successful.
//     */
//    public boolean handleExportCalibration(Calibration calib, File file, CalibrationExportWriter expWriter) {
//        try {
//            // do actual writing to export file here, using appropriate method
//            expWriter.writeCalibration(calib, file);
//
//            MessagePane.showInfo("Successfully exported calibration data to " + file.getName());
//
//            return true;
//        } catch (Exception e) {
//            AppendToLogfile.appendError(Thread.currentThread(), e);
//            currentConfig.getHANtuneManager().setError(e.getMessage());
//
//            return false;
//        }
//    }
    /**
     * This method creates a progressDialog and starts a calibration.
     *
     * @param id :The id of the calibration.
     */
    public void loadCalibration(final int id, final HANtune hantune) {
        String loadCalibrationConfirmMessage = "Are you sure you want to load this calibration into Working Parameter Set?";
        if (id != -1 && MessagePane.showConfirmDialog(loadCalibrationConfirmMessage, "Load calibration") == 0) {
            ProgressDialog progressDialog = new ProgressDialog();

            int length = calculateCalibrationLength(id);
            progressDialog.setProgressBarMaximum(length);
            progressDialog.showDialog();
            hantune.setEnabled(false);

            startCalibration(id, progressDialog);
        }
    }

    /**
     * This method calculates the length of the calibration.
     *
     * @param id :The id of the calibration.
     * @return The progress length
     */
    private int calculateCalibrationLength(final int id) {
        return currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id).getASAP2List().size();
    }

    /**
     * This method creates a new CalibrationThread and starts the calibration.
     *
     * @param id :The id of the calibration.
     * @param progressDialog :The progressDialog to keep track of the progress
     * of the calibration.
     */
    private void startCalibration(final int id, final ProgressDialog progressDialog) {
        try {
            CalibrationThread calibrationThread = new CalibrationThread(id, progressDialog);
            calibrationThread.start();
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            Logger.getLogger(HANtuneManager.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * This method updates the XML content related to the currently active
     * calibration.
     *
     * @param id :The id of the calibration.
     */
    private void storeCalibration(final int id) {
        for (ASAP2Characteristic asap2Characteristic : currentConfig.getASAP2Data().getASAP2Characteristics()
                .values()) {

            @SuppressWarnings("deprecation")
            Data data = currentConfig.getASAP2Data().getData(asap2Characteristic.getName());
            if (data != null) {
                ASAP2 asap2
                        = currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id).addNewASAP2();
                asap2.setName(asap2Characteristic.getName());
                fillAsap2ByteArrayValue(asap2Characteristic, asap2, data);
            }
        }
    }

    /**
     * This method fills the asap2ByteArrayValue with the charData obtained from
     * the given data.
     *
     * @param asap2Characteristic :The asap2Characteristic that needs to be
     * filled.
     * @param asap2 :The asap2 object to set the data in.
     * @param data :The data of the asap2Characteristic.
     */
    private void fillAsap2ByteArrayValue(final ASAP2Characteristic asap2Characteristic, ASAP2 asap2,
            final Data data) {
        int cols = asap2Characteristic.getColumnCount();
        int rows = asap2Characteristic.getRowCount();

        ASAP2Characteristic.Type type = asap2Characteristic.getType();
        switch (type) {
            case VALUE:
                char[] valueCharData = asap2Characteristic.convertValue(data.value);
                asap2.setByteArrayValue(Util.charA2byteA(valueCharData));
                break;
            case AXIS_PTS:
            case CURVE: {
                char[] charData = convertToCharData(asap2Characteristic, data, cols);
                asap2.setByteArrayValue(Util.charA2byteA(charData));
                break;
            }
            case MAP: {
                char[] mapCharData = convertToCharData(asap2Characteristic, data, cols, rows);
                asap2.setByteArrayValue(Util.charA2byteA(mapCharData));
                break;
            }
        }
    }

    /**
     * This method converts the given data to a char array with values.
     *
     * @param asap2Characteristic :The asap2Characteristic that needs to be
     * filled.
     * @param data :The data of the asap2Characteristic.
     * @param cols :The number of columns.
     */
    private char[] convertToCharData(final ASAP2Characteristic asap2Characteristic, final Data data,
            final int cols) {
        int datatypeSize = asap2Characteristic.getDatatypeSize();
        char[] charData = new char[cols * datatypeSize];
        for (int j = cols - 1; j >= 0; j--) {
            char[] value = asap2Characteristic.convertValue(((Object[]) data.value)[j]);
            System.arraycopy(value, 0, charData, j * datatypeSize, datatypeSize);
        }
        return charData;
    }

    /**
     * This method converts the given data to a char array with values.
     *
     * @param asap2Characteristic :The asap2Characteristic that needs to be
     * filled.
     * @param data :The data of the asap2Characteristic.
     * @param cols :The number of columns.
     * @param rows :The number of rows.
     */
    private char[] convertToCharData(final ASAP2Characteristic asap2Characteristic, final Data data,
            final int cols, final int rows) {
        int datatypeSize = asap2Characteristic.getDatatypeSize();
        char[] charData = new char[cols * rows * datatypeSize];
        for (int j = cols - 1; j >= 0; j--) {
            for (int k = rows - 1; k >= 0; k--) {
                char[] value = asap2Characteristic.convertValue(((Object[][]) data.value)[j][k]);
                System.arraycopy(value, 0, charData, (j * rows + k) * datatypeSize, datatypeSize);
            }
        }
        return charData;
    }

    /**
     * This method calibrates all parameters values at the
     * currentConfig.getXcp() slave device.
     *
     * @param asap2ref :The asap2 reference of the parameter.
     */
    private void calibrateParameter(final String asap2ref) {
        XCP xcp = currentConfig.getXcp();

        if (xcp != null && xcp.isConnected()) {
            @SuppressWarnings("deprecation")
            Data data = currentConfig.getASAP2Data().getData(asap2ref);
            if (data != null) {
                setParameterBasedOnType(asap2ref, data);
            }
        }
    }

    /**
     * This method checks for the type of the asap2Characteristic and sets the
     * parameter accordingly to the type.
     *
     * @param asap2ref :The asap2ref of the asap2Characteristic.
     * @param data :The data of the asap2Characteristic.
     */
    private void setParameterBasedOnType(final String asap2ref, final Data data) {
        ASAP2Characteristic asap2Characteristic
                = currentConfig.getASAP2Data().getASAP2Characteristics().get(asap2ref);
        int cols = asap2Characteristic.getColumnCount();
        int rows = asap2Characteristic.getRowCount();

        ASAP2Characteristic.Type type = asap2Characteristic.getType();
        switch (type) {
            case VALUE: {
                setCalibrationParameter(data, asap2ref);
                break;
            }
            case AXIS_PTS:
            case CURVE: {
                setCalibrationParameter(data, asap2ref, cols);
                break;
            }
            case MAP: {
                setCalibrationParameter(data, asap2ref, cols, rows);
                break;
            }
        }
    }

    /**
     * This method converts the data to an object and sets the parameter of the
     * asap2Characteristic.
     *
     * @param data :The data of the asap2Characteristic.
     * @param asap2ref :The asap2ref of the asap2Characteristic.
     */
    private void setCalibrationParameter(final Data data, final String asap2ref) {
        SendParameterDataHandler sendDataHandler
                = currentConfig.getHANtuneManager().getSendParameterDataHandler();

        Object obj = data.value;
        sendDataHandler.setParameter(asap2ref, obj, null, true);
    }

    /**
     * This method converts the data to an object array and sets the parameter
     * of the asap2Characteristic.
     *
     * @param data :The data of the asap2Characteristic.
     * @param asap2ref :The asap2ref of the asap2Characteristic.
     * @param cols :The number of columns.
     */
    private void setCalibrationParameter(final Data data, final String asap2ref, final int cols) {
        SendParameterDataHandler sendDataHandler
                = currentConfig.getHANtuneManager().getSendParameterDataHandler();

        Object[] obj = (Object[]) data.value;
        for (int j = 0; j < cols; j++) {
            int[] position = {j};
            sendDataHandler.setParameter(asap2ref, obj[j], position, true);
        }
    }

    /**
     * This method converts the data to an two dimensional object array and sets
     * the parameter of the asap2Characteristic.
     *
     * @param data :The data of the asap2Characteristic.
     * @param asap2ref :The asap2ref of the asap2Characteristic.
     * @param cols :The number of columns.
     * @param rows :The number os rows.
     */
    private void setCalibrationParameter(final Data data, final String asap2ref, final int cols, final int rows) {
        SendParameterDataHandler sendDataHandler
                = currentConfig.getHANtuneManager().getSendParameterDataHandler();

        Object[][] obj = ((Object[][]) data.value);
        for (int j = 0; j < cols; j++) {
            for (int k = 0; k < rows; k++) {
                int[] position = {j, k};
                sendDataHandler.setParameter(asap2ref, obj[j][k], position, true);
            }
        }
    }
}
