/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import ASAP2.ASAP2Characteristic;
import ErrorLogger.AppendToLogfile;
import haNtuneHML.Calibration;
import util.Util;

/**
 * This class exports a calibration to a MATLAB M-File.
 *
 * @author Mike
 */
public class CalibrationExportWriterMatlab implements CalibrationExportWriter {

    private CurrentConfig currentConfig;
    private PrintStream printStream;
    private List<String> notFoundNames = new ArrayList<>();

    /**
     * The constructor of the CalibrationExporter.
     */
    public CalibrationExportWriterMatlab() {
        currentConfig = CurrentConfig.getInstance();
    }


    /**
     * Export the specified calibration into a Matlab M-file.
     *
     * @param calibration: Holds the actual calibration data
     * @param file:        The filename of the calibration.
     */
    @Override
    public void writeCalibration(final Calibration calibration, final File file) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            printStream = new PrintStream(fileOutputStream);

            printCalibrationHeader(calibration);
            printCalibrationParameters(calibration);

            // close file
            printStream.close();
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            currentConfig.getHANtuneManager().setError(e.getMessage());
        }
    }

    public void writeToFile(String headerString, File file, List<LineDataObject> ihObjs) throws IOException {
        // not used, empty function
    }


    public String getWarningLines() {
        if (notFoundNames.isEmpty()) {
            return "";
        } else {
            StringBuilder warn = new StringBuilder(
                "<html><b>Warning: " + notFoundNames.size() + " calibration items could not be exported:</b></html>\n");
            notFoundNames.forEach(e -> warn.append(" - " + e + "\n"));
            return warn.toString();
        }
    }


    /**
     * This method prints the header of the calibration export file.
     */
    private void printCalibrationHeader(Calibration calib) {
        printStream.println("%% CALIBRATION ASAP2 EXPORT");
        printStream.println("%  Project:       " + currentConfig.getProjectFile());
        printStream.println("%  Calibration:   " + calib.getTitle());
        printStream.println("%  Created:       " + calib.getDate());
        printStream.println("%  Last Modified: " + calib.getDateMod());
        printStream.println("");
    }


    /**
     * This method handles the printing of a calibration parameter.
     *
     * @param calibration : The calibration to print
     */
    private void printCalibrationParameters(Calibration calibration) {
        printStream.println("%% PARAMETERS");
        ASAP2Characteristic asap2Characteristic;

        for (Calibration.ASAP2 calItem : calibration.getASAP2List()) {
            asap2Characteristic =
                currentConfig.getASAP2Data().getASAP2Characteristics().get(calItem.getName());

            if (asap2Characteristic != null) {
                printCalibrationParameter(calItem, asap2Characteristic);
            } else {
                notFoundNames.add(calItem.getName() + ": ASAP2 characteristic not found");
            }
        }
    }


    /**
     * This method determines the value of the given calibration item using ASAP2Characteristic and creates a string
     * value.
     */
    private String determineValue(final ASAP2Characteristic asap2Characteristic, final Calibration.ASAP2 cal) {
        byte[] asap2ByteArray = cal.getByteArrayValue();
        int asap2ArrayLength = cal.getByteArrayValue().length;
        if (!asap2Characteristic.checkDataSize(asap2ArrayLength)) {
            notFoundNames.add(asap2Characteristic.getName() + ": ASAP2 data length doesn't match");
            return "";
        }

        char[] data = Util.byteA2charA(asap2ByteArray, asap2ByteArray.length);
        if (data.length > 0) {
            return createStringValueByType(asap2Characteristic, data);
        } else {
            notFoundNames.add(asap2Characteristic.getName() + ": ASAP2 data empty");
            return "";
        }
    }

    /**
     * This method creates a string value by the type of the asap2Characteristic.
     *
     * @param data :The data of the asap2Characteristic.
     * @return returns a string value of the given data.
     */
    private String createStringValueByType(final ASAP2Characteristic asap2Characteristic, final char[] data) {
        int cols = asap2Characteristic.getColumnCount();
        int rows = asap2Characteristic.getRowCount();
        String value = "";

        switch (asap2Characteristic.getType()) {
            case VALUE:
                value = createStringValue(asap2Characteristic, data);
                break;

            case AXIS_PTS:
            case CURVE:
                value = createStringValue(asap2Characteristic, data, cols);
                break;

            case MAP:
                value = createStringValue(asap2Characteristic, data, cols, rows);
                break;

            default:
                break;
        }
        return value;
    }

    /**
     * This method creates a string value by converting the data and replacing
     * comma's.
     *
     * @param data :The data of the asap2Characteristic.
     * @return returns a string value of the given data.
     */
    private String createStringValue(final ASAP2Characteristic asap2Characteristic, final char[] data) {
        return Util.getStringValue(asap2Characteristic.convertValue(data), asap2Characteristic.getDecFormat()).replaceAll(",", ".");
    }

    /**
     * This method creates a string value by converting the data and replacing
     * comma's.
     *
     * @param data :The data of the asap2Characteristic.
     * @param cols :The number of columns.
     * @return returns a string value of the given data.
     */
    private String createStringValue(final ASAP2Characteristic asap2Characteristic, final char[] data, final int cols) {
        int byteCount = asap2Characteristic.getDatatypeSize();
        String value = "";

        for (int j = 0; j < cols; j++) {
            char[] val = new char[byteCount];
            System.arraycopy(data, j * val.length, val, 0, val.length);
            if (j > 0) {
                value += ", ";
            }
            value +=
                Util.getStringValue(asap2Characteristic.convertValue(val), asap2Characteristic.getDecFormat()).replaceAll(",", ".");
        }
        value = "[" + value + "]";

        return value;
    }

    /**
     * This method creates a string value by converting the data and replacing
     * comma's.
     *
     * @param data :The data of the asap2Characteristic.
     * @param cols :The number of columns.
     * @param rows :The number of rows.
     * @return returns a string value of the given data.
     */
    private String createStringValue(ASAP2Characteristic asap2Characteristic, char[] data, int cols,
                                     int rows) {
        int byteCount = asap2Characteristic.getDatatypeSize();
        String value = "";

        for (int j = 0; j < rows; j++) {
            if (j > 0) {
                value += "; ";
            }
            for (int k = 0; k < cols; k++) {
                char[] val = new char[byteCount];
                System.arraycopy(data, (k * rows + j) * val.length, val, 0, val.length);
                if (k > 0) {
                    value += ", ";
                }
                value += Util.getStringValue(asap2Characteristic.convertValue(val),
                    asap2Characteristic.getDecFormat()).replaceAll(",", ".");
            }
        }
        value = "[" + value + "]";

        return value;
    }

    /**
     * This method determines the units of the asap2Characteristic.
     *
     * @return returns a string representation of the asap2Characteristic
     * units.
     */
    private String determineUnits(ASAP2Characteristic asap2Characteristic) {
        String units = asap2Characteristic.getConversion();
        if (currentConfig.getASAP2Data().getASAP2CompuMethods().containsKey(units)) {
            units = currentConfig.getASAP2Data().getASAP2CompuMethods().get(units).getUnit();
        } else {
            units = "";
        }

        return units;
    }

    /**
     * This method determines the datatype of the asap2Characteristic.
     *
     * @return returns a string representation of the asap2Characteristic
     * datatype.
     */
    private String determineDatatype(ASAP2Characteristic asap2Characteristic) {
        String datatype = "";
        if (asap2Characteristic.getFactor() != 1) {
            // fixed point
            int wordLength = asap2Characteristic.getDatatypeSize() * 8;
            int fractionLength = (int) (Math.log(1 / asap2Characteristic.getFactor()) / Math.log(2));
            datatype = "fixdt(" + datatypeSigned(asap2Characteristic) + ", " + wordLength + ", " + fractionLength + ")";
        } else {
            datatype = createDatatypeString(asap2Characteristic);
        }

        return datatype;
    }

    /**
     * This method checks if the asap2Characteristic datatype if signed or not.
     *
     * @return returns 1 if datatype is signed or 0 if datatype is unsigned.
     */
    private int datatypeSigned(ASAP2Characteristic asap2Characteristic) {
        switch (asap2Characteristic.getDatatype()) {
            case SBYTE:
            case SWORD:
            case SLONG:
            case A_INT64:
                return 1;

            default:
                return 0;
        }
    }

    /**
     * This method creates a datatype string of the asap2Characteristic.
     *
     * @return returns a string representation of the asap2Characteristic
     * datatype.
     */
    private String createDatatypeString(ASAP2Characteristic asap2Characteristic) { //ticket refs#276
        String datatype = "";
        switch (asap2Characteristic.getDatatype()) {
            case BOOLEAN: {
                datatype = "boolean";
                break;
            }
            case UBYTE: {
                datatype = "uint8";
                break;
            }
            case SBYTE: {
                datatype = "int8";
                break;
            }
            case UWORD: {
                datatype = "uint16";
                break;
            }
            case SWORD: {
                datatype = "int16";
                break;
            }
            case ULONG: {
                datatype = "uint32";
                break;
            }
            case SLONG: {
                datatype = "int32";
                break;
            }
            case A_UINT64: {
                datatype = "uint64";
                break;
            }
            case A_INT64: {
                datatype = "int64";
                break;
            }
            case FLOAT32_IEEE: {
                datatype = "single";
                break;
            }
            case FLOAT64_IEEE: {
                datatype = "double";
                break;
            }
        }

        return datatype;
    }

    /**
     * This method prints the given calibration parameter of the
     * asap2Characteristic.
     *
     * @param calItem      :The calibration object.
     * @param asap2Charact :The characterization of the object
     */
    private void printCalibrationParameter(final Calibration.ASAP2 calItem, ASAP2Characteristic asap2Charact) {
        String itemValue = determineValue(asap2Charact, calItem);
        if (itemValue.isEmpty()) {
            return;
        }

        String name = calItem.getName();

        printStream.println(name + " = Simulink.Parameter;");
        printStream.println("    " + name + ".RTWInfo.StorageClass = 'ExportedGlobal';");
        printStream.println("    " + name + ".Value = " + itemValue + ";");  //changed .value to .Value to be compatible
        // for MatLab 2014 and newer.

        printStream.println("    " + name + ".Description = '" + asap2Charact.getDescription() + "';"); //changed
        // .description to .Description...

        if (asap2Charact.isLowerLimitEnabled()) {
            printStream.println("    " + name + ".Min = " + asap2Charact.getMinimum() + ";"); //changed .min to .Min...
        }

        if (asap2Charact.isUpperLimitEnabled()) {
            printStream.println("    " + name + ".Max = " + asap2Charact.getMaximum() + ";"); //changed .max to .Max...
        }
        printStream.println("    " + name + ".DocUnits = '" + determineUnits(asap2Charact) + "';");
        printStream.println("    " + name + ".DataType = '" + determineDatatype(asap2Charact) + "';"); //ticket refs#276
        printStream.println("");
    }

}
