/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package DAQList;

import ASAP2.ASAP2Measurement;
import HANtune.HANtune;
import XCP.XCPDaqList;
import datahandling.CurrentConfig;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to store all DAQ list information.
 * @author Jeroen
 */
public class DAQList {

    // attributes of a DAQ list
    private String name = "";
    private int relativeId = -1;
    private float prescaler = 0;
    private boolean active = false;
    private boolean reload = false;
    private XCPDaqList xcpDaqList = new XCPDaqList();
    private List<ASAP2Measurement> ASAP2Measurements = new ArrayList<>();

    public DAQList(String name) {
        this.name = name;
        this.xcpDaqList.setName(name);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
        this.xcpDaqList.setName(name);
    }

    /**
     * @return the relative identifier
     */
    public int getRelativeId() {
        return relativeId;
    }

    /**
     * @param relativeId the relative identifier
     */
    public void setRelativeId(int relativeId) {
        this.relativeId = relativeId;
        this.xcpDaqList.setEventNo((char)relativeId);
    }

    /**
     * @return the screenId
     */
    public String getScreenId() {
        return "D" + (HANtune.getInstance().daqLists.indexOf(this) + 1);
    }

    /**
     * @return the prescaler
     */
    public float getPrescaler() {
        return prescaler;
    }


    /**
     * @param prescaler the prescaler to set
     */
    public void setPrescaler(float prescaler) {
        this.prescaler = prescaler;
        this.xcpDaqList.setPrescaler((char)Math.round(prescaler));
    }


    /**
     * @param slaveFrequency
     * @return the sample frequency for the given slave frequency.
     */
    public float getSampleFrequency(float slaveFrequency){
        return slaveFrequency == 0 ? slaveFrequency : slaveFrequency / prescaler;
    }

    /**
     * @return a formatted String for the current frequency
     */
    public String getSampleFrequency() {
        float slaveFrequency = CurrentConfig.getInstance().getXcpsettings().getSlaveFrequency();
        float sampleFrequency = getSampleFrequency(slaveFrequency);
        String unit = " Hz";
        if (sampleFrequency > 1000) {
            sampleFrequency = sampleFrequency / 1000;
            unit = " kHz";
        }
        return new DecimalFormat("0.##").format(sampleFrequency) + unit;
    }

    /**
     * @return whether the DAQ list is active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active whether the DAQ list is active
     */
    public void setActive(boolean active) {
        this.active = active;
        HANtune.getInstance().updateDaqListListeners();
    }

    /**
     * @return the XCPDaqList for this DAQList
     */
    public XCPDaqList getXCPDaqList() {
        return xcpDaqList;
    }

    /**
     * Sets the XCPDaqList for this DAQList
     * @param xcpDaqList
     */
    public void setXCPDaqList(XCPDaqList xcpDaqList) {
        this.xcpDaqList = xcpDaqList;
    }

    /**
     * @return whether the DAQ list is active
     */
    public boolean mustReload() {
        return reload;
    }

    /**
     * @param reload whether the DAQ list must be reloaded at the slave
     */
    public void setReload(boolean reload) {
        this.reload = reload;
    }

    /**
     * @return the ASAP2Measurements
     */
    public List<ASAP2Measurement> getASAP2Measurements() {
        return ASAP2Measurements;
    }

    /**
     * @param ASAP2Measurements the ASAP2Measurements to set
     */
    public void setASAP2Measurements(List<ASAP2Measurement> ASAP2Measurements) {
        this.ASAP2Measurements = ASAP2Measurements;
    }

    public void addMeasurement(ASAP2Measurement measurement) {
        if (!ASAP2Measurements.contains(measurement)) {
            ASAP2Measurements.add(measurement);
        }
    }

    public void removeMeasurement(ASAP2Measurement measurement) {
        ASAP2Measurements.remove(measurement);
    }

    public boolean contains(ASAP2Measurement measurement) {
        return ASAP2Measurements.contains(measurement);
    }

    @Override
    public String toString() {
        return getScreenId();
    }
}
