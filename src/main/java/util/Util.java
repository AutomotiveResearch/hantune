/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package util;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ErrorLogger.AppendToLogfile;
import nl.han.hantune.scripting.Console;
import peak.can.basic.TPCANBaudrate;
import org.apache.commons.io.FilenameUtils;

/**
 * Contains various methods used throughout the application.
 *
 * @author Aart-Jan
 */
public class Util {

    public static final String LF = System.lineSeparator();

    // Utility classes should not have public constructors
    private Util() {
    }

    // holds decimal separator for current locale
    public static final char DEC_SEPARATOR_LOCALE
            = ((DecimalFormat) DecimalFormat.getInstance()).getDecimalFormatSymbols().getDecimalSeparator();

    // holds decimal separator for HANtune HML files
    public static final char DEC_SEPARATOR_HML = '.';

    /**
     * Converts a byte to a character.
     *
     * @param data
     * @return
     */
    public static char byte2char(byte data) {
        return (char) (data & 0xFF);
    }

    /**
     * Converts an array of bytes to an array of characters.
     *
     * @param b_data
     * @param length
     * @return
     */
    public static char[] byteA2charA(byte[] b_data, int length) {
        char[] c_data = new char[length];
        int i;
        for (i = 0; i < length; i++) {
            c_data[i] = byte2char(b_data[i]);
        }
        return c_data;
    }

    /**
     * Converts a character to a byte.
     *
     * @param data
     * @return
     */
    public static byte char2byte(char data) {
        return (byte) data;
    }

    /**
     * Converts an array of characters to an array of bytes.
     *
     * @param c_data
     * @return
     */
    public static byte[] charA2byteA(char[] c_data) {
        byte[] b_data = new byte[c_data.length];
        int i;
        for (i = 0; i < c_data.length; i++) {
            b_data[i] = char2byte(c_data[i]);
        }
        return b_data;
    }

    /**
     * Reverses the order of character elements of an array.
     *
     * @param array
     * @return
     */
    public static char[] reverseArray(char[] array) {
        char[] tmp = new char[array.length];
        int i, j = 0;
        for (i = array.length - 1; i >= 0; i--) {
            tmp[j] = array[i];
            j++;
        }
        return tmp;
    }

    /**
     * Swaps the bytes in given inBytes in groups of itemSize
     *
     * @param inBytes contains bytes to be swapped
     * @param itemSize contains size of items in array
     * @return new array of same size as inBytes with data bytes swapped in
     * groups of itemSize
     */
    public static byte[] swapEndianness(byte[] inBytes, int itemSize) {
        ByteBuffer bb = ByteBuffer.allocate(inBytes.length);
        switch (itemSize) {
            case 2:
                bb.asShortBuffer().put(ByteBuffer.wrap(inBytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer());
                break;

            case 4:
                bb.asIntBuffer().put(ByteBuffer.wrap(inBytes).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer());
                break;

            case 8:
                bb.asLongBuffer().put(ByteBuffer.wrap(inBytes).order(ByteOrder.LITTLE_ENDIAN).asLongBuffer());
                break;

            default:
                return inBytes; // don't change anything
        }

        return bb.array();
    }

    /**
     * Returns the filename without extension given filename.
     *
     * @param name
     * @return filename, without extension
     */
    public static String removeExtension(String name) {
        return FilenameUtils.removeExtension(name);
    }

    /**
     * Returns the file extension of a given filename.
     *
     * @param s
     * @return
     */
    public static String getExtension(String s) {
        String extension = null;
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            extension = s.substring(i + 1).toLowerCase();
        }
        return extension;
    }

// TODO remove this function
    /**
     * Returns the filename out of a given full filepath.
     *
     * @param s
     * @return
     */
    public static String getFilename(String s) {
        String[] t = s.split("\\x5c");
        if (t.length > 0) {
            return t[t.length - 1];
        }
        return "";
    }

    /**
     * Get project name
     *
     * @param projectFile
     * @return project name
     */
    public static String getProjectName(File projectFile) {
        String projectName = "NewProject";
        if (projectFile != null) {
            projectName = projectFile.getName();
            projectName = projectName.substring(0, projectName.lastIndexOf('.'));
        }
        return projectName;
    }

    /**
     * Method to convert value to string with current locale
     *
     * @param value holds a number to be converted to string equivalent in
     * current locale
     * @return string containing representation of value in current locale.
     * Grouping will be removed
     */
    public static String getStringValLocale(final Number value) {
        NumberFormat numberFormatter = NumberFormat.getNumberInstance();
        numberFormatter.setGroupingUsed(false);

        return numberFormatter.format(value);
    }

    public static double getDoubleValueLocale(final String value) throws ParseException {
        // use the default locale
        return NumberFormat.getInstance().parse(value).doubleValue();
    }

    /**
     * Method to convert a value to a formated string.
     *
     * @param value generic object value to convert.
     * @param format format of the formatted value.
     * @return String: new string value.
     */
    public static String getStringValue(final Object value, final String format) {
        if (value instanceof Number) {
            return new DecimalFormat(format).format(value);
        }
        throw new NumberFormatException("Value is not a Number");
    }

    /**
     * Returns a string representing the binary value of value, if value is a
     * Byte, Short or Integer.
     *
     * @param value the value to be parsed
     * @return a binary representation of value
     */
    public static String getBinaryStringValue(Object value) {
        if (value instanceof Byte
                || value instanceof Short
                || value instanceof Integer) {
            return Integer.toBinaryString(((Number) value).intValue());
        } else {
            throw new NumberFormatException("Value not a supported type");
        }
    }

    /**
     * Returns a string representing the hexadecimal value of value, if value is
     * a Byte, Short, Integer or Long.
     *
     * @param value the value to be parsed
     * @return a hexadecimal representation of value
     */
    public static String getHexadecimalStringValue(Object value) {
        String valueString;
        if (value instanceof Byte
                || value instanceof Short
                || value instanceof Integer
                || value instanceof Long) {
            valueString = String.format("%02X", ((Number) value));
            return valueString;
        } else {
            throw new NumberFormatException("Value not a supported type");
        }
    }

    /**
     * @param text the text to trim
     * @param length the length of the result
     * @return the trimmed text
     */
    public static String trimLeadingCharacters(String text, int length) {
        return text.substring(text.length() - length, text.length());
    }

    /**
     * @param text the text to add the characters to
     * @param character the character
     * @param length the length of the result
     * @return the text that was padded with the provided character
     */
    public static String addLeadingCharacters(String text, char character, int length) {
        StringBuilder builder = new StringBuilder(text);
        while (builder.length() < length) {
            builder.insert(0, character);
        }
        return builder.toString();
    }

    /**
     * Inserts a space every nth character. Useful for dividing hexadecimal or
     * binary representations into parts separated by whitespace.
     *
     * @param text the text to insert the whitespace to
     * @param partSize the size each part should have
     * @return the text divided into parts by whitespace
     */
    public static String divideWithWhitespace(String text, int partSize) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if (i % partSize == 0) {
                builder.append(" ");
            }
            builder.append(text.charAt(i));
        }
        return builder.toString();
    }

    public static String getReadableDataRate(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"bit/s", "kbit/s", "Mbit/s", "Gbit/s", "Tbit/s"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    /**
     * Returns an integer value of the specified generic value object.
     *
     * @param value
     * @return
     */
    public static int getIntValue(Object value) {
        if (value instanceof Number) {
            return ((Number) value).intValue();
        }
        throw new NumberFormatException("Value is not a Number");
    }

    /**
     * Returns a double value of the specified generic value object.
     *
     * @param value
     * @return
     */
    public static double getDoubleValue(Object value) {
        if (value instanceof Number) {
            if (value instanceof Float) { // Fix rounding errors which are common when using floats
                return Double.valueOf(value.toString()).doubleValue();
            }
            return ((Number) value).doubleValue();
        }
        throw new NumberFormatException("Value is not a Number");
    }

    /**
     * Returns a date string of the specified calendar object.
     *
     * @param c
     * @return
     */
    public static String getDateString(Calendar c) {
        return c.get(Calendar.DAY_OF_MONTH) + "-" + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.YEAR) + " " + c.get(Calendar.HOUR_OF_DAY) + ":" + ((c.get(Calendar.MINUTE) < 10) ? "0" : "") + c.get(Calendar.MINUTE);
    }

    /**
     * Converts the specified baudrate object to a string representation.
     *
     * @param tpcanBaudrate
     * @return
     */
    public static int baud2bits(TPCANBaudrate tpcanBaudrate) {
        int bits = 0;

        switch (tpcanBaudrate) {
            case PCAN_BAUD_1M:
                bits = 1024 * 1024;
                break;
            case PCAN_BAUD_500K:
                bits = 500 * 1024;
                break;
            case PCAN_BAUD_250K:
                bits = 250 * 1024;
                break;
            case PCAN_BAUD_125K:
                bits = 125 * 1024;
                break;
            case PCAN_BAUD_100K:
                bits = 100 * 1024;
                break;
            case PCAN_BAUD_50K:
                bits = 50 * 1024;
                break;
            case PCAN_BAUD_20K:
                bits = 20 * 1024;
                break;
            case PCAN_BAUD_10K:
                bits = 10 * 1024;
                break;
            case PCAN_BAUD_5K:
                bits = 5 * 1024;
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }
        return bits;
    }

    /**
     * Converts the specified baudrate object to a string representation.
     *
     * @param tpcanBaudrate
     * @return
     */
    public static String baud2string(TPCANBaudrate tpcanBaudrate) {
        String returnValue = null;

        switch (tpcanBaudrate) {
            case PCAN_BAUD_1M:
                returnValue = "1M";
                break;
            case PCAN_BAUD_500K:
                returnValue = "500K";
                break;
            case PCAN_BAUD_250K:
                returnValue = "250K";
                break;
            case PCAN_BAUD_125K:
                returnValue = "125K";
                break;
            case PCAN_BAUD_100K:
                returnValue = "100K";
                break;
            case PCAN_BAUD_50K:
                returnValue = "50K";
                break;
            case PCAN_BAUD_20K:
                returnValue = "20K";
                break;
            case PCAN_BAUD_10K:
                returnValue = "10K";
                break;
            case PCAN_BAUD_5K:
                returnValue = "5K";
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }
        return returnValue;
    }

    /**
     * Converts the specified baudrate string to the related baudrate object.
     *
     * @param tpcanBaudrateStr
     * @return
     */
    public static TPCANBaudrate string2baud(String tpcanBaudrateStr) {
        TPCANBaudrate tpcanBaudrate = null;

        switch (tpcanBaudrateStr) {
            case "1M":
                tpcanBaudrate = TPCANBaudrate.PCAN_BAUD_1M;
                break;
            case "500K":
                tpcanBaudrate = TPCANBaudrate.PCAN_BAUD_500K;
                break;
            case "250K":
                tpcanBaudrate = TPCANBaudrate.PCAN_BAUD_250K;
                break;
            case "125K":
                tpcanBaudrate = TPCANBaudrate.PCAN_BAUD_125K;
                break;
            case "100K":
                tpcanBaudrate = TPCANBaudrate.PCAN_BAUD_100K;
                break;
            case "50K":
                tpcanBaudrate = TPCANBaudrate.PCAN_BAUD_50K;
                break;
            case "20K":
                tpcanBaudrate = TPCANBaudrate.PCAN_BAUD_20K;
                break;
            case "10K":
                tpcanBaudrate = TPCANBaudrate.PCAN_BAUD_10K;
                break;
            case "5K":
                tpcanBaudrate = TPCANBaudrate.PCAN_BAUD_5K;
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }
        return tpcanBaudrate;
    }

    /**
     * Returns the color object for the specified colorname.
     *
     * @param colorName
     * @return
     */
    public static Color getColor(String colorName) {
        try {
            Field field = Class.forName("java.awt.Color").getField(colorName.toLowerCase());
            return (Color) field.get(null);
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            return Color.gray;
        }
    }

    /**
     * Calculates the major tick interval (of a gauge) for the specified value
     * range.
     *
     * @param lower
     * @param upper
     * @return
     */
    public static double calcMajorTick(double lower, double upper) {
        double diff = upper - lower;
        int tmin = 2;
        int tmax = 15;

        if (diff >= tmin) {
            for (int i = 0; i < 10; i++) {
                double x = diff / Math.pow(10, i);

                double x3 = x;
                if ((int) x3 == x3 && x3 >= tmin && x3 <= tmax) {
                    return Math.pow(10, i);
                }

                double x2 = x / 5;
                if ((int) x2 == x2 && x2 >= tmin && x2 <= tmax) {
                    return 5 * Math.pow(10, i);
                }

                double x1 = x / 25;
                if ((int) x1 == x1 && x1 >= tmin && x1 <= tmax) {
                    return 25 * Math.pow(10, i);
                }
            }
            return (int) (diff / 10);
        } else if (diff < tmin) {
            for (int i = 0; i < 10; i++) {
                double x = diff * Math.pow(10, i);

                double x3 = x;
                if ((int) x3 == x3 && x3 >= tmin && x3 <= tmax) {
                    return 1 / Math.pow(10, i);
                }

                double x2 = x / 5;
                if ((int) x2 == x2 && x2 >= tmin && x2 <= tmax) {
                    return 5 / Math.pow(10, i);
                }

                double x1 = x / 25;
                if ((int) x1 == x1 && x1 >= tmin && x1 <= tmax) {
                    return 25 / Math.pow(10, i);
                }
            }
            return (diff / 10);
        }

        return 1;
    }

    /**
     * method to convert a value to a double
     *
     * @param value value to convert
     * @return Double: double value
     */
    public static double convertToDouble(final Object value) {
        if (value instanceof Number) {
            return Double.parseDouble(value.toString());
        } else {
            throw new NumberFormatException();
        }
    }

    public static String htmlToAscii(String text) {
        text = text.replace("<br>", "\n");
        text = text.replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "\t");
        text = text.replace("&nbsp;", " ");
        text = text.replace("<html>", "").replace("</html>", "");
        return text;
    }

    public static String asciiToHtml(String text) {
        text = text.replace("\n", "<br>");
        text = text.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        text = text.replace(" ", "&nbsp;");
        return "<html>" + text + "</html>";
    }

    public static String getFontString(Font font) {
        String name = font.getName();
        String style = getFontStyle(font.getStyle());
        String size = Integer.toString(font.getSize());
        return name + " " + style + " " + size;
    }

    public static String getFontStyle(int style) {
        switch (style) {
            case 1:
                return "BOLD";
            case 2:
                return "ITALIC";
            case 3:
                return "BOLDITALIC";
            default:
                return "PLAIN";
        }
    }

    public static <T> Object[] getFormatArgumentArray(T number, String text) {
        List<T> arguments = new ArrayList<>();
        int count = text.length() - text.replace("%", "").length();
        for (int i = 0; i < count; i++) {
            arguments.add(number);
        }
        return arguments.toArray();
    }

    public static int executeCommand(final String... command) throws IOException, InterruptedException {
        Process process = Runtime.getRuntime().exec(command);
        process.waitFor();
        return process.exitValue();
    }

    public static int executeCommand(final String command) throws IOException, InterruptedException {
        Process proc = Runtime.getRuntime().exec(command);
        proc.waitFor();
        return proc.exitValue();
    }

    /**
     * Given relative filename in relStr will be set to absolute path relative
     * to given base path.
     *
     * @param basePathAbsolute: must contain full path to a file (file doesn't
     * have to exist). Must be URI string. (file:/C:/a/b/c/d/name%20space)
     * @param relStr holds path to be converted. Must be URI String.
     * (../e/name%20space)
     * @return Absolute in default FileSystem (C:/a/b/c/e/name space)
     */
    public static String resolvePath(final File basePathAbsolute, String relStr) {
        try {
            URI elem = new URI(relStr);
            elem = basePathAbsolute.toURI().resolve(elem);

            File file = new File(elem.getPath());
            return file.getPath();
        } catch (URISyntaxException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            return relStr;
        }
    }

    /**
     * Filenames in nameList will be set to absolute paths relative to given
     * base path
     *
     * @param basePathAbsolute: must contain full path to a file (file doesn't
     * have to exist)
     * @param nameList holds path's to be converted. Input: URI String, output:
     * relative path in default FileSystem
     */
    public static void resolvePaths(final File basePathAbsolute, List<String> nameList) {
        for (int idx = 0; idx < nameList.size(); idx++) {
            nameList.set(idx, resolvePath(basePathAbsolute, nameList.get(idx)));
        }
    }

    /**
     * Relative path in absStr relative to given baseParentFile will be returned
     *
     * @param baseParentFile (C:/a/b/c/name)
     * @param absStr (C:/a/b/abs name)
     * @return relativized path. (../abs%20name)
     */
    public static String relativizePath(File baseParentFile, String absStr) {
        File relFile = new File(absStr);

        if (relFile.isAbsolute()) {
            URI relURI = new File(absStr).toURI();
            try {
                URI rsltURI = baseParentFile.toURI().relativize(relURI);
                if (relURI.compareTo(rsltURI) == 0) {
                    // could not be relativized as a URI (URI relativize() cannot handle ../../file),
                    // therefore try relativize as a path
                    return baseParentFile.toPath().relativize(relFile.toPath()).toString().replaceAll(" ", "%20").replace('\\', '/');
                } else {
                    return rsltURI.toString();
                }

            } catch (IllegalArgumentException e) {
                // can still convert if 'other' has a different root, therefore try to continue
                return relURI.toString();
            }
        } else {
            return relFile.toString().replaceAll(" ", "%20").replace('\\', '/');
        }
    }

    /**
     * Filenames in nameList will be set to relative paths relative to given
     * absolute base path
     *
     * @param baseFileAbs holds name with full path to a file (doesn't have to
     * be existing file)
     * @param nameList holds path's to be converted. Input: abs path in default
     * FileSystem. Output: relative URI's as string
     */
    public static void relativizePaths(File baseFileAbs, List<String> nameList) {
        File baseParentFile = baseFileAbs.getParentFile();

        for (int idx = 0; idx < nameList.size(); idx++) {
            nameList.set(idx, relativizePath(baseParentFile, nameList.get(idx)));
        }
    }

    /**
     *
     * @param path the path of the file to be opened
     */
    public static void openFile(String path) {
        try {
            Desktop.getDesktop().open(new File(path));
        } catch (IOException ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
        }
    }

    /**
     * @param path the path to the file in the folder to be opened
     */
    public static void openFileLocation(String path) {
        try {
            Desktop.getDesktop().open(new File(Paths.get(path).getParent().toString()));
        } catch (IOException ex) {
            Console.print("Unable to open file location: " + ex);
        }
    }

    public static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().startsWith("windows");
    }

    public static boolean isLinux() {
        return System.getProperty("os.name").toLowerCase().startsWith("linux");
    }
}
