/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package can;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ErrorLogger.AppendToLogfile;
import util.MessagePane;

/**
 * @author Michiel Klifman
 */
public class DbcParser {

    //Build a pattern to match CAN Messages in a CAN Dababase file
    private static final String NEW_LINE = "\\r?\\n";
    private static final String MESSAGE = "BO_";
    private static final String ID = "(\\d+)";
    private static final String MESSAGE_NAME = "([A-Za-z0-9_]+)";
    private static final String MESSAGE_DLC = "(\\d+)";
    private static final String MESSAGE_SENDING_NODE = "(.*)";
    private static final Pattern MESSAGE_PATTERN = Pattern.compile(
        MESSAGE + " " + ID + " " + MESSAGE_NAME + ": " + MESSAGE_DLC + " ?" + MESSAGE_SENDING_NODE + "?");

    //Build a pattern to match CAN Signals in the resulting match of a CAN Message from a CAN Database file
    private static final String SIGNAL = "SG_";
    private static final String SG_NAME = "([A-Za-z0-9_]+)";
    private static final String SG_MULTIPLEXER = "([Mm0-9]+)?";
    private static final String SG_START_BIT = "([-+]?[0-9]*)";
    private static final String SG_TYPE = "([-+])";
    private static final String SG_FORMAT = "(\\d+)";
    private static final String SG_LENGTH = "(\\d+)";
    private static final String SG_FACTOR = "([-+]?[0-9]*\\.?[0-9]+(?:[eE][-+]?[0-9]+)?)";
    private static final String SG_OFFSET = "([-+]?[0-9]*\\.?[0-9]+(?:[eE][-+]?[0-9]+)?)";
    private static final String SG_MIN = "([-+]?[0-9]*\\.?[0-9]+(?:[eE][-+]?[0-9]+)?)";
    private static final String SG_MAX = "([-+]?[0-9]*\\.?[0-9]+(?:[eE][-+]?[0-9]+)?)";
    private static final String SG_UNIT = "(.*)";
    private static final Pattern SIGNAL_PATTERN = Pattern.compile(SIGNAL+" "+SG_NAME+" ?"+SG_MULTIPLEXER+" : "+SG_START_BIT+"\\|"+SG_LENGTH+"@"
            +SG_FORMAT +SG_TYPE+ " \\("+SG_FACTOR+"\\,"+SG_OFFSET+"\\) \\["+SG_MIN+"\\|"+SG_MAX+"\\] \""+SG_UNIT+"\"");

    //Build a pattern to match comments in a CAN Database file
    private static final Pattern COMMENT_PATTERN = Pattern.compile("CM_ ("+MESSAGE+"|"+SIGNAL+") "+ID+" ?"+SG_NAME+"? \"((.*?|"+NEW_LINE+")*)\";");
    //no need to instantiate
    private DbcParser() {
    }


    /**
     * Reads a CAN Database file, parses it and returns a DbcObject
     *
     * @param file the CAN Database file to parse
     * @return a CAN Database object
     */
    public static DbcObject parseDBC(File file) {
        DbcObject dbcObject = new DbcObject(file.getName(), file.getPath());
        try {
            String dbcFile = new String(Files.readAllBytes(Paths.get(file.getPath())));
            String[] lines = dbcFile.split(NEW_LINE + NEW_LINE);
            for (int i = 0; i < lines.length; i++) {
                parseMessages(dbcObject, lines[i]);
            }

            sortMessages(dbcObject.getMessages());
            parseComments(dbcObject, dbcFile);
        } catch (Exception ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
            MessagePane.showError("<html><b>An error occurred while parsing the DBC file: "
            + file.getAbsoluteFile()  + "</b></html>\n\n"
            + "File cannot be loaded. Please check if this file is valid.");
            return null;
        }
        return dbcObject;
    }

    /**
     * Uses a regular expression to try and match CAN Messages in a String of the contents of a CAN Database file.
     * @param dbc the CAN Database object in which the messages are stored
     * @param dbcText the CAN Database string to parse
     */
    private static void parseMessages(DbcObject dbc, String dbcText) {
        boolean extId = false;
        Matcher matcher = MESSAGE_PATTERN.matcher(dbcText);
        while (matcher.find()) {
            long id = Long.parseLong(matcher.group(1));
            if((id & 0x80000000) != 0){
                id &= ~(1 << 31);
                extId = true;
            }                                    //Extended ID? --> Toggle first bit
            String name = (matcher.group(2));
            int dlc = Integer.parseInt(matcher.group(3));
            String sendingNode = matcher.group(4);
            CanMessage message = new CanMessage(id, name, dlc);
            message.setExtendedID(extId);
            message.setSendingNode(sendingNode);
            message.setParent(dbc);
            parseSignals(message, dbcText);
            dbc.addMessage(message);
        }
    }

    /**
     * Uses a regular expression to match CAN Signals that belong to a CAN Message in a String.
     * @param message the CAN Message object in which the signals are stored
     * @param messageText the String in which the signals are matched
     */
    private static void parseSignals(CanMessage message, String messageText) {
        List<CanSignal> signals = new ArrayList<>();
        Matcher matcher = SIGNAL_PATTERN.matcher(messageText);
        while (matcher.find()) {
            String name = matcher.group(1);
            String multiplexer = matcher.group(2);
            int startBit = Integer.parseInt(matcher.group(3));
            int length = Integer.parseInt(matcher.group(4));
            int format = Integer.parseInt(matcher.group(5));
            String type = matcher.group(6);
            double factor = Double.parseDouble(matcher.group(7));
            double offset = Double.parseDouble(matcher.group(8));
            double minimum = Double.parseDouble(matcher.group(9));
            double maximum = Double.parseDouble(matcher.group(10));
            String unit = matcher.group(11);
            CanSignal signal = new CanSignal(message, name, multiplexer, startBit, length, format, type,
                factor, offset, minimum, maximum, unit);
            signals.add(signal);
        }
        message.setSignals(sortSignals(signals));
    }

    /**
     * Uses a regular expression to try and match all comments in a CAN Database file. All comments
     * (i.e. message comments, signal comments)are grouped together and not with there corresponding
     * signal or message. Because of this the comments must be matched separately.
     * @param dbcObject the CAN Database object in which the parsed comments will be set
     * @param dbcString the string in which the comments are matched
     */
    private static void parseComments(DbcObject dbcObject, String dbcString) {
        Matcher matcher = COMMENT_PATTERN.matcher(dbcString);
        while (matcher.find()) {
            String commentType = matcher.group(1);
            long id = Long.parseLong(matcher.group(2));
            if((id & 0x80000000) != 0){
                id &= ~(1 << 31);
            }                                    //Extended ID? --> Toggle first bit
            String name = matcher.group(3);
            String comment = matcher.group(4);
            switch (commentType) {
                case SIGNAL:
                    dbcObject.getMessageById(id).getSignalByShortName(name).setComment(comment);
                    break;
                case MESSAGE:
                    dbcObject.getMessageById(id).setComment(comment);
                    break;
            }
        }
    }

    /**
     * @param list the list of CanMessage objects to sort
     * @return the sorted list
     */
    private static List<CanMessage> sortMessages(List<CanMessage> list) {
        Collections.sort(list, (CanMessage message1, CanMessage message2) -> message1.getName().compareTo(message2.getName()));
        return list;
    }

    /**
     * @param list the list of CanSignal objects to sort
     * @return the sorted list
     */
    private static List<CanSignal> sortSignals(List<CanSignal> list) {
        Collections.sort(list, (CanSignal signal1, CanSignal signal2) -> signal1.getName().compareTo(signal2.getName()));
        return list;
    }


}
