/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package can;

import datahandling.BasicDescriptionFile;
import datahandling.Reference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains all the data and information of an imported CAN Database file
 * @author Michiel Klifman
 */
public class DbcObject extends BasicDescriptionFile {
    public static final String CAN_SOURCENAME_SEPARATOR = ".";
    public static final String DBC_SOURCENAME = "DBC";
    public static final String DBC_SOURCE_IDENTIFIER = DBC_SOURCENAME + CAN_SOURCENAME_SEPARATOR;

    private List<CanMessage> messages = new ArrayList<>();

    /**
     * @param name the (file)name to set
     */
    public DbcObject(String name, String path) {
        super(name, path);
    }

    /**
     * @return the messages
     */
    public List<CanMessage> getMessages() {
        return messages;
    }

    /**
     * @param messages the messages to set
     */
    public void setMessages(List<CanMessage> messages) {
        this.messages = messages;
    }

    /**
     * @param name the name of the message to get
     * @return the message with the corresponding name
     */
    public CanMessage getMessageByName(String name) {
        for(CanMessage message : messages) {
            if(message.getName().equals(name)) {
                return message;
            }
        }
        return null;
    }

    /**
     * @param id the id of the message to get
     * @return message with the corresponding id
     */
    public CanMessage getMessageById(long id) {
        for(CanMessage message : messages) {
            if(message.getId() == id) {
                return message;
            }
        }
        return null;
    }

    /**
     * @param message the message to add
     */
    public void addMessage(CanMessage message) {
        this.messages.add(message);
    }

    /**
     * @return a list of all the signals in the DBC file.
     */
    public List<CanSignal> getSignals() {
        List<CanSignal> signals = new ArrayList<>();
        for (CanMessage message : messages) {
            for (CanSignal signal : message.getSignals()) {
                signals.add(signal);
            }
        }
        return signals;
    }

    /**
     * @return a map of all the signals in the DBC file.
     */
    public Map<String, CanSignal> getSignalsMap() {
        Map<String, CanSignal> signalsMap = new HashMap<>();
        for (CanMessage message : messages) {
            for (CanSignal signal : message.getSignals()) {
                signalsMap.put(signal.getName(), signal);
            }
        }
        return signalsMap;
    }

    /**
     * @return a list of all the References from this file. In this case a list
     * of CanSignals.
     */
    @Override
    public List<? extends Reference> getReferences() {
        return getSignals();
    }

    /**
     * This overrides the toString method of Object so that an
     * object of this class can be used as a node in a JTree.
     * @return the name of this object
     */
    @Override
    public String toString() {
        return getName();
    }
}
