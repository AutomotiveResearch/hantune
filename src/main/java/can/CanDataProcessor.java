/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package can;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

import ErrorLogger.defaultExceptionHandler;
import XCP.XCPonCANBasic;
import datahandling.CurrentConfig;
import nl.han.hantune.datahandling.DataLogger.LogDataReaderCsv;
import nl.han.hantune.datahandling.DataLogger.LogDataWriterCsv;
import peak.can.basic.TPCANMsg;

/**
 *
 * @author Michiel
 */
public class CanDataProcessor {

    public static final String BIG_ENDIAN = "big endian";
    public static final String LITTLE_ENDIAN = "little endian";
    protected static CanConnectionHandler CANHANDLER = CurrentConfig.getInstance().getCan();

    private CanDataProcessor() {
        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
    }

    public static void processCanMessage(TPCANMsg msg, long timestamp) {
        CanMessage message = DbcManager.getMessageById(msg.getID());
        if (message != null) {
            List<CanSignal> lst = message.getSignals();
            if (!lst.isEmpty()) {
                LogDataWriterCsv datalog = LogDataWriterCsv.getInstance();
                boolean doLog = datalog.isWritingActive();
                boolean showValue = !LogDataReaderCsv.getInstance().isReading();

                for (CanSignal signal : message.getSignals()) {
                    BitSet signalBits = proccessCanSignal(signal, msg.getData());
                    long rawValue = signalBits.isEmpty() ? 0 : signalBits.toLongArray()[0];
                    double physicalValue = signal.getPhysicalValue(rawValue);

                    if (showValue) {
                        signal.setValueAndTime(physicalValue, timestamp);
                    }

                    if (doLog) {
                        datalog.addValue(signal.getName(), physicalValue);
                    }
                }
                if (doLog) {
                    datalog.writeValues(timestamp);
                }
                CANHANDLER.notifyMsgReceived(timestamp);
            }
        }
    }


    public static void processCanMessage(String msgName, double value, long timestamp) {
        CanMessage msg = DbcManager.getMessageByName(msgName.substring(0, msgName.indexOf('.')));

        if (msg != null) {
            for (CanSignal signal : msg.getSignals()) {
                if (signal.getName().equals(msgName)) {
                    signal.setValueAndTime(value, timestamp);
                    break;
                }
            }
        }
    }


    private static BitSet proccessCanSignal(CanSignal signal, byte[] data) {
        BitSet messageBits = BitSet.valueOf(data);
        BitSet signalBits = new BitSet();
        switch (signal.getFormat()) {
            case LITTLE_ENDIAN:
                signalBits = messageBits.get(signal.getStartBit(), signal.getStartBit() + signal.getLength());
                break;
            case BIG_ENDIAN:
                signalBits = messageBits.get((signal.getStartBit() + 1) - signal.getLength(), signal.getStartBit() + 1);
                break;
        }
        return signalBits;
    }

    /**
     * Creates a BitSet for the CAN signals of the provided CAN message.
     *
     * @param message - the CAN message containing the CAN signals and their
     * values.
     * @return a BitSet containing the values of the CAN signals belonging to
     * the provided message.
     */
    private static BitSet createMessage(CanMessage message) {
        BitSet messageBits = new BitSet();
        for (CanSignal signal : message.getSignals()) {
            BitSet signalBits = BitSet.valueOf(toByteArray((long) signal.getRawValue()));
            for (int i = signalBits.nextSetBit(0); i >= 0; i = signalBits.nextSetBit(i + 1)) {
                messageBits.set(i + signal.getStartBit());
            }
        }
        return messageBits;
    }

    /**
     * Sends a CAN message with specific value for a specific CAN signal. If
     * there are other signals belonging to this message, their current value
     * will be used.
     *
     * @param signal - the CAN signal of which the provided value must be send.
     * @param value - the value for the CAN signal that must be send.
     * @return whether message has been successfully send.
     */
    public static boolean sendSignal(CanSignal signal, double value) {
        BitSet messageBits = createMessageOfSignal(signal, value);
        byte[] data = Arrays.copyOf(messageBits.toByteArray(), signal.getParent().getDLC());
        return send((int) signal.getParent().getId(), data, signal.getParent().isExtendedID());
    }

    /**
     * Creates a BitSet for a CAN message with a specific value for a specific
     * CAN signal.
     *
     * @param signal - the CAN signal of which the provided value must be used.
     * @param value - the value for the CAN signal that must be used.
     * @return a BitSet containing the value for the provided CAN signal and
     * other values of signals belonging to the parent (CAN message) of the
     * provided CAN signal.
     */
    public static BitSet createMessageOfSignal(CanSignal signal, double value) {
        BitSet messageBits = new BitSet();
        for (CanSignal sibling : signal.getSiblings()) {
            double valueToSend = signal == sibling ? value : sibling.getRawValue();
            byte arr[] = toByteArray((long) valueToSend);
            BitSet signalBits = BitSet.valueOf(arr);
            for (int i = signalBits.nextSetBit(0); i >= 0; i = signalBits.nextSetBit(i + 1)) {
                messageBits.set(i + sibling.getStartBit());
            }
        }
        return messageBits;
    }

    /**
     * Tells the CAN transport layer to send a CAN message with the provided id
     * and data.
     *
     * @param id - the id of the message to send
     * @param data - the data the message should contain (i.e. the values for
     * the signals belonging to this message.
     * @return whether the message has been successfully send.
     */
    private static boolean send(int id, byte[] data, boolean extId) {
        boolean sent = false;
        // TODO: also to be handled by SocketCAN?
        if (CurrentConfig.getInstance().getXcp() instanceof XCPonCANBasic) {
            XCPonCANBasic xcpCanHandler = (XCPonCANBasic) CurrentConfig.getInstance().getXcp();
            if (xcpCanHandler != null && xcpCanHandler.shouldProcessCanData()) {
                sent = xcpCanHandler.sendCanMessage(id, data, extId);
                return sent;
            }
        }

        CanConnectionHandler canHandler = CurrentConfig.getInstance().getCan();
        if (canHandler != null) {
            sent = canHandler.sendCanMessage(id, data, extId);
        }
        return sent;
    }

    /**
     * Reverse the bytes in a byte array. Useful when dealing with big/little
     * endian byte arrays.
     *
     * @param bytes - the byte array to reverse.
     * @return a reversed byte array
     */
    public static byte[] reverseBytes(byte[] bytes) {
        byte[] reversed = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            reversed[i] = bytes[bytes.length - 1 - i];
        }
        return reversed;
    }

    /**
     * Convert a long to a byte array.
     *
     * @param value - the double to convert
     * @return a byte array
     */
    public static byte[] toByteArray(long value) {
        String unsignedLongString = Long.toUnsignedString(value);
        long unsignedLong = Long.parseLong(unsignedLongString);
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putLong(unsignedLong);
        bytes = reverseBytes(bytes);
        return bytes;
    }

    /**
     * Convert a double to a byte array.
     *
     * @param value - the double to convert
     * @return a byte array
     */
    public static byte[] toByteArray(double value) {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putDouble(value);
        return bytes;
    }

    /**
     * Convert a byte array to a double value.
     *
     * @param bytes - the bytes to convert
     * @return a double value
     */
    public static double toDouble(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getDouble();
    }

    /**
     * Convert a byte array to its binary String representation. Useful for
     * debugging.
     *
     * @param bytes - the bytes to convert
     * @return the String with the binary representation.
     */
    public static String getRealBinary(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bytes) {
            for (int i = 128; i > 0; i >>= 1) {
                if ((b & i) == 0) {
                    stringBuilder.append('0');
                } else {
                    stringBuilder.append('1');
                }
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Convert a byte array to its hexadecimal String representation. Useful for
     * debugging.
     *
     * @param bytes - the bytes to convert
     * @return the String with the hexadecimal representation
     */
    public static String byteArrayToHex(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            stringBuilder.append(String.format("%02x", b & 0xff));
        }
        return stringBuilder.toString();
    }
}
