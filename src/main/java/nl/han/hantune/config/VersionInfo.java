/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import ErrorLogger.AppendToLogfile;

import static HANtune.HANtune.shortApplicationName;

public class VersionInfo {

    private static final String VERSIONFILE_NAME = "config/version.properties";
    private static final String MASTER_BRANCH_NAME = "master";

    enum VersionInfoProperties {
        COMMIT_COUNT("HANtune.version.CommitCount", "0"),
        VERSION_TEXT("HANtune.version.VersionText", "Error '" + VERSIONFILE_NAME + "'"),
        VERSION_NUMBER("HANtune.version.VersionNumber", "0.0"),
        VERSION_BRANCHNAME("HANtune.version.BranchName", "--"),
        COMMIT_ID("HANtune.version.CommitId", "00000000");

        private final String key;
        private final String value;

        VersionInfoProperties(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

        private static Properties getAllProperties() {
            Properties defaultProps = new Properties();

            Arrays.asList(VersionInfoProperties.values()).forEach(val -> defaultProps.put(val.getKey(), val.getValue()));
            return defaultProps;
        }
    }

    private static VersionInfo instance = null;
    private static Properties versionProps = null;


    /**
     * Singleton, private constructor
     */
    private VersionInfo(String name) {
        fillVersionData(name);
    }

    public static VersionInfo getInstance() {
        if (instance == null) {
            instance = new VersionInfo(VERSIONFILE_NAME);
        }
        return instance;
    }

    /**
     * For unit testing only! Use this getInstanceTest() instead of getInstance()
     *
     * @return instance initialized from test file
     */
    protected static VersionInfo getInstanceTest(String fileName) {
        instance = new VersionInfo(fileName);
        return instance;
    }

    private static void fillVersionData(String name) {
        versionProps = new Properties(VersionInfoProperties.getAllProperties());
        try (FileInputStream versionFile = new FileInputStream(name)) {
            versionProps.load(versionFile);
        } catch (IOException e) {
            AppendToLogfile.appendMessage("Error reading version file: " + name + " could not be found",
                "Version information not available in " + shortApplicationName + ".");
        }
    }


    private String getVersionName() {
        return versionProps.getProperty(VersionInfoProperties.VERSION_TEXT.getKey());
    }

    private String getXYVersionNumber() {
        return versionProps.getProperty(VersionInfoProperties.VERSION_NUMBER.getKey());
    }

    private String getBranchName() {
        return versionProps.getProperty(VersionInfoProperties.VERSION_BRANCHNAME.getKey());
    }

    private boolean isMasterBranch() {
        return getBranchName().equals(MASTER_BRANCH_NAME);
    }

    public String getCommitId() {
        return versionProps.getProperty(VersionInfoProperties.COMMIT_ID.getKey());
    }

    public float getVersionFloat() {
        try {
            return Float.parseFloat(getXYVersionNumber());
        } catch (NumberFormatException e) {
            AppendToLogfile.appendMessage("Version format error: " + getXYVersionNumber() + " could not be converted",
                "Version " + VersionInfoProperties.VERSION_NUMBER.getValue() + " assumed");
            AppendToLogfile.appendError(Thread.currentThread(), e);
            return Float.parseFloat(VersionInfoProperties.VERSION_NUMBER.getValue()); // When this gives exception: Crash!
        }
    }

    private int getCommitCount() {
        return Integer.decode(versionProps.getProperty(VersionInfoProperties.COMMIT_COUNT.getKey()));
    }


    /**
     * @return "HANtune X.Y.cc"
     */
    public String getHANtuneXYVersionCommitCount() {
        return shortApplicationName + " " + getXYVersionCommitCount();
    }

    /**
     * @return "HANtune X.Y"
     */
    public String getHANtuneXYVersion() {
        return shortApplicationName + " " + getXYVersionNumber();
    }

    /**
     * @return "HANtune X.Y.ccc commit: a5b6c7d8"
     */
    public String getHANtuneXYVersionCommitCountCommitID() {
        return shortApplicationName + " " + getXYVersionCommitCount() + " commit: " + getCommitId();
    }


   /**
     * @return "X.Y.ccc - Version name" or "X.Y.ccc" if version name is empty
     */
    public String getXYVersionCommitCountVersionName() {
        return getXYVersionCommitCount() + (getVersionName().isEmpty() ? "" : " - " + getVersionName());
    }



    /**
     * @return "deadbeef - x-branch-name-topic" or "deadbeef" if master branch
     */
    public String getCommitIDBranchName() {
        return getCommitId() + (isMasterBranch() ? "" : " - " + getBranchName());
    }


    /**
     * @return "HANtune x-branch-name-topic - build ccc" or "HANtune - build ccc" (omit branchName if 'master' branch)
     */
    public String getHANtuneConditionalBranchNameBuildCommitCount() {
        return shortApplicationName + (isMasterBranch() ? "" : " " + getBranchName()) + " - build " + getCommitCount();
    }

    /**
     * @return "build ccc"
     */
    public String getBuildCommitCount() {
        return "build " + getCommitCount();
    }


    /**
     * @return "Branch: 5-branch-name-text" or "" if master
     */
    public String getBranchBranchName() {
        return (getBranchName().equals(MASTER_BRANCH_NAME) ? "" : "Branch: " + getBranchName());
    }


    /**
     * @return "X.Y.ccc"
     */
    public String getXYVersionCommitCount() {
        return getXYVersionNumber() + "." + getCommitCount();
    }

}
