/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting;

import java.lang.reflect.Field;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextPane;

import nl.han.hantune.config.VersionInfo;
import nl.han.hantune.gui.components.JLabelLink;
import nl.han.hantune.scripting.jython.InteractiveJythonInterpreter;
import org.python.util.InteractiveConsole;

import ErrorLogger.AppendToLogfile;
import bsh.util.JConsole;

/**
 * A wrapper around the JConsole from the BeanShell library. Connects the
 * console to an interactive Jython interpreter and provides the ability to
 * print to the HANtune console through a static context.
 *
 * @author Michiel Klifman
 */
public class Console {

    private static JConsole console;
    private static InteractiveJythonInterpreter interactiveInterpreter;

    private static final String PS1 = ">>> ";
    private static final String PS2 = "... ";

    static {
        startConsole();
    }

    /**
     * Creates new instances of the console GUI and interactive interpreter and
     * runs it in a new Thread.
     */
    public static void startConsole() {
        console = new JConsole();
        addMenuItems(console);
        interactiveInterpreter = new InteractiveJythonInterpreter(console.getIn());
        printWelcomeMessage();
        new Thread(interactiveInterpreter).start();
    }

    /**
     * Uses reflection to add new menu items to the JConsole.
     * @param console the console to add the menu items to
     */
    private static void addMenuItems(JConsole console) {
        Field f;
        try {
            f = console.getClass().getDeclaredField("menu");
            f.setAccessible(true);
            JPopupMenu menu = (JPopupMenu) f.get(console);
            JMenuItem resetConsoleMenuItem = new JMenuItem("Clear");
            resetConsoleMenuItem.addActionListener((e) -> clearConsole());
            menu.add(resetConsoleMenuItem);

            JMenuItem stopMenuItem = new JMenuItem("Interrupt");
            stopMenuItem.addActionListener((e) -> interactiveInterpreter.interrupt());
            menu.add(stopMenuItem);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
        }
    }

    /**
     * Clears all text in the console and reprints the welcome message.
     */
    public static void clearConsole() {
        JTextPane textPane = (JTextPane) console.getViewport().getView();
        textPane.setText("");
        printWelcomeMessage();
    }

    /**
     * Prints a welcome message to the console.
     */
    private static void printWelcomeMessage() {
        print("Welcome to " + VersionInfo.getInstance().getHANtuneXYVersionCommitCount() + "\n");
        print(InteractiveConsole.getDefaultBanner() + "\n");
        print("For more information visit:");
        JLabelLink labelLink = new JLabelLink("http://openmbd.com/wiki/HANtune/Scripting");
        JTextPane textPane = (JTextPane) console.getViewport().getView();
        textPane.setCaretPosition(textPane.getDocument().getLength());
        textPane.insertComponent(labelLink);
        labelLink.setLocation(labelLink.getX(), labelLink.getY() - 4);
        print("\n>>> ");
    }

    /**
     * Gets the GUI to the console
     * @return the current instance of the JConsole
     */
    public static JConsole getGUI() {
        return console;
    }

    /**
     * Prints to HANtune's console. (e.g. String, Ion)
     * @param object
     */
    public static void print(Object object) {
        console.print(object);
    }

    /**
     * @return wether or not a prompt has already been printed.
     */
    private static boolean isLastPrintPrompt() {
        JTextPane textPane = (JTextPane) console.getViewport().getView();
        String text = textPane.getText();
        if (text.length() > 4) {
            String lastText = text.substring(text.length() - 4, text.length());
            return lastText.equals(PS1) || lastText.equals(PS2);
        } else {
            return false;
        }
    }

    /**
     * Prints a prompt to the console (i.e. '... ' when more input is required
     * and '>>> ' otherwise.
     * @param moreInputRequired whether or not more input is required by the
     * interpreter
     */
    public static void printPrompt(boolean moreInputRequired) {
        if (!isLastPrintPrompt()) {
            console.print(!moreInputRequired ? PS1 : PS2);
        }
    }

    /**
     * Prints to HANtune's console and appends a new line. (e.g. String, Icon)
     * @param line
     */
    public static void println(Object line) {
        console.println(line);
    }

    /**
     * @return the interactiveInterpreter
     */
    public static InteractiveJythonInterpreter getInteractiveInterpreter() {
        return interactiveInterpreter;
    }
}
