/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting.jython;

import nl.han.hantune.scripting.Interpreter;
import nl.han.hantune.scripting.InterpreterFactory;
import nl.han.hantune.scripting.Script;
import datahandling.ReferenceStateListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michiel Klifman
 */
public class JythonScript implements Script {

    private final String name;
    private String path;

    private boolean running;
    private boolean runOnStartup;
    private final List<ReferenceStateListener> stateListeners = new ArrayList<>();
    private final Interpreter interpreter;

    public JythonScript(String name, String path) {
        this.name = name;
        this.path = path;
        interpreter = InterpreterFactory.getInterpreter(JythonScript.class);
    }

    @Override
    public void start() {
        interpreter.interpret(this);
    }

    @Override
    public void stop() {
        interpreter.interrupt(this);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSource() {
        return SOURCE;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public Interpreter getInterpreter() {
        return interpreter;
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public void setRunning(boolean running) {
        this.running = running;
        updateStateListeners();
    }

    @Override
    public boolean isRunOnStartup() {
        return runOnStartup;
    }

    @Override
    public void setRunOnStartup(boolean runOnStartup) {
        this.runOnStartup = runOnStartup;
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void setActive(boolean active) {
    }

    @Override
    public void addStateListener(ReferenceStateListener listener) {
        if (!stateListeners.contains(listener)) {
            stateListeners.add(listener);
        }
    }

    @Override
    public void removeStateListener(ReferenceStateListener listener) {
        stateListeners.remove(listener);
    }

    public void updateStateListeners() {
        stateListeners.stream().forEach(ReferenceStateListener::stateUpdate);
    }

    /**
     * Overrides the toString method of Object so that an object of this class
     * will display its name when used in Swing components (e.g. JTree).
     *
     * @return the name of this object
     */
    @Override
    public String toString() {
        return getName();
    }

}
