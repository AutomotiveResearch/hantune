/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting.jython;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import javax.swing.SwingUtilities;

import nl.han.hantune.scripting.Console;
import nl.han.hantune.scripting.Interpreter;
import nl.han.hantune.scripting.Script;
import org.python.core.Py;
import org.python.core.PyException;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

/**
 * Wrapper around Jython's PythonIinterpreter. Can start and stop the interpretation
 * of an object implementing the Script interface.
 *
 * @author Michiel Klifman
 */
public class JythonInterpreter implements Interpreter {

    private final PythonInterpreter interpreter;
    private Executor executor;
    private Printer printer;
    private OutputStream output;

    /**
     * Creates a JythonInterpreter with the default output (Printer inner class)
     */
    public JythonInterpreter() {
        this(null);
    }

    /**
     * Creates a JythonInterpreter with the specified output.
     * @param output - the OutputStream that should be used by the interpreter
     */
    public JythonInterpreter(OutputStream output) {
        this.output = output;
        PySystemState sys = new PySystemState();
        sys.path = Py.getSystemState().path;
        interpreter = new PythonInterpreter(null, sys);
        JythonInterpreterInitializer.initInterpreter(interpreter);
    }

    /**
     * Interprets the provided script based on its path.
     * @param script
     */
    @Override
    public void interpret(Script script) {
        PipedInputStream inputStream = new PipedInputStream();
        OutputStream outputStream;
        try {
            outputStream = output == null ? new PipedOutputStream(inputStream) : output;
        } catch (IOException ex) {
            return;
        }

        executor = new Executor(script, outputStream);
        printer = new Printer(inputStream);
        script.setRunning(true);
        executor.start();
        printer.start();
    }

    /**
     * Interrupts the threads that are used to interpret a script and print
     * its output.
     * @param script
     */
    @Override
    public void interrupt(Script script) {
        executor.interrupt();
        printer.interrupt();
    }

    /**
     * Sets a variable in the interpreter.
     * @param name - the name of the variable
     * @param value - the value of the variable
     */
    @Override
    public void set(String name, Object value) {
        interpreter.set(name, value);
    }

    /**
     * Inner class that extends Thread and executes a script when run. Because
     * the script is executed in a separate Thread, we can stop a script by
     * interrupting the thread it is running on.
     */
    private class Executor extends Thread {

        private final Script script;
        private final OutputStream output;

        public Executor(Script script, OutputStream output) {
            this.script = script;
            this.output = output;
            interpreter.setOut(output);
        }

        @Override
        public void run() {
            try {
                interpreter.execfile(script.getPath());
            } catch (PyException exception) {
                SwingUtilities.invokeLater(() -> Console.print(exception));
            } finally {
                SwingUtilities.invokeLater(() -> script.setRunning(false));
                try {
                    output.close();
                } catch (IOException ex) {
                    Console.print(ex);
                }
            }
        }
    }

    /**
     * Inner class that extends Thread and prints all output of the interpreter
     * to the console. While it is possible to connect the output of the
     * interpreter directly to console, this would put too much load on the
     * console when many script are running and printing to the console at the
     * same time.
     */
    private class Printer extends Thread {

        private final BufferedReader reader;

        public Printer(InputStream input) {
            reader = new BufferedReader(new InputStreamReader(input));
        }

        @Override
        @SuppressWarnings("CallToThreadYield")
        public void run() {
            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    Console.println(line);
                    Thread.yield();
                }
            } catch (IOException ex) {
            }
        }

        @Override
        public void interrupt() {
            try {
                reader.close();
            } catch (IOException ex) {
                    Console.print(ex);
            }
        }
    }

}
