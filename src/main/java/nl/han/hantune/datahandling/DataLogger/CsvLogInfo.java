/*
    Copyright (c) 2020 [HAN University of Applied Sciences]

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    */
package nl.han.hantune.datahandling.DataLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import DAQList.DAQList;
import HANtune.HANtune;
import can.DbcManager;
import datahandling.CurrentConfig;
import datahandling.Signal;
import util.Util;


public class CsvLogInfo implements LogInfo {
    public class DaqListInfo {
        private String id;
        private String name;
        private String freq;

        DaqListInfo(String daqId, String name, String freq) {
            this.id = daqId;
            this.name = name;
            this.freq = freq;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getFreq() {
            return freq;
        }
    }

    public class ProtocolSignalName {
        private String protocol;
        private String signal;

        ProtocolSignalName(String protocolName) {
            protocol = protocolName;
            signal = "";
        }

        public String getProtocol() {
            return protocol;
        }

        public void setProtocol(String prot) {
            this.protocol = prot;
        }

        public String getSignal() {
            return signal;
        }

        public void setSignal(String name) {
            this.signal = name;
        }
    }


    private String projectName = "";
    private String ecuId = "";
    private List<DaqListInfo> daqListInfos = new ArrayList<>();
    private List<String> dbcListInfos = new ArrayList<>();
    private List<ProtocolSignalName> protocolSignalNames = new ArrayList<>();
    private boolean isProtocolSignalNamesFilled = false;
    private char decimalSeparator = ',';

    private String startDate = "";


    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getEcuId() {
        return ecuId;
    }

    public void setEcuId(String ecuId) {
        if (ecuId == null) {
            this.ecuId = "";
        } else {
            this.ecuId = ecuId;
        }
    }

    public List<DaqListInfo> getDaqListInfos() {
        return daqListInfos;
    }

    public void addDaqListInfo(String daqId, String name, String freq) {
        this.daqListInfos.add(new DaqListInfo(daqId, name, freq));
    }

    public List<String> getDbcInfos() {
        return dbcListInfos;
    }

    public void addDbcInfo(String fileName) {
        this.dbcListInfos.add(fileName);
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public List<ProtocolSignalName> getProtocolSignalNames() {
        return protocolSignalNames;
    }

    public void addProtocolNames(String[] protNames) {
        protocolSignalNames = new ArrayList<>();
        Arrays.stream(protNames).forEach(name -> protocolSignalNames.add(new ProtocolSignalName(name)));
    }


    void addSignalNames(String[] sigNames) {
        for (int idx = 0; idx < sigNames.length; idx++) {
            protocolSignalNames.get(idx).setSignal(sigNames[idx]);
        }
        isProtocolSignalNamesFilled = sigNames.length > 0;
    }

    /**
     * Fill LogInfo based on data currently in CurrentConfig memory
     */
    void initCsvLogInfo(CurrentConfig currentConfig) {

        // Project name
        if (currentConfig.getProjectFile() != null) {
            setProjectName(Util.getFilename(currentConfig.getProjectFile().getName()));
        } else {
            setProjectName("<No File>");
        }

        // ECU id
        String id;
        if (currentConfig.getXcpsettings() != null && ((id = currentConfig.getXcpsettings().slaveid) != null)) {
            setEcuId(id);

            // DAQ list names
            if (!id.isEmpty()) {
                float slaveFrequency = currentConfig.getXcpsettings().getSlaveFrequency();
                for (DAQList daqList : HANtune.getInstance().daqLists) {
                    if (daqList.isActive()) {
                        addDaqListInfo(daqList.getScreenId(), daqList.getName(), Util
                            .getStringValue((double) daqList.getSampleFrequency(slaveFrequency), "0.##"));
                    }
                }
            }
        }

        fillProtocolAndSignalNames(currentConfig);

        // DBC names
        for (String name : DbcManager.getAllDbcNames()) {
            addDbcInfo(name);
        }

        setStartDate(new Date().toString());
    }


    private void fillProtocolAndSignalNames(CurrentConfig currentConfig) {
        List<Signal> signals = currentConfig.getAllReferencesOfType(Signal.class);
        // Create list containing protocol names: XCP XCP CAN...
        List<String> protNames = new ArrayList<>();

        String configProtocol = currentConfig.getProtocol().toString().toUpperCase();
        // Create list containing signal names: Signal_1 Signal_2 LedValue...
        List<String> sigNames = new ArrayList<>();
        signals.forEach(sig -> {
            if (configProtocol.contains(sig.getProtocolName().toUpperCase())) {
                sigNames.add(sig.getName());
                protNames.add(sig.getProtocolName());
            }
        });
        addProtocolNames(protNames.toArray(new String[0]));
        addSignalNames(sigNames.toArray(new String[0]));
    }


    public boolean isHeaderComplete() {
        return isProtocolSignalNamesFilled;
    }

    public char getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(char decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }

}
