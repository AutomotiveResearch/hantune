/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.net.ftp;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Michiel Klifman
 */
public class LocalFile implements FtpFile {
    
    private final File file;
    private boolean sorted = false;
    private final List<LocalFile> fileList = new ArrayList<>();
    
    public LocalFile(File file) {
        this.file = file;
    }

    private void sortFiles() {
        File[] files = file.listFiles();
        Arrays.sort(files, Comparator.comparing(File::isDirectory, Comparator.reverseOrder()).thenComparing(File::getName));
        for (File childFile : files) {
            fileList.add(new LocalFile(childFile));
        }
        sorted = true;
    }

    @Override
    public int getFileCount() {
        if (isDirectory() && !sorted) {
            sortFiles();
        }
        return fileList.size();
    }

    @Override
    public List<LocalFile> getFileList() {
        if (isDirectory() && !sorted) {
            sortFiles();
        }
        return fileList;
    }
    
    @Override
    public boolean isDirectory() {
        return file.isDirectory();
    }

    public String getPath() {
        return file.getAbsolutePath();
    }

    @Override
    public String toString() {
        return file.getName();
    }
}
