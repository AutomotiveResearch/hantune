/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.net.ftp;

import java.util.ArrayList;
import java.util.List;
import nl.han.hantune.net.ssh.SSHManager;

/**
 *
 * @author Michiel Klifman
 */
public class RemoteFile implements FtpFile {
    
    private final String name;
    private final String path;
    private final SSHManager ssh;
    private final boolean isDirectory;
    private final List<RemoteFile> fileList = new ArrayList<>();
    
    public RemoteFile(String path, SSHManager ssh, boolean isDirectory) {
        String[] paths = path.split("/");
        this.name = paths[paths.length-1];
        this.path = path;
        this.isDirectory = isDirectory;
        this.ssh = ssh;
    }

    private void downloadFileList() {
        String ls_folders = ssh.sendCommand("ls -d " + path + "*/");
        if (ls_folders.endsWith("\n")) {
            String[] folders = ls_folders.split("\n");
            for (String folder : folders) {
                fileList.add(new RemoteFile(folder, ssh, true));
            }
        }
        String ls_files = ssh.sendCommand("ls -dp " + path + "* | grep -v /$");
        if (ls_files.endsWith("\n")) {
            String[] files = ls_files.split("\n");
            for (String file : files) {
                fileList.add(new RemoteFile(file, ssh, false));
            }
        }
    }

    @Override
    public int getFileCount() {
        if (isDirectory && fileList.isEmpty()) {
            downloadFileList();
        }
        return fileList.size();
    }

    @Override
    public List<RemoteFile> getFileList() {
        if (isDirectory && fileList.isEmpty()) {
            downloadFileList();
        }
        return fileList;
    }
    
    @Override
    public boolean isDirectory() {
        return isDirectory;
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return name;
    }
    
}