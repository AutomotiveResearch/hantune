/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.components;

import java.awt.event.KeyEvent;

import util.Util;

/**
 * adapter class to convert numpad keyboard dot ('.') key (not regular keyboard '.' key) to decimal separator
 * for current locale
 */
public class NumpadDecimalLocaleConverter {
    private int location = KeyEvent.KEY_LOCATION_UNKNOWN;


    public void handleKeyPressedEvent(KeyEvent event) {
        location = event.getKeyLocation();
    }


    public void handleKeyTypedEvent(final KeyEvent event) {
        if (event.getKeyChar() == '.' && location == KeyEvent.KEY_LOCATION_NUMPAD) {
            event.setKeyChar(Util.DEC_SEPARATOR_LOCALE);
        }
    }

}
