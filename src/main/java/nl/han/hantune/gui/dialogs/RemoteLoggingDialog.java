package nl.han.hantune.gui.dialogs;

import ASAP2.ASAP2Measurement;
import HANtune.HanTuneDialog;
import datahandling.CurrentConfig;
import datahandling.RemoteLoggingTcpClient;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Michiel Klifman
 */
public class RemoteLoggingDialog extends HanTuneDialog {

    private final Set<String> signals = new HashSet<>();
    private final RemoteLoggingTcpClient c = new RemoteLoggingTcpClient();
    private final JTextArea responseArea = new JTextArea();
    private final JTextField nameField = new JTextField();
    private final JTextField frequencyField = new JTextField();
    private final JTextField durationField = new JTextField();
    private final JTextField ipField = new JTextField();
    private final JTextField portField = new JTextField();
    private final JTextField signalsField = new JTextField();
    private final JButton connectButton = new JButton("Connect");
    private final JButton disConnectButton = new JButton("Disconnect");
    private ScheduledExecutorService logStatusRequestScheduler;

    public RemoteLoggingDialog(Component c) {
        super(true);

        Box layoutBox = Box.createVerticalBox();

        Box serverBox = Box.createVerticalBox();
        serverBox.setBorder(new TitledBorder("Server"));

        Box ipBox = Box.createHorizontalBox();
        JLabel ipLabel = new JLabel("IP:", SwingConstants.RIGHT);
        ipLabel.setPreferredSize(new Dimension(150, 30));
        ipBox.add(ipLabel);
        ipField.setText(CurrentConfig.getInstance().getXcpsettings().getEthernetIP());
        ipField.setPreferredSize(new Dimension(150, 30));
        ipBox.add(ipField);
        serverBox.add(ipBox);

        Box portBox = Box.createHorizontalBox();
        JLabel portLabel = new JLabel("Port:", SwingConstants.RIGHT);
        portLabel.setPreferredSize(new Dimension(150, 30));
        portBox.add(portLabel);
        portField.setText("" + CurrentConfig.getInstance().getXcpsettings().getEthernetPort());
        portField.setPreferredSize(new Dimension(150, 30));
        portBox.add(portField);
        serverBox.add(portBox);

        layoutBox.add(serverBox);

        Box loggingBox = Box.createVerticalBox();
        loggingBox.setBorder(new TitledBorder("Logging Settings"));

        Box nameBox = Box.createHorizontalBox();
        JLabel nameLabel = new JLabel("Name:", SwingConstants.RIGHT);
        nameLabel.setPreferredSize(new Dimension(150, 30));
        nameBox.add(nameLabel);
        String a2l = "";
        if (CurrentConfig.getInstance().getASAP2Data() != null) {
            a2l = CurrentConfig.getInstance().getASAP2Data().getName();
            a2l = a2l.substring(0, a2l.lastIndexOf('.'));
        }
        nameField.setText(a2l);
        nameField.setPreferredSize(new Dimension(150, 30));
        nameBox.add(nameField);
        loggingBox.add(nameBox);

        Box frequencyBox = Box.createHorizontalBox();
        JLabel frequencyLabel = new JLabel("Sampletime (ms):", SwingConstants.RIGHT);
        frequencyLabel.setPreferredSize(new Dimension(150, 30));
        frequencyBox.add(frequencyLabel);
        frequencyField.setText("10");
        frequencyField.setPreferredSize(new Dimension(150, 30));
        frequencyBox.add(frequencyField);
        loggingBox.add(frequencyBox);

        Box durationBox = Box.createHorizontalBox();
        JLabel durationLabel = new JLabel("Duration (sec):", SwingConstants.RIGHT);
        durationLabel.setPreferredSize(new Dimension(150, 30));
        durationBox.add(durationLabel);
        durationField.setText("30");
        durationField.setPreferredSize(new Dimension(150, 30));
        durationBox.add(durationField);
        loggingBox.add(durationBox);

        updateSignals();

        Box signalsBox = Box.createHorizontalBox();
        JLabel signalsLabel = new JLabel("Signals:", SwingConstants.RIGHT);
        signalsLabel.setPreferredSize(new Dimension(150, 30));
        signalsBox.add(signalsLabel);
        signalsField.setPreferredSize(new Dimension(150, 30));
        signalsField.setEnabled(false);
        signalsField.setText("" + signals.size());
        signalsBox.add(signalsField);
        JButton modifySignalsButton = new JButton("Modify");
        modifySignalsButton.addActionListener(this::modifySignalsActionPerformed);
        signalsBox.add(modifySignalsButton);
        loggingBox.add(signalsBox);

        layoutBox.add(loggingBox);

        Box responseBox = Box.createVerticalBox();
        responseBox.setBorder(new TitledBorder("Connection info"));
        responseArea.setEditable(false);
        responseArea.setHighlighter(null);
        responseArea.setRows(4);
        responseArea.setOpaque(false);
        responseArea.setLineWrap(true);
        responseArea.setWrapStyleWord(true);
        responseArea.setFont(new Font("Tahoma", 0, 11));
        responseArea.setText("...");
        responseBox.add(responseArea);
        layoutBox.add(responseBox);

        Box buttonBox = Box.createHorizontalBox();
        connectButton.addActionListener(this::connect);
        buttonBox.add(connectButton);
        disConnectButton.addActionListener(this::disconnect);
        buttonBox.add(disConnectButton);
        layoutBox.add(buttonBox);

        this.add(layoutBox);
        pack();
        setSize(480, 380);
        setTitle("Remote Logging");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    private void updateSignals() {
        signals.clear();
        for (ASAP2Measurement s : CurrentConfig.getInstance().getHANtuneManager().getLogList()) {
            signals.add(s.getName());
        }
    }

    private void modifySignalsActionPerformed(ActionEvent e) {
        System.out.println(e);
        CurrentConfig.getInstance().getHANtuneManager().modifyLogList(1);
        updateSignals();
        signalsField.setText("" + signals.size());
    }

    private void connect(ActionEvent e) {
        System.out.println(e.getActionCommand());
        connectButton.setEnabled(false);
        disConnectButton.setEnabled(true);
            try {
                String response = c.startConnection(ipField.getText(), Integer.parseInt(portField.getText()), nameField.getText(), frequencyField.getText(), durationField.getText(), signals);
                responseArea.setText(response);
            } catch (IOException | NumberFormatException ex) {
                responseArea.setText(ex.getLocalizedMessage());
            }
        logStatusRequestScheduler = Executors.newScheduledThreadPool(1);
        logStatusRequestScheduler.scheduleAtFixedRate(() -> {
            try {
                String status_response = c.getStatus(ipField.getText(), Integer.parseInt(portField.getText()), nameField.getText(), frequencyField.getText(), durationField.getText(), signals);
                responseArea.setText(status_response);
            } catch (IOException | NumberFormatException ex) {
                responseArea.setText(ex.getLocalizedMessage());
            }
        }, 0, 5, TimeUnit.SECONDS);
    }

    private void disconnect(ActionEvent e) {
        System.out.println(e.getActionCommand());
            try {
                String response = c.cancel(ipField.getText(), Integer.parseInt(portField.getText()), nameField.getText(), frequencyField.getText(), durationField.getText(), signals);
                responseArea.setText(response);
            } catch (IOException | NumberFormatException ex) {
                responseArea.setText(ex.getLocalizedMessage());
            }
        disconnect();
    }

    private void disconnect() {
        if (logStatusRequestScheduler != null) {
            logStatusRequestScheduler.shutdownNow();
        }
        connectButton.setEnabled(true);
        disConnectButton.setEnabled(false);
    }

    @Override
    public void dispose() {
        disconnect();
        super.dispose();
    }

}