/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package nl.han.hantune.gui.dialogs;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.basic.BasicSliderUI;

import HANtune.HanTuneDialog;
import nl.han.hantune.gui.statusBar.HANtuneMainStatusBar;

import static javax.swing.SwingConstants.CENTER;

/**
 * @author Matthew Sabajo
 */
@SuppressWarnings("serial")
public class LogPlayerDialog extends HanTuneDialog {
    protected static final int MAX_SLIDER_VALUE = 10_000;
    protected static final int MIN_SLIDER_VALUE = 0;
    protected static final String SLIDER_TIME_POS = "sldrTimePos";

    private static final String TOTAL_DATA_LINES_IN_LOGFILE = "Total number of data lines found in logfile.";
    private static final String TOTAL_TIME_IN_LOGFILE_SEC = "Total time span of data in logfile (Sec).";

    private static final ImageIcon pauseIcon =
        new javax.swing.ImageIcon(LogPlayerDialog.class.getResource("/images/logplayerdialog/pause.png"));
    private static final ImageIcon endReadWhileWriteIcon =
        new javax.swing.ImageIcon(LogPlayerDialog.class.getResource("/images/logplayerdialog/endReadWhileWrite.png"));
    private static final ImageIcon playIcon =
        new javax.swing.ImageIcon(LogPlayerDialog.class.getResource("/images/logplayerdialog/play.png"));
    private static final ImageIcon endIcon =
        new javax.swing.ImageIcon(LogPlayerDialog.class.getResource("/images/logplayerdialog/end.png"));


    private int rotatorVal = 0;

    private int thumbXpos = 0;


    private class SliderPanel extends JPanel {
        @Override
        public void paint(Graphics g) {
            // prevent 'flickering' to null pos of slaves by painting here AND at BasicSliderUI() paintThumb().
            setSlavesLocation(thumbXpos);

            super.paint(g);
        }
    }


    public LogPlayerDialog() {

        Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/HANlogo.png"));
        this.setIconImage(img);

        initComponents();

        sldrTimePos.setMaximum(MAX_SLIDER_VALUE);
        sldrTimePos.setMinimum(MIN_SLIDER_VALUE);

        sldrTimePos.setUI(new BasicSliderUI(null) {
            @Override
            public void paintThumb(Graphics g) {
                // 'save' x-pos of thumbRect here
                thumbXpos = thumbRect.x;

                // prevent 'flickering' to null pos of slaves by painting here AND at SliderPanel.paint().
                super.paintThumb(g);
                setSlavesLocation(thumbXpos);
            }


            @Override
            public void scrollDueToClickInTrack(int direction) {
                if (direction > 0) {
                    handlePageUpLogging();
                } else {
                    handlePageDownLogging();
                }
            }
        });
    }


    private void setSlavesLocation(int xPos) {
        txtCurrentTime.setLocation(xPos, txtCurrentTime.getY());
        lblPlayBackSpeed.setLocation(xPos + sldrTimePos.getX(), lblPlayBackSpeed.getY());
    }


    private MouseListener txtCurrentTimeMouseListener = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent e) {
            activateTxtCurrentTime();
        }

        @Override
        public void mousePressed(MouseEvent e) {
            // not needed
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            // not needed
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            // not needed
        }

        @Override
        public void mouseExited(MouseEvent e) {
            // not needed
        }
    };


    protected void updateLiveRotator() {
        if (++rotatorVal > 3) {
            rotatorVal = 0;
        }

        switch (rotatorVal) {
            case 0:
                lblPlayBackSpeed.setText("live |");
                break;

            case 1:
                lblPlayBackSpeed.setText("live /");
                break;

            case 2:
                lblPlayBackSpeed.setText("live -");
                break;

            case 3:
                lblPlayBackSpeed.setText("live \\");
                break;

            default:
                break;
        }
    }


    protected void setCurrentTimeTxtField(String timeVal) {
        txtCurrentTime.setText(timeVal);
    }

    protected void initTotalTimeLabel(String timeVal, String hdrTxt) {
        lblEndTime.setText(timeVal);
        lblEndTime.setToolTipText("<html>" + TOTAL_TIME_IN_LOGFILE_SEC + "<br><br>Log header:<br>" + hdrTxt + "</html>");
    }

    protected void initTotalLinesLabel(String linesVal, String hdrTxt) {
        lblTotalLines.setText(linesVal);
        lblTotalLines.setToolTipText("<html>" + TOTAL_DATA_LINES_IN_LOGFILE + "<br><br>Log header:<br>" + hdrTxt + "</html>");
    }

    protected void updateTotalTimeLabel(String timeVal) {
        lblEndTime.setText(timeVal);
    }

    protected void updateTotalLinesLabel(String linesVal) {
        lblTotalLines.setText(linesVal);
    }

    protected void setPlayingState(boolean playing, int speedFactor, boolean isForward, boolean isLive) {
        if (playing) {
            btnPlayPause.setIcon(pauseIcon);
            if (isForward) {
                if (isLive) {
                    lblPlayBackSpeed.setText("live |");
                    txtCurrentTime.setBackground(HANtuneMainStatusBar.STATUSCOLOR_ON);
                    txtCurrentTime.setForeground(Color.white);
                } else {
                    lblPlayBackSpeed.setText(speedFactor + "x ►");
                    txtCurrentTime.setBackground(Color.white);
                    txtCurrentTime.setForeground(Color.black);
                }
            } else {
                lblPlayBackSpeed.setText("◄ " + speedFactor + "x");
                txtCurrentTime.setBackground(Color.white);
                txtCurrentTime.setForeground(Color.black);
            }
        } else {
            btnPlayPause.setIcon(playIcon);
            lblPlayBackSpeed.setText(" ");
            txtCurrentTime.setBackground(Color.white);
            txtCurrentTime.setForeground(Color.black);
        }
    }


    protected void setSliderPosition(int pos) {
        sldrTimePos.setValue(pos);
    }


    protected void updateSliderSlaveComponents(String timeVal) {
        if (timeVal != null) {
            setCurrentTimeTxtField(timeVal);
        }
    }

    protected void setDialogEndButtonIcon(boolean readWhileWrite) {
        if (readWhileWrite) {
            btnEnd.setIcon(endReadWhileWriteIcon);
        } else {
            btnEnd.setIcon(endIcon);
        }

    }


    protected void activateTxtCurrentTime() {
        txtCurrentTime.requestFocusInWindow();
        txtCurrentTime.selectAll();
    }

    protected void activateSliderComponent() {
        sldrTimePos.requestFocusInWindow();
    }

    protected void activateButtonPrev() {
        btnPreviousLogging.requestFocusInWindow();
    }

    protected void activateButtonNext() {
        btnNextLogging.requestFocusInWindow();
    }

    protected void activatePlayPauseBtn() {
        btnPlayPause.requestFocusInWindow();
    }

    protected void activateBeginningBtn() {
        btnBeginning.requestFocusInWindow();
    }

    protected void activateEndBtn() {
        btnEnd.requestFocusInWindow();
    }

    protected void activateFastForwardBtn() {
        btnFastForward.requestFocusInWindow();
    }

    protected void activateRewindBtn() {
        btnRewind.requestFocusInWindow();
    }

    Action nothingAction = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            //do nothing, prevent PAGE DOWN and PAGE UP to be handled twice
        }
    };

    private Action escapeAction = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Should NOT make dialog invisible, but activate slider instead
            activateSliderComponent();
        }
    };

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel8 = new SliderPanel();
        txtCurrentTime = new javax.swing.JTextField();
        sldrTimePos = new javax.swing.JSlider();
        btnPreviousLogging = new javax.swing.JButton();
        btnNextLogging = new javax.swing.JButton();
        lblPlayBackSpeed = new javax.swing.JLabel();
        lblEndTime = new javax.swing.JLabel();
        lblTotalLines = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnBeginning = new javax.swing.JButton();
        btnRewind = new javax.swing.JButton();
        btnPlayPause = new javax.swing.JButton();
        btnFastForward = new javax.swing.JButton();
        btnEnd = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(240, 240, 24));
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLocationByPlatform(true);
        setMaximumSize(new java.awt.Dimension(2147483647, 150));
        setMinimumSize(new java.awt.Dimension(453, 150));
        setName("Form"); // NOI18N
        setPreferredSize(new java.awt.Dimension(453, 150));
        setSize(new java.awt.Dimension(453, 150));
        setType(java.awt.Window.Type.UTILITY);

        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }

            public void windowLostFocus(java.awt.event.WindowEvent evt) {
                formWindowLostFocus(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel8.setMaximumSize(new java.awt.Dimension(32767, 200));
        jPanel8.setName("jPanel8"); // NOI18N
        txtCurrentTime.setHorizontalAlignment(CENTER);
        txtCurrentTime.setText("--,--");
        txtCurrentTime.setToolTipText("Current logtime from start of file (sec) / Goto file pos (G)");
        txtCurrentTime.setName("txtCurrentTime"); // NOI18N
        txtCurrentTime.addMouseListener(txtCurrentTimeMouseListener);
        txtCurrentTime.addActionListener(this::txtCurrentTimeActionPerformed);
        txtCurrentTime.getAccessibleContext().setAccessibleDescription("Current log position (sec)");

        sldrTimePos.setMaximum(MAX_SLIDER_VALUE);
        sldrTimePos.setValue(0);
        sldrTimePos.setName(SLIDER_TIME_POS); // NOI18N
        sldrTimePos.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                sldrTimePosMouseDragged(evt);
            }
        });
        sldrTimePos.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sldrTimePosKeyPressed(evt);
            }
        });

        btnPreviousLogging.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logplayerdialog/previous.png"))); // NOI18N
        btnPreviousLogging.setToolTipText("Go one step back in the log file");
        btnPreviousLogging.setName("btnPreviousLogging"); // NOI18N
        btnPreviousLogging.addActionListener(this::btnPreviousLoggingActionPerformed);
        btnPreviousLogging.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPreviousLoggingKeyPressed(evt);
            }
        });

        btnNextLogging.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logplayerdialog/next.png"))); // NOI18N
        btnNextLogging.setToolTipText("Go one step forward in the log file");
        btnNextLogging.setAutoscrolls(true);
        btnNextLogging.setName("btnNextLogging"); // NOI18N
        btnNextLogging.addActionListener(this::btnNextLoggingActionPerformed);
        btnNextLogging.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnNextLoggingKeyPressed(evt);
            }
        });

        lblPlayBackSpeed.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPlayBackSpeed.setText("1x");
        lblPlayBackSpeed.setToolTipText("Playback speed multiplier");
        lblPlayBackSpeed.setName("lblPlayBackSpeed"); // NOI18N

        lblEndTime.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEndTime.setText("Time: --");
        lblEndTime.setToolTipText(TOTAL_TIME_IN_LOGFILE_SEC);
        lblEndTime.setName("lblEndTime"); // NOI18N

        lblTotalLines.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTotalLines.setText("Lines: --");
        lblTotalLines.setToolTipText(TOTAL_DATA_LINES_IN_LOGFILE);
        lblTotalLines.setName("lblTotalLines"); // NOI18N

        jPanel2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel2.setDoubleBuffered(false);
        jPanel2.setName("jPanel2"); // NOI18N

        btnBeginning.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logplayerdialog/begin.png"))); // NOI18N
        btnBeginning.setToolTipText("Go to the beginning of log file (B)");
        btnBeginning.setName("btnBeginning"); // NOI18N
        btnBeginning.addActionListener(this::btnBeginningActionPerformed);
        btnBeginning.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnBeginningKeyPressed(evt);
            }
        });

        btnRewind.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logplayerdialog/rewind.png"))); // NOI18N
        btnRewind.setToolTipText("Fast reverse playback (R)");
        btnRewind.setName("btnRewind"); // NOI18N
        btnRewind.addActionListener(this::btnRewindActionPerformed);
        btnRewind.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnRewindKeyPressed(evt);
            }
        });

        btnPlayPause.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logplayerdialog/play.png"))); // NOI18N
        btnPlayPause.setToolTipText("Play/Pause the log file player (P)");
        btnPlayPause.setName("btnPlayPause"); // NOI18N
        btnPlayPause.addActionListener(this::btnPlayPauseActionPerformed);
        btnPlayPause.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPlayPauseKeyPressed(evt);
            }
        });

        btnFastForward.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logplayerdialog/fastforward.png"))); // NOI18N
        btnFastForward.setToolTipText("Fast forward playback (F)");
        btnFastForward.setName("btnFastForward"); // NOI18N
        btnFastForward.addActionListener(this::btnFastForwardActionPerformed);
        btnFastForward.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnFastForwardKeyPressed(evt);
            }
        });

        btnEnd.setIcon(endIcon); // NOI18N
        btnEnd.setToolTipText("Goto end of log file / Live play mode (E)");
        btnEnd.setName("btnEnd"); // NOI18N
        btnEnd.addActionListener(this::btnEndActionPerformed);
        btnEnd.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEndKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(btnBeginning, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRewind, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnPlayPause, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnFastForward, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnBeginning, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(btnFastForward, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPlayPause, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnRewind, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel8Layout.createSequentialGroup()
                            .addComponent(btnPreviousLogging, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel8Layout.createSequentialGroup()
                                    .addComponent(lblPlayBackSpeed)
                                    .addGap(0, 0, Short.MAX_VALUE))
                                .addGroup(jPanel8Layout.createSequentialGroup()
                                    .addComponent(sldrTimePos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnNextLogging, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(jPanel8Layout.createSequentialGroup()
                            .addComponent(txtCurrentTime, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(jPanel8Layout.createSequentialGroup()
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblEndTime, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblTotalLines, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(130, Short.MAX_VALUE))))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel8Layout.createSequentialGroup()
                            .addComponent(txtCurrentTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(2, 2, 2)
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel8Layout.createSequentialGroup()
                                    .addGap(1, 1, 1)
                                    .addComponent(btnPreviousLogging, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(sldrTimePos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                            .addGap(22, 22, 22)
                            .addComponent(btnNextLogging, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel8Layout.createSequentialGroup()
                            .addComponent(lblPlayBackSpeed)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(lblEndTime)
                            .addGap(0, 0, 0)
                            .addComponent(lblTotalLines, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel8Layout.createSequentialGroup()
                            .addGap(18, 18, 18)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGap(14, 14, 14))
        );


        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(4, 4, 4))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE))
        );

        // Handle ESC-key.
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
            .put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE_KEY");
        getRootPane().getActionMap().put("ESCAPE_KEY", escapeAction);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus
        handleInitComplete();
    }//GEN-LAST:event_formWindowGainedFocus

    private void formWindowLostFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowLostFocus
        handleFormWindowLostFocus();
    }//GEN-LAST:event_formWindowLostFocus

    private void btnEndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEndActionPerformed
        handleGotoEndLogging();
    }//GEN-LAST:event_btnEndActionPerformed

    private void btnFastForwardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFastForwardActionPerformed
        handleFastForwardBtn();
    }//GEN-LAST:event_btnFastForwardActionPerformed

    private void btnPlayPauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlayPauseActionPerformed
        handlePlayPauseBtn();
    }//GEN-LAST:event_btnPlayPauseActionPerformed

    private void btnRewindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRewindActionPerformed
        handleFastRewindBtn();
    }//GEN-LAST:event_btnRewindActionPerformed

    private void btnBeginningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBeginningActionPerformed
        handleGotoStartLogging();
    }//GEN-LAST:event_btnBeginningActionPerformed

    private void btnNextLoggingKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnNextLoggingKeyPressed
        logPlayerKeyPressed(evt);
    }//GEN-LAST:event_btnNextLoggingKeyPressed

    private void btnNextLoggingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextLoggingActionPerformed
        handleStepForward();
    }//GEN-LAST:event_btnNextLoggingActionPerformed

    private void btnPreviousLoggingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousLoggingActionPerformed
        handleStepBackward();
    }//GEN-LAST:event_btnPreviousLoggingActionPerformed

    private void sldrTimePosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sldrTimePosKeyPressed
        logPlayerKeyPressed(evt);
    }//GEN-LAST:event_sldrTimePosKeyPressed

    private void sldrTimePosMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sldrTimePosMouseDragged
        handleSliderMoved(sldrTimePos.getValue());
    }//GEN-LAST:event_sldrTimePosMouseDragged

    protected void txtCurrentTimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCurrentTimeActionPerformed
        handlePositionLogging(txtCurrentTime.getText());
    }//GEN-LAST:event_txtCurrentTimeActionPerformed

    private void btnPreviousLoggingKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPreviousLoggingKeyPressed
        logPlayerKeyPressed(evt);
    }//GEN-LAST:event_btnPreviousLoggingKeyPressed

    private void btnBeginningKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnBeginningKeyPressed
        logPlayerKeyPressed(evt);
    }//GEN-LAST:event_btnBeginningKeyPressed

    private void btnRewindKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnRewindKeyPressed
        logPlayerKeyPressed(evt);
    }//GEN-LAST:event_btnRewindKeyPressed

    private void btnPlayPauseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPlayPauseKeyPressed
        logPlayerKeyPressed(evt);
    }//GEN-LAST:event_btnPlayPauseKeyPressed

    private void btnFastForwardKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnFastForwardKeyPressed
        logPlayerKeyPressed(evt);
    }//GEN-LAST:event_btnFastForwardKeyPressed

    private void btnEndKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEndKeyPressed
        logPlayerKeyPressed(evt);
    }//GEN-LAST:event_btnEndKeyPressed

    protected void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
    }//GEN-LAST:event_formWindowClosed

    protected void handlePositionLogging(String newPos) {
        // override in base class
    }

    protected void handleStepForward() {
        // override in base class
    }

    protected void handleStepBackward() {
        // override in base class
    }

    protected void handlePageDownLogging() {
        // override in base class
    }

    protected void handlePageUpLogging() {
        // override in base class
    }

    protected void handleFormWindowLostFocus() {
        // override in base class
    }

    protected void handleGotoEndLogging() {
        // override in base class
    }

    protected void handleGotoStartLogging() {
        // override in base class
    }

    protected void handleSliderMoved(int val) {
        // override in base class
    }

    protected boolean handleInitComplete() {
        // override in base class
        return true;
    }

    protected void handlePlayPauseBtn() {
        // override in base class
    }

    protected void handleFastForwardBtn() {
        // override in base class
    }


    protected void handleFastRewindBtn() {
        // override in base class
    }

    protected void logPlayerKeyPressed(KeyEvent evt) {
        // override in base class
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LogPlayerDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LogPlayerDialog().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    javax.swing.JButton btnBeginning;
    javax.swing.JButton btnEnd;
    javax.swing.JButton btnFastForward;
    javax.swing.JButton btnNextLogging;
    javax.swing.JButton btnPlayPause;
    javax.swing.JButton btnPreviousLogging;
    javax.swing.JButton btnRewind;
    javax.swing.JPanel jPanel2;
    //    javax.swing.JPanel jPanel8;
    SliderPanel jPanel8;
    javax.swing.JLabel lblEndTime;
    javax.swing.JLabel lblPlayBackSpeed;
    javax.swing.JLabel lblTotalLines;
    javax.swing.JSlider sldrTimePos;
    javax.swing.JTextField txtCurrentTime;
    // End of variables declaration//GEN-END:variables
}
