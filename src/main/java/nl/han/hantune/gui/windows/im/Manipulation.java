/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.windows.im;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract class that must be extended by all manipulations to be used with the
 * ImageManipulator.
 *
 * @author Michiel Klifman
 */
public abstract class Manipulation {

    public static final String SATURATE = "Saturate";
    public static final String EXTRAPOLATE = "Extrapolate";

    private boolean active = true;
    private boolean saturate = true;
    private String mode = SATURATE;
    private Map<Double, double[]> boundsAndProperties = new LinkedHashMap<>();

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Manipulation() {
        boundsAndProperties.put(0.0, getMinimumDefaults());
        boundsAndProperties.put(100.0, getMaximumDefaults());
    }

    public void calculateManipulation(double value) {
        List<Double> bounds = new ArrayList<>(boundsAndProperties.keySet());
        double lowerBound = 0;
        double upperBound = 0;
        for (int i = 1; i < bounds.size(); i++) {
            lowerBound = bounds.get(i - 1);
            upperBound = bounds.get(i);
            if (inRange(value, lowerBound, upperBound) || belowBounds(value, lowerBound)) {
                break;
            }
        }

        double[] lowerSettings = boundsAndProperties.get(lowerBound);
        double[] upperSettings = boundsAndProperties.get(upperBound);
        double[] settings;

        if (value == lowerBound || (belowBounds(value, lowerBound) && saturate)) {
            settings = lowerSettings;
        } else if (value == upperBound || (aboveBounds(value, upperBound) && saturate)) {
            settings = upperSettings;
        } else {
            settings = extrapolate(value, lowerBound, upperBound, lowerSettings, upperSettings);
        }
        convertManipulation(settings);
    }

    /**
     * Implement this method to do conversions on the interpolated or
     * extrapolated settings based on the current signal value.
     *
     * @param settings the settings to do conversions on
     */
    public abstract void convertManipulation(double[] settings);

    /**
     * Implement this method to apply the manipulation on the graphics object of
     * this image.
     *
     * @param g the graphics object of the image that is to be manipulated.
     * @param image the image that is to be manipulated
     */
    public abstract void applyManipulation(Graphics2D g, BufferedImage image);

    private double[] extrapolate(double x, double x0, double x1, double[] y0, double[] y1) {
        double[] y = new double[y0.length];
        for (int i = 0; i < y.length; i++) {
            y[i] = extrapolate(x, x0, x1, y0[i], y1[i]);
        }
        return y;
    }

    private double extrapolate(double x, double x0, double x1, double y0, double y1) {
        return y0 + (x - x0) / (x1 - x0) * (y1 - y0);
    }

    private double interpolate(double x, double x0, double x1, double y0, double y1) {
        return y0 + (x - x0) * (y1 - y0) / (x1 - x0);
    }

    private boolean inRange(double value, double lowerBound, double upperBound) {
        return lowerBound <= value && value < upperBound;
    }

    private boolean belowBounds(double value, double lowerBound) {
        return value < lowerBound;
    }

    private boolean aboveBounds(double value, double upperBound) {
        return value > upperBound;
    }

    public abstract String getName();

    public abstract String[] getPropertyNames();

    public abstract String[] getUnits();

    public abstract double[] getMinimumDefaults();

    public abstract double[] getMaximumDefaults();

//    public abstract void settingsToDefault();
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Map<Double, double[]> getBoundsAndProperties() {
        return boundsAndProperties;
    }

    public void setBoundsAndProperties(Map<Double, double[]> map) {
        boundsAndProperties = map;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
        this.saturate = SATURATE.equals(mode);
    }

}
