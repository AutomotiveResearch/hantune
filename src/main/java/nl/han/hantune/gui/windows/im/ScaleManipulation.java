/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.windows.im;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author Michiel Klifman
 */
public class ScaleManipulation extends Manipulation {

    public static final String NAME = "Scale";

    private double x = 1;
    private double y = 1;

    @Override
    public void convertManipulation(double[] properties) {
        x = properties[0] / 100;
        y = properties[1] / 100;
    }

    @Override
    public void applyManipulation(Graphics2D g, BufferedImage image) {
        g.scale(x, y);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String[] getPropertyNames() {
        return new String[]{"Width", "Height"};
    }

    @Override
    public String[] getUnits() {
        return new String[]{"%", "%"};
    }

    @Override
    public double[] getMinimumDefaults() {
        return new double[]{100.0, 100.0};
    }

    @Override
    public double[] getMaximumDefaults() {
        return new double[]{100.0, 100.0};
    }

}
