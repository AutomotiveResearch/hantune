/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.windows.im;

/**
 *
 * @author Michiel Klifman
 */
public class ManipulationFactory {

    private final static String[] MANIPULATION_NAMES = new String[]{MoveManipulation.NAME, RotateManipulation.NAME, ScaleManipulation.NAME, TransparencyManipulation.NAME};

    /**
     * Creates a new manipulation for a given name.
     *
     * @param name the name of the manipulation to create
     * @return the created manipulation
     */
    public static Manipulation createManipulation(String name) {
        switch (name) {
            case MoveManipulation.NAME:
                return new MoveManipulation();
            case RotateManipulation.NAME:
                return new RotateManipulation();
            case ScaleManipulation.NAME:
                return new ScaleManipulation();
            case TransparencyManipulation.NAME:
                return new TransparencyManipulation();
            default:
                return null;
        }
    }

    public static String[] getManipulationNames() {
        return MANIPULATION_NAMES;
    }

}
