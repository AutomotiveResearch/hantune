/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.statusBar;

import java.awt.Color;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;

import HANtune.HANtune;

import static nl.han.hantune.gui.statusBar.HANtuneMainStatusBar.STATUSCOLOR_OFF;
import static nl.han.hantune.gui.statusBar.HANtuneMainStatusBar.STATUSCOLOR_ON;

@SuppressWarnings("serial")
public class ConnectedLabel extends JLabel {
    private HANtune hanTune;

    ConnectedLabel(HANtune hanTune) {
        super();
        this.hanTune = hanTune;

        setForeground(Color.WHITE);
        setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
            javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setOpaque(true);
        setPreferredSize(new java.awt.Dimension(175, 20));
        addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelConnectedMouseClicked(evt);
            }
        });

    }

    void setConnected(boolean isConnect, String connectStr) {
        setText("Connected: " + (connectStr.isEmpty() ? "-" : connectStr));
        if (isConnect) {
            setToolTipText("Click to disconnect (F5)");
            setBackground(STATUSCOLOR_ON);
        } else {
            setToolTipText("Click to connect (F5)");
            setBackground(STATUSCOLOR_OFF);
        }

    }

    private void labelConnectedMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == MouseEvent.BUTTON1) {
            // toggle currentConfig.getXcp() connection on/off
            hanTune.connectProtocol(!hanTune.isConnected());
        }
    }


}
