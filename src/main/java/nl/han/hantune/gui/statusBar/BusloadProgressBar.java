/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.statusBar;

import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.JProgressBar;
import javax.swing.plaf.basic.BasicProgressBarUI;

import HANtune.HANtune;
import datahandling.CurrentConfig;
import util.Util;


@SuppressWarnings("serial")
public class BusloadProgressBar extends JProgressBar {
    public static final int BUSLOAD_WARNING_PERCENTAGE = 70; // warning when more than 70% busload (orange)
    public static final int BUSLOAD_CRITICAL_PERCENTAGE = 90; // critical warning when more than 90% busload

    private HANtune hanTune;

    BasicProgressBarUI busloadBarUI = new BasicProgressBarUI() {
        @Override
        public void installUI(JComponent c) {
            super.installUI(c);
            setCellLength(1);
            setCellSpacing(0);
        }

        @Override
        protected Color getSelectionBackground() {
            return Color.black;
        }

        @Override
        protected Color getSelectionForeground() {
            return Color.black;
        }
    };


    BusloadProgressBar(HANtune haNtune) {
        super();
        this.hanTune = haNtune;

        setUI(busloadBarUI);
        setPreferredSize(new java.awt.Dimension(170, 20));
        setString("");
        setStringPainted(true);
    }

    /**
     * Updates the busload indicator bar.
     */
    void updateBusloadIndicator() {
        int busload = 0;
        // if connected, calculate the busload
        if (hanTune.currentConfig.getXcp() != null && hanTune.currentConfig.getXcp().isConnected()) {
            int totalSize = 0;
            for (int i = 0; i < hanTune.currentConfig.getXcp().getDaqLists().length; i++) {
                totalSize += hanTune.currentConfig.getXcp().getDaqLists()[i].getDataLen();
            }

            int load = (int) (hanTune.currentConfig.getHANtuneManager().getDaqListHighestFrequency() * totalSize);
            int bandwidth = hanTune.currentConfig.getBusBandwidth();

            busload = 100 * load / bandwidth;
        }

        updateBusloadIndicator(busload);
    }



    private void updateBusloadIndicator(int busload) {
        if (busload < 0) {
            busload = 0; // Todo: temporary fix because busload could become -1 occasionally
        }
        setToolTipText("Busload " + busload + "% (estimate)");
        if (busload < BUSLOAD_WARNING_PERCENTAGE) {
            setForeground(Color.GREEN);
        } else if (busload < BUSLOAD_CRITICAL_PERCENTAGE) {
            setForeground(Color.ORANGE);
        } else {
            setForeground(Color.RED);
        }
        setValue(busload);
        updateBusloadText(busload);
    }



    /**
     * Updates the text of the busload bar
     *
     * @param busload the current busload
     */
    private void updateBusloadText(int busload) {
        if (hanTune.isConnected()) {
            String busloadPercentage = busload + "%";
            String bandwidth = Util.getReadableDataRate(hanTune.currentConfig.getBusBandwidth());
            String tooltip = "Busload: " + busload + "% of " + bandwidth;
            if (hanTune.currentConfig.getProtocol() == CurrentConfig.Protocol.XCP_ON_UART
                && hanTune.currentConfig.getXcpsettings().isUsbVirtualComPort()) {
                busloadPercentage = "~ " + busloadPercentage;
                tooltip = "<html>" + tooltip
                    + ".<br/> Note this is only an estimate of the busload. When using a USB Virtual COM Port, the data rate"
                    + "<br/>is not limited by the baud rate. Therefore a default data rate of 1 Mbit/s is used to calculate the busload instead.</html>";
            }
            setString(busloadPercentage);
            setToolTipText(tooltip);

        } else {
            setString("0%");
            setToolTipText("Busload: not connected");
        }
    }


}
