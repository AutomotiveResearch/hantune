/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.statusBar;

import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.JProgressBar;
import javax.swing.plaf.basic.BasicProgressBarUI;

import HANtune.HANtune;

@SuppressWarnings("serial")
public class LagIndicatorBar extends JProgressBar {

    private HANtune hanTune;
    private long lastUpdateTimestamp = 0;
    private long updatePeriodAvg = 0;

    BasicProgressBarUI progressBarUI = new BasicProgressBarUI() {
        @Override
        public void installUI(JComponent c) {
            super.installUI(c);
            setCellLength(3);
            setCellSpacing(1);
        }
    };

    LagIndicatorBar(HANtune hantune) {
        super();
        this.hanTune = hantune;

        setUI(progressBarUI);
        setMaximum(10);
        setOrientation(1);
        setToolTipText("Lagindicator");
        setAlignmentX(0.0F);
        setMaximumSize(new java.awt.Dimension(20, 20));
        setMinimumSize(new java.awt.Dimension(20, 20));
        setPreferredSize(new java.awt.Dimension(20, 20));

        getAccessibleContext().setAccessibleDescription("Lag indicator");
    }

    void clearLagValue() {
        setValue(0);
    }

    void resetMovingAvrg() {
        updatePeriodAvg = (long) (10000 / hanTune.currentConfig.getHANtuneManager().getDaqListHighestFrequency());

    }

    /**
     * Updates the lag indicator bar.
     */
    void updateLagIndicator() {
        long timestamp = System.currentTimeMillis();
        updatePeriodAvg = (updatePeriodAvg * 2 + timestamp - lastUpdateTimestamp) / 3;
        lastUpdateTimestamp = timestamp;

        float div = 10000 / hanTune.currentConfig.getHANtuneManager().getDaqListHighestFrequency();

        int lag = (int) (4 * (updatePeriodAvg - div) / div);
        if (lag < 1) {
            setForeground(Color.GREEN);
        } else if (lag < 5) {
            setForeground(Color.ORANGE);
        } else {
            setForeground(Color.RED);
        }

        // if connected show at least a line to indicate "load"
        if (hanTune.currentConfig.getXcp() != null && hanTune.currentConfig.getXcp().isConnected()) {
            lag++;
        } else {
            lag = 0;
        }

        setValue(lag);
    }


    void setWarning() {
        setBackground(Color.red);
    }

}
