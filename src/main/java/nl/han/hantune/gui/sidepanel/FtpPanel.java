/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.sidepanel;

import components.sidepanel.SidePanel;
import static components.sidepanel.SidePanel.BORDER_COLOR;
import components.sidepanel.SubPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import nl.han.hantune.net.ftp.LocalFile;
import nl.han.hantune.net.ftp.RemoteFile;
import nl.han.hantune.net.ssh.SSHManager;

/**
 *
 * @author Michiel Klifman
 */
public class FtpPanel extends SidePanel {

    private final JTree localTree = new JTree();
    private final JTree remoteTree = new JTree();
    private final JTextField serverTextField = new JTextField();
    private final JTextField usernameTextField = new JTextField();
    private final JPasswordField passwordTextField = new JPasswordField();
    private SSHManager ssh;

    public FtpPanel() {
        super("SFTP");

        SubPanel serverPanel = new SubPanel();
        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        panel.setLayout(new GridBagLayout());
        LineBorder border = new LineBorder(BORDER_COLOR);
        serverPanel.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 3, 0), border));
        
        JLabel serverLabel = new JLabel("Server:", SwingConstants.RIGHT);
        serverLabel.setPreferredSize(new Dimension(20, 25));
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(3, 3, 3, 4);
        panel.add(serverLabel, gbc);
        Border outerBorder = BorderFactory.createLineBorder(BORDER_COLOR, 1);
        Border innerBorder = BorderFactory.createEmptyBorder(2, 2, 2, 2);
        serverTextField.setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));
        serverTextField.setPreferredSize(new Dimension(20, 25));
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.insets = new Insets(3, 0, 3, 3);
        panel.add(serverTextField, gbc);
        
        JLabel usernameLabel = new JLabel("Username:", SwingConstants.RIGHT);
        usernameLabel.setPreferredSize(new Dimension(20, 25));
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(0, 3, 3, 4);
        panel.add(usernameLabel, gbc);
        usernameTextField.setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));
        usernameTextField.setPreferredSize(new Dimension(20, 25));
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.insets = new Insets(0, 0, 3, 3);
        panel.add(usernameTextField, gbc);        
        
        JLabel passwordLabel = new JLabel("Password:", SwingConstants.RIGHT);
        passwordLabel.setPreferredSize(new Dimension(20, 25));
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.insets = new Insets(0, 3, 3, 4);
        panel.add(passwordLabel, gbc);
        passwordTextField.setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));
        passwordTextField.setPreferredSize(new Dimension(20, 25));
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.insets = new Insets(0, 0, 3, 3);
        panel.add(passwordTextField, gbc);   
        
        JButton connectButton = new JButton("Connect");
        connectButton.setPreferredSize(new Dimension(20, 25));
        connectButton.addActionListener((ActionEvent event) -> connect());
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.insets = new Insets(0, 3, 3, 4);
        panel.add(connectButton, gbc);
        JButton disconnectButton = new JButton("Disconnect");
        disconnectButton.setPreferredSize(new Dimension(20, 25));
        disconnectButton.addActionListener((ActionEvent event) -> disconnect());
        
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.insets = new Insets(0, 0, 3, 3);
        panel.add(disconnectButton, gbc);
        serverPanel.add(panel);
        this.add(serverPanel);

        SubPanel localPanel = new SubPanel("Local Files");
        JScrollPane localScrollPane = new JScrollPane();
        TreeModel model = new FtpTreeModel(new LocalFile(new File(System.getProperty("user.home"))));
        localTree.setModel(model);
        localScrollPane.setViewportView(localTree);
        localScrollPane.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        localPanel.add(localScrollPane);

        localPanel.setPreferredSize(new Dimension(240, 390));
        localPanel.setBorder(new EmptyBorder(0, 0, 3, 0));
        this.add(localPanel);

        SubPanel remotePanel = new SubPanel("Remote Files");
        JScrollPane remoteScrollPane = new JScrollPane();
        remoteTree.setModel(null);
        remoteScrollPane.setViewportView(remoteTree);
        remotePanel.add(remoteScrollPane);
        remotePanel.setPreferredSize(new Dimension(240, 390));
        this.add(remotePanel);

        layout.putConstraint(SpringLayout.NORTH, serverPanel, 0, SpringLayout.NORTH, contentPanel);
        layout.putConstraint(SpringLayout.WEST, serverPanel, 0, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.EAST, serverPanel, 0, SpringLayout.EAST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, localPanel, 0, SpringLayout.SOUTH, serverPanel);

        layout.putConstraint(SpringLayout.WEST, localPanel, 0, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.EAST, localPanel, 0, SpringLayout.EAST, contentPanel);
        layout.putConstraint(SpringLayout.SOUTH, localPanel, 0, SpringLayout.NORTH, remotePanel);

        layout.putConstraint(SpringLayout.WEST, remotePanel, 0, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.EAST, remotePanel, 0, SpringLayout.EAST, contentPanel);
        layout.putConstraint(SpringLayout.SOUTH, remotePanel, 0, SpringLayout.SOUTH, contentPanel);

        addEventListeners();
    }

    private void addEventListeners() {
        JPopupMenu menu = new JPopupMenu();
        JMenuItem menuItem = new JMenuItem("Download");
        menuItem.addActionListener(e -> download());
        menu.add(menuItem);
        remoteTree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {

                TreePath path = remoteTree.getSelectionPath();
                RemoteFile node = (RemoteFile) path.getLastPathComponent();
                TreePath treePath = localTree.getSelectionPath();
                LocalFile localFile = treePath == null ? (LocalFile) localTree.getModel().getRoot() : (LocalFile) treePath.getLastPathComponent();
                if (SwingUtilities.isRightMouseButton(event) && !node.isDirectory() && localFile.isDirectory()) {
                    menuItem.setText("Download to " + localFile.getPath());
                    menu.show(remoteTree, event.getX(), event.getY());
                }
            }
        });
    }

    private void connect() {
        ssh = new SSHManager(usernameTextField.getText(), passwordTextField.getText(), serverTextField.getText());
        ssh.connect();
        String root = ssh.sendCommand("pwd");
        String str_root = root.replace("\n", "") + "/";
        FtpTreeModel rftm = new FtpTreeModel(new RemoteFile(str_root, ssh, true));
        remoteTree.setModel(rftm);
    }

    private void disconnect() {
        ssh.close();
        remoteTree.setModel(null);
    }

    private void download() {
        TreePath remoteTreePath = remoteTree.getSelectionPath();
        String fileName = ((RemoteFile) remoteTreePath.getLastPathComponent()).toString();
        String remoteFile = ((RemoteFile) remoteTreePath.getLastPathComponent()).getPath();
        TreePath treePath = localTree.getSelectionPath();
        String localFile = ((LocalFile) treePath.getLastPathComponent()).getPath();
        ssh.downloadFile(remoteFile, localFile + "/" + fileName);
    }

}
