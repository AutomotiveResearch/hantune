/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.sidepanel;

import HANtune.HANtune;
import bsh.util.JConsole;
import nl.han.hantune.scripting.Console;
import components.sidepanel.SidePanel;
import components.sidepanel.SidePanelManager;
import components.sidepanel.SubPanel;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

/**
 * GUI component that holds the HANtune console
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class ConsolePanel extends SubPanel {

    public ConsolePanel() {
        super("Console");
    }

    /**
     * Initialize the console panel
     */
    public void initialize() {
        JConsole consoleGUI = Console.getGUI();
        consoleGUI.setBorder(BorderFactory.createLineBorder(SidePanel.BORDER_COLOR));
        this.add(consoleGUI);

        this.setBorder(new EmptyBorder(3, 0, 0, 0));
        this.getResizeButton().setButtonActionPerformed(e -> toggleSize());
        this.setMinimumSize(new Dimension(this.getWidth(), 18));
        this.setMaximumSize(new Dimension(this.getWidth(), 484));
        addResizeMouseListeners();
    }

    /**
     * Minimizes the console when it open and opens the console when it is
     * minimized.
     */
    public void toggleSize() {
        JTabbedPane tabbedPane = HANtune.getInstance().getTabbedPane();
        if (this.getHeight() != this.getMinimumSize().height) {
            if (tabbedPane != null) {
            tabbedPane.setSize(new Dimension(tabbedPane.getWidth(), tabbedPane.getHeight() + this.getHeight() - this.getMinimumSize().height));
            }
            this.setMaximizedSize(this.getSize());
            this.setLocation(this.getX(), this.getY() + this.getHeight() - this.getMinimumSize().height);
            this.minimize();
        } else {
            this.setMaximizedSize(new Dimension(this.getWidth(), this.getMaximizedSize().height));
            this.maximize();
            if (tabbedPane != null) {
            tabbedPane.setSize(new Dimension(tabbedPane.getWidth(), tabbedPane.getHeight() - this.getHeight() + this.getMinimumSize().height));
            }
            this.setLocation(this.getX(), this.getY() - this.getHeight() + this.getMinimumSize().height);
        }
        this.validate();
    }

    /**
     * Adds mouselisteners that allow the console panel to be resized.
     */
    private void addResizeMouseListeners() {
        MouseAdapter resizeAdapter = new MouseAdapter() {

            int amountToResize;
            boolean isResizing;

            @Override
            public void mouseEntered(MouseEvent event) {
                Component component = (Component) event.getSource();
                Container panel = (Container) component;

                if (event.getY() <= panel.getHeight() - panel.getInsets().top) {
                    component.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
                }
            }

            @Override
            public void mouseExited(MouseEvent event) {
                if (!isResizing) {
                    Component component = (Component) event.getSource();
                    component.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            }

            @Override
            public void mousePressed(MouseEvent event) {
                amountToResize = event.getY();
            }

            @Override
            public void mouseReleased(MouseEvent event) {
                Container panel = (Container) event.getSource();
                isResizing = false;
                if (event.getX() < panel.getWidth() - panel.getInsets().right) {
                    panel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            }

            @Override
            public void mouseDragged(MouseEvent event) {
                Component component = (Component) event.getSource();
                if (event.getY() <= component.getY()) {
                    isResizing = true;
                    int difference = amountToResize - event.getY();
                    int newY = component.getY() - difference;
                    int newHeight = component.getHeight() + difference;
                    if (newHeight < component.getMinimumSize().height) {
                        newY = newY - (component.getMinimumSize().height - newHeight);
                        newHeight = component.getMinimumSize().height;
                    } else if (newHeight > component.getMaximumSize().height) {
                        newY = newY - (component.getMaximumSize().height - newHeight);
                        newHeight = component.getMaximumSize().height;
                    }
                    component.setBounds(component.getX(), newY, component.getWidth(), newHeight);
                    component.validate();
                    boolean sidePanelVisible = SidePanelManager.getInstance().isSidePanelVisible();
                    HANtune.getInstance().adjustLayoutSize(sidePanelVisible);
                }
            }
        };
        this.addMouseListener(resizeAdapter);
        this.addMouseMotionListener(resizeAdapter);
    }

}
