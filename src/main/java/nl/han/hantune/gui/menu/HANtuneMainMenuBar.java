/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.menu;

import javax.swing.JMenuBar;

import HANtune.HANtune;
import java.awt.Dimension;

public class HANtuneMainMenuBar {

    private final JMenuBar mainMenu;

    private final FileMenu fileMenu;
    private final CommunicationMenu communicMenu;
    private final WindowMenu windowMenu;
    private final PreferencesMenu preferencesMenu;
    private final HelpMenu helpMenu;

    private Dimension oldDimensions;

    public HANtuneMainMenuBar(HANtune hantune) {
        mainMenu = new javax.swing.JMenuBar();

        fileMenu = new FileMenu(hantune);
        mainMenu.add(fileMenu);

        communicMenu = new CommunicationMenu(hantune);
        mainMenu.add(communicMenu);

        windowMenu = new WindowMenu(hantune);
        mainMenu.add(windowMenu);

        preferencesMenu = new PreferencesMenu(hantune);
        mainMenu.add(preferencesMenu);

        helpMenu = new HelpMenu(hantune);
        mainMenu.add(helpMenu);
    }

    public JMenuBar getMenuBar() {
        return mainMenu;
    }

    /**
     * Enables the layout related menuitems
     */
    public void enableLayoutItems(boolean enabled) {
        // Window menu-items
        windowMenu.enableLayoutItems(enabled);
    }

    public void updateItemsServiceModeVisibility() {
        fileMenu.updateItemsServiceModeVisibility();
        windowMenu.updateItemsServiceModeVisibility();
        preferencesMenu.updateItemsServiceModeVisibility();
    }

    public void updateWindowMenuChromelessState(boolean isChromeless, boolean isTransparent) {
        windowMenu.updateChromelessStateItems(isChromeless, isTransparent);
    }

    public void minimize() {
        oldDimensions = mainMenu.getPreferredSize();
        mainMenu.setPreferredSize(new Dimension(mainMenu.getWidth(), 0));
    }

    public void restore() {
        mainMenu.setPreferredSize(oldDimensions);
    }

}
