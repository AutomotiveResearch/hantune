/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.menu;

import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

@SuppressWarnings("serial")
public class MenuItem extends JMenuItem {

    public static class Builder {
        // required
        private String text;

        // optional
        private Icon  icon = null;
        private KeyStroke accelKey = null;
        private ActionListener actionListnr = null;
        private String toolTipText = null;


        public Builder(String text) {
            this.text = text;
        }


        public Builder icon(String name) {
            this.icon = new javax.swing.ImageIcon(getClass().getResource(name));
            return this;
        }


        public Builder acceleratorKey(int keyCode, int modifiers) {
            this.accelKey = javax.swing.KeyStroke.getKeyStroke(keyCode, modifiers);
            return this;
        }


        public Builder actionListener(ActionListener l) {
            this.actionListnr = l;
            return this;
        }

        public Builder toolTiptext(String text) {
            this.toolTipText = text;
            return this;
        }


        public MenuItem build() {
            return new MenuItem(this);
        }


    }


    private MenuItem(Builder bldr) {
        this.setText(bldr.text);

        if (bldr.accelKey != null) {
            this.setAccelerator(bldr.accelKey);
        }

        if (bldr.actionListnr != null) {
            this.addActionListener(bldr.actionListnr);
        }

        if (bldr.icon != null) {
            this.setIcon(bldr.icon);
        }

        if (bldr.toolTipText != null) {
            this.setToolTipText(bldr.toolTipText);
        }

    }
}
