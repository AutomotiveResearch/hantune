/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.menu;

import java.awt.event.KeyEvent;
import java.util.prefs.Preferences;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import HANtune.CommunicationSettings;
import HANtune.HANtune;
import HANtune.UserPreferences;
import datahandling.CurrentConfig;
import nl.han.hantune.datahandling.DataLogger.LogDataWriterCsv;

@SuppressWarnings("serial")
public class CommunicationMenu extends JMenu {
    private HANtune hanTune;
    private Preferences userPrefs = Preferences.userNodeForPackage(UserPreferences.class);

    private MenuItem connectMenuItem;
    private MenuItem settingsMenuItem;
    private JCheckBoxMenuItem datalogCheckboxMenuItem;
    private MenuItem datalogFilenameMenuItem;

    CommunicationMenu(HANtune hantune) {
        this.hanTune = hantune;
        setText("Communication");
        setMnemonic('C');

        connectMenuItem = new MenuItem.Builder("Connect")
            .acceleratorKey(KeyEvent.VK_F5, 0)
            .actionListener(evt -> xcpConnectMenuItemActionPerformed())
            .build();
        add(connectMenuItem);

        settingsMenuItem = new MenuItem.Builder("Communication Settings")
            .actionListener(evt -> xcpSettingsMenuItemActionPerformed())
            .build();
        add(settingsMenuItem);

        datalogCheckboxMenuItem = new javax.swing.JCheckBoxMenuItem(); // No builder available
        datalogCheckboxMenuItem.setText("Enable Datalogging");
        datalogCheckboxMenuItem.addActionListener(evt -> xcpDatalogCheckboxMenuItemActionPerformed());
        add(datalogCheckboxMenuItem);

        datalogFilenameMenuItem = new MenuItem.Builder("Modify Datalog Filename")
            .actionListener(evt -> xcpDatalogFilenameMenuItemActionPerformed())
            .build();
        add(datalogFilenameMenuItem);

        addMenuListener(commMenuListener);
    }


    void dataLoggingSetSelected(boolean select) {
        datalogCheckboxMenuItem.setSelected(select);
    }



    private void xcpConnectMenuItemActionPerformed() {
        // toggle currentConfig.getXcp() connection on/off
        hanTune.connectProtocol(!hanTune.isConnected());
    }

    private void xcpSettingsMenuItemActionPerformed() {
        if (hanTune.getCommunicationSettings() != null
            && hanTune.getCommunicationSettings().isDisplayable()) {
            hanTune.getCommunicationSettings().toFront();
        } else {
            hanTune.setCommunicationSettings(new CommunicationSettings());
            hanTune.getCommunicationSettings().setVisible(true);
        }
    }


    private void xcpDatalogCheckboxMenuItemActionPerformed() {
        LogDataWriterCsv writer = LogDataWriterCsv.getInstance();
        writer.setWritingStandBy(datalogCheckboxMenuItem.isSelected());
        writer.enableDatalogging(writer.isWritingStandby(), writer.isReadWhileWriteStandby(), false);

        hanTune.updateDatalogGuiComponents();
    }


    private void xcpDatalogFilenameMenuItemActionPerformed() {
        LogDataWriterCsv datalog = LogDataWriterCsv.getInstance();
        String name =
            HANtune.showQuestion("Datalog Filename", "Datalog Filename Prefix:", datalog.getPrefix());
        if (name != null) {
            datalog.setPrefix(name);
        }
    }

    private MenuListener commMenuListener = new MenuListener() {
        @Override
        public void menuSelected(MenuEvent e) {
            LogDataWriterCsv log = LogDataWriterCsv.getInstance();
            datalogCheckboxMenuItem.setSelected(log.isWritingStandby());

            CurrentConfig conf =  CurrentConfig.getInstance();
            if (conf.getXcp() != null && conf.getXcp().isConnected()) {
                connectMenuItem.setText("Disconnect from device");
            } else if (conf.getCan() != null && conf.getCan().isConnected()) {
                connectMenuItem.setText("Disconnect CAN");
            } else {
                connectMenuItem.setText("Connect");
            }
        }

        @Override
        public void menuDeselected(MenuEvent e) {
            // Nothing to do here
        }

        @Override
        public void menuCanceled(MenuEvent e) {
            // Nothing to do here
        }
    };
}
