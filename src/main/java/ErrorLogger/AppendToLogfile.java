/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorLogger;

import datahandling.CurrentConfig;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 * @author alaap
 */
public final class AppendToLogfile {

    private static int errorCount;

    /**
     * This method writes its arguments into the logfile. Provide separate
     * strings per errorLog line.
     *
     * @param s ArrayList of strings
     */
    public synchronized static void appendMessage(String... s) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("errorLog.txt", true)))) {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss - dd-MM-YYYY");
            Date date = new Date();
            out.println("****** " + dateFormat.format(date) + " ******");
            for (String message : s) {
                out.println(message);
            }
            out.flush();
            //out.close();
        } catch (IOException e) {
            System.out.println(e.getMessage() + "\n" + e);
        }
    }

    /**
     * Appends the stack trace with metadata to the logfile. Please insert the
     * following code snippet into every catch-code block:
     * AppendToLogfile.appendError(Thread.currentThread(), e);
     *
     * @param t Current Tread where the error appears
     * @param e Throwable, provides the stack trace
     */
    public synchronized static void appendError(Thread t, Throwable e) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("errorLog.txt", true)))) {
            CurrentConfig currentConfig = CurrentConfig.getInstance();

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            out.println(dateFormat.format(date) + "  nr " + errorCount);
            out.println("Connection type: " + currentConfig.getProtocol());
            if (currentConfig.getASAP2Data() != null) {
                out.println("ProjectComments: " + currentConfig.getASAP2Data().getEpromID());
            } else {
                out.println("ProjectComments: no active asap2 file");
            }
            out.println("An exception occurred in thread: " + t.getName());
            out.println(t.getClass());
            out.println("ID: " + t.getId() + ", Priority: " + t.getPriority());
            out.println("ThreadGroup: " + t.getThreadGroup());
            e.printStackTrace(out);
            out.flush();
            System.out.println(errorCount + " Exception occurred: " + e.getMessage() + ", class: " + e.getClass());
            errorCount++;

        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + ex);
        }
    }
    /**
     * This starts a new thread to write system info to the errorlog for debugging purposes.
     * It retrieves information from Windows which takes a while to process.
     * Therefore it gets it's own thread.
     */
    public static void writeSystemInfo(){
            GetSystemInfo writeSystemInfoToLogfile = new GetSystemInfo();       //Write system info to logfile for debugging purposes
            writeSystemInfoToLogfile.setPriority(Thread.MIN_PRIORITY);          //May as well take a little while, but may not slow other processes down
            writeSystemInfoToLogfile.start();
    }

}
