/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorLogger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author la_obwlf
 */
public class defaultExceptionHandler implements Thread.UncaughtExceptionHandler {
//needs:  Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler()); to be
//added in the threads
//Look into log4j libraries
    /**
     * Catches all uncaught exceptions from all threads where the uncaughtExceptionHandler is set
     * @param t Thread where the error occurred in
     * @param e Exception which occurred
     */
    @Override
    public void uncaughtException(Thread t, Throwable e) {

        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("errorLog.txt", true)))) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            out.println(dateFormat.format(date));
            out.println("An UNCAUGHT exception occurred in thread: " + t.getName());
            out.println(t.getClass());
            out.println("ID: " + t.getId() + ", Priority: " + t.getPriority());
            out.println("ThreadGroup: " + t.getThreadGroup());
            e.printStackTrace(out);
            // no close or flush neccessary because after the JVM shuts down it will take care of this
        } catch (IOException ex) {
            //Nothing you can do with an exception here
        }
    }

    /**
     * Catches all uncaught exceptions from all threads where the uncaughtExceptionHandler is set
     * @param t Thread where the error occurred in
     * @param e Exception which occurred
     * @param s Optional String for additional info
     */
    public void uncaughtException(Thread t, Throwable e, String s) {

        uncaughtException(t, e);
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("errorLog.txt", true)))) {
            out.println(s);
            // no close or flush neccessary because after the JVM shuts down it will take care of this
        } catch (IOException ex) {
            //Nothing you can do with an exception here
        }
    }

}
