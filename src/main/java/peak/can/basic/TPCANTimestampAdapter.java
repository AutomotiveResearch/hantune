package peak.can.basic;

public class TPCANTimestampAdapter extends TPCANTimestamp {

    private static final long MAX_UNSIGNED_INT_OFFSET = 2^32;

    /*
     * returns the total number of microseconds since device startup also counting the millis overflows
     */
    public long getTotalMicros() {
        return getTotalMillis() * 1000L + (long)getMicros();
    }

    public long getTotalMillis() {
        return (Integer.toUnsignedLong(getMillis()) + MAX_UNSIGNED_INT_OFFSET * getMillis_overflow());
    }

}
