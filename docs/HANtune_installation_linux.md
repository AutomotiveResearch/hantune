>Copyright (c) 2020 [HAN University of Applied Sciences]
>
>Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## HANtune installation on linux
### Using PCAN-USB interface (XCP on CAN communication). 
#### When SocketCAN is available (kernel v3.4 or higher)
SocketCAN only functions correctly using JAVA 11 (or higher). Tested on Ubuntu 20.04.4 <br>
Copy the following files (included in HANtune.zip) as sudo:
* ```cp ./config/linux/pcan_usb.conf /etc/modules-load.d```
* ```cp ./config/linux/80-can.network /etc/systemd/network```

Execute the following commands (sudo):
* ```systemctl start systemd-networkd```
* ```systemctl enable systemd-networkd```

Now start HANtune, connect PCAN-USB interface. Set Connection type to: ```XCP on CAN```
CAN communication should be working.
<br>
#### When a different baudrate is required. 
* Exit HANtune
* change bitrate parameter in file 80-can.network
* restart network (sudo): ```systemctl restart systemd-networkd``` 
* Start HANtune<br>

#### Alternatively, to change SocketCAN params one time only (above files not involved):<br>
Preconditions to connect with SocketCAN, after reset.<br>
* Bring up kernel modules:<br>
  * ```sudo modprobe can```
  * ```sudo modprobe can_raw```
  * ```sudo modprobe peak_usb```<br>
<br>
* Up socketCan:<br>
  * ```sudo ip link set can0 up type can bitrate 500000```

The above commands must be executed after each restart.

### When XCP on USB/UART communication is required on USB
In HANtune on linux, connect to ```ttyUSBx``` or ```ttyACMx``` as shown in the pull-down of Communication Settings.
To see which tty USB devices are available. After your USB device has been connected type ```ls -l /sys/class/tty | grep usb```.
Before HANtune can communicate however, the rights of these devices should be properly set in /dev (read-write). These devices however, will 
only be present when the hardware is actually connected to your computer. Therefore use the content of config/linux/50-myusb.rules to set rights automatically each time they are connected.
You can append the last 2 effective lines to the file ```/etc/udev/rules.d/50-myusb.rules``` if the file is already present, 
or you can place the complete file in the given folder if 50-mysb.rules doesn't exist.
