#Copyright (c) 2020 [HAN University of Applied Sciences]

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
#BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
#OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#For more information see: http://openmbd.com/wiki/HANtune/MATLAB_Interface

#Change this to your location of the MATLAB engine
enginePath = "C:\Program Files\MATLAB\R2017b\extern\engines\java\jar\engine.jar"

#Add matlab engine to the classpath
from datahandling import CurrentConfig
CurrentConfig.getInstance().addSoftwareLibrary(enginePath)

#Import matlab engine
from com.mathworks.engine import MatlabEngine

#Initialize matlab engine
engines = MatlabEngine.findMatlab()
engine = MatlabEngine.connectMatlab(engines[0])

#Add engine as a global variable to all interpreters
from nl.han.hantune.scripting import ScriptingManager
ScriptingManager.getInstance().setGlobal("engine", engine)

#Redirect matlab output to HANtune console
from java.lang import System
from nl.han.hantune.scripting import Console
System.setOut(Console.getGUI().getOut())
System.setErr(Console.getGUI().getErr())

print 'Connected to MATLAB'
